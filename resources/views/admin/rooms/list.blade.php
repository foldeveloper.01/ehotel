@extends('admin.layouts.app')
    @section('styles')<style type="text/css">.flex-1{display: none;}</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Room Type</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Room Type</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row row-cards">
                            <!-- COL-END -->
                            <div class="col-xl-12 col-lg-12">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="card">
                                            <div class="card-body p-2">
        <a href="{{url('admin/rooms/create')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Add Room Type</button>
        </a>
        <a href="{{url('admin/roomsseason')}}">
          <button id="" class="btn btn-primary mb-4">Manage Season Date</button>
        </a>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-11">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-4">
                                                <div class="card text-primary bg-primary-transparent">
                                                    <div class="card-body">
                                                        <h4 class="card-title text-center">Total Room : {{@$room_setting->total_room}}</h4>
                                                    </div>
                                                </div>
                                            </div><div class="col-md-12 col-xl-4">
                                                <div class="card text-secondary bg-secondary-transparent">
                                                    <div class="card-body">
                                                        <h4 class="card-title text-center">Alotted Room : {{@$room_setting->alotted_room}}</h4>
                                                    </div>
                                                </div>
                                            </div><div class="col-md-12 col-xl-4">
                                                <div class="card text-success bg-success-transparent">
                                                    <div class="card-body">
                                                        <h4 class="card-title text-center">Remaining Room : {{@$room_setting->remaining_room}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        @if(count(@$list)!='0')
                                        @foreach(@$list as $key=>$value)
                                            <div class="col-sm-6 col-md-6 col-xl-3 alert">
                                                <div class="card">
                                                    <div class="product-grid6">
                                                        <div class="product-image6 p-5">
                                                            <!-- <ul class="icons-wishlist">
                                                                <li><a href="javascript:void(0)" class="bg-danger text-white border-danger border" data-bs-dismiss="alert" aria-hidden="true"><i class="fe fe-heart"></i></a></li>
                                                            </ul> -->
                                                            <a href="#" class="bg-light">
                                                                <img style="height: 306px;" class="img-fluid br-7 w-100" src="{{asset('storage/images/rooms/'.@$value->image)}}" alt="img">
                                                            </a>
                                                        </div>
                                                        <div class="card-body pt-0">
                                                            <div class="product-content text-center">
                                                                <h1 class="title fw-bold fs-20"><a href="#">{{@$value->room_type}}</a></h1>
                                                                                                                          
                                                                                                                              
                                                              <a href="{{url('admin/roomsmanage/'.@$value->id)}}" class="btn btn-secondary btn-sm mb-0">
                                                                Manage Room Number
                                                              </a>
                                                              <a href="{{url('admin/rooms/update/'.@$value->id)}}" class="btn btn-sm btn-primary">
                                                                <i class="fe fe-edit"></i>
                                                              </a>
                                                              <a onclick="deletefunction(event,1);" href="" class="btn  btn-sm btn-danger">
                                                                <i class="fe fe-x"></i>
                                                              </a>
                                                              <form id="delete_form1" method="POST" action="{{url('admin/rooms/delete/'.@$value->id)}}"> 
                                                                @csrf
                                                                <input name="_method" type="hidden" value="POST">
                                                              </form>
                                                            </div>&nbsp;
                                                            <div class="mb-0 but-css">
                                                               <!--  <span>Status</span> :@if(@$value->status=='0') <span class="badge bg-success ms-1 px-2 pb-1 mb-0">Yes </span> @else <span class="badge bg-danger ms-1 px-2 pb-1 mb-0">Suspended</span> @endif -->
                                                                    <span class="show_online">Show Online</span> :@if(@$value->show_online=='0') <span class="badge bg-success ms-1 px-2 pb-1 mb-0">Yes </span> @else <span class="badge bg-danger ms-1 px-2 pb-1 mb-0">No</span> @endif
                                                            </div>  
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            <div class="card-body pt-0">
                                                            <div class="product-content text-center">
                                                        No Result!
                                                    </div>
                                                </div>
                                            @endif





                                           


                                        </div>
                                        <div class="mb-5">
                                            <div class="float-end">
                                                <ul class="pagination mb-5">
                                                   {{ @$list->links() }}
                                                </ul>
                                                <!-- <ul class="pagination mb-5">
                                                    <li class="page-item page-prev disabled">
                                                        <a class="page-link" href="javascript:void(0)" tabindex="-1">Prev</a>
                                                    </li>
                                                    <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                                                    <li class="page-item page-next">
                                                        <a class="page-link" href="javascript:void(0)">Next</a>
                                                    </li>
                                                </ul> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- COL-END -->
                            </div>
                            <!-- ROW-1 CLOSED -->
                        </div>
                        <!-- ROW-1 END-->

        @endsection

    @section('scripts')
    <script type="text/javascript">function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }</script>
    @endsection
