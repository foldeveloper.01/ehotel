@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Room Type</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Room Type</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row row-sm">
  <div class="col-md-12 col-xl-10">
    <div class="card">
      <div class="card-body">
        <div class="">
          <div class="row">
                <div class="col-md-12 col-xl-4">
                  <div class="card text-primary bg-primary-transparent">
                      <div class="card-body">
                          <h4 class="card-title">Total Room : {{@$room_setting->total_room}}</h4>
                      </div>
                  </div>
                 </div>
                <div class="col-md-12 col-xl-4">
                    <div class="card text-secondary bg-secondary-transparent">
                        <div class="card-body">
                            <h4 class="card-title">Alotted Room : {{@$room_setting->alotted_room}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xl-4">
                      <div class="card text-success bg-success-transparent">
                          <div class="card-body">
                              <h4 class="card-title">Remaining Room : {{@$room_setting->remaining_room}}</h4>
                          </div>
                      </div>
                </div>
          </div>
          <div class="form-group">
            <div class="col-xl-12">
              <div class="card">
                <div class="card-body">
                  <div class="panel panel-primary">
                    <div class="tab-menu-heading">
                      <div class="tabs-menu">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs panel-danger">
                          <li style="margin-right: 4px;">
                            <a href="#tab13" class="active" data-bs-toggle="tab">Room Details</a>
                          </li>
                         <!--  <li style="margin-right: 4px;">
                            <a href="#tab14" data-bs-toggle="tab">
                              <span></span>Pricing</a>
                          </li>
                          <li style="margin-right: 4px;">
                            <a href="#tab15" data-bs-toggle="tab">
                              <span></span>Overview</a>
                          </li>
                          <li style="margin-right: 4px;">
                            <a href="#tab16" data-bs-toggle="tab">
                              <span></span>Amenities</a>
                          </li> -->
                        </ul>
                      </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                      <div class="tab-content">
                        <div class="tab-pane active " id="tab13">
                          <div class="row">
        <form id="create" action="{{url('admin/rooms/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
        <input type="hidden" name="action_tab" value="room_details">
       <!--  <div class="card-header">
          <h3 class="card-title">Create Guest User</h3>
        </div> -->        
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Room Type Name</label>
                <input type="text" value="{{old('room_type')}}" name="room_type" class="form-control" id="room_type" placeholder="Room Type Name">
               @if($errors->has('room_type')) <div class="error">{{ $errors->first('room_type') }}</div> @endif
               </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Total Rooms</label>
                <input type="text" value="{{old('total_room')}}" name="total_room" class="form-control" id="total_room" placeholder="Total Rooms">
              @if($errors->has('total_room')) <div class="error">{{ $errors->first('total_room') }}</div> @endif
             </div>
            </div>
          </div>
          <div class="row"> 
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="occupants">Occupants(Number of Persons)</label>
                <input type="number" value="{{old('occupants')}}" name="occupants" class="form-control" id="occupants" placeholder="Occupants">
              @if($errors->has('occupants')) <div class="error">{{ $errors->first('occupants') }}</div> @endif
             </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="showonline">Show Online</label>
              <br>
              <div class="custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="show_online" value="0" checked="">
                  <span class="custom-control-label">Yes</span>
                </label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="show_online" value="1">
                  <span class="custom-control-label">No</span>
                </label>
              </div>
            </div>
          </div>
          
          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="view">View (Eg: Beach or Hills)</label>
              <input type="text" value="{{old('view')}}" name="view" class="form-control" id="view" placeholder="View">
              @if($errors->has('view')) <div class="error">{{ $errors->first('view') }}</div> @endif
            </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="size">Size (Eg: 35 m2 / 376 ft2)</label>
              <input type="text" value="{{old('size')}}" name="size" class="form-control" id="size" placeholder="Size">
              @if($errors->has('size')) <div class="error">{{ $errors->first('size') }}</div> @endif
            </div>
            </div>
          </div>

          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="bed">Bed (Eg: King-size or twin beds)</label>
              <br>
              <input type="text" name="bed" value="{{old('bed')}}" class="form-control" id="bed" placeholder="Bed"> @if($errors->has('bed')) <div class="error">{{ $errors->first('bed') }}</div> @endif
            </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="description">Short Description</label>
              <br>
              <textarea name="description" class="form-control" rows="2" placeholder="Short Description">{{old('description')}}</textarea> @if($errors->has('description')) <div class="error">{{ $errors->first('description') }}</div> @endif
            </div>
            </div>
          </div>
          <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                 <label for="exampleInputnumber">Room Image (450 × 300) (Max upload file size 2MB)</label>
                 <input class="form-control" accept="image/*" name="image" type="file">
                </div>
               </div>
               <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Facilities</label>
                <select multiple="multiple" name="facilities[]" id="facilities " class="form-control select2 form-select">
                  <option value="">Please select</option>
                  @if(@$facilities!='')
                  @foreach($facilities as $key=>$value)
                  <option value="{{$value->id}}">{{$value->name}}</option>
                  @endforeach
                  @endif                  
                </select>
              </div>
            </div>            
          </div>
          <div class="row">              
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="room_number_start_with">Room Number starts with (Eg:101,201,301,401)</label>
                <br>
                <input type="number" max="99999" name="room_number_start_with" value="{{old('room_number_start_with')}}" class="form-control" id="room_number_start_with" placeholder="Room number start with"> @if($errors->has('room_number_start_with')) <div class="error">{{ $errors->first('room_number_start_with') }}</div> @endif
              </div>
            </div>          
          </div>

      <div class="card-footer">
        <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
    </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  var current_date = "{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d-m-Y')}}";
   $(function() {
      $('#date').datetimepicker({
        //defaultDate: defaulsst,  
        pickTime: false,
        minView: 2,
        format: 'dd-mm-yyyy',
        autoclose: true,
      });      
    });
   $('#date').val(current_date);
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      room_type: {
        required: true
      },total_room: {
        required: true
      },
      image: {
        required: true
      },
      occupants: {
        required: true
      },
    },
    messages: {
      room_type: {
        required: "Please enter room type name",
      },total_room: {
        required: "Please enter total rooms",
      },
      image: {
        required: "Please upload image",
      },
      occupants: {
        required: "Please enter room occupants",
      },
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection