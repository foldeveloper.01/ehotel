@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Logo</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Logo</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <div class="col-xl-12">
    <form id="create_user" action="{{url('admin/logo')}}" method="post" enctype="multipart/form-data"> @csrf <div class="card">
        <div class="card-header">
          <h3 class="card-title">Logo Update</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="@if(@$data->logo!=null && @$data->logo!='') col-lg-7 @else col-lg-8 @endif col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Upload Logo (300 × 120) (Max upload file size 2MB)</label>
                <input class="form-control" accept="image/*" name="logo" type="file">
              </div>
            </div> @if(@$data->logo!=null && @$data->logo!='') <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img class="d-flex overflow-visible" src="{{asset('storage/images/logo/'.@$data->logo)}}" />
                <a target="_blank" href="{{asset('storage/images/logo/'.@$data->logo)}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @endif
          </div>
          <div class="row">
            <div class="@if(@$data->favicon!=null && @$data->favicon!='') col-lg-7 @else col-lg-8 @endif col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Upload Favicon (64 × 64) (Max upload file size 2MB)</label>
                <input class="form-control" accept="image/*" name="favicon" type="file">
              </div>
            </div> @if(@$data->favicon!=null && @$data->favicon!='') <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img class="d-flex overflow-visible" src="{{asset('storage/images/logo/'.@$data->favicon)}}" />
                <a target="_blank" href="{{asset('storage/images/logo/'.@$data->favicon)}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @endif
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Update</button>
          <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script> @endsection