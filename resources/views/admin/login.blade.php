@extends('admin.layouts.custom-app')
@section('styles')
@endsection
@section('class')
<!-- BACKGROUND-IMAGE -->
<div class="login-img">
    @endsection
    @section('content')
    <!-- CONTAINER OPEN -->
    <div class="col-login mx-auto mt-7">
        <div class="text-center">
          <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img" alt=""> 
     </div>
 </div>
 <div class="container-login100">                       
    <div class="wrap-login100 p-6">
        <form id="admin_login_form" class="login100-form validate-form" action="{{route('adminLoginPost')}}" method="post">
            @csrf
            @if(Session::has('error'))
            <div class="alert alert-error text-center">
               {{Session::get('error')}}
           </div>
           @endif
           @include('sweetalert::alert')
           <span class="login100-form-title pb-5">
            Login
        </span>
        <div class="panel panel-primary">
            <div class="tab-menu-heading">
                <div class="tabs-menu1">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs">
                        <li class="mx-0"><a href="#tab5" class="active" data-bs-toggle="tab">Email</a></li>
                        <!-- <li class="mx-0"><a href="#tab6" data-bs-toggle="tab">Mobile</a></li> -->
                    </ul>
                </div>
            </div>
            <div class="panel-body tabs-menu-body p-0 pt-5">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab5">
                        <div class="wrap-input100 validate-input input-group" data-bs-validate="Valid email is required: ex@abc.xyz">
                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                <i class="zmdi zmdi-email text-muted" aria-hidden="true"></i>
                            </a>
                            <input name="email" class="input100 form-control @error('email') is-invalid @enderror" value="{{old('email')}}" type="text" placeholder="Email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <span id="errorToShow4"></span>
                        <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                            <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                            </a>
                            <input name="password" class="input100 form-control @error('password') is-invalid @enderror" type="password" placeholder="Password">@error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <span id="errorToShow5"></span>
                        <div class="text-end pt-4">
                            <p class="mb-0"><a href="{{url('admin/forgotpassword')}}" class="text-primary ms-1">Forgot Password?</a></p>
                        </div>
                        <div class="container-login100-form-btn">
                            <!-- <a href="{{url('index')}}" class="login100-form-btn btn-primary"> -->
                                <button type="submit" class="btn btn-primary w-100" tabindex="4">Login</button>
                                <!-- </a> --><br>
                            </div>
                        </div>
                         <div class="text-center pt-3">
                            <!-- <p class="text-dark mb-0">Not a member?<a href="{{url('admin/signup')}}" class="text-primary ms-1">Sign UP</a></p> -->
                                            </div>
                                            <!--<label class="login-social-icon"><span>Login with Social</span></label>
                                             <div class="d-flex justify-content-center">
                                                <a href="javascript:void(0)">
                                                    <div class="social-login me-4 text-center">
                                                        <i class="fa fa-google"></i>
                                                    </div>
                                                </a>
                                                <a href="javascript:void(0)">
                                                    <div class="social-login me-4 text-center">
                                                        <i class="fa fa-facebook"></i>
                                                    </div>
                                                </a>
                                                <a href="javascript:void(0)">
                                                    <div class="social-login text-center">
                                                        <i class="fa fa-twitter"></i>
                                                    </div>
                                                </a>
                                            </div> -->
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<!-- CONTAINER CLOSED -->

@endsection
@section('scripts')

<!-- GENERATE OTP JS -->
<script src="{{asset('assets/admin/js/generate-otp.js')}}"></script>
<script type="text/javascript">
        // just for the demos, avoids form submit
    /*jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
  });*/
    $( "#admin_login_form" ).validate({
        submitHandler : function(form) {
            form.submit();
        },
        rules: {
            email: {
              required: true
              //email: true
          },
          password: {
              required: true,
              minlength: 5
          }
      },
      messages: {        
        email: {
            required: "Please enter username or email",
        },
        password: {
            required: "Please enter password",
            minlength: jQuery.validator.format("At least {0} characters required!")
        },
    },
    errorPlacement: function(error, element) {
                var n = element.attr("name");
                if (n == "email") {
                    error.appendTo("#errorToShow4").css('color','#dc3545');
                }
                if (n == "password") {
                    error.appendTo("#errorToShow5").css('color','#dc3545');
                }
     }
});
</script>
@endsection
