@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Add-on Product</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Add-on Product</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/addonproduct/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Add-on Product</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Add-on Name</label>
                <input type="text" value="" name="add_on_name" class="form-control" id="add_on_name" placeholder="Add-on Name">
              </div> @if($errors->has('add_on_name')) <div class="error">{{ $errors->first('add_on_name') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Add-on Code</label>
                <input type="text" value="" name="add_on_code" class="form-control" id="add_on_code" placeholder="Add-on Code">
              </div> @if($errors->has('add_on_code')) <div class="error">{{ $errors->first('add_on_code') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Amount (RM)</label>
                <input type="user_name" name="add_on_amount" value="" class="form-control" id="add_on_amount" placeholder="Amount (RM)"> @if($errors->has('add_on_amount')) <div class="error">{{ $errors->first('add_on_amount') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Show Online</label>
              <br>
              <div class="custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="show_online" value="0" checked="">
                  <span class="custom-control-label">Yes</span>
                </label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="show_online" value="1">
                  <span class="custom-control-label">No</span>
                </label>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Service Charge</label>
              <br>
              <div class="custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="service_charge" value="0" checked="">
                  <span class="custom-control-label">Enable</span>
                </label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="service_charge" value="1">
                  <span class="custom-control-label">Disable</span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <a href="{{url('admin/addonproduct')}}" class="btn btn-danger my-1">Back</a>
          <button class="btn btn-success my-1" value="submit">Save</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      add_on_name: {
        required: true
      },
      add_on_code: {
        required: true
      },
      add_on_amount: {
        required: true
      }
    },
    messages: {
      add_on_name: {
        required: "Please enter name",
      },
      add_on_code: {
        required: "Please enter code",
      },
      add_on_amount: {
        required: "Please enter amount",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection