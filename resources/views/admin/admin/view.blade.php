@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Admin</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin/admins')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Update</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<div class="row">
  <div class="col-xl-8 col-lg-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Personal Information</h4>
      </div>
      <div class="card-body">
        <h6>The information provided will reflect on your invoices</h6>
        <br>
        <form class="form-horizontal" id="admin_management" action="{{url('admin/admins/myaccountupdate/'.Request::segment(4))}}" method="post"> @csrf <div class=" row mb-4">
            <label class="col-md-3 form-label">Name</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="name" name="name" value="{{@$userData->firstname}}">
            </div>
          </div>
          <div class=" row mb-4">
            <label class="col-md-3 form-label" for="example-email">Address</label>
            <div class="col-md-9">
              <textarea id="address" name="address" class="form-control mb-4" placeholder="Address" rows="4" style="height: 107px;">{{@$userData->address}}</textarea>
            </div>
          </div>
          <div class=" row mb-4">
            <label class="col-md-3 form-label">Phone number</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="phone_number" id="phone_number" value="{{@$userData->phone_number}}">
            </div>
          </div>
          <div class=" row mb-4">
            <label class="col-md-3 form-label">Organization</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="organization" placeholder="Organization" id="organization" value="{{@$userData->organization}}">
            </div>
          </div>
          <div class=" row mb-4">
            <label class="col-md-3 form-label">Additional Details</label>
            <div class="col-md-9">
              <textarea id="additional_details" name="additional_details" class="form-control mb-4" placeholder="Additional Details" rows="4" style="height: 107px;">{{@$userData->additional_details}} </textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button class="btn btn-primary">Save changes</button>
          </div>
        </form>
        <!--         <div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog"><div class="modal-dialog modal-dialog-scrollable" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title">Modal title</h5><button class="btn-close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div><div class="modal-body"><form class="form-horizontal" id="admin_management"><div class=" row mb-4"><label class="col-md-3 form-label">Name</label><div class="col-md-9"><input type="text" class="form-control" id="name" name="name" value="{{@$userData->firstname}}"></div></div><div class=" row mb-4"><label class="col-md-3 form-label" for="example-email">Address</label><div class="col-md-9"><textarea id="address" name="address" class="form-control mb-4" placeholder="Address" rows="4" style="height: 107px;">{{@$userData->address}}</textarea></div></div><div class=" row mb-4"><label class="col-md-3 form-label">Phone number</label><div class="col-md-9"><input type="text" class="form-control" value="{{@$userData->phone_number}}"></div></div><div class=" row mb-4"><label class="col-md-3 form-label">Organization</label><div class="col-md-9"><input type="text" class="form-control" placeholder="text" id="organization" value="{{@$userData->organization}}"></div></div><div class=" row mb-4"><label class="col-md-3 form-label">Additional Details</label><div class="col-md-9"><input type="text" class="form-control" readonly="" value="Readonly value"></div></div><div class="modal-footer"><button class="btn btn-secondary" data-bs-dismiss="modal">Close</button><button class="btn btn-primary">Save changes</button></div></form></div></div></div></div> -->
      </div>
    </div>
  </div>
  <div class="col-xl-8 col-lg-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Account Setting</h4>
      </div>
      <div class="card-body">
        <form class="form-horizontal">
          <div class=" row mb-4">
            <label class="col-md-3 form-label">Email</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="email" id="email" value="{{@$userData->email}}">
            </div>
          </div>
          <div class=" row mb-4">
            <label class="col-md-3 form-label" for="example-email">Password</label>
            <div class="col-md-9">
              <input type="email" id="password" name="password" class="form-control" placeholder="password">
            </div>
          </div>
          <div class=" row mb-4">
            <label class="col-md-3 form-label">Manage two-factor authentication</label>
            <div class="col-md-9">
              <input type="password" class="form-control" value="password">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  $("body").on('click', '.toggle-password', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#password");
    if (input.attr("type") === "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
  $("body").on('click', '.toggle-password1', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#password_confirmation");
    if (input.attr("type") === "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create_user").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      firstname: {
        required: true,
        minlength: 3
      },
      email: {
        required: true,
        email: true,
      },
      usertype: {
        required: true
      }
      /*,
                  password: {
                    required: true,
                    minlength: 5
                  },
                  password_confirmation: {
                    required: true,
                    equalTo: "#password"
                  }*/
    },
    messages: {
      firstname: {
        required: "Please enter name",
      },
      email: {
        required: "Please enter email",
      },
      usertype: {
        required: "Please select usertype",
      }
      /*,
                      password: {
                          required: "Please enter password",
                      },
                      password_confirmation: {
                          required: "Please enter confirm password",
                      }*/
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "password") {
        error.appendTo("#errorpassword").css('color', '#FF0000').css("fontSize", "14px").css('float', 'center');
      } else if (element.attr("name") == "password_confirmation") {
        error.appendTo("#errorpassword1").css('color', '#FF0000').css("fontSize", "14px").css('float', 'center');
      } else {
        error.insertAfter(element);
      }
    },
  });
</script> @endsection