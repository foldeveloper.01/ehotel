@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Admin</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">List</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- Row -->
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Admin List</h3>
      </div>
      <div class="card-body">
        <a href="{{url('admin/admins/create')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Add New Admin</button>
        </a>
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable3">
            <!-- <div class="category-filter"><select id="categoryFilter" class="form-control cate_gory-filters"><option value="" selected>Status: All</option><option value="Active">Status: Active</option><option value="Suspend">Status: Suspend</option></select></div> -->
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Name</th>
                <th class="wd-15p border-bottom-0">User Name</th>
                <th class="wd-20p border-bottom-0">Email</th>
                <th class="wd-15p border-bottom-0">Contact Number</th>
                <th class="wd-15p border-bottom-0">Type</th>
                <th class="wd-25p border-bottom-0">Status</th>
                <th class="wd-25p border-bottom-0">OfficerPassword</th>
                <th class="wd-25p border-bottom-0">Created date</th>
                <th class="wd-25p border-bottom-0">Actions</th>
              </tr>
            </thead>
            <tbody> @if(@$userlist) @foreach(@$userlist as $key=>$data) @php $value = \App\Models\Admintypes::find($data->admin_type_id); @endphp <tr>
                <td>{{$key+1}}</td>
                <td>{{$data->firstname}}</td>
                <td>{{$data->username}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->number}}</td>
                <td>{{@$data->AdminTyps->type}}</td>
                <td>
                  <div class="col-xl-2 ps-1 pe-1">
                    <div class="form-group">
                      <label class="custom-switch form-switch mb-0">
                        <input type="checkbox" id="status_{{@$data->id}}" name="status" class="custom-switch-input" @if(@$data->status=='0') checked @endif> <span class="custom-switch-indicator custom-switch-indicator-md" data-on="Yes" data-off="No"></span> @if($data->status==0) <span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Active</span> @else <span class="badge bg-danger-transparent rounded-pill text-danger p-2 px-3">Suspend</span> @endif </label>
                    </div>
                  </div>
                </td>
                <td style="text-align: center;">
                  <a class="form-control-sm btn btn-warning-light" data-bs-effect="effect-super-scaled" data-bs-toggle="modal" href="#modaldemo{{$data->id}}">Update</a>
                </td>
                <div class="modal fade" id="modaldemo{{$data->id}}">
                  <div class="modal-dialog modal-dialog-centered text-center" role="document">
                    <div class="modal-content modal-content-demo">
                      <div class="modal-header">
                        <h6 class="modal-title">Update Officer Password</h6>
                        <button aria-label="Close" class="btn-close" data-bs-dismiss="modal">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form method="POST" action="{{ url('admin/admins/officerpassword', $data->id) }}"> @csrf <div class="modal-body">
                          <input name="_method" type="hidden" value="DELETE">
                          <div class="row">
                            <label for="inputPassword" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                              <input type="password" placeholder="Enter Password" name="password" class="form-control" id="password" required>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-success">Update</button>
                      </form>
                      <button class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
        </div>
        <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y h:i')}}</td>
        <td class="text-center align-middle">
          <!-- <form id="send_email{{$data->id}}" method="POST" action="{{ url('admin/admins/emailSend', $data->id) }}"> @csrf
                                                            <input name="_method" type="hidden" value="DELETE"></form><a onclick="send_email(event,{{$data->id}},'{{$data->email}}');" class="bg-success text-white border-success border" data-bs-toggle="tooltip"><i class="fa fa-expeditedssl"></i></a> -->
          <a href="{{url('admin/admins/update/'.$data->id)}}" class="btn btn-sm btn-primary">
            <i class="fe fe-edit"></i>
          </a>
          <a onclick="deletefunction(event,{{$data->id}});" href="" class="btn btn-sm btn-danger">
            <i class="fe fe-x"></i>
          </a>
          <form id="delete_form{{$data->id}}" method="POST" action="{{ url('admin/admins/delete', $data->id) }}"> @csrf <input name="_method" type="hidden" value="DELETE">
          </form>
        </td>
        </tr> @endforeach @endif </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.custom-switch-input').on('change', function() {
      if($(this).prop('checked')==true){
        var status_message = 'Are you sure, Do you want to Activate the data?';
      }else{
        var status_message = 'Are you sure, Do you want to Suspend the data?';
      }
      var option = confirm(status_message);    
      if (!option) {
        if ($(this).prop('checked')) {
            $(this).prop('checked', !$(this).prop('checked'));
        } else {
            $(this).prop('checked', !$(this).prop('checked'));
        }
        return false;
      }
      var isChecked = $(this).is(':checked');
      var id = $(this).attr('id');
      var selectedData;
      var $switchLabel = $('.custom-switch-indicator');
      //console.log('isChecked: ' + isChecked);
      if (isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      //console.log('Selected data: ' + selectedData);
      $.ajax({
        url: site_url + '/admin/admins/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: selectedData,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();
            if (selectedData == 'Yes') {
              var success = 'Activate';
              toastr.success(success + ' successfully');
            } else {
              var success = 'Suspend';
              toastr.error(success + ' successfully');
            }
            location.reload();
            /*setTimeout(function() {
              location.reload();
            }, 1000);*/
          } else {
            toastr.clear();
            toastr.error('something went wrong');
            setTimeout(function() {
              location.reload();
            }, 1000);
          }
        }
      });
    });
    $('#responsive-datatable3').dataTable({
      "lengthChange": false,
      language: {
        searchPlaceholder: "Search...",
        scrollX: "100%",
        sSearch: ""
      }
    });
    /*var table = $('#responsive-datatable3').DataTable();
    $("#filterTable_filter.dataTables_filter").append($("#categoryFilter"));
    var categoryIndex = 0;
    $("#responsive-datatable3 th").each(function (i) {
      if ($($(this)).html() == "Status") {
        categoryIndex = i; return false;
      }
    });
    $.fn.dataTable.ext.search.push(
      function (settings, data, dataIndex) {
        var selectedItem = $('#categoryFilter').val()
        var category = data[categoryIndex];
        if (selectedItem === "" || category.includes(selectedItem)) {
          return true;
        }
        return false;
      }
    );
    $("#categoryFilter").change(function (e) {
      table.draw();
    });
    table.draw();*/
  });

  function send_email(event, id, email) {
    event.preventDefault();
    swal({
      title: "Password will be sent to " + email + ". Is the email correct?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Send',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#send_email" + id).submit();
      } else {
        swal("Cancel", "You Canceled this one", "error");
      }
    });
  }

  function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }
</script> @endsection