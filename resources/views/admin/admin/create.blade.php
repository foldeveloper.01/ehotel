@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Admin Create</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin/admins')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create_user" action="{{url('admin/admins/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Admin Create</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Name</label>
                <input type="text" value="{{old('firstname')}}" name="firstname" class="form-control" id="name" placeholder="Name"> @if($errors->has('firstname')) <div class="error">{{ $errors->first('firstname') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">User Name</label>
                <input type="text" value="{{old('username')}}" name="username" class="form-control" id="username" placeholder="User Name"> @if($errors->has('username')) <div class="error">{{ $errors->first('username') }}</div> @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail1" placeholder="Email address"> @if($errors->has('email')) <div class="error">{{ $errors->first('email') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="contactnumber">Contact Number</label>
                <input type="number" name="phone_number" value="{{old('number')}}" class="form-control" id="phone_number" placeholder="Contact number" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label for="user-type">User Type</label>
                  <select name="usertype" class="form-control select2 form-select">
                    <option value="">Please select one</option> @if(@$admintypes) @foreach(@$admintypes as $key=>$value) <option value="{{@$value->id}}" @if(old('usertype')==$value->id) selected @endif>{{@$value->type}}</option> @endforeach @endif
                  </select>
                </div>
                <span id="errorusertype"></span>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label for="status">Status</label>
                  <select name="status" class="form-control select2 form-select">
                    <option value="0" selected>Active</option>
                    <option value="1">Suspend</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label for="password">Password</label>
                  <div class="wrap-input100 validate-input input-group" id="Password-toggle1">
                    <a href="javascript:void(0)" class="input-group-text bg-white text-muted zmdi zmdi-eye toggle-password">
                      <i class="" aria-hidden="true"></i>
                    </a>
                    <input class="input100 form-control" type="password" placeholder="Password" name="password" id="password" required>
                  </div>
                </div>
                <span id="errorpassword"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <a href="{{url('admin/admins')}}" class="btn btn-danger my-1">Back</a>
          <button class="btn btn-success my-1" value="submit">Save</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  $("body").on('click', '.toggle-password', function() {
    var input = $("#password");
    if (input.attr("type") === "password") {
      $(this).removeClass("zmdi-eye");
      $(this).addClass("zmdi zmdi-eye-off");
      input.attr("type", "text");
    } else {
      $(this).addClass("zmdi zmdi-eye");
      $(this).removeClass("zmdi-eye-off");
      input.attr("type", "password");
    }
  });
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create_user").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      firstname: {
        required: true,
        minlength: 3
      },
      username: {
        required: true,
        minlength: 3
      },
      email: {
        required: true,
        email: true,
      },
      usertype: {
        required: true
      },
      password: {
        required: true,
        minlength: 8
      },
      status: {
        required: true
      }
    },
    messages: {
      firstname: {
        required: "Please enter name",
      },
      username: {
        required: "Please enter username",
      },
      email: {
        required: "Please enter email",
      },
      usertype: {
        required: "Please select usertype",
      },
      password: {
        required: "Please enter password",
      },
      status: {
        required: "Please select status",
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "password") {
        error.appendTo("#errorpassword").css('color', '#FF0000').css("fontSize", "14px").css('float', 'center');
      } else if (element.attr("name") == "usertype") {
        error.appendTo("#errorusertype").css('color', '#FF0000').css("fontSize", "14px").css('float', 'center');
      } else {
        error.insertAfter(element);
      }
    },
  });
</script> @endsection