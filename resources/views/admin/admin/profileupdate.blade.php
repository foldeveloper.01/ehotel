@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Profile Update</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin/admins')}}">Profile</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Update</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="update_user" action="{{url('admin/admins/profile/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Profile Update</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">User Name</label>
                <input type="text" value="{{@$getuserData->username}}" name="username" class="form-control" id="username" placeholder="User Name"> @if($errors->has('username')) <div class="error">{{ $errors->first('username') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" value="{{@$getuserData->email}}" class="form-control" id="exampleInputEmail1" placeholder="Email address"> @if($errors->has('email')) <div class="error">{{ $errors->first('email') }}</div> @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="password">Password</label>
                <div class="wrap-input100 validate-input input-group" id="Password-toggle1">
                  <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                    <i class="zmdi zmdi-eye text-muted fa fa-fw fa-eye field_icon toggle-password" aria-hidden="true"></i>
                  </a>
                  <input class="input100 form-control" type="password" placeholder="Password" name="password" id="password">
                </div>
              </div>
              <span id="errorpassword"></span>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="password">Officer Password</label>
                <div class="wrap-input100 validate-input input-group" id="Password-toggle2">
                  <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                    <i class="zmdi zmdi-eye text-muted fa fa-fw fa-eye field_icon toggle-password1" aria-hidden="true"></i>
                  </a>
                  <input class="input100 form-control" type="password" placeholder="Officer Password" name="officer_password" id="officer_password">
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="contactnumber">Mobile Number</label>
                <input type="number" name="phone_number" value="{{@$getuserData->phone_number}}" class="form-control" id="phone_number" placeholder="Mobile number" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$">
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <a href="{{url('admin/admins')}}" class="btn btn-danger my-1">Cancel</a>
          <button class="btn btn-success my-1" value="submit">Save</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  $("body").on('click', '.toggle-password', function() {
    var input = $("#password");
    if (input.attr("type") === "password") {
      $(this).removeClass("zmdi-eye");
      $(this).addClass("zmdi zmdi-eye-off");
      input.attr("type", "text");
    } else {
      $(this).addClass("zmdi zmdi-eye");
      $(this).removeClass("zmdi-eye-off");
      input.attr("type", "password");
    }
  });
  $("body").on('click', '.toggle-password1', function() {
    var input = $("#officer_password");
    if (input.attr("type") === "password") {
      $(this).removeClass("zmdi-eye");
      $(this).addClass("zmdi zmdi-eye-off");
      input.attr("type", "text");
    } else {
      $(this).addClass("zmdi zmdi-eye");
      $(this).removeClass("zmdi-eye-off");
      input.attr("type", "password");
    }
  });
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#update_user").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      username: {
        required: true,
        minlength: 3
      },
      email: {
        required: true,
        email: true,
      },
      status: {
        required: true
      }
    },
    messages: {
      username: {
        required: "Please enter username",
      },
      email: {
        required: "Please enter email",
      },
      status: {
        required: "Please select status",
      }
    },
    /*errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                },*/
  });
</script> @endsection