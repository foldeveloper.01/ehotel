@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Rooms Manage</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Rooms Manage List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">        
        <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
      </div>
      <div class="card-body">
        @if(count($list) < @$room->total_room)
        <a href="{{url('admin/roomsmanage/create/'.Request::segment('3'))}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Create Rooms Manage</button>
        </a>
        @endif
        <div class="table-responsive">
          <div class="row">
                                            <div class="col-md-12 col-xl-4">
                                                <div class="card text-primary bg-primary-transparent">
                                                    <div class="card-body">
                                                        <h4 class="card-title text-center">Total Room : {{@$room_setting->total_room}}</h4>
                                                    </div>
                                                </div>
                                            </div><div class="col-md-12 col-xl-4">
                                                <div class="card text-secondary bg-secondary-transparent">
                                                    <div class="card-body">
                                                        <h4 class="card-title text-center">Alotted Room : {{@$room_setting->alotted_room}}</h4>
                                                    </div>
                                                </div>
                                            </div><div class="col-md-12 col-xl-4">
                                                <div class="card text-success bg-success-transparent">
                                                    <div class="card-body">
                                                        <h4 class="card-title text-center">Remaining Room : {{@$room_setting->remaining_room}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Room No</th>
                <th class="wd-15p border-bottom-0">Building Name</th>
                <th class="wd-15p border-bottom-0">Floor Name</th>
                <th class="wd-15p border-bottom-0">Lock Id</th>
                <th class="wd-15p border-bottom-0">Position</th>
                <th class="wd-15p border-bottom-0">Status</th>
                <th class="wd-15p border-bottom-0">Bad/Reserve</th>
                <th class="wd-15p border-bottom-0">Action</th>
              </tr>
            </thead>
            <tbody> @if(@$list) @foreach(@$list as $key=>$data) <tr>
                <td>{{@$key+1}}</td>
                <td>
                  <input class="form-control form-control-sm col-sm-4 position" type="text" name="roomno" id="roomno_{{@$data->id}}" value="{{@$data->room_no}}" @if($data->status==1) readonly @endif>
                </td>
                <td>
                  <input class="form-control form-control-sm col-sm-4 position" type="text" name="buildingname" id="buildingname_{{@$data->id}}" value="{{@$data->building_name}}" @if($data->status==1) readonly @endif>
                </td>
                <td>
                  <input class="form-control form-control-sm col-sm-4 position" type="text" name="floorname" id="floorname_{{@$data->id}}" value="{{@$data->floor_name}}" @if($data->status==1) readonly @endif>
                </td>
                <td>
                  <input class="form-control form-control-sm col-sm-4 position" type="text" name="lockid" id="lockid_{{@$data->id}}" value="{{@$data->lock_id}}" @if($data->status==1) readonly @endif>
                </td>
                <td>
                  <input class="form-control form-control-sm col-sm-4 position" type="text" name="position" id="position_{{@$data->id}}" value="{{@$data->position}}">
                </td>
                <td>
                  <div class="col-xl-2 ps-1 pe-1">
                    @if($data->status==0) <span id="status_{{@$data->id}}" class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Active</span> @else <span class="badge bg-danger-transparent rounded-pill text-danger p-2 px-3">Suspend</span> @endif
                  </div>
                </td>

                <td><a href="{{url('admin/roomsmanage/rooms/'.$data->id)}}"><button class="btn btn-primary">Bad/Reserve</button><a></td>
                <td class="text-center align-middle">
                  @if($data->status==0)
                  <a onclick="deletefunction(event,{{$data->id}});" href="" class="btn btn-sm btn-danger">
                    <i class="fe fe-x"></i>
                  </a>
                  @endif               
                  <form id="delete_form{{$data->id}}" method="POST" action="{{ url('admin/roomsmanage/delete', $data->id) }}"> @csrf <input name="_method" type="hidden" value="POST">
                  </form>
                </td>
              </tr> @endforeach @endif </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.custom-switch-input').on('change', function() {
      var isChecked = $(this).is(':checked');
      var id = $(this).attr('id');
      var selectedData;
      var $switchLabel = $('.custom-switch-indicator');
      //console.log('isChecked: ' + isChecked);
      if (isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      //console.log('Selected data: ' + selectedData);
      //alert(site_url+'/admin/bookingwindows/ajax_status_update');
      $.ajax({
        url: site_url + '/admin/roomsmanage/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: selectedData,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();
            if (selectedData == 'Yes') {
              var success = 'Activate';
              toastr.success(success + ' successfully');
            } else {
              var success = 'Suspend';
              toastr.error(success + ' successfully');
            }
          } else {
            toastr.clear();
            toastr.error('something went wrong');
          }
        }
      });
    });

    $('.position').on('blur', function() {
      var isChecked = 'Yes';
      var id = $(this).attr('id');
      //console.log($(this).val());
      if( $(this).val() != '' ) {
      $.ajax({
        url: site_url + '/admin/roomsmanage/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: $(this).val(),
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            console.log(response['type']);
            toastr.clear();
            if(response['type']=='roomno'){
              console.log(response['message']);
               if(response['message']!=''){
                 toastr.error(response['message']);
               }else{
                 toastr.success('Room No updated successfully');
               }               
            }else if(response['type']=='position'){
               toastr.success('Sort updated successfully');
               /*setTimeout(function(){*/
                 window.location.reload(1);
              /*}, 1000);*/
            }else if(response['type']=='buildingname'){
               toastr.success('Building name updated successfully');              
            }else if(response['type']=='floorname'){
               toastr.success('Floor name updated successfully');              
            }else if(response['type']=='lockid'){
               toastr.success('Lockid updated successfully');              
            }            
          } else {
            toastr.clear();
            toastr.error('something went wrong');
          }
        }
      });
    }
    });

  });

  function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }

  function status(msg,msg1,attr) {
    //Active/Suspend //Suspend/Active
    //$(attr).attr('id')
    if(msg=='Active'){
      var message = 'Do you want to '+msg1+' the rooms manage';
    }else if(msg=='Suspend'){
      var message = 'Do you want to '+msg1+' the rooms manage';
    }
    event.preventDefault();
    swal({
      title: "Status update?",
      text: message,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Change',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
            var id = $(attr).attr('id');
            $.ajax({
              url: site_url + '/admin/roomsmanage/ajax_status_update',
              type: 'post',
              dataType: 'json',
              data: {
                id: id,
                selectedData: 1,
                _token: '{{csrf_token()}}'
              },
              success: function(response) {
                if (response['data'] == 1) {
                  console.log(response['type']);
                  toastr.clear();
                  if(response['type']=='status'){
                     toastr.error('status suspend updated successfully');
                     toastr.clear();
                  }            
                } else {
                  toastr.clear();
                  toastr.error('something went wrong');
                }
                window.location.reload(1);
              }
            });
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }
</script> @endsection