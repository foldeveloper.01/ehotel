@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Rooms Manage</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Rooms Manage</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
                    <div class="col-md-12 col-xl-4">
                        <div class="card text-primary bg-primary-transparent">
                            <div class="card-body">
                                <h4 class="card-title text-center">Total Room : {{@$room->total_room}}</h4>
                            </div>
                        </div>
                    </div><div class="col-md-12 col-xl-4">
                        <div class="card text-secondary bg-secondary-transparent">
                            <div class="card-body">
                                <h4 class="card-title text-center">Alotted Room : {{@$rooms_manage_count}}</h4>
                            </div>
                        </div>
                    </div><div class="col-md-12 col-xl-4">
                        <div class="card text-success bg-success-transparent">
                            <div class="card-body">
                                <h4 class="card-title text-center">Remaining Room : {{@$remaining_room}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
<div class="row">
  <form id="create" action="{{url('admin/roomsmanage/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Rooms Manage</h3>
        </div>
        <input type="hidden" name="room_id" value="{{Request::segment(4)}}">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Room Number starts with</label>
                <input type="text" value="" name="name" class="form-control" id="name" placeholder="Name">
               @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Total Room Number</label>
                <input type="text" value="{{@$remaining_room}}" name="total_number" class="form-control" id="total_number" placeholder="total number">
               @if($errors->has('total_number')) <div class="error">{{ $errors->first('total_number') }}</div> @endif
               </div>
            </div>
          </div>         
        </div>
        <div class="card-footer">
        <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      name: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection