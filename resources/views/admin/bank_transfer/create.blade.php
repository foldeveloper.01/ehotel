@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Bank Transfer</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Bank Transfer</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/banktransfer/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Bank Transfer</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Bank Name</label>
                <input type="text" value="" name="bank_name" class="form-control" id="bank_name" placeholder="Bank Name">
              </div> @if($errors->has('bank_name')) <div class="error">{{ $errors->first('bank_name') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Account Name</label>
                <input type="text" value="" name="account_name" class="form-control" id="account_name" placeholder="Account Name">
              </div> @if($errors->has('account_name')) <div class="error">{{ $errors->first('account_name') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Account number</label>
                <input type="number" name="account_number" value="" class="form-control" id="account_number" placeholder="Account number"> @if($errors->has('account_number')) <div class="error">{{ $errors->first('account_number') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Swift code</label>
              <br>
              <input type="text" name="swiftcode" value="" class="form-control" id="swiftcode" placeholder="Swift code"> @if($errors->has('swiftcode')) <div class="error">{{ $errors->first('swiftcode') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Booking automatically deleted, if not bank-in after booking confirmed by bank-in</label>
              <br>
              <div class="custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="booking_automatically_deleted" value="0" checked="">
                  <span class="custom-control-label">Enable</span>
                </label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="booking_automatically_deleted" value="1">
                  <span class="custom-control-label">Disable</span>
                </label>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Number of days waiting for bank-in after booking</label>
              <br>
              <input type="number" name="waiting_days" value="" class="form-control" id="waiting_days" placeholder="Waiting days"> @if($errors->has('waiting_days')) <div class="error">{{ $errors->first('waiting_days') }}</div> @endif
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Image</label>
                <input class="form-control" accept="image/*" name="image" type="file">
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
        <a href="{{url('admin/banktransfer')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      bank_name: {
        required: true
      },
      account_name: {
        required: true
      },
      account_number: {
        required: true
      }
    },
    messages: {
      bank_name: {
        required: "Please enter bank name",
      },
      account_name: {
        required: "Please enter account name",
      },
      account_number: {
        required: "Please enter account number",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection