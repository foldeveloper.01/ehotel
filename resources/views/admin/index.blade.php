@extends('admin.layouts.app')
    @section('styles')
      @endsection
        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Dashboard</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE START -->
                        @if(Auth()->guard('admin')->user()->admin_type_id==1)
                        <div class="row row-cards">   

                        <div class="col-md-12 col-lg-4 col-xl-4">
                                <a href="{{url('admin/bookinglisting')}}">
                                <div class="card dashboard-bookinglist">
                                    <!-- <div class="card-header">
                                        <h3 class="card-title">
                                            Numbers counter
                                        </h3>
                                    </div> -->
                                    <div class="card-body text-center">
                                        <div class="counter-icon bg-primary-transparent box-shadow-primary num-counter mx-auto"> 
                                        <span class="mdi mdi-list-box-outline"></span>
                                        <i class="fa fa-list-alt text-white" aria-hidden="true"></i>
                                         </div>
                                        <h5 class="text-white">BOOKING LIST</h5>
                                        <!-- <h2 class="counter">0.8998</h2> -->
                                    </div>
                                </div>
                              </a>
                            </div>


                            <div class="col-md-12 col-lg-4 col-xl-4">
                                <a href="{{url('admin/checkinlisting')}}">
                                <div class="card dashboard-checkin">
                                    <!-- <div class="card-header">
                                        <h3 class="card-title">
                                            Numbers counter
                                        </h3>
                                    </div> -->
                                    <div class="card-body text-center">
                                        <div class="counter-icon bg-primary-transparent box-shadow-primary num-counter mx-auto "> 
                                        <span class="mdi mdi-login text-white"></span>
                                         </div>
                                        <h5 class="text-white">CHECK-IN</h5>                              
                                    </div>
                                </div>
                                </a>
                            </div>
                            
                            <!-- COL-END -->
                            <div class="col-md-12 col-lg-4 col-xl-4">
                                <a href="{{url('admin/checkoutlisting')}}">
                                <div class="card dashboard-checkout">
                                    <!-- <div class="card-header">
                                        <h3 class="card-title">
                                            Numbers counter
                                        </h3>
                                    </div> -->
                                    <div class="card-body text-center">
                                        <div class="counter-icon bg-primary-transparent box-shadow-primary num-counter mx-auto"> 
                                        <span class="mdi mdi-logout text-white"></span>
                                    </div>
                                        <h5 class="text-white">CHECK-OUT</h5>
                                        <!-- <h2 class="counter"> 2,56989.32</h2> -->
                                    </div>
                                </div>
                               </a>
                            </div>
                            <!-- COL-END -->
                            
                            <div class="col-md-12 col-lg-4 col-xl-4">
                                <a href="{{url('admin/guestuser')}}">
                                <div class="card dashboard-guest">
                                    <!-- <div class="card-header">
                                        <h3 class="card-title">
                                            Numbers counter
                                        </h3>
                                    </div> -->
                                    <div class="card-body text-center">
                                        <div class="counter-icon bg-primary-transparent box-shadow-primary num-counter mx-auto">
                                            <i class="fa fa-user-circle text-white" aria-hidden="true"></i>

                                        </div>
                                        <h5 class="text-white">GUEST</h5>
                                        <!-- <h2 class="counter"> 2,56989.32</h2> -->
                                    </div>
                                </div>
                               </a>
                            </div>
                            
                             <div class="col-md-12 col-lg-4 col-xl-4">
                               <a href="{{url('admin/paidinvoice')}}">
                                 <div class="card dashboard-invoice">
                                    <!-- <div class="card-header">
                                        <h3 class="card-title">
                                            Numbers counter
                                        </h3>
                                    </div> -->
                                    <div class="card-body text-center">
                                        <div class="counter-icon bg-primary-transparent box-shadow-primary num-counter mx-auto"> <i class="fa fa-file-text-o text-white" aria-hidden="true"></i> </div>
                                        <h5 class="text-white">INVOICE</h5>                              
                                    </div>
                                 </div>
                              </a>
                            </div>
                            <!-- COL-END -->
                            
                            <!-- COL-END -->
                            <div class="col-md-12 col-lg-4 col-xl-4">
                                <a href="{{url('admin/housekeepinglist')}}">
                                 <div class="card dashboard-housekeeping">
                                    <!-- <div class="card-header">
                                        <h3 class="card-title">
                                            Numbers counter
                                        </h3>
                                    </div> -->
                                    <div class="card-body text-center">
                                        <div class="counter-icon bg-primary-transparent box-shadow-primary num-counter mx-auto"> 
                                            <i class="side-menu__icon fe fe-home text-white"></i> </div>
                                        <h5 class="text-white">HOUSE KEEPING</h5>
                                        <!-- <h2 class="counter">0.8998</h2> -->
                                    </div>
                                 </div>
                               </a>
                            </div>
                        </div>
                        @else
                          <!-- Other Dashboard -->
                        @endif
                        
                        <!-- PAGE END -->
      @endsection
    @section('scripts')    
    <script type="text/javascript">
        $("#responsive-datatable1").DataTable({language:{searchPlaceholder:"Search...",scrollX:"100%",sSearch:""}});
    </script>
    @endsection
