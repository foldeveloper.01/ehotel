@extends('admin.layouts.app')
@section('styles')
<style type="text/css">.flex-1{display: none;}</style>
@endsection
@section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
   <h1 class="page-title">Check-Out Listing</h1>
   <div>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Check-Out Listing</li>
      </ol>
   </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row row-cards">
   <!-- COL-END -->
   <div class="col-xl-12 col-lg-12">
      <div class="row">
         <div class="col-xl-12">
            <div class="card">
               <div class="card-body p-2">
                  <a href="{{url('admin/bookinglisting/newbooking/?type=normal')}}">
                  <button id="table2-new-row-button" class="btn btn-primary mb-4">New Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=midnight')}}">
                  <button id="" class="btn btn-primary mb-4">Midnight Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=monthly')}}">
                  <button id="" class="btn btn-primary mb-4">Monthly Booking</button>
                  </a>
               </div>
               <form id="search_form" action="{{url(Request::segment(1).'/'.Request::segment(2))}}" method="get" enctype="multipart">
                 
            <div class="row row-sm p-2">              
                  <!-- <div class="col-lg-2">                        
                        <select name="payment" id="payment" class="form-control select2 form-select">
                           <option value="">Payment</option>
                           <option value="1" @if(Request::get('payment')=='1') selected @endif>Paid</option>
                           <option value="0" @if(Request::get('payment')=='0') selected @endif>Not Paid</option>
                           <option value="2" @if(Request::get('payment')=='2') selected @endif>Pay Later</option>
                        </select>
                  </div> -->
                  <div class="col-lg-2">                        
                        <select name="booking_status" id="booking_status" class="form-control select2 form-select">
                           <option value="">Booking Status</option>                          
                           <option value="3" @if(Request::get('booking_status')=='3') selected @endif>Check-out</option>
                           <option value="4" @if(Request::get('booking_status')=='4') selected @endif>Today Check-out</option>
                        </select>
                  </div>
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input type="text" name="search" id="search" class="form-control" value="{{Request::get('search')}}" placeholder="Searching.....">
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="btn-list">
                          <input type="button" class="btn btn-primary" id="data_filter" value="Search"> 
                          <input type="button" class="btn btn-danger" id="reset" value="Reset">                                                    
                      </div>                      
                  </div>                  
              </div>
               @csrf
           </form>
            </div>
         </div>
      </div>
      <!-- COL-END -->
   </div>
   <!-- ROW-1 CLOSED -->
   <div class="row">
      <div class="col-12">
        <!-- start loop -->
        @if($list->isEmpty())
           <div class="p-4 bg-light border border-bottom-0"><h1>No Record</h1></div>
        @endif
        @foreach($list as $key=>$data)
        <?php if($data->payment_status==0){ $get_payment_status ='payment_failed';}else{$get_payment_status ='payment_success';} ?>
         <div class="card">
            <div class="card-body {{$get_payment_status}}">                
               <div class="top-footer">                  
                  <div class="row">
                     <div class="col-lg-4 col-md-12">
                        <h3 class="text-primary">Booking NO: {{@$data->booking_number}}</h3>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fe fe-info fs-20"></i></span>
                           </div>
                           <div>
                             <?php $bookingstatus = $data->get_booking_status();
                             $status_class_name = 'btn-success';
                             if($bookingstatus=='Pending') {
                               $status_class_name = 'btn-warning';
                             }
                              ?>
                              <strong class="btn {{$status_class_name}} disabled btn-sm">{{$bookingstatus}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fe fe-user fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{ucwords(@$data->guest->name)}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fa fa-address-card-o fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{@$data->guest->ic_passport_no}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fe fe-phone fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{@$data->guest->contact_number}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fa fa-calendar fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="@if(@$data->booking_type!='walk-in') fa fa-globe fs-20 @else mdi mdi-walk fs-20 @endif"></i></span>
                           </div>
                           <div>
                              <strong>{{ucwords(@$data->booking_type)}}</strong>
                           </div>
                        </div>
                        <!-- <div class="btn-list">                           
                           <a id="{{@$data->id}}" class='btn btn-success btn-sm bg-success-gradient click_status text-white' data-bs-toggle='modal' data-bs-target='#scrollingmodal'><i class="fa fa-sticky-note-o"></i></a>
                           <a id="{{@$data->id}}" class='btn btn-warning btn-sm bg-warning-gradient add_guest_data text-white'><i class="fa fa-user"></i></a>
                           <a href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->id)}}" class="btn btn-success btn-sm bg-success-gradient text-white click_print text-white"><i class="fa fa-calendar"></i></a>
                           <input type="hidden" value="{{@$data->booking_number}}" id="bookingnumber_{{@$data->id}}">
                           @if(@$data->payment_status==0)
                           <button id="{{@$data->id}}" class='btn btn-danger btn-sm bg-danger-gradient remove_booking'><i class="fa fa-remove"></i></button>
                           @endif                          
                        </div> -->                       

                        <div class="btn-list">                           
                           <a id="{{@$data->id}}" class='btn btn-success btn-sm bg-success-gradient click_status text-white' data-toggle="tooltip" title="Add Remark" data-bs-toggle='modal' data-bs-target='#scrollingmodal'><i class="fa fa-sticky-note-o"></i></a>
                           <a id="{{@$data->id}}" class='btn btn-warning btn-sm bg-warning-gradient add_guest_data text-white' data-bs-placement="top" data-bs-toggle="tooltip" title="Add Guest Details"><i class="fa fa-user"></i></a>
                           <input type="hidden" value="{{@$data->booking_number}}" id="bookingnumber_{{@$data->id}}">
                           @if(@$data->payment_status==0)
                             <a href="{{url('admin/bookinglisting/newbooking/booking_view/'.@$data->id)}}"  class='btn btn-success btn-sm bg-success-gradient text-white click_print' data-bs-placement="top" data-bs-toggle="tooltip" title="Booking Details"><i class="fa fa-calendar"></i></a>

                             <a href="{{url('admin/bookinglisting/newbooking/send_email_to_customer/'.@$data->id)}}" class='btn btn-success btn-sm bg-success-gradient text-white' data-bs-placement="top" data-bs-toggle="tooltip" title="Send Email to Customer"><i class="fa fa-envelope"></i></a>

                             <a href="{{url('admin/bookinglisting/newbooking/send_sms_to_customer/'.@$data->id)}}" class='btn btn-success btn-sm bg-success-gradient text-white' data-bs-placement="top" data-bs-toggle="tooltip" title="Send sms to Customer"><i class="fa fa-phone"></i></a>
                             <a id="{{@$data->id}}" class='btn btn-danger btn-sm bg-danger-gradient remove_booking text-white' data-bs-placement="top" data-bs-toggle="tooltip" title="Remove Booking"><i class="fa fa-remove"></i></a>
                           @else
                             <!-- <a href="{{url('admin/bookinglisting/newbooking/refund_view/'.@$data->id)}}"  class='btn btn-success btn-sm bg-success-gradient text-white'><i class="fa fa-dollar"></i></a> -->
                             <a href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->id)}}" class="btn btn-success btn-sm bg-success-gradient text-white click_print" data-bs-placement="top" data-bs-toggle="tooltip" title="Booking Details"><i class="fa fa-calendar"></i></a></a>
                           @endif                          
                        </div>
                     </div>
                     <br>
                     <div class="col-lg-4 col-md-12">
                        <h6>Room Information: </h6>
                        <ul class="list-unstyled mb-4">
                           <li><h1 class="display-7 text-secondary fw-bold my-3">@php $last_key = count($data->get_booking_details()); $i = 0; @endphp @foreach($data->get_booking_details() as $key=>$val) @if (++$i === $last_key) {{@$val->rooms_number->room_no}} @else {{@$val->rooms_number->room_no}}, @endif @endforeach</h1></li>
                           <li>
                               <?php $get_total_amount = Helper::get_total_amount($data->id);?>
                                @if(@$data->payment_status==0)
                                <h4 class="display-8 text-danger fw-bold my-3">-RM {{abs(@$get_total_amount)}}</h4>
                                @else
                                   @if($get_total_amount < 0 )
                                      <h4 class="display-8 text-danger fw-bold my-3">-RM {{number_format(abs(@$get_total_amount),2)}}</h4>
                                    @else
                                      <h4 class="display-8 text-success fw-bold my-3">RM {{number_format(@$get_total_amount,2)}}</h4>
                                   @endif
                                @endif
                         </li>                           
                        </ul>
                        <div class="btn-list">
                             @if(@$data->payment_status==0)
                               <!-- <a class="btn btn-warning btn-sm bg-warning-gradient">Pending</a> -->
                               <a href="{{url('admin/bookinglisting/newbooking/payment/'.$data->id)}}" class="btn btn-success btn-sm text-white bg-success-gradient">Pay</a>
                             @endif
                             @if(@$data->booking_status==1)
                               <!-- <a class="btn btn-success btn-sm text-white">{{$data->get_booking_status()}}</a> -->
                               <a href="{{url('admin/bookinglisting/newbooking/productaddon/'.$data->id)}}" class="btn btn-secondary btn-sm text-white">Add-on</a>
                               <a href="{{url('admin/bookinglisting/newbooking/edit_billing_view/'.$data->id)}}" class="btn btn-success btn-sm text-white">Edit</a>
                               @if($get_total_amount < 0 )
                                 <a id="{{@$data->id}}_{{@$data->booking_number}}_{{@$get_total_amount}}" class='btn btn-warning btn-sm bg-warning-gradient click_payment text-white' data-bs-toggle='modal' data-bs-target='#scrollingmodal1'>Pay</a>
                               @endif
                             @endif
                           <!-- <button class="btn btn-icon  btn-success">Confirmed</button>
                           <button class="btn btn-icon  btn-success">Add-on</button>
                           <button class="btn btn-icon  btn-success">Edit</i></button> -->                           
                        </div>
                     </div>

                     <div class="col-lg-2 col-md-12">
                        <ul class="list-unstyled mb-3">
                           <div class="card border border-secondary p-1 click_checkin_date" data-toggle="tooltip" title="Check-In">
                              <div class="ribbon-price">
                                 <input type="hidden" class="get_payment_status" value="{{@$data->payment_status}}">
                                 <input type="hidden" class="get_checkin_date" value="{{@$data->check_in}}">
                                 <input type="hidden" class="get_checkout_date" value="{{@$data->check_out}}">
                                 <input type="hidden" class="get_booking_id" value="{{@$data->id}}">
                                 <input type="hidden" class="get_checkin_rooms_count" value="{{@$data->checkin_rooms_count()}}">
                                 <span class="badge bg-secondary text-white">Check-In</span>
                              </div>
                              <div class="princing-item mb-3">
                                 <div class="pricing-divider text-center pt-5">
                                    <h4 class="display-5 text-secondary fw-bold my-3"> {{@$data->checkin_date_booking()[0]}} </h4>
                                    <h3 class="text-secondary">{{@$data->checkin_date_booking()[1]}}</h3>
                                    <h3 class="text-secondary">{{@$data->checkin_date_booking()[2]}}</h3>
                                 </div>
                              </div>
                           </div>
                           @if(@$data->payment_status!=0)
                           <div class="btn-list">
                              @if(@$data->booking_status==0)
                                <a href="{{url('admin/bookinglisting/booking_change_date/'.$data->id)}}" class="btn btn-primary btn-sm">Change date</a>
                              @else
                                <a href="{{url('admin/bookinglisting/booking_extend_date/'.$data->id)}}" class="btn btn-primary btn-sm">Extend Date</a>
                              @endif
                              <a href="{{url('admin/bookinglisting/booking_change_room/'.$data->id)}}" class="btn btn-primary btn-sm">Change room</a>
                           </div>
                           @endif
                        </ul>                        
                     </div>

                     <div class="col-lg-2 col-md-12">
                        <ul class="list-unstyled mb-3">
                           <div class="card border border-secondary p-1 click_checkout_date" data-toggle="tooltip" title="Check-Out">
                              <div class="ribbon-price">                                 
                                 <input type="hidden" class="get_checkin_date" value="{{@$data->check_in}}">
                                 <input type="hidden" class="get_checkout_date" value="{{@$data->check_out}}">
                                 <input type="hidden" class="get_booking_id" value="{{@$data->id}}">
                                 <input type="hidden" class="get_checkin_rooms_count" value="{{@$data->checkin_rooms_count()}}">
                                 <input type="hidden" class="get_checkout_rooms_count" value="{{@$data->checkout_rooms_count()}}">
                                 <span class="badge bg-secondary text-white">Check-Out</span>
                              </div>
                              <div class="princing-item mb-3">
                                 <div class="pricing-divider text-center pt-5">
                                    <h4 class="display-5 text-secondary fw-bold my-3"> {{@$data->checkout_date_booking()[0]}} </h4>
                                    <h3 class="text-secondary">{{@$data->checkout_date_booking()[1]}}</h3>
                                    <h3 class="text-secondary">{{@$data->checkout_date_booking()[2]}}</h3>
                                 </div>
                              </div>
                           </div>
                            @if(@$data->payment_status!=0)
                           <div class="btn-list">                                            
                              <a href="{{url('admin/bookinglisting/newbooking/billing_view/'.@$data->id)}}" class="btn btn-primary btn-sm">View</a>
                              <a href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->id)}}" class="btn btn-primary btn-sm click_print">Print</a>
                           </div>
                           @endif
                        </ul>                       
                     </div>
                  </div>
                </div>                
            </div>           
         </div>
          <!-- end loop -->
                  @endforeach
      </div>
      <!-- COL-END -->
   </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{ $list->onEachSide(1)->links() }}
                            </ul>
                        </nav>
   
</div>

<div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Booking No : <span id="booking_id"></span> </h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-lg-10 col-md-12">
                          <div class="form-group">
                            <b>Guest Name:</b> <span id="guest_name"></span><br>
                            <b>Contact Number:<b> <span id="guest_contact_number"></span><br>
                            <b>Email:<b> <span id="guest_email"></span><br>
                            <b>Amount:<b> <span id="booking_amount"></span><br>
                            <b>Booking Date:<b> <span id="booking_date"></span><br>
                          </div>
                      </div>                   
                    <form id="remark_form" name="remark_form" action="{{url('admin/depositremark/create')}}" method="post">
                    @csrf      
                    <input type="hidden" id="bookingid" name="bookingid" value="">       
                    <input type="hidden" id="booking_no" name="booking_no" value="">       
                      <div class="col-lg-10 col-md-12">
                        <div class="form-group">
                          <label for="remark"><b>Remark<b></label>
                          <textarea class="form-control mb-4" placeholder="Remark" name="remark" rows="4"></textarea>
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>


                  <div class="col-xl-12">
                            <div class="card">
                                    <div class="table-responsive" id="remark_table">
                                        <table class="table border text-nowrap text-md-nowrap table-striped mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Remark</th>
                                                    <th>Added By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                  </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>

<div class="modal fade" id="scrollingmodal1" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Booking No : <span class="payment_booking_id"></span> </h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                  <div class="modal-body">
                       <form id="payment_form" name="payment_form" action="{{url('admin/bookinglisting/pay_due_amount')}}" method="post">
                          @csrf      
                          <input type="hidden" id="payment_booking_id" name="payment_booking_id" value="">                            
                            <div class="row mb-4">
                                 <label class="col-md-3 form-label">Pay Amount</label>
                                 <div class="col-md-9">
                                     <input type="amount" class="form-control mb-4" min="0" max="" placeholder="Amount" name="amount" id="payment_amount">
                                 </div>
                            </div>
                            <div class="row mb-4">
                                 <label class="col-md-3 form-label">Pay By</label>
                                 <div class="col-md-9">
                                     <select name="payment_method" id="payment_method" class="form-control">
                                        <option value="1">Cash</option>
                                        <option value="4">C/Card</option>
                                        <option value="6">Cheque</option>
                                     </select>
                                 </div>
                             </div>

                           <div class="payment_card">
                              <div class="row mb-4">
                                 <label class="col-md-3 form-label">Bank Name</label>
                                 <div class="col-md-9">
                                     <input type="text" id="card_bank_name" name="card_bank_name" class="form-control" value="">
                                 </div>
                              </div>
                              <div class="row mb-4">
                                 <label class="col-md-3 form-label">Card Holder Name</label>
                                 <div class="col-md-9">
                                     <input type="text" id="card_name" name="card_name" class="form-control" value="">
                                 </div>
                              </div><div class="row mb-4">
                                 <label class="col-md-3 form-label">Card Number</label>
                                 <div class="col-md-9">
                                     <input type="text" id="card_number" name="card_number" class="form-control" value="">
                                 </div>
                              </div><div class="row mb-4">
                                 <label class="col-md-3 form-label">Expiry date</label>
                                 <div class="col-md-9">
                                     <input type="text" id="card_expiry" name="card_expiry" class="form-control" value="">
                                 </div>
                              </div>
                           </div>

                           <div class="payment_cheque">
                              <div class="row mb-4">
                                 <label class="col-md-3 form-label">Bank Name</label>
                                 <div class="col-md-9">
                                     <input type="text" id="cheque_bank_name" name="cheque_bank_name" class="form-control" value="" placeholder="Bank Name">
                                 </div>
                              </div>
                              <div class="row mb-4">
                                 <label class="col-md-3 form-label">Cheque Number</label>
                                 <div class="col-md-9">
                                     <input type="text" id="cheque_number" name="cheque_number" class="form-control" value="" placeholder="Cheque Number">
                                 </div>
                              </div>
                           </div>                           
                       </form>
                       <span class="text-red err_msg">All fields Required*</span>
                       <div class="modal-footer">
                              <button type="submit" id="pay_button" class="btn btn-primary">Pay</button>
                       </div>                    
                  </div>
            </div>
      </div>
</div>

<!-- ROW-1 END-->
@endsection
@section('scripts')
<script type="text/javascript">
   $(document).ready(function() {
      $("body").tooltip({ selector: '[data-toggle=tooltip]' });
      var get_today_date = "{{\Carbon\Carbon::now()->format('Y-m-d')}}";
     //create remark
       //click view new windows for print
      $(document).on('click','.click_print',function(e){
         e.preventDefault();
         window.open($(this).attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

      $(document).on('click','.add_guest_data',function(){
         //redirect new page
          var id=$(this).attr('id');
          var redirect = site_url + '/admin/bookinglisting/newbooking/get_room_guest_details/'+id;
          window.location.href=redirect;
          return false;
         //redirect new page
      });

      //filter
      $(document).on('change','#payment',function(e) { 
          $('#search_form').submit();
      });
      $(document).on('change','#booking_status',function(e) {
          $('#search_form').submit();
      });
      $(document).on('click','#data_filter',function(e) { 
         if( $('#search').val() =='' &&  $('#booking_status').val() =='' && $('#payment').val() =='') {
            toastr.clear();
            toastr.warning('Input data is required');
            return false;
         }
         $('#search_form').submit();
      });

      $(document).on('click','#reset',function() {
         //redirect new page          
         //if( $('#search').val() !='' ||  $('#booking_status').val() !='' || $('#payment').val() !='') {
          var path = "/{{Request::segment(1)}}/";
          var path1 = "{{Request::segment(2)}}";
          var redirect = site_url + path + path1;
          window.location.href=redirect;
          return false;
        //}
         //redirect new page
      });
      //filter

      //payment section
      $(document).on('click','.click_payment',function() {
          //console.log($(this).attr('id'));
          $('.err_msg').hide();
          $('.payment_card').hide();
          $('.payment_cheque').hide();
          $('#payment_method').val(1);
          var bookingid  = $(this).attr('id').split('_')[0];
          var booking_no = $(this).attr('id').split('_')[1];          
          var amount = $(this).attr('id').split('_')[2];          
          $("#payment_booking_id").val(bookingid);
          $(".payment_booking_id").text(booking_no);
          $("#payment_amount").val(Math.abs(amount));
          $("#payment_amount").attr({
               "max" : Math.abs(amount),
               "min" : 1
           });
          return false;
      });
      $(document).on('change','#payment_method',function() {
            $('.payment_card').hide();
            $('.payment_cheque').hide();        
            if($(this).val()==4) {
              $('.payment_card').show();
            }else if($(this).val()==6) {
              $('.payment_cheque').show();
            }
            return false;
      });
      $(document).on('click','#pay_button',function() {
         $('.err_msg').hide();
         if($('#payment_method :checked').val() == 4) {
               if($('#card_bank_name').val()=='' || $('#card_name').val()=='' || $('#card_number').val()=='' || $('#card_expiry').val()=='' ) {
                  $('.err_msg').show();
                  return false;
                }
            }else if($('#payment_method :checked').val() == 6) {
               if($('#cheque_bank_name').val()=='' || $('#cheque_number').val()=='') {
                  $('.err_msg').show();
                  return false;
               }
            }
         $('#payment_form').submit();
      });
      //payment section

      //start checkin click in function
      $(document).on('click','.click_checkin_date',function(){
         var payment_status = $(this).find('.get_payment_status').val();
         var get_checkin_date = $(this).find('.get_checkin_date').val();
         var get_checkout_date = $(this).find('.get_checkout_date').val();
         var get_booking_id = $(this).find('.get_booking_id').val();
         var get_checkin_rooms_count = $(this).find('.get_checkin_rooms_count').val();         
         if(payment_status==0){
           return false;
         }
         //if all room checked in condition check
         if(get_checkin_rooms_count==0){
            toastr.clear();
            toastr.warning('Aready Check-in all rooms. proceed in checkout process');
            return false;
         }
         //if all room checked in condition check
         if(!confirm("Are you sure you want to check-in this?")){
           return false;           
         }
         get_today_date = new Date(get_today_date);
         get_checkin_date = new Date(get_checkin_date);
         if(get_checkin_date.toDateString()===get_today_date.toDateString()){
            var redirect = site_url + '/admin/checkinlisting/checkin_confirm/view/'+get_booking_id;
            window.location.href=redirect;
         }else if(get_today_date < get_checkin_date) {
            alert('Check-in date on '+date_change(get_checkin_date));
         }else if(get_today_date > get_checkin_date) {
            //alert('Check-in date has already passed. Check-in date is '+date_change(get_checkin_date)+', Check-out date is '+date_change(get_checkout_date)+'');
            toastr.clear();
            toastr.error('Check-in date has already passed. Check-in date is '+date_change(get_checkin_date)+', Check-out date is '+date_change(get_checkout_date)+'');
         }else{
            console.log('date not matching');
         }
         return false;         
      });
      //end checkin click in function

       //start checkin click out function
      $(document).on('click','.click_checkout_date',function(){
         var get_checkin_date = $(this).find('.get_checkin_date').val();
         var get_checkout_date = $(this).find('.get_checkout_date').val();
         var get_booking_id = $(this).find('.get_booking_id').val();
         var get_checkin_rooms_count = $(this).find('.get_checkin_rooms_count').val();         
         var get_checkout_rooms_count = $(this).find('.get_checkout_rooms_count').val();
         
         //if all room checked-in condition check
         if(get_checkin_rooms_count==0){
               var redirect = site_url + '/admin/checkoutlisting/checkout_confirm/view/'+get_booking_id;
               window.location.href=redirect;

               /*get_today_date = new Date(get_today_date);
               get_checkout_date = new Date(get_checkout_date);
               //checkout date is same day going to checkout
               if(get_checkout_date.toDateString()===get_today_date.toDateString()){
                   if(get_checkout_rooms_count==0){
                     alert('already checkout all rooms.');
                     return false;
                   }                   
                   if(!confirm("Are you sure you want to check-out this?")){
                       return false;           
                   }
                   var redirect = site_url + '/admin/checkoutlisting/checkout_confirm/view/'+get_booking_id;
                   window.location.href=redirect;
                   //checkout date is same day going to checkout
               }
               if(new Date().getTime() - get_checkout_date.getTime() > 0){
                  toastr.clear();
                  toastr.warning('Check-out date has already passed. Check-out date is '+date_change(get_checkout_date)+'');
                  //alert('Check-out date has already passed. Check-out date is '+date_change(get_checkout_date)+'');
               }if(new Date().getTime() - get_checkout_date.getTime() < 0){
                   if(!confirm("Are you sure you want to check-out? your check-out date is "+date_change(get_checkout_date)+"")){
                       return false;           
                   }
                   if(get_checkout_rooms_count==0){                     
                     toastr.clear();
                     toastr.warning('Already checkout all rooms.');
                     return false;
                   }
                  var redirect = site_url + '/admin/checkoutlisting/checkout_confirm/view/'+get_booking_id;
                  window.location.href=redirect;
               }*/
               
          }
         //if all room checked-in condition check
         return false;         
      });
      //end checkin click out function

      function date_change(newdate){
            var formattedDate = new Date(newdate);
            var d = formattedDate.getDate();
            var m =  formattedDate.getMonth();
            m += 1;  // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            return d + "-" + m + "-" + y;
      }

      $(document).on('click','.remove_booking',function(){
         if (!confirm("Are you sure you want to delete? This action cannot be undone.")){
           return false;
         }else{
            var removeid = $(this).attr('id');
            var getbookingno = $("#bookingnumber_"+removeid+"").val();
            //console.log(getbookingno.val());
            $.ajax({
                url: site_url + '/admin/bookinglisting/remove_due_booking',
                type: 'post',
                dataType: 'json',
                data: {
                  id: removeid,
                  _token: '{{csrf_token()}}'
                },
                success: function(data) {
                   toastr.clear();
                   toastr.success('Booking ID : '+getbookingno+' removed successfully');
                }
              });
            $(this).closest(".card").remove();                       
         }
      });

      $(document).on('click','.delete_remark',function(){
         if (!confirm("Do you want to remove")){
           return false;
         }else{
            //console.log($(this).attr('id'));
            $(this).closest("tr").remove();
            var removeid = $(this).attr('id');
            $.ajax({
                url: site_url + '/admin/depositremark/remove',
                type: 'post',
                dataType: 'json',
                data: {
                  id: removeid,
                  _token: '{{csrf_token()}}'
                },
                success: function(data) {
                   toastr.clear();
                   toastr.success('Remark removed successfully');
                }
              });
         }
      });

      $(document).on('click','.click_status',function(){
          $("#bookingid").val($(this).attr('id'));
          $.ajax({
          url: site_url + '/admin/depositremark/get',
          type: 'post',
          dataType: 'json',
          data: {
            id: $(this).attr('id'),
            //selectedData: selectedData,
            _token: '{{csrf_token()}}'
          },
          success: function(data) {
            $("#booking_id").html(data['booking_id']);
            $("#booking_no").val(data['booking_id']);
            $("#guest_name").html(data['guest_name']);
            $("#guest_contact_number").html(data['guest_number']);
            $("#guest_email").html(data['guest_email']);
            $("#booking_amount").html(data['amount']);
            $("#booking_date").html(data['booking_date']);              
              var tr = '';              
              $.each(data.tablerow, function(i, item) {
               var delete_action = '<button type="button" id="'+item.id+'" class="btn  btn-sm btn-danger delete_remark"><span class="fe fe-trash-2"> </span></button>';
                tr += '<tr><td>' + item.created_at + '</td><td>' + item.remarks + '</td><td>' + item.admin_user_id + '</td><td>' + delete_action + '</td></tr>';
              });
              $('#remark_table tbody').html(tr);
          }
        });
      });
     //create remark

    $("#remark_form").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      remark: {
        required: true
      }
    },
    messages: {
      remark: {
        required: "Please enter remark",
      }
    },    
  });
  });
function deletefunction(event, id) {
   event.preventDefault();
   swal({
     title: "Are you sure you want to delete this record?",
     text: "If you delete this, it will be gone forever.",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: '#DD6B55',
     confirmButtonText: 'Delete',
     cancelButtonText: "Cancel",
     closeOnConfirm: false,
     closeOnCancel: true
   }, function(isConfirm) {
     if (isConfirm) {
       $("#delete_form" + id).submit();
     } else {
       swal("Cancel", "Your data has not been removed", "error");
     }
   });
   }
</script>
@endsection