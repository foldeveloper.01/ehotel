@extends('admin.layouts.app') @section('styles')<style type="text/css">.select2-hidden-accessible{
  z-index:99999 !important;
}</style> @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">House Keeping</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">House Keeping List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">House Keeping List</h3>
      </div>
      <div class="card-body">
        <!-- <a href="{{url('admin/coupon/create')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Create Coupon</button>
        </a>        -->
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="data" >
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Booking No</th>
                <th class="wd-15p border-bottom-0">Room No</th>
                <th class="wd-15p border-bottom-0">Check-in</th>                
                <th class="wd-15p border-bottom-0">Check-out</th>
                <th class="wd-15p border-bottom-0">Added Date</th>
                <th class="wd-1p border-bottom-0">Release Date</th>
                <th class="wd-1p border-bottom-0">Assigned User</th>
                <th class="wd-1p border-bottom-0">Status</th>
                <th class="wd-1p border-bottom-0">Action</th>
              </tr>
            </thead>           
          </table>
        </div>

        <div class="modal fade" id="add_remark" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Booking No : <span id="booking_id"></span> </h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-lg-10 col-md-12">
                          <div class="form-group">
                            <b>Guest Name:</b> <span id="guest_name"></span><br>
                            <b>Contact Number:<b> <span id="guest_contact_number"></span><br>
                            <b>Email:<b> <span id="guest_email"></span><br>
                            <b>Amount:<b> <span id="booking_amount"></span><br>
                            <b>Booking Date:<b> <span id="booking_date"></span><br>
                          </div>
                      </div>                   
                    <form id="remark_form" name="remark_form" action="{{url('admin/depositremark/create')}}" method="post">
                    @csrf      
                    <input type="hidden" id="bookingid" name="bookingid" value="">       
                    <input type="hidden" id="booking_no" name="booking_no" value="">       
                      <div class="col-lg-10 col-md-12">
                        <div class="form-group">
                          <label for="remark"><b>Remark<b></label>
                          <textarea class="form-control mb-4" placeholder="Remark" name="remark" rows="4"></textarea>
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>


                     <div class="col-xl-12">
                            <div class="card">
                                    <div class="table-responsive" id="remark_table">
                                        <table class="table border text-nowrap text-md-nowrap table-striped mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Remark</th>
                                                    <th>Added By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                  </div>
                            </div>
                     </div>
                 </div>
            </div>
      </div>
</div>

               <!-- Modal -->
        <div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Housekeeping User list</h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <form id="user_select_form" action="{{url('admin/housekeepinglist')}}" method="post" enctype="multipart">
                      @csrf
                      <input type="hidden" name="id" id="id">
                    <div class="modal-body">
                       <div class=" row mb-4">
                            <label for="inputEmail3" class="col-md-3 form-label">User name</label>
                            <div class="col-md-9">
                              <select name="user_id" id="user_id" class="form-control" data-placeholder="Choose one"><option label="Choose one"></option> @if(@$housekeeping_user_list) @foreach(@$housekeeping_user_list as $key=>$value) <option value="{{@$value->id}}" @if(@$value->id==Auth()->guard('admin')->user()->id) selected @endif>{{@$value->username}}</option> @endforeach @endif </select>
                            </div>
                       </div>
                    </div>
                    </form>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" id="form">Save changes</button>
                    </div>
                  
                </div>
            </div>
        </div>


        <form id="status_form" action="{{url('admin/housekeepinglist')}}" method="post" enctype="multipart">
          @csrf
          <input type="hidden" name="status_id" id="status_id">                    
          <input type="hidden" name="status" id="status">                    
        </form>


      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
    $(document).ready(function() {

      $(document).on('click','.click_status',function(){
          $("#bookingid").val($(this).attr('id'));
          $.ajax({
          url: site_url + '/admin/depositremark/get',
          type: 'post',
          dataType: 'json',
          data: {
            id: $(this).attr('id'),
            //selectedData: selectedData,
            _token: '{{csrf_token()}}'
          },
          success: function(data) {
            $("#booking_id").html(data['booking_id']);
            $("#booking_no").val(data['booking_id']);
            $("#guest_name").html(data['guest_name']);
            $("#guest_contact_number").html(data['guest_number']);
            $("#guest_email").html(data['guest_email']);
            $("#booking_amount").html(data['amount']);
            $("#booking_date").html(data['booking_date']);              
              var tr = '';              
              $.each(data.tablerow, function(i, item) {
               var delete_action = '<button type="button" id="'+item.id+'" class="btn  btn-sm btn-danger delete_remark"><span class="fe fe-trash-2"> </span></button>';
                tr += '<tr><td>' + item.created_at + '</td><td>' + item.remarks + '</td><td>' + item.admin_user_id + '</td><td>' + delete_action + '</td></tr>';
              });
              $('#remark_table tbody').html(tr);
          }
        });
      });
     //create remark
        //assign admin create
        $('body').on('click', '#user_assign', function (event) {
        $('#user_id').val('');
        $("#id").val($(this).closest('span').attr('id'));
       });
        //assign admin update
        $('body').on('click', '#user_assign_update', function (event) {        
        $("#id").val($(this).closest('span').attr('id'));
        $('#user_id').val($('#id').val().split("/")[1]);
       });

        $('body').on('click', '#form', function (event) {
           $('#user_select_form').validate();
           if ($('#user_select_form').valid()) {
                 $('#user_select_form').submit();
            }
       });

    $( "#user_select_form" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            user_id: {
              required: true,
            },
          },
            messages: {        
                user_id: {
                    required: "This field is required",
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "domain_agreement") {
                    error.appendTo("#agreement_error").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                }/*else if (element.attr("name") == "password_confirmation") {
                    error.appendTo("#errorpassword1").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                }*/else {
                    error.insertAfter(element);
                }                
               },
        });


      var table = $('#data').DataTable({
         processing: true,
         serverSide: true,
         "pageLength": 10,
         "paging": true,
         ajax: "{{url('admin/housekeepinglist/getdata')}}",
         columns: [
            { data: 'id' },
            { data: 'booking_number' },
            { data: 'room_numbers' },
            { data: 'check_in' },
            { data: 'check_out' },
            { data: 'created_at' },
            { data: 'release_date' },
            { data: 'user_id' },
            { data: 'status' },
            { data: 'action' }
         ],'columnDefs': [ {
            'targets': [0,8],
            'orderable': false,
         }],
         "order": [[5,"desc"]],
      });

      table.on('draw.dt', function () {
          var info = table.page.info();
          table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
              cell.innerHTML = i + 1 + info.start;
          });
      });

      //$('.custom-switch-input').on('change', function() {
      $('body').on('change', '.custom-switch-input', function() {
      var isChecked = $(this).is(':checked');
      var id = $(this).attr('id');
      var selectedData;
      var $switchLabel = $('.custom-switch-indicator');
      //console.log('isChecked: ' + isChecked);
      if (isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      //console.log('Selected data: ' + selectedData);
      $.ajax({
        url: site_url + '/admin/housekeepinglist/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: selectedData,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();
            if (selectedData == 'Yes') {
              var success = 'Status Activate';
              toastr.success(success + ' successfully');
            } else {
              var success = 'Status Suspend';
              toastr.error(success + ' successfully');
            }
          } else {
            toastr.clear();
            toastr.error('something went wrong');
          }
        }
      });
    });    
  });

function AssignRoom(event, id, user_id) {
    let obj = event.target;
    var status_text = obj.textContent;    
    event.preventDefault();
    swal({
      title: "Are you sure you want to assign this room?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Update',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $('#user_id').val(user_id);
        $('#id').val(id+'/'+user_id);
        $('#user_select_form').submit();
      } else {
        swal("Cancel", "Your data has not been updated", "error");
      }
    });
  }


  function update_status(event, id) {
    let obj = event.target;
    var status_text = obj.textContent;
    if(status_text=='Cleaning'){
      var alert_text = 'Cleaning to Completed';
    }else{
      var alert_text = 'Completed to Cleaning';
    }
    event.preventDefault();
    swal({
      title: "Are you sure change the status '"+alert_text+"'",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Update',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $('#status').val(status_text);
        $('#status_id').val(id);
        $('#status_form').submit();
      } else {
        swal("Cancel", "Your data has not been updated", "error");
      }
    });
  }

 //click view new windows for print
      $(document).on('click','.click_print',function(e){
         e.preventDefault();
         window.open($(this).attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print


</script> @endsection