@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Coupon</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Update Coupon</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="update" action="{{url('admin/coupon/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Update Coupon</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Name</label>
                <input type="text" value="{{@$data->name}}" name="name" class="form-control" id="name" placeholder="Name">
              </div> @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Code Starts with (Ex: Newyear)</label>
                <input type="text" value="{{@$data->code}}" name="code" class="form-control" id="code" placeholder="Code">
              </div> @if($errors->has('code')) <div class="error">{{ $errors->first('code') }}</div> @endif
            </div>
          </div> 
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Discount Type</label>
              <br>
              <div class="custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="discount_type" value="amount" @if(@$data->discount_type=='amount') checked="" @endif>
                  <span class="custom-control-label">Amount</span>
                </label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="discount_type" value="percentage" @if(@$data->discount_type=='percentage') checked="" @endif>
                  <span class="custom-control-label">Percentage</span>
                </label>
              </div>
            </div>            
          </div>
          <div id="amount_div">
          <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label for="Amount">Amount</label>
                  <div class="wrap-input100 validate-input input-group">
                    <button class="btn btn-info btn-pill">
                      MYR
                    </button>
                    <input class="form-control" type="text" placeholder="Amount" name="amount" id="amount" value="{{@$data->amount}}">
                  </div>
                  <span id="amounterror"></span>
                </div>                
              </div>
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="exampleInputnumber">Start Date - End Date</label>
              <br>
              <input type="text" name="amount_start_date" value="" class="form-control" id="amount_start_date" placeholder="Start Date - End Date"> @if($errors->has('amount_start_date')) <div class="error">{{ $errors->first('amount_start_date') }}</div> @endif
            </div>
            </div>            
          </div>
        </div>
          <div id="percentage_div">
          <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="form-group">
                  <label for="Room">Room</label>
                  <div class="wrap-input100 validate-input input-group">
                    <select id="room" name="room[]" multiple="multiple" class="multi-select" placeholder="Choose one" >
                      @foreach(@$room_types as $key=>$val)
                      <option value="{{@$val->id}}" @if(in_array($val->id,$get_selected_roomid)) selected @endif>{{@$val->room_type}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>                
              </div>
             <div class="col-lg-3 col-md-6">
              <div class="form-group">
              <label for="exampleInputnumber">Start Date - End Date</label>
              <br>
              <input type="text" name="percentage_start_date" class="form-control" id="percentage_start_date" placeholder="Start Date - End Date"> @if($errors->has('percentage_start_date')) <div class="error">{{ $errors->first('percentage_start_date') }}</div> @endif
            </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="form-group">
                <label for="exampleInputname">Normal Price</label>
                <input type="text" value="{{@$data->getCouponRoomPrice->normal_price}}" name="normal_price" class="form-control" id="normal_price" placeholder="Normal Price">
              </div> @if($errors->has('normal_price')) <div class="error">{{ $errors->first('normal_price') }}</div> @endif
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="form-group">
                <label for="exampleInputname">Weekend Price</label>
                <input type="text" value="{{@$data->getCouponRoomPrice->weekend_price}}" name="weekend_price" class="form-control" id="weekend_price" placeholder="Weekend Price">
              </div> @if($errors->has('weekend_price')) <div class="error">{{ $errors->first('weekend_price') }}</div> @endif
            </div>           
          </div>

          <div class="row">
            <div class="col-lg-3 col-md-6">
              <div class="form-group">
                <label for="exampleInputname">Promotion Price</label>
                <input type="text" value="{{@$data->getCouponRoomPrice->promotion_price}}" name="promotion_price" class="form-control" id="promotion_price" placeholder="Promotion Price">
              </div> @if($errors->has('promotion_price')) <div class="error">{{ $errors->first('promotion_price') }}</div> @endif
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="form-group">
                <label for="exampleInputname1">School Holiday Price</label>
                <input type="text" value="{{@$data->getCouponRoomPrice->school_holiday_price}}" name="school_holiday_price" class="form-control" id="school_holiday_price" placeholder="School Holiday Price">
              </div> @if($errors->has('school_holiday_price')) <div class="error">{{ $errors->first('school_holiday_price') }}</div> @endif
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="form-group">
                <label for="exampleInputname1">Public Holiday Price</label>
                <input type="text" value="{{@$data->getCouponRoomPrice->public_holiday_price}}" name="public_holiday_price" class="form-control" id="public_holiday_price" placeholder="Public Holiday Price">
              </div> @if($errors->has('public_holiday_price')) <div class="error">{{ $errors->first('public_holiday_price') }}</div> @endif
            </div>
          </div>
        </div>
          
          <div class="row">
            <span><b>Coupon is Valid for Check-in Between</b></span><br>
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="exampleInputnumber">From Date - To Date</label>
              <br>
              <input type="text" value="" name="from_date" class="form-control" id="from_date" placeholder="From Date - To Date"> @if($errors->has('from_date')) <div class="error">{{ $errors->first('from_date') }}</div> @endif
            </div>
            </div>            
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Total Coupon</label>
                <input type="text" value="{{@$data->total_coupon}}" name="total_coupon" class="form-control" id="total_coupon" placeholder="Total Coupon">
              </div> @if($errors->has('total_coupon')) <div class="error">{{ $errors->first('total_coupon') }}</div> @endif
            </div>                     
          </div>

          <div class="row">    <span><b>(Optional)</b></span>       
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" name="is_remark" value="yes" @if(@$data->is_remark=='yes') checked @endif>
                  <span class="custom-control-label">When user enter coupon, if "Remark" field must enter by user, please enable checkbox</span>
                </label>
                <!-- <label for="status">Remark</label> -->
                <input type="text" name="is_remark_text" value="{{@$data->is_remark_text}}" class="form-control" id="is_remark_text" placeholder="Remark"> @if($errors->has('is_remark_text')) <div class="error">{{ $errors->first('is_remark_text') }}</div> @endif
              </div>
            </div>            
          </div>
        
          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                 <option value="0" @if(@$data->status=="0") selected @endif>Active</option>
                  <option value="1" @if(@$data->status=="1") selected @endif>Suspend</option>
                </select>
              </div>
            </div>
             
          </div>
        </div>
        <div class="card-footer">
        <a href="{{url('admin/coupon')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts')
<script src="{{asset('assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- MULTI SELECT JS-->
<script src="{{asset('assets/admin/plugins/multipleselect/multiple-select.js')}}"></script>
<script src="{{asset('assets/admin/plugins/multipleselect/multi-select.js')}}"></script>
 <script type="text/javascript">
  
   // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
    $( document ).ready(function() {
      //$("#percentage_div").css("display", "none");
      //$("input[type=radio][name=discount_type]").prop("checked", true).trigger("click");
      @if(session('active_tab')=='1' )
      $("#percentage_div").css("display", "none");
      $('#amount').prop('required', true);
      $('#room').prop('required', false);      
      @else
      $("#percentage_div").css("display", "block");
      $("#amount_div").css("display", "none");
      $('#amount').prop('required', false);
      $('#room').prop('required', true);
      @endif
      $('input[type=radio][name=discount_type]').change(function() {
        if (this.value == 'amount') {
            $("#percentage_div").css("display", "none");
            $("#amount_div").css("display", "block");
            $('#amount').prop('required', true);
            $('#room').prop('required', false);
        }
        else if (this.value == 'percentage') {
            $("#percentage_div").css("display", "block");
            $("#amount_div").css("display", "none");
            $('#amount').prop('required', false);
            $('#room').prop('required', true);
        }
      });
    });

   var current_date = "{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d-m-Y')}}";
   var start_date = "{{\Carbon\Carbon::parse(@$data->start_date)->format('d-m-Y')}}";
   var end_date = "{{\Carbon\Carbon::parse(@$data->end_date)->format('d-m-Y')}}";
   var check_in_from_date = "{{\Carbon\Carbon::parse(@$data->check_in_from_date)->format('d-m-Y')}}";
   var check_in_to_date = "{{\Carbon\Carbon::parse(@$data->check_in_to_date)->format('d-m-Y')}}";
   
   $("#from_date").daterangepicker({
   singleDatePicker: false,
   minDate:new Date(),
   locale: {
    format: 'DD-MM-YYYY'
   }});

   $("#amount_start_date").daterangepicker({
   singleDatePicker: false,
   minDate:new Date(),
   locale: {
    format: 'DD-MM-YYYY'
   }});
   amount_start_date(start_date, end_date);
   $('#amount_start_date').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));      
            $('#from_date').data('daterangepicker').setStartDate(picker.startDate.format('DD-MM-YYYY'));
            $('#from_date').data('daterangepicker').setEndDate(picker.endDate.format('DD-MM-YYYY'));
            $('#from_date').daterangepicker({ 
                showCustomRangeLabel:false
                , minDate: picker.startDate.format('DD-MM-YYYY')
                , maxDate: picker.endDate.format('DD-MM-YYYY')
                , locale: {
                    format: 'DD-MM-YYYY'
                } 
            });
            $('#from_date').val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
            $('#from_date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#from_date').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
            });
    });

   $('#amount_start_date').on('cancel.daterangepicker', function(ev, picker) {
      $('#amount_start_date').val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
   });
  function amount_start_date(start, end) {
        //alert('amount_start_date');
        $('#amount_start_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
        $('#from_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
  }



  $("#percentage_start_date").daterangepicker({
   singleDatePicker: false,
   minDate:new Date(),
   locale: {
    format: 'DD-MM-YYYY'
   }});
   percentage_start_date(start_date, end_date);
   $('#percentage_start_date').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));      
            $('#from_date').data('daterangepicker').setStartDate(picker.startDate.format('DD-MM-YYYY'));
            $('#from_date').data('daterangepicker').setEndDate(picker.endDate.format('DD-MM-YYYY'));
            $('#from_date').daterangepicker({ 
                showCustomRangeLabel:false
                , minDate: picker.startDate.format('DD-MM-YYYY')
                , maxDate: picker.endDate.format('DD-MM-YYYY')
                , locale: {
                    format: 'DD-MM-YYYY'
                } 
            });
            $('#from_date').val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
            $('#from_date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#from_date').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
            });
    });

   $('#percentage_start_date').on('cancel.daterangepicker', function(ev, picker) {
    $('#percentage_start_date').val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
   });
  function percentage_start_date(start, end) {    
        $('#percentage_start_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
        //$('#from_date').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
  }



  $("#create").validate({
    submitHandler: function(form) {
      if($('input[type=radio][name=discount_type]:checked').val()=='amount'){
         $('#room').prop('required', false);
         //$('#amount').prop('required', true);
         if(!$("#amount").val()) {
          //alert('empty amount');
          return false;
         }
         return true;
     }else{
         //$('#room').prop('required', true);
         if (typeof $("#room").val() !== 'undefined' && $("#room").val().length > 0) {
            console.log('myArray is not empty.');
          }else{
            //alert("Please select room type");
            toastr.clear();
            toastr.warning("Please select room type");
            return false;
          }
          return true;
     }
      form.submit();
    },
    rules: {
      name: {
        required: true
      },
      code: {
        required: true
      },
      start_date: {
        required: true
      },
      end_date: {
        required: true
      },
      total_voucher: {
        required: true
      },
      /*amount: {
        required: true
      },*/
      total_coupon: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name",
      },
      code: {
        required: "Please enter code",
      },
      start_date: {
        required: "Please select start date",
      },
      end_date: {
        required: "Please select end date",
      },
      total_voucher: {
        required: "Please enter total voucher",
      },
     /* amount: {
        required: "Please enter amount",
      },*/
      total_coupon: {
        required: "Please enter total coupon",
      }
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "amount") {
            error.appendTo("#amounterror").css('color','#FF0000').css("fontSize", "14px").css('float','center');
        }else {
            error.insertAfter(element);
        }                
      },
  });
</script> @endsection