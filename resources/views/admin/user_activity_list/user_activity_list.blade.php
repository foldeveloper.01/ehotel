@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Audit Trail</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Audit Trail</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Audit Trail</h3>
      </div>
      <div class="card-body">
        <!-- <a href="{{url('admin/createcontactby')}}"><button id="table2-new-row-button" class="btn btn-primary mb-4"> Add New</button></a> -->
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Date</th>
                <th class="wd-15p border-bottom-0">User Name</th>
                <th class="wd-15p border-bottom-0">User Email</th>
                <th class="wd-15p border-bottom-0">Action</th>
                <th class="wd-15p border-bottom-0">Details</th>
              </tr>
            </thead>
            <tbody> @if(@$list) @foreach(@$list as $key=>$data) <tr> <?php
                                                           $actions =  $data->action;
                                                           $getdata = json_decode($data->details, true); 
                                                           if(isset($getdata['deleted_at'])){
                                                              $actions = 'delete';
                                                            }           
                                                         ?> <td>{{$key+1}}</td>
                <td>{{ \Carbon\Carbon::parse(@$data->created_at)->format('d/m/Y h:i:s')}}</td>
                <td>{{@$data->userDetail->firstname}}</td>
                <td>{{@$data->userDetail->email}}</td>
                <td>{{@$actions}}</td>
                <td>{{@$data->description}} {{@$data->contentId}} </td>
              </tr> @endforeach @endif </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  /*$(document).ready(function() {
        var table = $('#responsive-datatable5').DataTable({
            dom: 'Bfrtip',
            select: true,
            lengthMenu: [
                  [10, 25, 50, -1],
                  ['10 rows', '25 rows', '50 rows', 'Show all rows']
              ],
             buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
             buttons: [
                 { extend: 'pdf', text: '
    <i class="fas fa-file-pdf fa-1x" aria-hidden="true">PDF</i>' },
                  { extend: 'csv', text: '
    <i class="fas fa-file-csv fa-1x">CSV</i>' },
                  { extend: 'excel', text: '
    <i class="fas fa-file-excel" aria-hidden="true">EXCEL</i>' },
                  { extend: 'copy', text: '
    <i class="fas fa-file-copy" aria-hidden="true">COPY</i>' },
                  { extend: 'print', text: '
    <i class="fas fa-file-print" aria-hidden="true">PRINT</i>' },
                  'pageLength'
              ],
        });
        table.buttons().container()
              .appendTo('#datatable_wrapper .col-md-6:eq(0)');
        });*/
</script> @endsection