@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Tax Setting</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Tax Setting</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="tax_setting" action="{{url('admin/taxsettings')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tax Setting</h3>
        </div>
        <div class="card-body">
          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="exampleInputnumber">Booking Tax Type</label>
              <br>
              <div class="custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="booking_type" value="0" @if(@$settingData->tax_type =='0') checked="" @endif>
                  <span class="custom-control-label">Percentage(%)</span>
                </label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <label class="custom-control custom-radio-md">
                  <input type="radio" class="custom-control-input" name="booking_type" value="1" @if(@$settingData->tax_type =='1') checked="" @endif>
                  <span class="custom-control-label">Amount(RM)</span>
                </label>
              </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Percentage(%)</label>
                <input type="number" name="percentage" value="@if(@$settingData->tax_percentage!==''){{@$settingData->tax_percentage}}@endif" class="form-control" id="percentage" placeholder="Percentage(%)"> @if($errors->has('percentage')) <div class="error">{{ $errors->first('percentage') }}</div> @endif
              <span id="error_percentage"></span>
            </div>              
            </div>                       
          </div> 
          <div class="row">
            <div class="col-lg-4 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Amount(RM)</label>
                <input type="number" value="@if(@$settingData->tax_amount!==''){{@$settingData->tax_amount}}@endif" name="amount" class="form-control" id="amount" placeholder="Amount(RM)">
                <span id="error_amount"></span>
              </div>  @if($errors->has('amount')) <div class="error">{{ $errors->first('amount') }}</div> @endif
              
            </div>
                       
          </div>
                   
        </div>
        <div class="card-footer">
          <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
          <button type="button" class="btn btn-success my-1" id="submit_button" value="submit">Save</button>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts')
<script type="text/javascript">
  $( "#submit_button" ).click(function( event ) {
              $("#error_amount").text('');
              $("#error_percentage").text('');
       if($("input[name='booking_type']:checked").val()==0){
             if(!$('#percentage').val()){
                $("#error_percentage").css('color','#FF0000').css("fontSize", "14px").css('float','center').text('Please enter percentage');
                return false;
             }
       }else if($("input[name='booking_type']:checked").val()==1){
             if(!$('#amount').val()){
                $("#error_amount").css('color','#FF0000').css("fontSize", "14px").css('float','center').text('Please enter amount');
                return false;
             }
       }
       if ($('#tax_setting').valid()) {
           $("#tax_setting").submit();
        }
  });
  $("#tax_setting").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      booking_type: {
        required: true
      }
    },
    messages: {
      booking_type: {
        required: "Please select booking type name",
      }
    }    
  });
</script>
@endsection