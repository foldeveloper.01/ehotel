@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Site Setting</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Setting</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <div class="col-xl-7">
    <form id="create_user" action="{{url('admin/settings')}}" method="post" enctype="multipart/form-data"> @csrf <input type="hidden" name="action_tab" value="site">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Site Setting</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Site Name</label>
                <input type="text" value="@if(@$settingData->site_name!==''){{@$settingData->site_name}}@endif" name="sitename" class="form-control" id="exampleInputname" placeholder="Site Name"> @if($errors->has('sitename')) <div class="error">{{ $errors->first('sitename') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Site link</label>
                <input type="text" value="@if(@$settingData->site_link!==''){{@$settingData->site_link}}@endif" name="sitelink" class="form-control" id="exampleInputname1" placeholder="Site Link"> @if($errors->has('sitelink')) <div class="error">{{ $errors->first('sitelink') }}</div> @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" value="@if(@$settingData->email!==''){{@$settingData->email}}@endif" class="form-control" id="exampleInputEmail1" placeholder="Email address"> @if($errors->has('email')) <div class="error">{{ $errors->first('email') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Phone</label>
              <input type="text" name="number" value="@if(@$settingData->phone!==''){{@$settingData->phone}}@endif" class="form-control" id="exampleInputnumber" placeholder="phone"> @if($errors->has('number')) <div class="error">{{ $errors->first('number') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label class="form-label">Address</label>
                <textarea name="address" value="@if(@$settingData->address!==''){{@$settingData->address}}@endif" class="form-control" rows="6" placeholder="Enter Address">@if(@$settingData->address!==''){{@$settingData->address}}@endif</textarea> @if($errors->has('address')) <div class="error">{{ $errors->first('address') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Fax</label>
              <input type="text" name="fax" value="@if(@$settingData->fax!==''){{@$settingData->fax}}@endif" class="form-control" id="exampleInputnumber" placeholder="Fax"> @if($errors->has('fax')) <div class="error">{{ $errors->first('fax') }}</div> @endif
            </div>
          </div>
          <!-- <div class="row"><div class="@if(@$settingData->logo!=null && @$settingData->logo!='') col-lg-5 @else col-lg-6 @endif col-md-12"><div class="form-group"><label for="exampleInputnumber">Logo <span class="text-red">(Max upload file size 2MB)</span></label><input class="form-control" accept="image/*" name="logo" type="file"></div></div>
                                            @if(@$settingData->logo!=null && @$settingData->logo!='')
                                        <div class="col-lg-1 col-md-12"><div class="form-group"><label class="form-label"><br></label><img height="150px;" width="150px;" class="avatar avatar-md br-7"  src="{{asset('storage/images/settings/'.@$settingData->logo)}}"/><a target="_blank" href="{{asset('storage/images/settings/'.@$settingData->logo)}}"><i class="fe fe-eye "></i></a></div></div>
                                        @endif                                                                    
                                        
                                       <div class="@if(@$settingData->favicon!=null && @$settingData->favicon!='') col-lg-5 @else col-lg-6 @endif col-md-12"><div class="form-group"><label for="exampleInputnumber">Favicon Image <span class="text-red">(Max upload file size 2MB)</span></label><input class="form-control" accept="image/*" name="favicon" type="file"></div></div>
                                        @if(@$settingData->favicon!=null && @$settingData->favicon!='')
                                        <div class="col-lg-1 col-md-12"><div class="form-group"><label class="form-label"><br></label><img height="150px;" width="150px;" class="avatar avatar-md br-7"  src="{{asset('storage/images/settings/'.@$settingData->favicon)}}"/><a target="_blank" href="{{asset('storage/images/settings/'.@$settingData->favicon)}}"><i class="fe fe-eye "></i></a></div></div>
                                        @endif
                                    </div> -->
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Business Registration No</label>
                <input type="text" name="business_registration" value="@if(@$settingData->business_registration!==''){{@$settingData->business_registration}}@endif" class="form-control" id="exampleInputnumber10" placeholder="Business Registration No"> @if($errors->has('business_registration')) <div class="error">{{ $errors->first('business_registration') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Meta Author</label>
                <input type="text" name="meta_author" value="@if(@$settingData->meta_author!==''){{@$settingData->meta_author}}@endif" class="form-control" id="exampleInputnumber5" placeholder="Meta Author"> @if($errors->has('meta_author')) <div class="error">{{ $errors->first('meta_author') }}</div> @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Meta kewords</label>
              <input type="text" name="meta_keywords" value="@if(@$settingData->meta_keywords!==''){{@$settingData->meta_keywords}}@endif" class="form-control" id="exampleInputnumber5" placeholder="Meta keywords"> @if($errors->has('meta_keywords')) <div class="error">{{ $errors->first('meta_keywords') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Meta Description</label>
              <input type="text" name="meta_descriptions" value="@if(@$settingData->meta_descriptions!==''){{@$settingData->meta_descriptions}}@endif" class="form-control" id="exampleInputnumber6" placeholder="Meta Descriptions"> @if($errors->has('meta_descriptions')) <div class="error">{{ $errors->first('meta_descriptions') }}</div> @endif
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <label class="custom-control custom-checkbox-md">
                <input type="checkbox" class="custom-control-input" name="website_maintenance" value="1" @if(@$settingData->website_maintenance=='1') checked="" @endif> <span class="custom-control-label">Check Enable Maintenance Mode</span>
              </label>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Update</button>
          <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
  <div class="col-xl-5">
    <form id="create_user" action="{{url('admin/settings')}}" method="post" enctype="multipart/form-data"> @csrf <input type="hidden" name="action_tab" value="social">
      <div class="card">
        <div class="card-header">
          <div class="card-title">Social Setting</div>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label class="form-label text-teal">WhatsApp (https://wa.me/60123456789)</label>
            <div class="wrap-input100 validate-input input-group" id="Password-toggle">
              <button type="button" class="btn btn-icon btn-success">
                <i class="fa fa-whatsapp"></i>
              </button>
              <input class="input100 form-control" type="text" name="whatsapp" placeholder="WhatsApp" value="{{@$settingData->whatsapp}}">
            </div>
          </div>
          <div class="form-group">
            <label class="form-label text-indigo">Facebook (https://www.facebook.com/Facebook)</label>
            <div class="wrap-input100 validate-input input-group" id="Password-toggle1">
              <button type="button" class="btn btn-icon btn-facebook">
                <i class="fa fa-facebook"></i>
              </button>
              <input class="input100 form-control" type="text" name="facebook" placeholder="Facebook" value="{{@$settingData->facebook}}">
            </div>
          </div>
          <div class="form-group">
            <label class="form-label text-pink">Instagram (https://www.instagram.com/Instagram)</label>
            <div class="wrap-input100 validate-input input-group" id="Password-toggle2">
              <button type="button" class="btn btn-icon btn-instagram">
                <i class="fa fa-instagram"></i>
              </button>
              <input class="input100 form-control" type="text" name="instagram" placeholder="Instagram" value="{{@$settingData->instagram}}">
            </div>
          </div>
          <div class="form-group">
            <label class="form-label text-secondary">Twitter (https://twitter.com/Twitter)</label>
            <div class="wrap-input100 validate-input input-group" id="Password-toggle2">
              <button type="button" class="btn btn-icon btn-twitter">
                <i class="fa fa-twitter"></i>
              </button>
              <input class="input100 form-control" type="text" name="twitter" placeholder="Twitter" value="{{@$settingData->twitter}}">
            </div>
          </div>
          <div class="form-group">
            <label class="form-label text-red">YouTube (https://www.youtube.com/watch?v=YouTube)</label>
            <div class="wrap-input100 validate-input input-group" id="Password-toggle2">
              <button type="button" class="btn btn-icon btn-youtube">
                <i class="fa fa-youtube"></i>
              </button>
              <input class="input100 form-control" type="text" name="youtube" placeholder="Youtube" value="{{@$settingData->youtube}}">
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Update</button>
          <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') @endsection