@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Payment Type</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Update Payment Type</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/paymentstype/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Update Payment Type</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Payment Name</label>
                <input type="text" value="{{@$data->payment_name}}" name="payment_name" class="form-control" id="payment_name" placeholder="Payment Name">
              </div> @if($errors->has('payment_name')) <div class="error">{{ $errors->first('payment_name') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Merchant ID</label>
                <input type="text" value="{{@$data->merchant_id}}" name="merchant_id" class="form-control" id="merchant_id" placeholder="Merchant ID">
              </div> @if($errors->has('merchant_id')) <div class="error">{{ $errors->first('merchant_id') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Merchant Code</label>
                <input type="text" name="merchant_code" value="{{@$data->merchant_code}}" class="form-control" id="merchant_code" placeholder="Merchant Code"> @if($errors->has('merchant_code')) <div class="error">{{ $errors->first('merchant_code') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Merchant Key</label>
              <br>
              <input type="text" name="merchant_key" value="{{@$data->merchant_key}}" class="form-control" id="merchant_key" placeholder="Merchant Key"> @if($errors->has('merchant_key')) <div class="error">{{ $errors->first('merchant_key') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Merchant Password</label>
                <input type="text" name="merchant_password" value="{{@$data->merchant_password}}" class="form-control" id="merchant_password" placeholder="Merchant Password"> @if($errors->has('merchant_password')) <div class="error">{{ $errors->first('merchant_password') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">API Key</label>
              <br>
              <input type="text" name="api_key" value="{{@$data->api_key}}" class="form-control" id="api_key" placeholder="API Key"> @if($errors->has('api_key')) <div class="error">{{ $errors->first('api_key') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Collection ID</label>
                <input type="text" name="collection_id" value="{{@$data->collection_id}}" class="form-control" id="collection_id" placeholder="Collection ID"> @if($errors->has('collection_id')) <div class="error">{{ $errors->first('collection_id') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Redirect URL</label>
              <br>
              <input type="text" name="redirect_url" value="{{@$data->redirect_url}}" class="form-control" id="redirect_url" placeholder="Redirect URL"> @if($errors->has('redirect_url')) <div class="error">{{ $errors->first('redirect_url') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">ID Type</label>
              <br>
              <select name="id_type" class="form-control select2 form-select">
                <option value="">Choose one</option>
                <option value="1" @if(@$data->id_type=="1") selected @endif>Mobile No</option>
                <option value="2" @if(@$data->id_type=="2") selected @endif>NRIC No</option>
                <option value="3" @if(@$data->id_type=="3") selected @endif>Passport No</option>
                <option value="4" @if(@$data->id_type=="4") selected @endif>Army / Police ID</option>
                <option value="5" @if(@$data->id_type=="5") selected @endif>Business Reg No</option>
              </select> @if($errors->has('id_type')) <div class="error">{{ $errors->first('id_type') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">ID Number</label>
                <input type="text" name="id_number" value="{{@$data->id_number}}" class="form-control" id="id_number" placeholder="ID number"> @if($errors->has('id_number')) <div class="error">{{ $errors->first('id_number') }}</div> @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Success URL</label>
                <input type="text" name="success_url" value="{{@$data->success_url}}" class="form-control" id="success_url" placeholder="Success URL"> @if($errors->has('success_url')) <div class="error">{{ $errors->first('success_url') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Failure URL</label>
              <br>
              <input type="text" name="failure_url" value="{{@$data->failure_url}}" class="form-control" id="failure_url" placeholder="Failure URL"> @if($errors->has('failure_url')) <div class="error">{{ $errors->first('failure_url') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Image</label>
                <input class="form-control" accept="image/*" name="image" type="file">
              </div>
            </div> @if(@$data->image!=null && @$data->image!='') <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img height="150px;" width="150px;" class="avatar avatar-md br-7" src="{{asset('storage/images/payment_type/'.@$data->image)}}" />
                <a target="_blank" href="{{asset('storage/images/payment_type/'.@$data->image)}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @else <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img height="150px;" width="150px;" class="avatar avatar-md br-7" src="{{asset('storage/images/no_image.png')}}" />
                <a target="_blank" href="{{asset('storage/images/no_image.png')}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @endif <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" @if(@$data->status=="0") selected @endif>Active</option>
                  <option value="1" @if(@$data->status=="1") selected @endif>Suspend</option>
                </select>
              </div>
            </div>
          </div>
          <div class="card-footer">
        <a href="{{url('admin/paymentstype')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
        </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      payment_name: {
        required: true
      }
    },
    messages: {
      payment_name: {
        required: "Please enter payment name",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection