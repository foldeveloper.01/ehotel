@extends('admin.layouts.app') @section('styles')
@endsection @section('content')
<!-- PAGE-HEADER -->

<div class="page-header">
  <h1 class="page-title">Guest Stay Report</h1>
  <div>
    <ol class="breadcrumb">      
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)"></a>
      </li>
      <li class="breadcrumb-item" aria-current="page"></li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Guest Stay Report</h3>
      </div>
      <div class="card-body">        
     <!--     <a href="{{url('admin/guest_stay_report/export')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Export</button>
        </a> -->
        <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" fdprocessedid="99smch">Filter By Year</button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(2px, 39px, 0px);">
                        <a class="dropdown-item" href="https://www.backpacker.com.my/ehotel/admin/sales_report_monthly/view/2013">2013 </a>               
                    </div>
          </div>
       <!--          <div class="row row-sm">
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input name="min" id="min" class="form-control  mb-4 " placeholder="Min Date" type="text">
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input name="max" id="max" class="form-control mb-4" placeholder="Max Date" type="text">                                                      
                      </div>
                  </div>
                  <div class="col-lg-1">
                      <div class="form-group">
                          <input type="button" class="btn btn-danger" id="reset_filter" value="Reset">                                                      
                      </div>
                  </div>
              </div> -->
       <canvas id="myChart" style="width:100%;max-width:600px"></canvas>
<section id="no-more-tables" class="table-responsive mt-1 no-more-tables">
                <table class="table table-striped">
                  <thead class="bg-primary white">
                    <tr>
                      <th class="numeric">Month</th>
                      <th class="numeric"> Sales</th>
                    
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($list as $k =>$v)
                  <tr>
                    <td>{{$v->months}}</td>
                    <td>{{$v->sums}}</td>
                  </tr>
                  @endforeach
                  <tr>
                   <td>Total</td>
                    <td>{{$total_Amount}}   
                    </td>
                  </tr>
                    
                    
                  </tbody>
                </table>
              </section>
       
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<script type="text/javascript">
var  yValues= <?php print_r(json_encode($amount));?>;
var xValues =<?php print_r(json_encode($months));?>;
console.log(xValues);


new Chart("myChart", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    title: {
      display: true,
      text: "Monthly Sale Report of "+ <?php echo $years;?>
    }
  }
});  

</script> @endsection