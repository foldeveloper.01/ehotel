@extends('admin.layouts.app') @section('styles')
@endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Guest Stay Report</h1>
  <div>
    <ol class="breadcrumb">      
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)"></a>
      </li>
      <li class="breadcrumb-item" aria-current="page"></li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Guest Stay Report</h3>
      </div>
      <div class="card-body">        
         <a href="{{url('admin/guest_stay_report/export')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Export</button>
        </a>
                <div class="row row-sm">
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input name="min" id="min" class="form-control  mb-4 " placeholder="Min Date" type="text">
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input name="max" id="max" class="form-control mb-4" placeholder="Max Date" type="text">                                                      
                      </div>
                  </div>
                  <div class="col-lg-1">
                      <div class="form-group">
                          <input type="button" class="btn btn-danger" id="reset_filter" value="Reset">                                                      
                      </div>
                  </div>
              </div>
        </a>
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="guestuser_report" >
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Room Number</th>
                <th class="wd-15p border-bottom-0">Guest Name</th>
                <th class="wd-15p border-bottom-0">Check-in</th>
                <th class="wd-15p border-bottom-0">Check-out</th>
              </tr>
            </thead>           
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<script type="text/javascript">

  var minDate, maxDate;
 
// Custom filtering function which will search data in column four between two values
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = minDate.val();
        var max = maxDate.val();
        var date = new Date( data[4] );
 
        if (
            ( min === null && max === null ) ||
            ( min === null && date <= max ) ||
            ( min <= date   && max === null ) ||
            ( min <= date   && date <= max )
        ) {
            return true;
        }
        return false;
    }
);


  $(document).ready(function() {

    /*minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });*/

    //click view new windows for print
      $(document).on('click','.click_print',function(e){
         e.preventDefault();
         window.open($(this).attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

    // DataTable
     var tables = $('#guestuser_report').DataTable({
         processing: true,
         serverSide: true,
         "pageLength": 10,
         "paging": true,
         "cmd" : "refresh",
         //"from": $("#min").val(),
         //"to"  : $("#max").val(),            
         //ajax: "{{url('admin/guest_stay_report/ajax_guest_stay_report')}}",
         "ajax": {
        'type': 'POST',
        'url': "{{url('admin/guest_stay_report/ajax_guest_stay_report')}}",
         'data':function(dtParms){
            dtParms.minDate = $('#min').val();
            dtParms.maxDate = $('#max').val();
            dtParms._token = "{{ csrf_token() }}";
            return dtParms
         },
       },
         columns: [
            { data: 'id' },
            { data: 'rooms' },
            { data: 'guest' },
            { data: 'checkin_date_time' },
            { data: 'checkout_date_time' },
         ],'columnDefs': [ {
            'targets': [0],
            'orderable': false,
         }],       
         "order": [[3,"desc"]],
      });

     tables.on('draw.dt', function () {
          var info = tables.page.info();
          tables.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
              cell.innerHTML = i + 1 + info.start;
          });
      });
     
       $('input[type=search]').attr("id","search_clear");
       $('#reset_filter').on('click', function () {
           $('#min').val('');
           $('#max').val('');          
           tables.draw();
       });

       $("#min").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,           
        }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          var newDate = addDays(minDate,1);
          $('#max').datepicker('setStartDate', newDate);
          $('#max').val(moment(newDate).format("DD-MM-YYYY"));
            if($("#max").val()!='') {
              tables.draw();
            }                    
        });

       $("#max").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,           
        }).on('changeDate', function (selected) { 
            if($("#min").val()!='') {
              tables.draw();
            } 
                       
        });

        function addDays(date, days) {
           const dateCopy = new Date(date);
           dateCopy.setDate(date.getDate() + days);
           return dateCopy;
        } 


  });

   

</script> @endsection