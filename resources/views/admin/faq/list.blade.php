@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">FAQ</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">FAQ List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">FAQ List</h3>
      </div>
      <div class="card-body">
        <a href="{{url('admin/faq/create')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Create FAQ</button>
        </a>
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Question</th>
                <th class="wd-15p border-bottom-0">Answer</th>
                <th class="wd-15p border-bottom-0">Status</th>
                <th class="wd-15p border-bottom-0">Created_at</th>
                <th class="wd-15p border-bottom-0">Action</th>
              </tr>
            </thead>
            <tbody> @if(@$list) @foreach(@$list as $key=>$data) <tr>
                <td>{{@$key+1}}</td>
                <td>{{@$data->question}}</td>
                <td>{!!@$data->answer!!}</td>
                <td>
                  <div class="col-xl-2 ps-1 pe-1">
                    <div class="form-group">
                      <label class="custom-switch form-switch mb-0">
                        <input type="checkbox" id="status_{{@$data->id}}" name="status" class="custom-switch-input" @if(@$data->status=='0') checked @endif> <span class="custom-switch-indicator custom-switch-indicator-md" data-on="Yes" data-off="No"></span>
                      </label>
                    </div>
                  </div>
                </td>
                <td>{{ \Carbon\Carbon::parse(@$data->created_at)->format('d/m/Y h:i:s')}}</td>
                <td class="text-center align-middle">
                  <a href="{{url('admin/faq/update/'.$data->id)}}" class="btn btn-sm btn-primary">
                    <i class="fe fe-edit"></i>
                  </a>
                  <a onclick="deletefunction(event,{{$data->id}});" href="" class="btn btn-sm btn-danger">
                    <i class="fe fe-x"></i>
                  </a>
                  <form id="delete_form{{$data->id}}" method="POST" action="{{ url('admin/faq/delete', $data->id) }}"> @csrf <input name="_method" type="hidden" value="POST">
                  </form>
                </td>
              </tr> @endforeach @endif </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.custom-switch-input').on('change', function() {
      if($(this).prop('checked')==true){
        var status_message = 'Are you sure, Do you want to Activate the data?';
      }else{
        var status_message = 'Are you sure, Do you want to Suspend the data?';
      }
      var option = confirm(status_message);    
      if (!option) {
        if ($(this).prop('checked')) {
            $(this).prop('checked', !$(this).prop('checked'));
        } else {
            $(this).prop('checked', !$(this).prop('checked'));
        }
        return false;
      }
      var isChecked = $(this).is(':checked');
      var id = $(this).attr('id');
      var selectedData;
      var $switchLabel = $('.custom-switch-indicator');
      //console.log('isChecked: ' + isChecked);
      if (isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      //console.log('Selected data: ' + selectedData);
      //alert(site_url+'/admin/bookingwindows/ajax_status_update');
      $.ajax({
        url: site_url + '/admin/faq/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: selectedData,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();
            if (selectedData == 'Yes') {
              var success = 'Activate';
              toastr.success(success + ' successfully');
            } else {
              var success = 'Suspend';
              toastr.error(success + ' successfully');
            }
          } else {
            toastr.clear();
            toastr.error('something went wrong');
          }
        }
      });
    });
  });

  function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }
</script> @endsection