@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
/*@media print {
    #si-printer {
        display :  none !important;
    }#si-printer1 {
        display :  none !important;
    }
}
.print:last-child {
     page-break-after: auto;
}*/
</style>


    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Invoice</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                    <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img logo-3" alt="logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{@$setting->address}}
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <h3>#Invoice-{{@$invoice->invoice_number}}</h3>
                                                <h5><b>Booking No:</b> {{@$invoice->receipt_no}}</h5>
                                                <h5><b>Check-in:</b> {{\Carbon\carbon::parse(@$booking->check_in)->format('d-m-Y')}}</h5>
                                                <h5><b>Check-out:</b> {{\Carbon\carbon::parse(@$booking->check_out)->format('d-m-Y')}}</h5>
                                                <h5><b>Date:</b>{{\Carbon\carbon::parse(@$invoice->invoice_date)->format('d-m-Y')}}</h5>
                                                <h5><b>Issued By:</b>{{ucwords(@$invoice->admin->firstname)}}</h5>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="h3">Invoice To:</p>
                                                <p class="fs-15 fw-semibold mb-0">{{@$invoice->guest->name}}</p>
                                                <address>
                                                        {{@$invoice->guest->address}}<br>
                                                        @if(@$invoice->guest->city)
                                                         {{@$invoice->guest->city}},
                                                        @endif
                                                        @if(@$invoice->guest->state)
                                                         {{@$invoice->guest->state}},
                                                        @endif
                                                         <br>
                                                        @if(@$invoice->guest->country)
                                                         {{@$invoice->guest->country}},
                                                        @endif
                                                        @if(@$invoice->guest->postcode)
                                                         {{@$invoice->guest->postcode}}
                                                        @endif
                                                        <br>
                                                        {{@$invoice->guest->email}}
                                                    </address>
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> -->
                                        </div>
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th>Date</th>
                                                        <th>Description</th>
                                                        <th class="text-end">Amount(RM)</th>
                                                    </tr>                                                                                                         
                                                        <?php $booking_details_id=[];$booking_addon_id=[]; ?> 
                                                        @foreach($booking->booking_details as $key=>$data)
                                                          @php $booking_details_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                            <td >{{@$data->rooms->room_type}} - {{@$data->rooms_number->room_no}}</td>
                                                            <td class="text-end">{{@$data->room_price}}</td>
                                                        </tr>
                                                        @endforeach
                                                        @foreach($booking->booking_exsit_addon_details as $key=>$data)
                                                        @php $booking_addon_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                            <td >{{@$data->getaddon->add_on_name}} - {{@$data->quantity}} Qty</td>
                                                            <td class="text-end">{{@$data->total_amount}}</td>
                                                        </tr>
                                                        @endforeach
                                                        
                                                        @foreach($booking->booking_payment_details as $paymentkey=>$payment)
                                                        @if(@$payment->refund_payment_check==1 && @$payment->invoice_type !='refund')
                                                          <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Refund for booking Early Checkout </td>
                                                            <td class="text-end">{{@$payment->total_amount}}</td>
                                                          </tr>
                                                          <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Refund Paid Early Checkout </td>
                                                            <td class="text-end">-{{@$payment->total_amount}}</td>
                                                          </tr>
                                                        @endif
                                                        @endforeach
                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Gross</td>
                                                            <td class="fw-bold text-end h4">{{number_format(@$total_amount-@$booking->service_tax_amount,2)}}</td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Total Excl.Tax</td>
                                                            <td class="fw-bold text-end h4">{{number_format(@$total_amount-@$booking->service_tax_amount,2)}}</td>
                                                          </tr> 
                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Service Charge
                                                            @if(@$booking->service_tax_details!=null)
                                                              @if(explode('_',@$booking->service_tax_details)[0]!='amount')
                                                                @ {{explode('_',@$booking->service_tax_details)[1]}} %
                                                              @else
                                                                @ {{explode('_',@$booking->service_tax_details)[1]}} MYR
                                                              @endif
                                                             @endif
                                                            </td>
                                                            <td class="text-end">MYR {{@$booking->service_tax_amount}}</td>
                                                          </tr>

                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Discount</td>
                                                            <td class="text-end">-{{number_format(@$booking->discount_amount,2)}}</td>
                                                          </tr>

                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Total Payable Incl. Tax</td>
                                                            <td class="fw-bold text-end h4">{{number_format(@$total_amount,2)}}</td>
                                                          </tr>                                                    
                                                    
                                                    @foreach($booking->booking_exsit_addon_payment_details($booking->id) as $key=>$data)
                                                      @if($data->refund_payment_check==0)
                                                        <tr>                                                            
                                                           <td colspan="2" class="text-end">{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}} - Payment By Cash {{ucwords(@$data->payment_type)}}</td>
                                                           <td class="text-end">-{{@$data->total_amount}}</td>
                                                        </tr>
                                                      @endif
                                                    @endforeach
                                                        <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Total Payment Outstanding</td>
                                                            <td class="fw-bold text-end h4">{{@$balance_amount}}</td>
                                                        </tr>
                                                    @foreach($booking->booking_payment_details as $paymentkey=>$payment)
                                                        @if(@$payment->refund_payment_check==1 && @$payment->invoice_type =='refund')
                                                          <tr> 
                                                            <td colspan="2" class="text-uppercase text-end" >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}} -
                                                             Refund for booking </td>
                                                            <td class="fw-bold text-end">{{@$payment->total_amount}}</td>
                                                          </tr>                                                          
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <address class="pt-3">COVID-19 Hotel Procedure<br>

All guests will be requested to have their body temperature checked, hands disinfection and fill in the Health Declaration Form upon arrival.
Semua pelawat ke Hotel akan direkod suhu badan, tangan perlu disanitasi, juga perlu mengisi borang deklarasi kesihatan.<br>

Hotel Rooms is limited to 2 persons per room from the same address.
Bilik Penginapan kami dihadkan kepada 2 orang sebilik sahaja dari alamat yang sama.<br>

At our Hotel, do practise social distancing and keep at least two metres away from other people.
Penjarakkan sosial 2 meter diamalkan di Hotel kami.<br>

Frequent cleaning and sanitation routine will be done at our Hotel. Hand sanitizers are provided for staff and guest.
Pembersihan dan sanitasi yang kerap di Hotel kami serta kemudahan ‘hand sanitizer’ disediakan.<br>

In-room dining service are provided.
Servis makan di dalam bilik disediakan.
                                        </address>
                                    </div>
                                    <div class="card-footer text-end">                                        
                                        <button id="si-printer" type="button" class="btn btn-danger mb-1" onclick="javascript:window.print();"><i class="si si-printer"></i> Print Invoice</button>
                                    </div>
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')



    @endsection
