@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
/*@media print {
    #si-printer {
        display :  none !important;
    }#si-printer1 {
        display :  none !important;
    }
}
.print:last-child {
     page-break-after: auto;
}*/
</style>


    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Invoice</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{url('admin/paidinvoice/invoice_booking_save')}}" method="post" enctype="multipart" autocomplete="off">
                                    @csrf
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                    <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img logo-3" alt="logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{@$setting->address}}
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <!-- <h3>#Invoice-{{@$invoice->invoice_number}}</h3> -->
                                                <h5><b>Booking No:</b> {{@$booking->booking_number}}</h5>                                                
                                                <h5> Date: <input class="form-control text-end common_date" type="text" name="booking_date" id="booking_date" style="width: 149px;float: right;" value="{{Carbon\Carbon::parse(@$booking->created_at)->format('d-m-Y')}}">
                                                </h5>
                                            </div>                                            
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="h3">Booking Details:</p>
                                                <p class="fs-15 fw-semibold mb-0">{{@$invoice->guest->name}}</p>
                                                <address>
                                                        {{@$invoice->guest->address}}<br>
                                                        @if(@$invoice->guest->city)
                                                         {{@$invoice->guest->city}},
                                                        @endif
                                                        @if(@$invoice->guest->state)
                                                         {{@$invoice->guest->state}},
                                                        @endif
                                                        @if(@$invoice->guest->country)
                                                         {{@$invoice->guest->country}},
                                                        @endif
                                                        @if(@$invoice->guest->postcode)
                                                         {{@$invoice->guest->postcode}}
                                                        @endif
                                                        <br>                                                        
                                                    </address>
                                                    <p class="fs-15 fw-semibold mb-0">Phone:{{@$invoice->guest->contact_number}}</p>
                                                    <p class="fs-15 fw-semibold mb-0">IC No:{{@$invoice->guest->ic_passport_no}}</p>
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> -->
                                        </div>
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th>#</th>
                                                        <th>Description</th>
                                                        <th>Check-in</th>
                                                        <th>Check-out</th>
                                                        <th>U/Price(RM)</th>
                                                        <th>Subtotal(RM)</th>                                                        
                                                    </tr>                                                                                                         
                                                        <?php $booking_details_id=[];$booking_addon_id=[]; ?> 
                                                        @foreach($booking->get_booking_details() as $key=>$data)
                                                          @php $booking_details_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td>{{$key+1}}</td>
                                                            <td>
                                                                <select class="form-control" name="roomlist[{{$data->id}}]">
                                                                  @foreach(@$data->rooms->rooms_number as $key=>$val)
                                                                    <option value="{{@$val->id}}" @if(@$data->room_number_id==@$val->id) selected @endif>{{@$val->room_no}}</option>
                                                                  @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input class="form-control common_date" type="text" name="checkin_date[{{$data->id}}]" value="{{@$data->checkin_date()}}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control common_date" type="text" name="checkout_date[{{$data->id}}]" value="{{@$data->checkout_date()}}">
                                                            </td>
                                                            <td>
                                                                <input class="form-control" type="text" name="room_price[{{$data->id}}]" value="{{@$data->room_price}}">
                                                            </td>
                                                            <td class="text-end">
                                                                <input class="form-control" type="text" name="subtotal_amount[{{$data->id}}]" value="{{@$data->subtotal_amount}}">
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="5" class="fw-bold text-uppercase text-end">Service Charge
                                                            @if(@$booking->service_tax_details!=null)
                                                              @if(explode('_',@$booking->service_tax_details)[0]!='amount')
                                                                @ {{explode('_',@$booking->service_tax_details)[1]}} %
                                                              @else
                                                                @ {{explode('_',@$booking->service_tax_details)[1]}} MYR
                                                              @endif
                                                             @endif
                                                            </td>
                                                            <td class="text-end">
                                                                <input class="form-control" type="text" name="tax_amount" value="{{@$booking->service_tax_amount}}">
                                                            </td>
                                                        </tr> 
                                                   
                                                        
                                                </tbody>
                                            </table>
                                        </div><br>  
                                        Payment Methods:
                                        <select id="payment_method" name="payment_method" class="form-control col-lg-2"> 
                                            <option value="1">Cash</option>
                                            <option value="2">Credit Bill</option>
                                            <option value="3">Bank-In</option>
                                            <option value="4">Credit Card</option>
                                            <option value="5">Debit card</option>
                                            <option value="6">Cheque</option>
                                            <option value="7">Skip for no payment</option>
                                            <option value="8">Pay Later</option>
                                            <option value="9">Free</option>
                                        </select>
                                    </div>
                                    <div class="card-footer text-end">                                       
                                        <a href="{{url('admin/paidinvoice')}}" class="btn btn-danger my-1">Back</a>
                                        <button class="btn btn-success my-1" value="submit">Update</button>      
                                    </div>
                                </div>
                             </form>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')
    <script type="text/javascript">
        $(function () { 
            $(".common_date").datepicker({
                 format: 'dd-mm-yyyy',
                 autoclose: true,
            });
        });
    </script>



    @endsection
