@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Third Party</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Third Party</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <div class="col-xl-12">
    <form id="create" action="{{url('admin/thirdpartytype/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Third Party</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Name</label>
                <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name" placeholder="Name">
              </div> @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
            </div>            
          </div>          

          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label class="form-label">PayTo</label>
                <div class="custom-radio custom-control-inline">
                  <label class="custom-control custom-radio-md">
                    <input type="radio" class="custom-control-input" name="payto" value="0"> <span class="custom-control-label">Hotel</span>
                  </label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <label class="custom-control custom-radio-md">
                    <input type="radio" class="custom-control-input" name="payto" value="1"> <span class="custom-control-label">Thirdparty</span>
                  </label>
                </div>
                <div id="pay_to_error"></div>
              </div>
              
            </div>            
          </div>

          <div class="row">            
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Logo</label>
                <input class="form-control" accept="image/*" name="logo" type="file">
              </div> @if($errors->has('logo')) <div class="error">{{ $errors->first('logo') }}</div> @endif
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6 col-md-12">        
              <div class="form-group">
                <label for="position">Sort</label>
                <input type="number" value="0" name="position" class="form-control" id="position" placeholder="Sort">
              </div> @if($errors->has('position')) <div class="error">{{ $errors->first('position') }}</div> @endif            
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label class="form-label">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Save</button>
          <a href="{{url('admin/thirdpartytype')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
 
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      name: {
        required: true
      },
      payto: {
        required: true
      },
      logo: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name",
      },
      payto: {
        required: "Please enter payto",
      },logo: {
        required: "Please upload logo",
      }
    },errorPlacement: function(error, element) {
      if (element.attr("name") == "payto") {
          error.appendTo("#pay_to_error").css('color','#FF0000').css("fontSize", "14px").css('float','center');
      }else {
          error.insertAfter(element);
      }                
     },
   
  });
</script> @endsection