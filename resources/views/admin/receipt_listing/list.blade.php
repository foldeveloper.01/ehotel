@extends('admin.layouts.app') @section('styles') @endsection <style>.flex-1{display: none;}</style> @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Receipt List</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Receipt List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <!-- <div class="card-header">
        <h3 class="card-title">Receipt List</h3>
      </div> -->
      <div class="card-body">
                  <a href="{{url('admin/bookinglisting/newbooking/?type=normal')}}">
                  <button id="table2-new-row-button" class="btn btn-primary mb-4">New Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=midnight')}}">
                  <button id="" class="btn btn-primary mb-4">Midnight Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=monthly')}}">
                  <button id="" class="btn btn-primary mb-4">Monthly Booking</button>
                  </a>
        <form id="search_form" action="{{url(Request::segment(1).'/'.Request::segment(2))}}" method="get" enctype="multipart">
                 
            <div class="row row-sm p-2">              
                  <div class="col-lg-2">                        
                        <select name="issued_by" id="issued_by" class="form-control select2 form-select">
                           <option value="">Issued By</option>
                           @if(!$admin_list->isEmpty())
                             @foreach($admin_list as $key=>$val)
                              <option value="{{$key}}" @if(Request::get('issued_by')==$key) selected @endif>{{ucwords($val)}}</option>
                             @endforeach
                           @endif                           
                        </select>
                  </div>
                  <div class="col-lg-2">                        
                        <select name="payment_type" id="payment_type" class="form-control select2 form-select">
                           <option value="">Payment Type</option>                          
                           <option value="1" @if(Request::get('payment_type')=='1') selected @endif>Cash</option>
                           <option value="2" @if(Request::get('payment_type')=='2') selected @endif>Credit bill</option>
                           <option value="3" @if(Request::get('payment_type')=='3') selected @endif>Bank in</option>
                           <option value="4" @if(Request::get('payment_type')=='4') selected @endif>Credit card</option>
                           <option value="5" @if(Request::get('payment_type')=='5') selected @endif>Debit card</option>
                           <option value="6" @if(Request::get('payment_type')=='6') selected @endif>Cheque</option>
                           <option value="8" @if(Request::get('payment_type')=='8') selected @endif>Pay later</option>
                        </select>
                  </div>
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input type="text" name="search" id="search" class="form-control" value="{{Request::get('search')}}" placeholder="Searching.....">
                      </div>
                  </div>
                  <div class="col-lg-1">
                      <div class="form-group">                      
                         <input type="text" value="{{\Request::get('min_date')}}" name="min_date" class="form-control" id="min_date" placeholder="Min Date" autocomplete="off">
                     </div>
                  </div>
                   <div class="col-lg-1">
                      <div class="form-group">                      
                         <input type="text" value="{{\Request::get('max_date')}}" name="max_date" class="form-control" id="max_date" placeholder="Max Date" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="btn-list">
                          <input type="button" class="btn btn-primary" id="data_filter" value="Search"> 
                          <input type="button" class="btn btn-danger" id="reset" value="Reset">                                                    
                      </div>                      
                  </div>                  
              </div>
               @csrf
           </form>
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable22">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Date</th>
                <th class="wd-15p border-bottom-0">Receipt No</th>
                <th class="wd-15p border-bottom-0">Booking No</th>
                <th class="wd-15p border-bottom-0">Guest</th>
                <th class="wd-15p border-bottom-0">Amount</th>
                <th class="wd-15p border-bottom-0">Payment</th>
                <th class="wd-15p border-bottom-0">Issued By</th>
                <th class="wd-15p border-bottom-0">Print</th>
              </tr>
            </thead>
            <tbody>              
              @if(@$list)
               @foreach(@$list as $key=>$data)
                <tr>
                  <td>{{@$key+1}}</td>
                  <td>{{Carbon\Carbon::parse(@$data->payment_created_at)->format('d-m-Y')}}</td>
                  <td>
                     <a class="a_click custom_a" href="{{url('admin/bookinglisting/newbooking/receipt_view/'.@$data->receipt_table_id)}}">
                        #{{@$data->receipt_number}}                    
                     </a>
                  </td>
                  <td>
                    <a class="a_click custom_a" href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->book_table_id)}}"> {{@$data->booking_number}} </a>
                  </td>
                  <td>{{@$data->customer_name}}</td>
                  <td>@if(@$data->refund_payment_check==1 && @$data->invoice_type !='refund') <span class="text-red">-{{@$data->receipt_amount}}</span> @else <span class="text-green">{{@$data->receipt_amount}}</span> @endif</td>
                  <td>{{@$data->get_payment_type($data->payment)}}</td>
                  <td>{{@$data->issued_by}}</td>
                  <td><button class="btn btn-yellow white btn-sm click_view">Print</button> <a href="{{url('admin/bookinglisting/newbooking/receipt_view/'.@$data->receipt_table_id)}}">
                  </td>
                </tr> 
               @endforeach
              @endif
            </tbody>
          </table>
          @if($list->isEmpty())
                <div class="p-4 bg-light border border-bottom-0"><h1>No Record</h1></div>
              @endif
        </div>
        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{ $list->onEachSide(0)->links() }}
                            </ul>
                        </nav>
      </div>

    </div>
    
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    //click view new windows for print
      $(document).on('click','.click_view',function(e){
         e.preventDefault();
         //console.log($(this).parent().find('a').attr('href'));
         window.open($(this).parent().find('a').attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      $(document).on('click','.a_click',function(e){
         e.preventDefault();
         //console.log($(this).parent().find('a').attr('href'));
         window.open($(this).attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");
         return false;         
      });
      //click view new windows for print

      //filter
      $(document).on('change','#payment_type',function(e) { 
          $('#search_form').submit();
      });
      $(document).on('change','#issued_by',function(e) {
          $('#search_form').submit();
      });
      $(document).on('click','#data_filter',function(e) { 
         if( $('#search').val() =='' && $('#min_date').val() =='' && $('#max_date').val() =='' && $('#payment_type').val() =='' && $('#issued_by').val() =='') {
            toastr.clear();
            toastr.warning('Input data is required');
            return false;
         }
         $('#search_form').submit();
      });

      $(document).on('click','#reset',function() {
         //redirect new page          
         //if( $('#search').val() !='' ||  $('#booking_status').val() !='' || $('#payment').val() !='') {
          var path = "/{{Request::segment(1)}}/";
          var path1 = "{{Request::segment(2)}}";
          var redirect = site_url + path + path1;
          window.location.href=redirect;
          return false;
        //}
         //redirect new page
      });

        $(function () {            
            $("#min_date").datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            //todayBtn:  1,
            autoclose: true,
            orientation: "bottom left"
            //startDate: 'today'
        }).on('changeDate', function (selected) {           
            var minDate = new Date(selected.date.valueOf());            
            var newDate = addDays(minDate,1);
            $('#max_date').datepicker('setStartDate', newDate);
            $('#max_date').val(moment(newDate).format("DD-MM-YYYY"));
            $('#max_date').focus();            
        });
        $("#max_date").datepicker({
            format: 'dd-mm-yyyy',
            //todayBtn:  1,
            autoclose: true,
            orientation: "bottom left",
            //todayHighlight: true,
            //startDate: checkout_date,
        }).on('changeDate', function (selected) {
            //
        });
        function addDays(date, days) {
           const dateCopy = new Date(date);
           dateCopy.setDate(date.getDate() + days);
           return dateCopy;
        }       
    });
      //filter

  }); 
</script> @endsection