@extends('admin.layouts.app')
@section('styles')
<style type="text/css">.flex-1{display: none;}</style>
@endsection
@section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
   <h1 class="page-title">Booking History Listing</h1>
   <div>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Booking History Listing</li>
      </ol>
   </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row row-cards">
   <!-- COL-END -->
   <div class="col-xl-12 col-lg-12">
      <div class="row">
         <div class="col-xl-12">
            <div class="card">
               <div class="card-body p-2">
                   <a href="{{url('admin/bookinglisting/newbooking/?type=normal')}}">
                  <button id="table2-new-row-button" class="btn btn-primary mb-4">New Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=midnight')}}">
                  <button id="" class="btn btn-primary mb-4">Midnight Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=monthly')}}">
                  <button id="" class="btn btn-primary mb-4">Monthly Booking</button>
                  </a>
               </div>
               <form id="search_form" action="{{url(Request::segment(1).'/'.Request::segment(2))}}" method="get" enctype="multipart">
                 
            <div class="row row-sm p-2">              
                  <!-- <div class="col-lg-2">                        
                        <select name="payment" id="payment" class="form-control select2 form-select">
                           <option value="">Payment</option>
                           <option value="1" @if(Request::get('payment')=='1') selected @endif>Paid</option>
                           <option value="0" @if(Request::get('payment')=='0') selected @endif>Not Paid</option>
                           <option value="2" @if(Request::get('payment')=='2') selected @endif>Pay Later</option>
                        </select>
                  </div>
                  <div class="col-lg-2">                        
                        <select name="booking_status" id="booking_status" class="form-control select2 form-select">
                           <option value="">Booking Status</option>
                           <option value="1" @if(Request::get('booking_status')=='1') selected @endif>Check-In</option>
                           <option value="2" @if(Request::get('booking_status')=='2') selected @endif>Today Check-in</option>
                           <option value="3" @if(Request::get('booking_status')=='3') selected @endif>Check-out</option>
                           <option value="4" @if(Request::get('booking_status')=='4') selected @endif>Today Check-out</option>
                        </select>
                  </div> -->
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input type="text" name="search" id="search" class="form-control" value="{{Request::get('search')}}" placeholder="Searching.....">
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="btn-list">
                          <input type="button" class="btn btn-primary" id="data_filter" value="Search"> 
                          <input type="button" class="btn btn-danger" id="reset" value="Reset">                                                    
                      </div>                      
                  </div>                  
              </div>
               @csrf
           </form>
            </div>
         </div>
      </div>
      <!-- COL-END -->
   </div>
   <!-- ROW-1 CLOSED -->
   <div class="row">
      <div class="col-12">
        <!-- start loop -->
        @if($list->isEmpty())
           <div class="p-4 bg-light border border-bottom-0"><h1>No Record</h1></div>
        @endif
        @foreach($list as $key=>$data)
        <?php if($data->payment_status==3){ $get_payment_status ='payment_refund';}elseif($data->payment_status==0){$get_payment_status ='payment_failed';}else{$get_payment_status ='payment_success';} ?>
         <div class="card">
            <div class="card-body {{$get_payment_status}}">                
               <div class="top-footer">                  
                  <div class="row">
                     <div class="col-lg-4 col-md-12">
                        <h3 class="text-primary">Booking NO: {{@$data->booking_number}}</h3>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fe fe-info fs-20"></i></span>
                           </div>
                           <div>
                             <?php $bookingstatus = $data->get_booking_status();
                             $status_class_name = 'btn-success';
                             if($bookingstatus=='Pending') {
                               $status_class_name = 'btn-warning';
                             }elseif($bookingstatus=='Refunded'){
                               $status_class_name = 'btn-danger';
                             }
                              ?>
                              <strong class="btn {{$status_class_name}} disabled btn-sm">{{$bookingstatus}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fe fe-user fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{ucwords(@$data->guest->name)}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fa fa-address-card-o fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{@$data->guest->ic_passport_no}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fe fe-phone fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{@$data->guest->contact_number}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="fa fa-calendar fs-20"></i></span>
                           </div>
                           <div>
                              <strong>{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</strong>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mb-3 mt-3">
                           <div class="me-4 text-center text-primarys">
                              <span><i class="@if(@$data->booking_type!='walk-in') fa fa-globe fs-20 @else mdi mdi-walk fs-20 @endif"></i></span>
                           </div>
                           <div>
                              <strong>{{ucwords(@$data->booking_type)}}</strong>
                           </div>
                        </div>
                        <div class="btn-list">                           
                           <a id="{{@$data->id}}" class='btn btn-success btn-sm bg-success-gradient click_status text-white' data-bs-toggle='modal' data-bs-target='#scrollingmodal' data-toggle="tooltip" title="Add Remark" data-bs-toggle='modal'><i class="fa fa-sticky-note-o"></i></a>
                           <a id="{{@$data->id}}" class='btn btn-warning btn-sm bg-warning-gradient add_guest_data text-white' data-bs-placement="top" data-bs-toggle="tooltip" title="Add Guest Details"><i class="fa fa-user"></i></a>
                           <a href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->id)}}" class="btn btn-success btn-sm bg-success-gradient text-white click_print text-white" data-bs-placement="top" data-bs-toggle="tooltip" title="Booking Details"><i class="fa fa-calendar"></i></a>
                           <input type="hidden" value="{{@$data->booking_number}}" id="bookingnumber_{{@$data->id}}">                           
                          
                        </div>
                     </div>
                     <br>
                     <div class="col-lg-4 col-md-12">
                        <h6>Room Information: </h6>
                        <ul class="list-unstyled mb-4">
                           <li><h1 class="display-7 text-secondary fw-bold my-3">@php $last_key = count($data->get_booking_details()); $i = 0; @endphp @foreach($data->get_booking_details() as $key=>$val) @if (++$i === $last_key) {{@$val->rooms_number->room_no}} @else {{@$val->rooms_number->room_no}}, @endif @endforeach</h1></li>
                           <li>
                                
                                <a class="btn btn-success btn-sm text-white">{{ucwords(@$data->booking_payment_details->first()->payment_type)}}</a>
                                <!-- <a class="btn btn-primary btn-sm text-white">{{$data->get_booking_status()}}</a> -->
                                
                           </li>                           
                        </ul>
                     </div>
                     <div class="col-lg-2 col-md-12">
                        <ul class="list-unstyled mb-3">
                           <div class="card border border-secondary p-1 click_checkin_date">
                              <div class="ribbon-price">
                                 <input type="hidden" class="get_payment_status" value="{{@$data->payment_status}}">
                                 <input type="hidden" class="get_checkin_date" value="{{@$data->check_in}}">
                                 <input type="hidden" class="get_checkout_date" value="{{@$data->check_out}}">
                                 <input type="hidden" class="get_booking_id" value="{{@$data->id}}">
                                 <input type="hidden" class="get_checkin_rooms_count" value="{{@$data->checkin_rooms_count()}}">
                                 <span class="badge bg-secondary text-white">Check-In</span>
                              </div>
                              <div class="princing-item mb-3">
                                 <div class="pricing-divider text-center pt-5">
                                    <h4 class="display-5 text-secondary fw-bold my-3"> {{@$data->checkin_date_booking()[0]}} </h4>
                                    <h3 class="text-secondary">{{@$data->checkin_date_booking()[1]}}</h3>
                                    <h3 class="text-secondary">{{@$data->checkin_date_booking()[2]}}</h3>
                                 </div>
                              </div>
                           </div>
                          <!--  <div class="btn-list">                                            
                           <a href="javascript:void(0)" class="btn btn-primary btn-sm">Change date</a>
                           <a href="javascript:void(0)" class="btn btn-primary btn-sm">Change room</a>
                        </div> -->
                        </ul>                        
                     </div>

                     <div class="col-lg-2 col-md-12">
                        <ul class="list-unstyled mb-3">
                           <div class="card border border-secondary p-1 click_checkout_date">
                              <div class="ribbon-price">                                 
                                 <input type="hidden" class="get_checkin_date" value="{{@$data->check_in}}">
                                 <input type="hidden" class="get_checkout_date" value="{{@$data->check_out}}">
                                 <input type="hidden" class="get_booking_id" value="{{@$data->id}}">
                                 <input type="hidden" class="get_checkin_rooms_count" value="{{@$data->checkin_rooms_count()}}">
                                 <input type="hidden" class="get_checkout_rooms_count" value="{{@$data->checkout_rooms_count()}}">
                                 <span class="badge bg-secondary text-white">Check-Out</span>
                              </div>
                              <div class="princing-item mb-3">
                                 <div class="pricing-divider text-center pt-5">
                                    <h4 class="display-5 text-secondary fw-bold my-3"> {{@$data->checkout_date_booking()[0]}} </h4>
                                    <h3 class="text-secondary">{{@$data->checkout_date_booking()[1]}}</h3>
                                    <h3 class="text-secondary">{{@$data->checkout_date_booking()[2]}}</h3>
                                 </div>
                              </div>
                           </div>
                            <!-- <div class="btn-list">                                            
                           <a href="{{url('admin/bookinglisting/newbooking/billing_view/'.@$data->id)}}" class="btn btn-primary btn-sm">View</a>
                           <a href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->id)}}" class="btn btn-primary btn-sm click_print">Print</a>
                        </div> -->
                        </ul>                       
                     </div>
                  </div>
                </div>                
            </div>           
         </div>
          <!-- end loop -->
                  @endforeach
      </div>
      <!-- COL-END -->
   </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{ $list->onEachSide(1)->links() }}
                            </ul>
                        </nav>
   
</div>

<div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Booking No : <span id="booking_id"></span> </h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-lg-10 col-md-12">
                          <div class="form-group">
                            <b>Guest Name:</b> <span id="guest_name"></span><br>
                            <b>Contact Number:<b> <span id="guest_contact_number"></span><br>
                            <b>Email:<b> <span id="guest_email"></span><br>
                            <b>Amount:<b> <span id="booking_amount"></span><br>
                            <b>Booking Date:<b> <span id="booking_date"></span><br>
                          </div>
                      </div>                   
                    <form id="remark_form" name="remark_form" action="{{url('admin/depositremark/create')}}" method="post">
                    @csrf      
                    <input type="hidden" id="bookingid" name="bookingid" value="">       
                    <input type="hidden" id="booking_no" name="booking_no" value="">       
                      <div class="col-lg-10 col-md-12">
                        <div class="form-group">
                          <label for="remark"><b>Remark<b></label>
                          <textarea class="form-control mb-4" placeholder="Remark" name="remark" rows="4"></textarea>
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>


                  <div class="col-xl-12">
                            <div class="card">
                                    <div class="table-responsive" id="remark_table">
                                        <table class="table border text-nowrap text-md-nowrap table-striped mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Remark</th>
                                                    <th>Added By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                  </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>

<!-- ROW-1 END-->
@endsection
@section('scripts')
<script type="text/javascript">
   $(document).ready(function() {
      $("body").tooltip({ selector: '[data-toggle=tooltip]' });
      var get_today_date = "{{\Carbon\Carbon::now()->format('Y-m-d')}}";
     //create remark
       //click view new windows for print
      $(document).on('click','.click_print',function(e){
         e.preventDefault();
         window.open($(this).attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

      $(document).on('click','.add_guest_data',function(){
         //redirect new page
          var id=$(this).attr('id');
          var redirect = site_url + '/admin/bookinglisting/newbooking/get_room_guest_details/'+id;
          window.location.href=redirect;
          return false;
         //redirect new page
      });

      //filter      
      $(document).on('click','#data_filter',function(e) { 
         if( $('#search').val() =='') {
            toastr.clear();
            toastr.warning('Input data is required');
            return false;
         }
         $('#search_form').submit();
      });

      $(document).on('click','#reset',function() {
         //redirect new page          
         //if( $('#search').val() !='' ||  $('#booking_status').val() !='' || $('#payment').val() !='') {
          var path = "/{{Request::segment(1)}}/";
          var path1 = "{{Request::segment(2)}}";
          var redirect = site_url + path + path1;
          window.location.href=redirect;
          return false;
        //}
         //redirect new page
      });
      //filter
   

      $(document).on('click','.delete_remark',function(){
         if (!confirm("Do you want to remove")){
           return false;
         }else{
            //console.log($(this).attr('id'));
            $(this).closest("tr").remove();
            var removeid = $(this).attr('id');
            $.ajax({
                url: site_url + '/admin/depositremark/remove',
                type: 'post',
                dataType: 'json',
                data: {
                  id: removeid,
                  _token: '{{csrf_token()}}'
                },
                success: function(data) {
                   toastr.clear();
                   toastr.success('Remark removed successfully');
                }
              });
         }
      });

      $(document).on('click','.click_status',function(){
          $("#bookingid").val($(this).attr('id'));
          $.ajax({
          url: site_url + '/admin/depositremark/get',
          type: 'post',
          dataType: 'json',
          data: {
            id: $(this).attr('id'),
            //selectedData: selectedData,
            _token: '{{csrf_token()}}'
          },
          success: function(data) {
            $("#booking_id").html(data['booking_id']);
            $("#booking_no").val(data['booking_id']);
            $("#guest_name").html(data['guest_name']);
            $("#guest_contact_number").html(data['guest_number']);
            $("#guest_email").html(data['guest_email']);
            $("#booking_amount").html(data['amount']);
            $("#booking_date").html(data['booking_date']);              
              var tr = '';              
              $.each(data.tablerow, function(i, item) {
               var delete_action = '<button type="button" id="'+item.id+'" class="btn  btn-sm btn-danger delete_remark"><span class="fe fe-trash-2"> </span></button>';
                tr += '<tr><td>' + item.created_at + '</td><td>' + item.remarks + '</td><td>' + item.admin_user_id + '</td><td>' + delete_action + '</td></tr>';
              });
              $('#remark_table tbody').html(tr);
          }
        });
      });
     //create remark

    $("#remark_form").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      remark: {
        required: true
      }
    },
    messages: {
      remark: {
        required: "Please enter remark",
      }
    },    
  });
  });
function deletefunction(event, id) {
   event.preventDefault();
   swal({
     title: "Are you sure you want to delete this record?",
     text: "If you delete this, it will be gone forever.",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: '#DD6B55',
     confirmButtonText: 'Delete',
     cancelButtonText: "Cancel",
     closeOnConfirm: false,
     closeOnCancel: true
   }, function(isConfirm) {
     if (isConfirm) {
       $("#delete_form" + id).submit();
     } else {
       swal("Cancel", "Your data has not been removed", "error");
     }
   });
   }
</script>
@endsection