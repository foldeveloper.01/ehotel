@extends('admin.layouts.app') @section('styles')
<link href="{{asset('assets/admin/css/bootstrap-datepicker.standalone.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/css/js-year-calendar.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/admin/css/room_season.css')}}" rel="stylesheet" />
@endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Rooms Manage</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Rooms Manage List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      
      <div class="card-body">
                <div class="form-row">     
                                                           
                      <div class="col-xl-2 mb-1">
                          <select class="form-select form-select select2" id="season_year" name="season_year">
                              <option value="{{date('Y')}}" @if(@$current_year==date('Y')) selected @endif>{{date('Y')}}</option>
                              <option value="{{date('Y')+1}}" @if(@$current_year==date('Y')+1) selected @endif>{{date('Y')+1}}</option>
                              <option value="{{date('Y')+2}}" @if(@$current_year==date('Y')+2) selected @endif>{{date('Y')+2}}</option>
                          </select>
                      </div>
                                                                  
                      <div class="col-xl-5 mb-2">
                          <div class="btn-list">
                              @if(!$room_season_type->isEmpty())
                              @foreach(@$room_season_type as $key=>$data)
                              <a class="btn {{@$data->button_class_name}}  text-white">{{@$data->name}}</a>
                              @endforeach
                              @endif
                          </div>
                      </div>
                </div><br>
        
           <form action="{{url('admin/roomsseason')}}" enctype="multipart" method="get" id="season_year_form">
              @csrf
              <input type="hidden" name="season_year_update" id="season_year_update">
           </form>
        
        <div class="table-responsive">
           
         <div id="calendar"></div>

<div class="modal fade" id="event-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Season Date</h5>
        <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">        
        <form class="form-horizontal" id="season_form" action="{{url('admin/roomsseason')}}" enctype="multipart" method="post">
          @csrf
          <input type="hidden" name="event-index">
          <div class="form-group row">
            <label for="event-name" class="col-sm-4 control-label">Title</label>
            <div class="col-sm-8">
              <input id="event-name" name="event-name" type="text" class="form-control" required placeholder="Title">
            </div>
          </div>          
          <div class="form-group row">
            <label for="min-date" class="col-sm-4 control-label">Dates</label>
            <div class="col-sm-8">
              <div class="input-group input-daterange" data-provide="datepicker5">
                <input id="min-date" data-date-format="dd/mm/yyyy" name="event-start-date" type="text" class="form-control" readonly onkeydown="return false">
                <div class="input-group-prepend input-group-append">
                    <div class="btn btn-secondary input-group-text1">to</div>
                </div>
                <input id="max-date" data-date-format="dd/mm/yyyy" name="event-end-date" type="text" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group row">
              @if(!$room_season_type->isEmpty())
              @foreach(@$room_season_type as $key=>$data)
              <div class="col-xl-6 col-md-6">
                  <div class="form-group">            
                      <div class="custom-controls-stacked">
                          <label class="custom-control custom-radio">
                                  <input type="radio" class="custom-control-input" id="season_type_id_{{@$data->id}}" name="season_type" value="{{@$data->id}}">
                                  <span class="custom-control-label">{{@$data->name}}</span>
                              </label>                                                        
                      </div>
                  </div>
              </div>
              @endforeach
              <span id="errorSpan"></span>
              @endif
          </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" id="save-event">
          Save
        </button>
      </div>
        </form>
      </div>      
    </div>
  </div>
</div>
<div id="context-menu">
</div>

        </div>
      </div>
      <div class="card-footer">
        <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>        
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')

<script type="text/javascript">
  var currentStartDate = "{{\Carbon\Carbon::parse(Carbon\Carbon::create($current_year)->startOfYear())->format('d/m/Y')}}";
  var currentEndDate = "{{\Carbon\Carbon::parse(Carbon\Carbon::create($current_year)->endOfYear())->format('d/m/Y')}}";
  $(document).ready(function() {
      $('#season_year').on('change', function() {
        $('#season_year_update').val($(this).val());
        $( "periodChanged" ).trigger( "click" );
        $('#season_year_form').submit();
      });
  });

  $(function() {
    
    $('#min-date').datepicker5({
            container: "#event-modal",
            "option": {beforeShow: function(i) { if ($(i).attr('readonly')) { return false; } }}
        });
    $('#max-date').datepicker5({
            container: "#event-modal"
        });
});

let calendar = null;
var currentYear = {{@$current_year}};
function editEvent(event) {
    console.log(event.room_season_type_id);
    $('#event-modal input[name="event-index"]').val(event ? event.id : '');
    $('#event-modal input[name="event-name"]').val(event ? event.name : '');    
    $("#season_type_id_"+event.room_season_type_id+"").prop("checked", true);
    //$('#event-modal input[name="season_type"]').val(event ? event.room_season_type_id : '');
    //$('#event-modal input[name="event-location"]').val(event ? event.location : '');
    $('#event-modal input[name="event-start-date"]').datepicker5('update', event ? event.startDate : '');
    $('#event-modal input[name="event-end-date"]').datepicker5('update', event ? event.endDate : '');    
    //$('#event-modal input[name="event-start-date"]').datepicker5('option', {minDate: new Date(event.startDate)});
    $("label.error").hide();
    $(".error").removeClass("error");
    $('#event-modal').modal('show');
}

function deleteEvent(event) {
    RemoveEvent(event.id);
    var dataSource = calendar.getDataSource();
    calendar.setDataSource(dataSource.filter(item => item.id !== event.id));
}

function RemoveEvent(id) {
      $.ajax({
        url: site_url + '/admin/roomsseason/removeEvent',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();            
            toastr.success('Removed event successfully');
          } else {
            toastr.clear();
            toastr.error('This event already removed');
          }
        }
      });
}

function saveEvent() {
    var event = {
        id: $('#event-modal input[name="event-index"]').val(),
        name: $('#event-modal input[name="event-name"]').val(),
        //location: $('#event-modal input[name="event-location"]').val(),
        startDate: $('#event-modal input[name="event-start-date"]').datepicker5('getDate'),
        endDate: $('#event-modal input[name="event-end-date"]').datepicker5('getDate')
    }
    
    var dataSource = calendar.getDataSource();

    if (event.id) {
        for (var i in dataSource) {
            if (dataSource[i].id == event.id) {
                dataSource[i].name = event.name;
                //dataSource[i].location = event.location;
                dataSource[i].startDate = event.startDate;
                dataSource[i].endDate = event.endDate;
            }
        }
    }
    else
    {
        var newId = 0;
        for(var i in dataSource) {
            if(dataSource[i].id > newId) {
                newId = dataSource[i].id;
            }
        }        
        newId++;
        event.id = newId;
        //console.log(event);    
        dataSource.push(event);
    }
    
    calendar.setDataSource(dataSource);
    $('#event-modal').modal('hide');
}

$(function() {
    //var currentYear = new Date().getFullYear();    
    calendar = new Calendar('#calendar', { 
        enableContextMenu: true,
        enableRangeSelection: true,
        style:'background',
        //dateFormat: "dd/mm/yyyy",
        //minDate: new Date(),
        //MaxDate: new Date(),
        startDate: new Date(currentStartDate),
        endDate: new Date(currentEndDate),
        contextMenuItems:[
            {
                text: 'Update',
                click: editEvent
            },
            {
                text: 'Delete',
                click: deleteEvent
            }
        ],
        dataSource: (period) => {
    console.log(period.year);
  },
        selectRange: function(e) {
            editEvent({ startDate: e.startDate, endDate: e.endDate });
        },
        mouseOnDay: function(e) {
            if(e.events.length > 0) {
                var content = '';
                
                for(var i in e.events) {
                  //console.log(e.events[i]);
                    content += '<div class="event-tooltip-content">'
                                    + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                + '</div>';
                }
            
                $(e.element).popover({ 
                    trigger: 'manual',
                    container: 'body',
                    html:true,
                    content: content
                });
                
                $(e.element).popover('show');
            }
        },
        mouseOutDay: function(e) {
            if(e.events.length > 0) {
                $(e.element).popover('hide');
            }
        },
        dayContextMenu: function(e) {
            $(e.element).popover('hide');
        },
        dataSource: [
          @foreach($final_data as $data)
          {
                id: {{$data["id"]}},
                room_season_type_id: '{{$data["room_season_type_id"]}}',
                name: '{{$data["name"]}}',
                color:['{{$data["color"]}}'],
                startDate: new Date('{{$data["startDate"]}}'),
                endDate: new Date('{{$data["endDate"]}}')
          },
          @endforeach            
        ],
        periodChanged: function(e) {
            console.log(`New period selected: ${e.startDate} ${e.endDate}`);
        },
        clickDay: function(e) {
            console.log("Click on day: " + e.date + " (" + e.events.length + " events)");
        },
    });
    
    $('#save-event').click(function() {
    if($("#season_form").valid()){
      saveEvent();
     }        
    });

   /* document.querySelector('.calendar').addEventListener('periodChanged', function(e) {
  console.log(`New period selected: ${e.startDate} ${e.endDate}`);
})*/
});

$("#season_form").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      'event-name': {
        required: true
      },
      season_type: {
        required: true
      },
      'event-start-date': {
        required: true
      },
      'event-end-date': {
        required: true
      }
    },
    messages: {
      'event-name': {
        required: "Please enter event name",
      },
      season_type: {
        required: "Please select season type",
      },
      'event-start-date': {
        required: "Please select startDate",
      },
      'event-end-date': {
        required: "Please select endDate",
      }
    },errorPlacement: function(error, element) {
      if (element.attr("name") == "season_type") {
          error.appendTo("#errorSpan").css('color','#FF0000').css("fontSize", "14px").css('float','center');
      }else {
          error.insertAfter(element);
      }                
     }
  });

/*const calendar = new Calendar('.calendar');
calendar.setStyle('background');
calendar.setMaxDate(new Date());*/

</script> @endsection