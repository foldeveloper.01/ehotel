@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Gallery</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Update Gallery Category</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/gallery/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Update Gallery Category</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Category Name</label>
                <input type="text" value="{{@$data->category_name}}" name="category_name" class="form-control" id="category_name" placeholder="Category Name">
              </div> @if($errors->has('category_name')) <div class="error">{{ $errors->first('category_name') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Sort No</label>
                <input type="text" value="{{@$data->position}}" name="position" class="form-control" id="position" placeholder="Sort No">
              </div> @if($errors->has('position')) <div class="error">{{ $errors->first('position') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" @if(@$data->status=='0') selected @endif>Active</option>
                  <option value="1" @if(@$data->status=='1') selected @endif>Suspend</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
        <a href="{{url('admin/gallery')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      category_name: {
        required: true
      }
    },
    messages: {
      category_name: {
        required: "Please enter name",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection