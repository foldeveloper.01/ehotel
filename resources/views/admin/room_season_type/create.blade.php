@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Room Season Types</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Room Season Types</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/roomseasontype/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Room Season Types</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Name</label>
                <input type="text" value="" name="name" class="form-control" id="name" placeholder="Name">
               @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Button Color</label>
                <input type="text" value="" name="button_class_name" class="form-control" id="button_class_name" placeholder="Button Class Name">
               @if($errors->has('button_class_name')) <div class="error">{{ $errors->first('button_class_name') }}</div> @endif
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Color Code</label>
                <input type="text" value="" name="color_code" class="form-control" id="color_code" placeholder="Enter color code">
               @if($errors->has('color_code')) <div class="error">{{ $errors->first('color_code') }}</div> @endif
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Sort</label>
                <input type="number" value="0" name="position" class="form-control" id="position" placeholder="Position">
                @if($errors->has('position')) <div class="error">{{ $errors->first('position') }}</div> @endif
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
          <div class="card-footer">
        <a href="{{url('admin/roomseasontype')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
        </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      name: {
        required: true
      },
      button_class_name: {
        required: true
      },color_code: {
        required: true
      },
      status: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name",
      },
      button_class_name: {
        required: "Please enter button color",
      },
      color_code: {
        required: "Please enter color code",
      },
      status: {
        required: "Please select status",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection