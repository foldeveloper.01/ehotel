@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Gallery Photo for {{@$category->category_name}}</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Gallery List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="{{url('admin/subgallery/'.Request::segment(3).'/create')}}">
            <button id="table2-new-row-button" class="btn btn-primary mb-4">Create New Photo</button>
          </a>
          <a href="{{url('admin/gallery')}}">
            <button id="table2-new-row-button" class="btn btn-primary mb-4">Back To Gallery Category</button>
          </a>
        </h3>
      </div>
      <div class="card-body">
        <div class="text-wrap">
          <div class="">
            <div class="row"> @if(@$list!=='') @foreach(@$list as $key=>$data) <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4">
                <div class="file-image p-2">
                  <div class="product-image">
                    <a target="_blank" href="{{asset('storage/images/subgallery/'.@$data->image)}}">
                      <img src="{{asset('storage/images/subgallery/'.@$data->image)}}" alt="{{@$data->image}}" class="w-100">
                    </a>
                    <ul class="icons">
                      <li>
                        <a href="{{url('admin/subgallery/update/'.@$data->id)}}" class="bg-secondary">
                          <i class="fe fe-edit"></i>
                        </a>
                      </li>
                      <li>
                        <a target="_blank" href="{{asset('storage/images/subgallery/'.@$data->image)}}" class="bg-primary">
                          <i class="fe fe-eye"></i>
                        </a>
                      </li>
                      <li>
                        <a onclick="deletefunction(event,{{$data->id}});" href="" class="bg-danger">
                          <i class="fe fe-trash"></i>
                        </a>
                      </li>
                    </ul>
                    <a href="{{url('admin/subgallery/update/'.@$data->id)}}">
                      <span class="file-name">{{@$data->title}}
                    </a>
                    <form id="delete_form{{$data->id}}" method="POST" action="{{ url('admin/subgallery/delete', $data->id) }}"> @csrf <input name="_method" type="hidden" value="POST">
                    </form>
                    <br>
                    <div class="form-group">
                      <div class="input-group input-group-sm">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Sort No</span>
                        <input type="text" class="form-control position" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" name="position" id="position_{{@$data->id}}" value="{{@$data->position}}">
                      </div>
                    </div>
                  </div>
                </div>
              </div> @endforeach @endif </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.custom-switch-input').on('change', function() {
      var isChecked = $(this).is(':checked');
      var id = $(this).attr('id');
      var selectedData;
      var $switchLabel = $('.custom-switch-indicator');
      //console.log('isChecked: ' + isChecked);
      if (isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      //console.log('Selected data: ' + selectedData);
      //alert(site_url+'/admin/bookingwindows/ajax_status_update');
      $.ajax({
        url: site_url + '/admin/subgallery/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: selectedData,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();
            if (selectedData == 'Yes') {
              var success = 'Status Activate';
              toastr.success(success + ' successfully');
            } else {
              var success = 'Status Suspend';
              toastr.error(success + ' successfully');
            }
          } else {
            toastr.clear();
            toastr.error('something went wrong');
          }
        }
      });
    });
  });
  $('.position').on('keyup', function() {
    var isChecked = 'Yes';
    var id = $(this).attr('id');
    $.ajax({
      url: site_url + '/admin/subgallery/ajax_status_update',
      type: 'post',
      dataType: 'json',
      data: {
        id: id,
        selectedData: $(this).val(),
        _token: '{{csrf_token()}}'
      },
      success: function(response) {
        if (response['data'] == 1) {
          toastr.clear();
          toastr.success('Sort Updated successfully');
        } else {
          toastr.clear();
          toastr.error('something went wrong');
        }
      }
    });
  });

  function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }
</script> @endsection