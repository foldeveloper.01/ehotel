@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Gallery Photo for {{@$category->category_name}}</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Gallery</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/subgallery/').'/'.Request::segment('3').'/save'}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Add New Gallery Photo for {{@$category->category_name}}</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Title</label>
                <input type="text" value="" name="title" class="form-control" id="title" placeholder="Title">
              </div> @if($errors->has('title')) <div class="error">{{ $errors->first('title') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Title 1</label>
                <input type="text" value="" name="title1" class="form-control" id="title1" placeholder="Title 1">
              </div> @if($errors->has('title1')) <div class="error">{{ $errors->first('title1') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Sort No</label>
                <input type="text" value="0" name="position" class="form-control" id="position" placeholder="Sort No">
              </div> @if($errors->has('position')) <div class="error">{{ $errors->first('position') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="bannerimage">Banner Image (1920 × 950) (Max upload file size 2MB)</label>
                <input class="form-control" accept="image/*" name="image" type="file">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <a href="{{url('admin/gallery/'.Request::segment(3).'/subgallery')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      title: {
        required: true
      },
      image: {
        required: true
      }
    },
    messages: {
      title: {
        required: "Please enter title",
      },
      image: {
        required: "Please upload image",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection