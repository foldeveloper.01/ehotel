@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Site Content</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Update Site Content</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <div class="col-xl-12">
    <form id="create" action="{{url('admin/sitecontent/update/'.Request::segment('4'))}}" method="post" enctype="multipart/form-data"> @csrf <div class="card">
        <div class="card-header">
          <h3 class="card-title">Update Site Content</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Menu Name</label>
                <input type="text" value="{{@$data->menu_name}}" name="menu_name" class="form-control" id="menu_name" placeholder="Menu Name">
              </div> @if($errors->has('menu_name')) <div class="error">{{ $errors->first('menu_name') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Title</label>
                <input type="text" value="{{@$data->title}}" name="title" class="form-control" id="title" placeholder="Title">
              </div> @if($errors->has('title')) <div class="error">{{ $errors->first('title') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="description">Description</label>
                <textarea class="summernote" name="description">{!!@$data->description!!}</textarea>
              </div> @if($errors->has('description')) <div class="error">{{ $errors->first('description') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <label for="short_description">Short Description</label>
              <textarea class="summernote" name="short_description">{!!@$data->short_description!!}</textarea>
            </div> @if($errors->has('short_description')) <div class="error">{{ $errors->first('short_description') }}</div> @endif
          </div>
          <div class="row">
            <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Image 1</label>
                <input class="form-control" accept="image/*" name="image" type="file">
              </div> @if($errors->has('image')) <div class="error">{{ $errors->first('image') }}</div> @endif
            </div> @if(@$data->image!=null && @$data->image!='') <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img height="150px;" width="150px;" class="avatar-xxl" src="{{asset('storage/images/sitecontent/'.@$data->image)}}" />
                <a target="_blank" href="{{asset('storage/images/sitecontent/'.@$data->image)}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @endif <div class="col-lg-5 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Image 2</label>
                <input class="form-control" accept="image/*" name="image1" type="file">
              </div> @if($errors->has('image1')) <div class="error">{{ $errors->first('image1') }}</div> @endif
            </div> @if(@$data->image1!=null && @$data->image1!='') <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img height="150px;" width="150px;" class="avatar-xxl" src="{{asset('storage/images/sitecontent/'.@$data->image1)}}" />
                <a target="_blank" href="{{asset('storage/images/sitecontent/'.@$data->image1)}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @endif
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label class="form-label">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" @if(@$data->status=="0") selected @endif>Active</option>
                  <option value="1" @if(@$data->status=="1") selected @endif>Suspend</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Update</button>
          <a href="{{url('admin/banner')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  $(document).ready(function() {
    $('.summernote').summernote();
  });
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      menu_name: {
        required: true,
        maxlength: 200
      },
      title: {
        required: true,
        maxlength: 200
      }
    },
    messages: {
      menu_name: {
        required: "Please enter menu name",
      },
      title: {
        required: "Please enter title",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection