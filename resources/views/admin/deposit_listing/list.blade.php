@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Deposit List</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Deposit List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <!-- <div class="card-header">
        <h3 class="card-title">Deposit List</h3>
      </div> -->
      <div class="card-body">
                  <a href="{{url('admin/bookinglisting/newbooking/?type=normal')}}">
                  <button id="table2-new-row-button" class="btn btn-primary mb-4">New Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=midnight')}}">
                  <button id="" class="btn btn-primary mb-4">Midnight Booking</button>
                  </a>
                  <a href="{{url('admin/bookinglisting/newbooking/?type=monthly')}}">
                  <button id="" class="btn btn-primary mb-4">Monthly Booking</button>
                  </a>          
        
        <form id="search_form" action="{{url(Request::segment(1).'/'.Request::segment(2))}}" method="get" enctype="multipart">
                 
            <div class="row row-sm p-2">             
                 
                  <div class="col-lg-2">                        
                        <select name="deposite_status" id="deposite_status" class="form-control select2 form-select">
                           <option value="">Status</option>
                           <option value="1" @if(Request::get('deposite_status')=='1') selected @endif>Deposit Paid</option>
                           <option value="2" @if(Request::get('deposite_status')=='2') selected @endif>Deposit Refunded</option>
                        </select>
                  </div>
                  <div class="col-lg-2">
                      <div class="form-group">
                          <input type="text" name="search" id="search" class="form-control" value="{{Request::get('search')}}" placeholder="Searching.....">
                      </div>
                  </div>
                  <div class="col-lg-2">
                      <div class="btn-list">
                          <input type="button" class="btn btn-primary" id="data_filter" value="Search"> 
                          <input type="button" class="btn btn-danger" id="reset" value="Reset">                                                    
                      </div>                      
                  </div>                  
              </div>
               @csrf
           </form>

        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable22">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Booking No</th>
                <th class="wd-15p border-bottom-0">Guest Name</th>
                <th class="wd-15p border-bottom-0">Room No</th>
                <th class="wd-15p border-bottom-0">Check-in</th>
                <th class="wd-15p border-bottom-0">Check-out</th>
                <th class="wd-15p border-bottom-0">Amount</th>
                <th class="wd-15p border-bottom-0">Pay By</th>
                <th class="wd-15p border-bottom-0">Status</th>
                <th class="wd-15p border-bottom-0">Action</th>
              </tr>
            </thead>
            <tbody>
              @if(@$list)
               @foreach(@$list as $key=>$data)
                <tr>
                  <td>{{@$key+1}}</td>
                  <td> <a href="{{url('admin/bookinglisting/newbooking/billing_print_view/'.@$data->id)}}" class="white click_print custom_a">{{@$data->booking_number}}</a> </td>
                   
                  <td><b>{{@$data->guest->name}}<br>{{@$data->guest->ic_passport_no}}<br>{{@$data->guest->contact_number}}</b></td>
                  <td>@php $last_key = count($data->get_checkin_rooms_data()); $i = 0; @endphp @foreach($data->get_checkin_rooms_data() as $key=>$val) @if (++$i === $last_key) {{@$val->rooms_number->room_no}} @else {{@$val->rooms_number->room_no}}, @endif @endforeach
                  </td>

                  <td>{{@$data->checkin_date()}}</td>
                  <td>{{@$data->checkout_date()}}</td>
                  <td>RM{{@$data->deposit_amount}}</td>
                  <td>{{@$data->deposit_payment_status}}</td>
                  <td>@if($data->deposit=='yes' && $data->deposit_refund=='no') Paid
                    <a id="{{@$data->booking_status}}" class="btn btn-success bg-success-gradient btn-sm refund_amount" href="{{url('admin/deposit_refund/refund/'.@$data->id)}}"> Refund Deposit </a>                       
                   @else 
                       Refunded
                       <a id="{{@$data->booking_status}}" class="btn btn-success bg-success-gradient btn-sm cancel_refund" href="{{url('admin/deposit_refund/cancelrefund/'.@$data->id)}}"> Cancel Refund </a>
                   @endif
                  </td>
                  <td>
                    <!-- <button class="btn btn-yellow white"><i class="fa fa-print" aria-hidden="true"></i></button> -->                    
                    <a href="{{url('admin/depositremark/receipt_view/'.@$data->id)}}" class="btn btn-yellow white click_print"><i class="fa fa-print" aria-hidden="true"></i></a>
                    <button id="{{@$data->id}}" class='btn btn-success bg-success-gradient click_status' data-bs-toggle='modal' data-bs-target='#scrollingmodal'><i class="fa fa-sticky-note-o"></i></button>
                  </td>
                </tr> 
               @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                {{ $list->onEachSide(0)->links() }}
                            </ul>
                        </nav>
      </div>

    </div>
    
  </div>
</div>
<div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Booking No : <span id="booking_id"></span> </h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-lg-10 col-md-12">
                          <div class="form-group">
                            <b>Guest Name:</b> <span id="guest_name"></span><br>
                            <b>Contact Number:<b> <span id="guest_contact_number"></span><br>
                            <b>Email:<b> <span id="guest_email"></span><br>
                            <b>Amount:<b> <span id="booking_amount"></span><br>
                            <b>Booking Date:<b> <span id="booking_date"></span><br>
                          </div>
                      </div>                   
                    <form id="remark_form" name="remark_form" action="{{url('admin/depositremark/create')}}" method="post">
                    @csrf      
                    <input type="hidden" id="bookingid" name="bookingid" value="">       
                    <input type="hidden" id="booking_no" name="booking_no" value="">       
                      <!-- <div class="col-lg-10 col-md-12">
                          <div class="form-group">
                            <label class="form-label"><b>Status<b></label>
                            <select name="status" class="form-control">
                              <option value="0" selected>Paid</option>
                              <option value="1">Void</option>
                            </select>
                          </div>
                      </div> -->
                      <div class="col-lg-10 col-md-12">
                        <div class="form-group">
                          <label for="remark"><b>Remark<b></label>
                          <textarea class="form-control mb-4" placeholder="Remark" name="remark" rows="4"></textarea>
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>


                  <div class="col-xl-12">
                            <div class="card">
                                    <div class="table-responsive" id="remark_table">
                                        <table class="table border text-nowrap text-md-nowrap table-striped mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Remark</th>
                                                    <th>Added By</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                  </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">

    //click refund
      $(document).on('click','.refund_amount',function(e){
         e.preventDefault();
         if($(this).attr('id')!=2){
            toastr.clear();
            toastr.warning('After check-out only, you can refund deposit');
            return false;
         }
         if (!confirm("Do you want to REFUND the deposit now?")){
           return false;
         }
         window.location.href = $(this).attr('href'); 
         console.log($(this).attr('id'));
      });
    //click refund

    //click refund
      $(document).on('click','.cancel_refund',function(e){
         e.preventDefault();         
         if (!confirm("Do you want to change the status to Paid?")){
           return false;
         }
         window.location.href = $(this).attr('href'); 
         console.log($(this).attr('id'));
      });
    //click refund
      


  $(document).ready(function() {


      //filter      
      $(document).on('change','#deposite_status',function(e) {
          $('#search_form').submit();
      });
      $(document).on('click','#data_filter',function(e) { 
         if( $('#search').val() =='' &&  $('#deposite_status').val() =='') {
            toastr.clear();
            toastr.warning('Input data is required');
            return false;
         }
         $('#search_form').submit();
      });

      $(document).on('click','#reset',function() {
         //redirect new page          
         //if( $('#search').val() !='' ||  $('#booking_status').val() !='' || $('#payment').val() !='') {
          var path = "/{{Request::segment(1)}}/";
          var path1 = "{{Request::segment(2)}}";
          var redirect = site_url + path + path1;
          window.location.href=redirect;
          return false;
        //}
         //redirect new page
      });
      //filter
      
      //click view new windows for print
      $(document).on('click','.click_print',function(e){
         e.preventDefault();
         window.open($(this).attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

    $(document).on('click','.delete_remark',function(){
         if (!confirm("Do you want to remove")){
           return false;
         }else{
            //console.log($(this).attr('id'));
            $(this).closest("tr").remove();
            var removeid = $(this).attr('id');
            $.ajax({
                url: site_url + '/admin/depositremark/remove',
                type: 'post',
                dataType: 'json',
                data: {
                  id: removeid,
                  _token: '{{csrf_token()}}'
                },
                success: function(data) {
                   toastr.clear();
                   toastr.success('Remark removed successfully');
                }
              });
         }
      });

     //create remark
      $(document).on('click','.click_status',function(){
          $("#bookingid").val($(this).attr('id'));
          $.ajax({
          url: site_url + '/admin/depositremark/get',
          type: 'post',
          dataType: 'json',
          data: {
            id: $(this).attr('id'),
            //selectedData: selectedData,
            _token: '{{csrf_token()}}'
          },
          success: function(data) {
            $("#booking_id").html(data['booking_id']);
            $("#booking_no").val(data['booking_id']);
            $("#guest_name").html(data['guest_name']);
            $("#guest_contact_number").html(data['guest_number']);
            $("#guest_email").html(data['guest_email']);
            $("#booking_amount").html(data['amount']);
            $("#booking_date").html(data['booking_date']);              
              var tr = '';
              $.each(data.tablerow, function(i, item) {
               var delete_action = '<button type="button" id="'+item.id+'" class="btn  btn-sm btn-danger delete_remark"><span class="fe fe-trash-2"> </span></button>';
                tr += '<tr><td>' + item.created_at + '</td><td>' + item.remarks + '</td><td>' + item.admin_user_id + '</td><td>' + delete_action + '</td></tr>';
              });
              $('#remark_table tbody').html(tr);
          }
        });
      });
     //create remark

    $("#remark_form").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      remark: {
        required: true
      }
    },
    messages: {
      remark: {
        required: "Please enter remark",
      }
    },    
  });
  });
</script> @endsection