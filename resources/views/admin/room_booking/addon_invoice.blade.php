@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
@media print {
    #si-printer {
        display :  none !important;
    }#si-printer1 {
        display :  none !important;
    }
}
.print:last-child {
     page-break-after: auto;
}
</style>


    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Invoice</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                    <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img logo-3" alt="logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{@$setting->address}}
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <h3>Booking No-{{@$booking->booking_number}}</h3>
                                                <h5><b>Check-in:</b> {{\Carbon\carbon::parse(@$booking->check_in)->format('d-m-Y')}}</h5>
                                                <h5><b>Check-out:</b> {{\Carbon\carbon::parse(@$booking->check_out)->format('d-m-Y')}}</h5>
                                                <h5><b>Date:</b>{{\Carbon\Carbon::parse($booking_payment->created_at)->format('d-m-Y')}}</h5>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="h3">Invoice To:</p>
                                                <p class="fs-15 fw-semibold mb-0">{{ucwords(@$booking->guest->name)}}</p>                                  
                                                <address>
                                                        {{@$booking->guest->address}}<br>
                                                        @if(@$booking->guest->city)
                                                         {{@$booking->guest->city}},
                                                        @endif
                                                        @if(@$booking->guest->state)
                                                         {{@$booking->guest->state}},
                                                        @endif
                                                         <br>
                                                        @if(@$booking->guest->country)
                                                         {{@$booking->guest->country}},
                                                        @endif
                                                        @if(@$booking->guest->postcode)
                                                         {{@$booking->guest->postcode}}
                                                        @endif
                                                        <br>
                                                        {{@$booking->guest->email}}
                                                    </address>
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> -->
                                        </div>
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap table-striped">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th>Date</th>
                                                        <th>Description</th>
                                                        <th>Rate / Night(s)</th>
                                                        <th class="text-end">Amount(RM)</th>
                                                    </tr>
                                                    @if($invoice_type=='new')
                                                        <?php $total=0;?>
                                                        @foreach($booking->booking_new_addon_details as $key=>$data)
                                                        <tr> 
                                                            <td>{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                            <td>{{@$data->getaddon->add_on_name}}</td>
                                                            <td class="">{{@$data->price}} / {{@$data->quantity}} Qty</td>
                                                            <td class="text-end">{{@$data->total_amount}}</td>
                                                        </tr>
                                                         <?php $total+=number_format($data->total_amount,2)?>
                                                        @endforeach
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td class="fw-bold text-uppercase text-end"><b>Heritage Charge</b></td>
                                                            <td class="text-end">{{number_format($booking->booking_new_addon_details->first()->service_tax,2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td class="fw-bold text-uppercase text-end"><b>Total</b></td>
                                                            <td class="text-end">{{number_format($total+$booking->booking_new_addon_details->first()->service_tax,2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>{{\Carbon\Carbon::parse($booking_payment->created_at)->format('d-m-Y')}}</td>
                                                            <td></td>
                                                            <td >Payment By {{$booking_payment->payment_type}}</td>
                                                            <td class="text-end">-{{number_format($booking_payment->total_amount,2)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td class="fw-bold text-uppercase text-end">Balance</td>
                                                            <td class="text-end">{{number_format($balance_amount,2)}}</td>
                                                        </tr>
                                                    @else                                                        
                                                        <?php $booking_details_id=[];$booking_addon_id=[];?> 
                                                        @foreach($booking->get_booking_details() as $key=>$data)
                                                          @php $booking_details_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                            <td >{{@$data->rooms->room_type}} - {{@$data->rooms_number->room_no}}</td>
                                                            <td> {{@$data->room_price}} / {{@$data->number_of_days}} Night(s) </td>
                                                            <td class="text-end">{{@$data->subtotal_amount}}</td>
                                                        </tr>
                                                        @endforeach
                                                        @foreach($booking->booking_exsit_addon_details as $key=>$data)
                                                        @php $booking_addon_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                            <td >{{@$data->getaddon->add_on_name}}</td>
                                                            <td class="">{{@$data->price}} / {{@$data->quantity}} Qty</td>
                                                            <td class="text-end">{{@$data->total_amount}}</td>
                                                        </tr>
                                                        @endforeach
                                                          <tr>
                                                            <td colspan="3" class="fw-bold text-uppercase text-end">Service Charge @if(@$booking->service_tax_details!=null)
                                                                    @if(explode('_',@$booking->service_tax_details)[0]!='amount')
                                                                      @ {{explode('_',@$booking->service_tax_details)[1]}} %
                                                                    @else
                                                                      @ {{explode('_',@$booking->service_tax_details)[1]}} MYR
                                                                    @endif
                                                                @endif</td>
                                                            <td class="text-end">MYR {{@$booking->service_tax_amount}}</td>
                                                          </tr>                                                                                         
                                                          <tr>
                                                            <td colspan="3" class="fw-bold text-uppercase text-end">Total</td>
                                                            <td class="fw-bold text-end h4">MYR {{number_format(@$balance_amount,2)}}</td>
                                                          </tr>
                                                          @foreach($booking->booking_exsit_addon_payment_details($booking->id) as $key=>$data)
                                                            <tr>
                                                               <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                               <td ></td>
                                                               <td >Payment By {{ucwords(@$data->payment_type)}}</td>
                                                               <td class="text-end">-{{@$data->total_amount}}</td>
                                                            </tr>
                                                          @endforeach
                                                          @foreach($booking->booking_change_payment_details($booking->id) as $key=>$data)
                                                            <tr>
                                                               <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                               <td ></td>
                                                               <td >Payment By {{ucwords(@$data->payment_type)}}</td>
                                                               <td class="text-end">-{{@$data->total_amount}}</td>
                                                            </tr>
                                                          @endforeach
                                                          <tr>
                                                            <td colspan="3" class="fw-bold text-uppercase text-end">Balance</td>
                                                            <td class="fw-bold text-end h4">MYR {{number_format((@$booking->total_amount-$balance_amount),2)}}</td>
                                                          </tr>
                                                        @endif                                                   
                                                </tbody>
                                            </table>
                                        </div><br>
                                     
                         <div class="table-responsive">
                                <table class="table table-bordered table-hover mb-0 text-nowrap table-striped">
                                    <tbody>
                                        <tr class=" ">
                                            <th>Date</th>
                                            <th>Receipt No</th>
                                            <th>Payment Type</th>
                                            <th>Amount(RM)</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($booking->booking_payment_details as $key=>$data)
                                         @if(@$data->room_booking_receipt->receipt_amount)
                                        <tr> 
                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                            <td >#{{@$data->room_booking_receipt->receipt_no}}</td>
                                            <td >{{@$data->payment_type}}</td>
                                            <td id="rowamount_{{@$data->room_booking_receipt->id}}">{{number_format(@$data->room_booking_receipt->receipt_amount,2)}}</td>
                                            <td ><span class="badge bg-success-transparent rounded-pill text-success p-2 px-3">Paid</span></td>
                                            <td>
                                                <button id="{{@$data->room_booking_receipt->id}}" class="btn btn-yellow white edit_price" data-bs-toggle='modal' data-bs-target='#scrollingmodal'>Edit Amount</button>
                                                <a href="{{url('admin/bookinglisting/newbooking/receipt_view/'.@$data->room_booking_receipt->id)}}"><button class='btn btn-success bg-success-gradient click_view'>View</button></a>
                                               <!--  bookinglisting/newbooking/receipt_view/ -->
                                            </td>
                                        </tr>
                                         @endif
                                        @endforeach                                             
                                    </tbody>
                                </table>
                            </div>
                            </div>

                            <div class="card-footer">
                                <a href="{{url('admin/bookinglisting')}}" class="btn btn-danger my-1">Back</a>            
                            </div>
                                   
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

                        <div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                <form id="save_price_form">
                                    <div class="modal-body">
                                      <div class="col-lg-10 col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="receipt_id" id="receipt_id">
                                          <label for="remark"><b>Amount<b></label>
                                          <input class="form-control mb-4" placeholder="Amount" id="amount" name="amount" type="number" pattern="^\d*(\.\d{0,2})?$">
                                        </div>
                                      </div>
                                    <div class="modal-footer">
                                        <button type="button" id="save_price_button" class="btn btn-primary">Save</button>
                                    </div>
                                   </div>
                               </form>
                                </div>
                            </div>
                        </div>

        @endsection

    @section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
      //click view new windows for print
      $(document).on('click','.click_view',function(e){
         e.preventDefault();
         window.open($(this).parent().parent().find('a').attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

      $(document).on('click','.edit_price',function(){
         $("#receipt_id").val($(this).attr('id'));
          var id = $(this).attr('id');
          $("#amount").val($(this).parent().parent().find('#rowamount_'+id+'').html());
      });

      $("#save_price_form").validate({
            submitHandler: function(form) {
              form.submit();
            },
            rules: {
              amount: {
                required: true,
                number: true
              }
            },
            messages: {
              amount: {
                required: "Please enter amount",
              }
            },    
       });

      $(document).on('click','#save_price_button',function(){        
        if ($('#save_price_form').valid()) {
                 var receipt_id = $("#receipt_id").val();
                 var amount = $("#amount").val();
                  $.ajax({
                  url: site_url + '/admin/bookinglisting/newbooking/addon_invoice/update_price',
                  type: 'post',
                  dataType: 'json',
                  data: {
                    id: receipt_id,
                    amount: amount,
                    _token: '{{csrf_token()}}'
                  },
                  success: function(data) {
                      toastr.clear();
                      toastr.success('Amount updated successfully');
                      $("#rowamount_"+receipt_id+"").html(data.amount);
                    }           
                });
                $('.btn-close').click();
            }
      });      
      
    });
</script>
    @endsection
