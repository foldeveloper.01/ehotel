@extends('admin.layouts.app')
    @section('styles')<style type="text/css">.styleee{color: red;}.flex-1{display: none;}</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Booking Addon</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Booking Addon</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row row-cards">
                            <!-- COL-END -->
                            <div class="col-xl-12 col-lg-12">
                                <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-7">
                <div class="card cart">
                    <div class="card-header">
                        <span class="fw-bold text-success">Add-on for Booking Number :</span>&nbsp;<b>{{@$booking->booking_number}}</b>&nbsp;&nbsp;                        
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-vcenter">
                                <thead>
                                    <tr class="border-top">
                                        <th>Add-On</th>
                                        <th>Price</th>
                                        <th class="table_single_header">Quantity</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @if(!$list_add_on_product->isEmpty())
                                   @foreach($list_add_on_product as $key=>$data)
                                    <tr>                                       
                                        <td>{{$data->add_on_name}}</td>
                                        <td >RM{{$data->add_on_amount}}</td>
                                        <td>
                                            <div class="input-group input-indec input-indec1">
                                                <span class="input-group-btn">
                                                    <button type="button" class="quantity_minus btn btn-white btn-number btn-icon br-7" >
                                                        <i class="fa fa-minus text-muted"></i>
                                                    </button>
                                                </span>
                                                <input type="hidden" class="form-control text-center rowidvalue" value="{{$data->id}}">
                                                <input type="hidden" class="form-control text-center addon_name" value="{{$data->add_on_name}}">
                                                <input type="hidden" class="form-control text-center amount" value="{{$data->add_on_amount}}">
                                                <input type="hidden" class="form-control text-center addon_tax_condition" value="{{$data->service_charge_enable}}">
                                                <input type="text" id="quantity{{$data->id}}" name="quantity" class="form-control text-center qty" value="0">
                                                <span class="input-group-btn">
                                                    <button type="button" class="quantity-right-plus btn btn-white btn-number btn-icon br-7 quantity_add">
                                                        <i class="fa fa-plus text-muted"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="fw-bold">RM<span class="@if(@$data->service_charge_enable==1)add_on_amount_sub @else add_on_amount_addition @endif" id="addonsubtotal_{{$data->id}}">0.00</span></td>                                        
                                    </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="input-group mb-1">
                                    <input type="text" class="form-control" placeholder="Search ...">
                                    <span class="input-group-text btn btn-primary">Apply Coupon</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 text-end"><a href="javascript:void(0)" class="btn btn-default disabled btn-md">Update Cart</a></div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="col-lg-12 col-xl-5 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Totals</div>
                    </div>
                    <div class="card-body py-2">
                        <div class="table-responsive">
                            <table class="table table-borderless text-nowrap mb-0" id="checkout_table">
                                <tbody>
                                    <tr>
                                    </tr>                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                        <div class="card subtotal_table">                   
                            <div class="card-body py-2">
                                <div class="table-responsive">
                                    <table class="table table-borderless text-nowrap mb-0">
                                        <?php $getTax = Helper::getTaxSetting(); ?>
                                        <tbody>
                                            <tr>
                                                <td class="text-start">Sub Total</td>
                                                <td class="text-end">RM<span class="ms-1 fw-bold  ms-auto" id="subtotal">00.00</span></td>
                                            </tr>
                                            <tr>
                                                <td class="text-start">Service Charge @if($getTax['type']=='percentage')@ {{$getTax['value']}}% @endif</td>
                                                @if($getTax['type']=='percentage')
                                                <td class="text-end">RM <span class="fw-bold text-success" id="taxamount">0.00</span></td>
                                                @else
                                                <td class="text-end">RM <span class="fw-bold text-success" id="taxamount">0.00</span></td>
                                                @endif
                                           </tr>
                                            <tr>
                                                <td class="text-start fs-18">Total Bill</td>
                                                <td class="text-end">RM<span class="ms-1 fw-bold fs-23" id="total">0.00</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        <form id="checkout_form" class="checkout_form" action="{{url('admin/bookinglisting/newbooking/saveproductaddon/'.Request::segment(5))}}" method="post" enctype="multipart">
                                @csrf                                 
                                <input type="hidden" name="addon_id[]" value="">
                                <input type="hidden" name="addon_quantity[]" value="">
                                <input type="hidden" name="subtotal_amount" id="subtotal_amount" value="00.00">
                                <input type="hidden" name="total_amount" id="total_amount" value="00.00">
                                <input type="hidden" name="tax_amount" id="tax_amount" value="00.00"> 
                                <input type="hidden" name="invoice_type" id="invoice_type" value="">
                                <input type="hidden" name="tax_type" id="tax_type" value="{{$getTax['type']}}_{{$getTax['value']}}">                                 
                                <div class="card-body">
                                        <div class="card-pay">
                                            <ul class="tabs-menu nav">
                                                
                                                <li class="tab_check"><a href="#tab21" data-bs-toggle="tab" class="active"><i class="fa fa-money"></i> Others</a></li>
                                                <li class="tab_check"><a href="#tab20" data-bs-toggle="tab"><i class="fa fa-credit-card"></i> Credit Card</a></li>
                                                <li class="tab_check"><a href="#tab22" data-bs-toggle="tab" class=""><i class="fa fa-credit-card"></i>Debit Card</a></li>
                                            </ul>


                                            <div class="tab-content">                                                
                                                <div class="tab-pane active show" id="tab21">
                                                    <label class="custom-control custom-radio-md">
                                                            <input type="radio" class="custom-control-input" id="payment_type1" name="payment_type" value="1" checked>
                                                            <span class="custom-control-label">Cash</span>
                                                    </label>
                                                    <label class="custom-control custom-radio-md">
                                                            <input type="radio" class="custom-control-input" id="payment_type2" name="payment_type" value="2">
                                                            <span class="custom-control-label">Credit Bill</span>
                                                    </label>
                                                    <label class="custom-control custom-radio-md">
                                                            <input type="radio" class="custom-control-input" id="payment_type3" name="payment_type" value="3">
                                                            <span class="custom-control-label">Bank-in</span>
                                                    </label>
                                                     <input type="text" class="form-control" name="bank_name" value="" placeholder="Bank Name" id="bank_name"><br>
                                                </div>
                                                <div class="tab-pane" id="tab20">
                                                   <input type="radio" class="custom-control-input" name="payment_type" id="payment_type4" value="4">
                                                    <div class="form-group">
                                                        <label class="form-label">CardHolder Name</label>
                                                        <input type="text" name="cardholder_name" class="form-control" placeholder="Name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Card number</label>
                                                        <div class="input-group">
                                                            <input name="cardholder_number" type="text" class="form-control" placeholder="Card number">
                                                            <span class="input-group-text btn btn-secondary shadow-none">
                                                                    <i class="fa fa-cc-visa"></i> &nbsp; <i class="fa fa-cc-amex"></i> &nbsp;
                                                                    <i class="fa fa-cc-mastercard"></i>
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label class="form-label">Expiration</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" placeholder="MM" name="month">
                                                                    <input type="number" class="form-control" placeholder="YY" name="year">
                                                                </div>
                                                            </div>
                                                        </div>                                                      
                                                    </div>                                                    
                                                </div>
                                                <div class="tab-pane" id="tab22">
                                                    <input type="radio" class="custom-control-input" id="payment_type1" name="payment_type" value="5">
                                                    <div class="form-group">
                                                        <label class="form-label">CardHolder Name</label>
                                                        <input type="text" name="cardholder_name1" class="form-control" placeholder="Name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Card number</label>
                                                        <div class="input-group">
                                                            <input type="text" name="cardholder_number1" class="form-control" placeholder="Card number">
                                                            <span class="input-group-text btn btn-secondary shadow-none">
                                                                    <i class="fa fa-cc-visa"></i> &nbsp; <i class="fa fa-cc-amex"></i> &nbsp;
                                                                    <i class="fa fa-cc-mastercard"></i>
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label class="form-label">Expiration</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" placeholder="MM"  name="month1">
                                                                    <input type="number" class="form-control" placeholder="YY" name="year1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label class="form-label">CVV <i class="fa fa-question-circle" data-bs-placement="top" data-bs-toggle="tooltip" title="CVV is the last three digits on the back of your credit card"></i></label>
                                                                <input type="number" class="form-control" required="">
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </form>


                            <div class="card-footer">
                                <div class="btn-list">
                                    <button id="1" class="btn btn-success bg-success-gradient mt-3 invoice_type">Create new invoice</button>
                                    <button id="2" class="btn btn-warning bg-warning-gradient mt-3 invoice_type">Add to same invoice</button>
                                    </div>
                            </div>
                       </div>                    
                </div>
            </div>
        </div>

        
                                <!-- COL-END -->
                            </div>
                            <!-- ROW-1 CLOSED -->
                        </div>
                        <!-- ROW-1 END-->       

        @endsection

    @section('scripts')

    <script type="text/javascript">
    $( document ).ready(function() {        
        //div hide
        $('.subtotal_table').hide();
        //div hide
        $('#bank_name').css('display','none');
        $("input[name=payment_type][value=" + 1 + "]").prop('checked', true);
        $(document).on('click','.tab_check',function(){
             if($(this).children("a").attr("href")=='#tab20'){
                 $("input[name=payment_type][value=" + 4 + "]").prop('checked', true);
             }else if($(this).children("a").attr("href")=='#tab21'){
                 $("input[name=payment_type][value=" + 1 + "]").prop('checked', true);
             }else if($(this).children("a").attr("href")=='#tab22'){
                 $("input[name=payment_type][value=" + 5 + "]").prop('checked', true);
             }
        });
        $(document).on('click','input[name=payment_type]',function(){
            if($(this).val()==3){
                $('#bank_name').prop('required',true);
                $('#bank_name').css('display','block');
            }else{
                $('#bank_name').prop('required',false);
                $('#bank_name').css('display','none');
            }
        })
        
        $(document).on("click", ".invoice_type", function() {
           if($(this).attr('id')==1){
            $('#invoice_type').val('new');
           }else{
            $('#invoice_type').val('exist');
           }
           if($("input[name='payment_type']:checked").val()==1 || $("input[name='payment_type']:checked").val()==2){
             $("#checkout_form").submit();
           }else if($("input[name='payment_type']:checked").val()==3){
                var valid = true,
                errorMessage = "";
                if ($('#bank_name').val() == '') {
                   errorMessage  = "please enter bank name";
                   valid = false;
                }
                if( !valid && errorMessage.length > 0){
                   toastr.clear();
                   toastr.error(errorMessage);
                   return false;
                }
               $("#checkout_form").submit();
           }else if($("input[name='payment_type']:checked").val()==4) {                
                if ($("input[name='cardholder_name']").val() == '') {
                   errorMessage  = "please enter card name";
                   toastr.clear();
                   toastr.error(errorMessage); false; return false;
                }if ($("input[name='cardholder_number']").val() == '') {
                   errorMessage  = "please enter card number";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }if ($("input[name='month']").val() == '') {
                   errorMessage  = "please enter card expire month";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }if ($("input[name='year']").val() == '') {
                   errorMessage  = "please enter card expire year";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }                
                $("#checkout_form").submit();
           }else if($("input[name='payment_type']:checked").val()==5) {               
                if ($("input[name='cardholder_name1']").val() == '') {
                   errorMessage  = "please enter card name";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }if ($("input[name='cardholder_number1']").val() == '') {
                   errorMessage  = "please enter card number";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }if ($("input[name='month1']").val() == '') {
                   errorMessage  = "please enter card expire month";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }if ($("input[name='year1']").val() == '') {
                   errorMessage  = "please enter card expire year";
                   toastr.clear();
                   toastr.error(errorMessage); return false;
                }                
                $("#checkout_form").submit();
           }
           //$('#checkout_form').submit();
        })              
        
        
        $('.quantity_add').on('click', function() {
            var $qty = $(this).closest('div').find('.qty');
            var currentVal = parseInt($qty.val());            
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
            
            //get price and subtotal
              var $id = $(this).closest('div').find('.rowidvalue');
              var $price = $(this).closest('div').find('.amount');
              var $addon_name = $(this).closest('div').find('.addon_name');
              var tax_condition = $(this).closest('div').find('.addon_tax_condition').val();
              var id = parseInt($id.val());              
              var price = parseFloat($price.val());
              var addon_name = $addon_name.val();
              var sub_total = (price * parseInt($qty.val()));                           
              $('#addonsubtotal_'+id+'').html(sub_total.toFixed(2));
            //end price and subtotal
            addon_product_clone(id,addon_name,price,$qty.val(),tax_condition);
            //div show
              $('.subtotal_table').show();
            //div show  
        });

        var myArrayId = [];
        var myArrayIdquantity = [];
        $('.quantity_minus').on('click', function() {
            var $qty = $(this).closest('div').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
                //get price
                var minus_value = currentVal-1;
               
                //start update price and subtotal
                  var $id = $(this).closest('div').find('.rowidvalue');
                  var $price = $(this).closest('div').find('.amount');
                  var $addon_name = $(this).closest('div').find('.addon_name');
                  var tax_condition = $(this).closest('div').find('.addon_tax_condition').val();
                  var id = parseInt($id.val());              
                  var price = parseFloat($price.val());
                  var addon_name = $addon_name.val();
                  var sub_total = (price * parseInt($qty.val()-1));
                //end update price and subtotal
                if(minus_value==0){
                  var $id = $(this).closest('div').find('.rowidvalue');
                  var id = parseInt($id.val());
                  $('#addonsubtotal_'+id+'').text('0.00');
                  //remove td if zero assigned values
                  //reset array
                  
                  //update quantity                               
                      var idposition = myArrayId.indexOf(id);                      
                      myArrayIdquantity[idposition] = 0;
                      myArrayIdquantity = myArrayIdquantity.filter(val => val !== 0);                      
                      myArrayId = myArrayId.filter(e => e !==id );
                      $('input[name="addon_id[]"]').val(myArrayId);
                      $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                  //update quantity
                  
                  $('#addontr_'+id).remove();
                  $('#addontr_'+id).html('');
                  //remove td if zero assigned values
                  //start update price
                    /*var addition_price = parseFloat($('#total_amount').val()) - parseFloat(price);
                    $('#total_amount').val(addition_price.toFixed(2));
                    $('#subtotal_amount').val(addition_price.toFixed(2));
                    $('#subtotal').html(addition_price.toFixed(2));
                    $('#total').html(addition_price.toFixed(2));*/
                    var addition_price = parseFloat($('#subtotal_amount').val()) - parseFloat(price);                    
                    $('#subtotal_amount').val(addition_price.toFixed(2));
                    $('#subtotal').html(addition_price.toFixed(2));
                    var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                    $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                    $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                    $('#total_amount').val(getdata['total_amount'].toFixed(2));  
                    $('#total').html(getdata['total_amount'].toFixed(2));
                  //end update price 

                  if(myArrayId.length==0){
                     //div show
                       $('.subtotal_table').hide();
                     //div show 
                    }

                }else{
                  var sub_total = (price * parseInt(minus_value));                           
                  $('#addonsubtotal_'+id+'').html(sub_total.toFixed(2));
                    //append updated price values
                    //start update td
                    var quantity = '#addon_quantity'+id;
                    $(''+quantity+'').html(minus_value);
                    var addon_amount = '#addon_amount_'+id;
                    var sub_total = (price * parseInt(minus_value));
                    $(''+addon_amount+'').html(sub_total.toFixed(2));
                    //end update td
                    
                    //start update price
                    /*var addition_price = parseFloat($('#total_amount').val()) - parseFloat(price);
                    $('#subtotal_amount').val(addition_price.toFixed(2));
                    $('#total_amount').val(addition_price.toFixed(2));
                    $('#subtotal').html(addition_price.toFixed(2));
                    $('#total').html(addition_price.toFixed(2));*/
                    var addition_price = parseFloat($('#subtotal_amount').val()) - parseFloat(price);
                    $('#subtotal_amount').val(addition_price.toFixed(2));                    
                    $('#subtotal').html(addition_price.toFixed(2));
                    var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                    $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                    $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                    $('#total_amount').val(getdata['total_amount'].toFixed(2));  
                    $('#total').html(getdata['total_amount'].toFixed(2));
                    //end update price
                    //append updated price values

                    //update quantity                
                      var idposition = myArrayId.indexOf(id);                
                      myArrayIdquantity[idposition] = parseInt($qty.val());
                      $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                    //update quantity
                }
                //end price
            }
            
        });

        

     
     function addon_product_clone(id,addon_name,price,qty,tax_condition) {
              //start append product add
              const checkId = myArrayId.includes(id); 
              if(checkId){
                //start update td
                var quantity = '#addon_quantity'+id;
                $(''+quantity+'').html(qty);
                var addon_amount = '#addon_amount_'+id;
                var sub_total = (price * parseInt(qty));
                $(''+addon_amount+'').html(sub_total.toFixed(2));
                //end update td
            
                //start update price
                var multiple_price = (price * 1);
                var addition_price = parseFloat($('#subtotal_amount').val()) + parseFloat(multiple_price);
                /*$('#total_amount').val(addition_price.toFixed(2));
                $('#subtotal_amount').val(addition_price.toFixed(2));
                $('#subtotal').html(addition_price.toFixed(2));
                $('#total').html(addition_price.toFixed(2));*/
                $('#subtotal_amount').val(addition_price.toFixed(2));
                $('#subtotal').html(addition_price.toFixed(2));
                var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                $('#total_amount').val(getdata['total_amount'].toFixed(2));  
                $('#total').html(getdata['total_amount'].toFixed(2));
                //end update price
                
                //update quantity                
                  var idposition = myArrayId.indexOf(id);                
                  myArrayIdquantity[idposition] = parseInt(qty);
                  $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                //update quantity

              }else{

                //update price
                var multiple_price = (price * parseInt(qty));
                var addition_price = parseFloat($('#subtotal_amount').val()) + parseFloat(multiple_price);
                /*$('#total_amount').val(addition_price.toFixed(2));
                $('#subtotal_amount').val(addition_price.toFixed(2));
                $('#subtotal').html(addition_price.toFixed(2));
                $('#total').html(addition_price.toFixed(2));*/
                $('#subtotal_amount').val(addition_price.toFixed(2));
                $('#subtotal').html(addition_price.toFixed(2));                
                var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                $('#total_amount').val(getdata['total_amount'].toFixed(2));  
                $('#total').html(getdata['total_amount'].toFixed(2));
                //update price                
                
                //insert new td
                myArrayId.push(id);
                myArrayIdquantity.push(parseInt(qty));
                var trtd = "<tr id='addontr_"+id+"'><td class='text-start'><label>ADD-ON</label><br><b id='addon_name"+id+"'>"+addon_name+"</b></td>  <td class='text-start'><label></label><br><b id='addon_quantity"+id+"'>"+qty+"</b></td>   <td class='text-end'><label>RM</label><br><span class='delete_price' id='addon_amount_"+id+"'>"+price.toFixed(2)+"</span></td>   <td class='text-end'><label></label><br><a class='btn text-danger bg-danger-transparent btn-icon py-0' data-bs-toggle='tooltip'><span class='bi bi-trash fs-13'></span></a></td>   </tr>";
                $('#checkout_table tr:last').after(trtd);
                //add on product id and quantity store array
                $('input[name="addon_id[]"]').val(myArrayId);
                $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                //add on product id and quantity store array
              }              
              //console.log(myArrayId);
            //end append product add
     }

     function calculate_tax_amount(type,type_amount,sub_total,old_tax_amount,tax_condition) {
        var returnData=[];        
        if(type=='percentage'){
            if(tax_condition==1) {
              returnData['percentage_amount'] = parseFloat(old_tax_amount);
            } else {              
               var sum = 0;
                $('.add_on_amount_sub').each(function(){
                    sum += parseFloat($(this).text());
                });              
              returnData['percentage_amount'] = parseFloat(parseFloat(sub_total)-parseFloat(sum))*parseFloat(type_amount)/100;
            }            
            returnData['total_amount'] = parseFloat(sub_total)+parseFloat(returnData['percentage_amount']);
        }else{
            //console.log(type_amount);
            if(tax_condition==1) {
              if(old_tax_amount==0){
                 returnData['percentage_amount'] = parseFloat(0.00);
              }else{
                 returnData['percentage_amount'] = parseFloat(old_tax_amount);
              }
              returnData['total_amount'] = parseFloat(sub_total)+parseFloat(old_tax_amount);
            }else{
              returnData['percentage_amount'] = parseFloat(type_amount);
              returnData['total_amount'] = parseFloat(sub_total)+parseFloat(type_amount);
            }            
        }
        return returnData;
     }

    //click remove td
        $(document).on("click", ".bi-trash", function() {            
            var $deleteprice = $(this).closest('tr').find('.delete_price');
            //this only for addon product delete
            if($(this).closest('tr').find('.delete_price').attr('id')){
                var reset_qty = $(this).closest('tr').find('.delete_price').attr('id');
                var removeItem = reset_qty.split("_")[2];
                //end quantity
                var idposition = myArrayId.indexOf(parseInt(removeItem));                      
                myArrayIdquantity[idposition] = 0;
                myArrayIdquantity = jQuery.grep(myArrayIdquantity, function(value) {
                  return value != 0;
                });
                $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                //end quantity
                myArrayId = jQuery.grep(myArrayId, function(value) {
                  return value != removeItem;
                });
                console.log(myArrayId);
                $('#quantity'+reset_qty.split("_")[2]).val('0')
                $('#addonsubtotal_'+reset_qty.split("_")[2]).text('0.00');
                //remove add on product id
                $('input[name="addon_id[]"]').val(myArrayId);
                //remove add on product id
            }
            //this only for addon product delete
            //start update price
           // var addition_price = parseFloat($('#total_amount').val()) - parseFloat($deleteprice.html());
            var addition_price = parseFloat($('#subtotal_amount').val()) - parseFloat($deleteprice.html());            
            $('#subtotal_amount').val(addition_price.toFixed(2));
            $('#subtotal').html(addition_price.toFixed(2));
            var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val());
            $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
            $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
            $('#total_amount').val(getdata['total_amount'].toFixed(2));  
            $('#total').html(getdata['total_amount'].toFixed(2));
            //end update price
            $(this).closest("tr").empty();
            if(myArrayId.length==0){
                //div show
                  $('.subtotal_table').hide();
                //div show 
            }

        });
        //click remove td
  
    });
    
    </script>
    @endsection
