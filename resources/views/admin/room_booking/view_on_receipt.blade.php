@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
/*@media print {
    #si-printer {
        display :  none !important;
    }#si-printer1 {
        display :  none !important;
    }
}
.print:last-child {
     page-break-after: auto;
}*/
</style>


    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Receipt</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Receipt</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                    <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img logo-3" alt="logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{@$setting->address}}
                                                    </address>
                                                </div>
                                            </div>
                                            <!-- <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <h3>Booking No-{{@$receipt->booking_number}}</h3>
                                                <h5><b>Check-in:</b> {{\Carbon\carbon::parse(@$receipt->check_in)->format('d-m-Y')}}</h5>
                                                <h5><b>Check-out:</b> {{\Carbon\carbon::parse(@$receipt->check_out)->format('d-m-Y')}}</h5>
                                                <h5><b>Date:</b>{{\Carbon\Carbon::parse(@$booking_payment->created_at)->format('d-m-Y')}}</h5>
                                            </div> -->
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="h4">Receive Payment From: {{ucwords(@$receipt->getbooking_data->guest->name)}}</p>
                                                <p class="fs-15 fw-semibold mb-0">Receipt: #{{@$receipt->receipt_no}}</p> 
                                                <p class="fs-15 fw-semibold mb-0">Date: {{Carbon\Carbon::parse(@$receipt->created_at)->format('d-m-Y')}}</p> 
                                                <p class="fs-15 fw-semibold mb-0">Payment Type: {{ucwords(@$receipt->payment_type)}}</p> 
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> -->
                                        </div><br>                                       
                                     
                                     <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap table-striped">
                                                <tbody>
                                                    <tr class="">
                                                        <td class="text-end">
                                                            @if(@$receipt->get_payment_data->refund_payment_check=='1' && @$receipt->get_payment_data->invoice_type =='refund')
                                                             Refund for Booking Ref No
                                                            @else
                                                             Payment for Booking Ref No
                                                            @endif
                                                             : {{@$receipt->getbooking_data->booking_number}}</td>
                                                    </tr>
                                                    <tr class="">                                                        
                                                        <td class="text-end">Total: RM @if(@$receipt->get_payment_data->refund_payment_check=='1') - @endif {{@$receipt->receipt_amount}}</td>
                                                    </tr>                                                    
                                                </tbody>                                                
                                            </table>
                                            <br>This is a computer generated document. No signature is required.
                                    <div class="card-footer">                                                                         
                                        <button id="si-printer" type="button" class="btn btn-success mb-1 text-end" onclick="javascript:window.print();"><i class="si si-printer"></i>Print</button>
                                    </div>                                    
                                </div>
                                </div>
                                   
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

       

        @endsection

    @section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    });
</script>
    @endsection
