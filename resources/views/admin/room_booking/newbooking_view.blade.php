@extends('admin.layouts.app')
    @section('styles')<style type="text/css">.styleee{color: red;}.flex-1{display: none;}</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Booking Addon</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Booking Page</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row row-cards">
                            <!-- COL-END -->
                            <div class="col-xl-12 col-lg-12">
                                <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-7">
                <div class="card cart">
                    <!-- <div class="card-header">                        
                        <span class="fw-bold text-success">Guest Name:</span>&nbsp;<b><span id="show_guest_name"></span></b>
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-vcenter">
                                <thead>
                                    <tr class="border-top">
                                        <th>Add-On</th>
                                        <th>Price</th>
                                        <th class="table_single_header">Quantity</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>

                                @if(!$list_add_on_product->isEmpty())
                                   @foreach($list_add_on_product as $key=>$data)
                                    <tr>                                       
                                        <td>{{$data->add_on_name}}</td>
                                        <td >RM{{$data->add_on_amount}}</td>
                                        <td>
                                            <div class="input-group input-indec input-indec1">
                                                <span class="input-group-btn">
                                                    <button type="button" class="quantity_minus btn btn-white btn-number btn-icon br-7" >
                                                        <i class="fa fa-minus text-muted"></i>
                                                    </button>
                                                </span>
                                                <input type="hidden" class="form-control text-center rowidvalue" value="{{$data->id}}">
                                                <input type="hidden" class="form-control text-center addon_name" value="{{$data->add_on_name}}">
                                                <input type="hidden" class="form-control text-center amount" value="{{$data->add_on_amount}}">
                                                <input type="hidden" class="form-control text-center addon_tax_condition" value="{{$data->service_charge_enable}}">
                                                <input type="text" id="quantity{{$data->id}}" name="quantity" class="form-control text-center qty" value="0">
                                                <span class="input-group-btn">
                                                    <button type="button" class="quantity-right-plus btn btn-white btn-number btn-icon br-7 quantity_add">
                                                        <i class="fa fa-plus text-muted"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="fw-bold">RM<span class="@if(@$data->service_charge_enable==1)add_on_amount_sub @else add_on_amount_addition @endif" id="addonsubtotal_{{$data->id}}">0.00</span></td>                                        
                                    </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="input-group mb-1">
                                    <input type="text" class="form-control" placeholder="Search ...">
                                    <span class="input-group-text btn btn-primary">Apply Coupon</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 text-end"><a href="javascript:void(0)" class="btn btn-default disabled btn-md">Update Cart</a></div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="col-lg-12 col-xl-5 col-sm-12 col-md-12">
                <div class="card">
                    <dl class="row card-body py-2">
                        <dt class="col-sm-4">Check-In Date</dt>
                        <dd class="col-sm-6"> {{Request::get('checkin')}} </dd>
                        <dt class="col-sm-4">Check-Out Date</dt>
                        <dd class="col-sm-6"> {{Request::get('checkout')}} </dd>
                        <dt class="col-sm-4">Guest Name</dt>
                        <dd class="col-sm-6"><span id="show_guest_name"></span></dd>                                
                    </dl>
                    <div class="card-body py-2">
                        <div class="table-responsive">
                            <table class="table table-borderless text-nowrap mb-0" id="checkout_table">
                                <tbody>
                                <form id="checkout_form" class="checkout_form" action="{{url('admin/bookinglisting/newbooking/save')}}" method="post" enctype="multipart">
                                    @csrf
                                @if($finalData)
                                        <?php
                                          $start_date = \Request::get('checkin');
                                          $end_date = \Request::get('checkout');
                                        ?>
                                        <thead class="table-primary">
                                           <tr>                                                        
                                             <th>Room / Item</th>
                                             <th>Room Number</th>
                                             <th class="text-end">Amount</th>
                                             <th class="text-end"></th>
                                           </tr>
                                        </thead>
                                  @foreach($finalData as $key=>$data)
                                    <tr>
                                        <input type="hidden" class="roomtypeid" value="{{@$data->id}}_{{@$key}}">
                                        <input type="hidden" name="roomname[{{$data->id}}][]" value="{{@$data->room_type}}">
                                        <input type="hidden" name="roomprice[{{$data->id}}][]" value="{{@$data->get_rooms_price($data->id,$start_date,$end_date,null)}}">
                                        <td class="text-start"> 
                                            <label>Room Name</label><br>
                                            <b class="room_type_name">{{@$data->room_type}}</b>
                                        </td>
                                        <td class="text-center">
                                            <!-- <label>Room Number</label><br> -->
                                            <select class="form-control chosen-select2 check_room_no" name="roomsnumber[{{$data->id}}][]">
                                                   @if($data->availableRoomsNumber!=='')
                                                     <!-- <option value="">Choose...</option> -->
                                                     @foreach($data->availableRoomsNumber as $key=>$val)
                                                     <option value="{{$key}}">{{$val}}</option>
                                                     @endforeach
                                                   @endif
                                            </select>
                                        </td>
                                        <td class="text-end">
                                            <label>{{@$numberOfDays}} Night</label><br>
                                            <input class="numberOfDays" type="hidden" value="{{@$numberOfDays}}">
                                            <span class="delete_price">{{@$data->roomsprice}}</span>
                                        </td>
                                        <td class="text-end">
                                            <label></label><br>
                                            <a class="btn text-danger bg-danger-transparent btn-icon py-0" data-bs-toggle="tooltip"><span class="bi bi-trash fs-13"></span></a>
                                        </td>
                                    </tr>
                                  @endforeach
                                @endif
                                <?php $getTax = Helper::getTaxSetting(); $tax_price = Helper::getTaxAmount($totalroomprice);

                                 ?>
                                <input type="hidden" name="checkin" id="checkin_date" value="{{Request::get('checkin')}}">
                                <input type="hidden" name="checkout" id="checkout_date" value="{{Request::get('checkout')}}">
                                <input type="hidden" name="booking_type" id="booking_type" value="{{@$booking_type}}">
                                <input type="hidden" name="guest_id" id="guest_id" value="">
                                <input type="hidden" name="number_of_rooms" id="number_of_rooms" value="{{Request::get('number_of_rooms')}}">
                                <input type="hidden" name="subtotal_amount" id="subtotal_amount" value="{{@$totalroomprice}}">
                                <input type="hidden" name="tax_amount" id="tax_amount" value="{{$tax_price}}">
                                <input type="hidden" name="total_amount" id="total_amount" value="{{@$totalroomprice+$tax_price}}">
                                <input type="hidden" name="tax_type" id="tax_type" value="{{$getTax['type']}}_{{$getTax['value']}}">                   
                                <input type="hidden" name="addon_id[]" value="">
                                <input type="hidden" name="addon_quantity[]" value="">
                                <input type="hidden" name="discount_details" id="discount_details" value="">
                                <input type="hidden" name="discount_amount" id="discount_amount" value="0.00">
                                <input type="hidden" name="room_type_id" id="room_type_id" value="{{@$room_type_id}}">
                                </form>

                                </tbody>
                            </table>
                        </div>
                    </div>  
                    <div class="card">                   
                        <div class="card-body py-2">
                            <div class="table-responsive">
                                <table class="table table-borderless text-nowrap mb-0">
                                    <tbody>
                                        <tr>
                                            <td class="text-start">Sub Total</td>
                                            <td class="text-end">RM <span class="ms-1 fw-bold  ms-auto text-success" id="subtotal">{{@$totalroomprice}}</span></td>
                                        </tr> 
                                        <tr>
                                            <td class="text-start">Service Charge @if($getTax['type']=='percentage')@ {{$getTax['value']}}% @endif</td>

                                            @if($getTax['type']=='percentage')
                                            <td class="text-end">RM <span class="fw-bold text-success" id="taxamount">{{$tax_price}}</span></td>
                                            @else
                                            <td class="text-end">RM <span class="fw-bold text-success" id="taxamount">{{$tax_price}}</span></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-start col-md-7 col-sm-6">
                                                <div class="input-group mb-0">
                                                    <input type="text" id="voucher_coupon_name" name="voucher_coupon_name" class="form-control" placeholder="Enter Voucher or Coupon">
                                                    <span class="input-group-text btn btn-primary @if($coupon_voucher_count==0)disabled @endif" id="voucher_coupon_button">Apply</span>
                                                </div>
                                                <span id="voucher_coupon_invalid"></span>
                                            </td>
                                            <td class="text-end">RM -<span class="fw-bold text-success voucher_coupon_price">0.00</span></td>
                                        </tr>
                                       
                                        <tr>
                                            <td class="text-start fs-18">Total Bill</td>
                                            <td class="text-end">RM<span class="ms-1 fw-bold fs-23" id="total">{{@$totalroomprice+$tax_price}}</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="btn-list">
                                <button class="btn btn-primary mt-3" id="previous_page">Back</button>
                                <button class="btn btn-success bg-success-gradient mt-3" data-bs-toggle="modal" data-bs-target="#scrollingmodal1">New Guest</button>
                                <button class="btn btn-warning bg-warning-gradient mt-3"><a class="text-white" data-bs-effect="effect-scale" data-bs-toggle="modal" href="#modaldemo8">Existing Guest</a></button>
                                <button class="btn btn-primary mt-3" id="submit_button">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
                                <!-- COL-END -->
                            </div>
                            <!-- ROW-1 CLOSED -->
                        </div>
                        <!-- ROW-1 END-->
        <div class="modal fade" id="modaldemo8">
            <div class="modal-dialog modal-dialog-centered text-center" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Existing Guests</h6><button aria-label="Close" class="btn-close" data-bs-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <select class="form-control chosen-select" id="existing_guest">
                                    <option value="">Choose...</option>
                                    @if(!$guest_list->isEmpty())
                                     @foreach($guest_list as $key=>$data)
                                     <option value="{{$data->id}}">{{ucfirst($data->name)}} ({{$data->ic_passport_no}})</option>
                                     @endforeach
                                    @endif
                                </select>
                    </div>
                    <!-- <div class="modal-footer">
                        <button class="btn btn-primary">Save changes</button> <button class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>

        <!--Existing user Modal -->
       <!--  <div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Existing guests</h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <div class="modal-body">
                        <div class="col-auto">
                                <label class="visually-hidden" for="autoSizingSelect">Preference</label>
                                <select class="form-control chosen-select" id="existing_guest">
                                    <option value="">Choose...</option>
                                    @if(!$guest_list->isEmpty())
                                     @foreach($guest_list as $key=>$data)
                                     <option value="{{$data->id}}">{{ucfirst($data->name)}} ({{$data->ic_passport_no}})</option>
                                     @endforeach
                                    @endif
                                </select>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Existing user Model -->

        <!--New Guest user Modal -->
        <div class="modal fade" id="scrollingmodal1" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Guest User</h5>
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                    </div>
                    <div class="modal-body">
                        <form id="create">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="exampleInputname">Guest Name</label>
                                <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name" placeholder="Guest Name">
                              </div> @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
                            </div>
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                              <label for="exampleInputnumber">Address</label>
                              <br>
                              <textarea name="address" id="address" class="form-control" rows="1" placeholder="Enter Address">{{old('address')}}</textarea> @if($errors->has('address')) <div class="error">{{ $errors->first('address') }}</div> @endif
                            </div>
                            </div>            
                          </div>
                          <div class="row">            
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="exampleInputname1">IC/Passport No</label>
                                <input type="text" value="{{old('ic_passport_no')}}" name="ic_passport_no" class="form-control" id="ic_passport_no" placeholder="IC/Passport No">
                              </div> @if($errors->has('ic_passport_no')) <div class="error">{{ $errors->first('ic_passport_no') }}</div> @endif
                            </div>                             
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="status">Country</label>
                                <select id="country" name="country" class="form-control chosen-select1">
                                  @foreach(@$country as $key=>$value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>           
                          </div>          
                          <div class="row">
                             <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                              <label for="exampleInputnumber">Email</label>
                              <br>
                              <input type="email" name="email" value="{{old('email')}}" class="form-control" id="email" placeholder="Email"> @if($errors->has('email')) <div class="error">{{ $errors->first('email') }}</div> @endif
                            </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="status">State</label>
                                <select id="state" name="state" class="form-control chosen-select1">
                                  <option value="">Please select</option>
                                  @foreach(@$state as $key=>$value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>                      
                          </div>
                          <div class="row">
                            <!-- <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                  <a href="javascript:void(0)" class="input-group-text bg-white text-muted zmdi zmdi-eye toggle-password">
                                    <i class="" aria-hidden="true"></i>
                                  </a>
                                  <input class="input100 form-control" type="password" placeholder="Password" name="password" id="password">
                                </div> <span id="password_error"></span> @if($errors->has('password')) <div class="error">{{ $errors->first('password') }}</div> @endif
                              </div>
                            </div> -->
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                              <label for="exampleInputnumber">Contact Number</label>
                              <br>
                              <input type="text" name="contact_number" value="{{old('contact_number')}}" class="form-control" id="contact_number" placeholder="Contact Number" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$"> @if($errors->has('contact_number')) <div class="error">{{ $errors->first('contact_number') }}</div> @endif
                            </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="status">City</label>
                                <select id="city" name="city" class="form-control chosen-select1">
                                  <option value="">Please select</option>                 
                                </select>
                              </div>
                            </div>            
                          </div>
                          <div class="row">                            
                             <div class="col-lg-6 col-md-12">
                              <label for="exampleInputnumber">Postcode</label>
                              <br>
                              <input type="text" name="postcode" value="{{old('postcode')}}" class="form-control" id="postcode" placeholder="Postcode"> @if($errors->has('postcode')) <div class="error">{{ $errors->first('postcode') }}</div> @endif
                            </div>             
                          </div>        
                          <div class="row" style="display:none;">            
                             <div class="col-lg-6 col-md-12">
                              <div class="form-group">
                                <label for="status">Status</label>
                                <select id="status" name="status" class="form-control chosen-select1">
                                  <option value="0" selected>Active</option>
                                  <option value="1">Suspend</option>
                                </select>
                              </div>
                            </div>                             
                          </div>
                      </div>
                    </form>
                    </div>
                    <div class="modal-footer">                        
                        <button class="btn btn-primary" id="create_guest_user">Next</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- New Guest user Model -->

        @endsection

    @section('scripts')

    <script type="text/javascript">
    $( document ).ready(function() {
        //coupon voucher div
        $('#voucher_coupon_invalid').hide();
        $("#voucher_coupon_button").click(function () {
            $('#voucher_coupon_invalid').hide();
            if($('#discount_details').val()==''){
                if($('#voucher_coupon_name').val()=='') {
                    $('#voucher_coupon_invalid').html('Required').css('color','red').delay(2000).fadeOut('slow');
                    $('#voucher_coupon_invalid').show();
                    return false;
                }
            }
            var checkin_date = "{{Request::get('checkin')}}";
            var checkout_date = "{{Request::get('checkout')}}";
            var code = $('#voucher_coupon_name').val();
            var room_type_id = $('#room_type_id').val();
            //console.log(room_type_id);
            //return false;
            $.ajax({
            url: site_url + '/admin/coupon_voucher/applied',
            type: 'post',
            dataType: 'json',
            data: {
              code:code,room_type_id:room_type_id,checkin_date:checkin_date,checkout_date:checkout_date,_token:'{{csrf_token()}}'
            },
            success: function(data) {
                $('#voucher_coupon_invalid').hide();
                if(data.response==3) {
                    toastr.clear();
                    toastr.error('please check parameter');
                    return false;
                }
                if(data.response==0) {
                    //success
                         //voucher coupon                         
                           $('#voucher_coupon_invalid').show();
                           $('.voucher_coupon_price').html(data.amount);                       
                           $('#voucher_coupon_invalid').html(data.message).css('color','green').delay(3000).fadeOut('slow');
                           //price applied                        
                            var get_old_total = $('#total_amount').val();
                            //new amount
                            if($('#discount_amount').val()=='0.00') {
                              var new_amount = parseFloat(get_old_total)-parseFloat(data.amount);
                              $('#total_amount').val(new_amount.toFixed(2)); $('#total').html(new_amount.toFixed(2));
                            }else{
                                if(data.type=='coupon_percentage') {
                                  if($('#discount_amount').val()!=data.amount) {
                                      if($('#discount_amount').val()<data.amount){
                                        var different_amount = parseFloat($('#discount_amount').val())-parseFloat(data.amount);
                                        var new_amount = parseFloat(get_old_total)+parseFloat(different_amount);
                                        //return false;
                                      }else{
                                        var different_amount = parseFloat($('#discount_amount').val())-parseFloat(data.amount);
                                        var new_amount = parseFloat(get_old_total)+parseFloat(different_amount);
                                        //return false;
                                      }
                                      new_amount = new_amount.toFixed(2);                                      
                                      $('#total_amount').val(new_amount); $('#total').html(new_amount);
                                   }
                                }
                            }                          
                            //inside form 
                            $('#discount_details').val(data.details);
                            $('#discount_amount').val(data.amount);                         
                         //voucher coupon
                    return false;
                } else {
                    //invalid
                        //voucher coupon                         
                           $('#voucher_coupon_invalid').show();
                           $('.voucher_coupon_price').html(data.amount);                       
                           $('#voucher_coupon_invalid').html(data.message).delay(3000).fadeOut('slow');
                           //price applied                        
                            var get_old_total = $('#total_amount').val();
                            var get_old_discount = $('#discount_amount').val();
                            //new amount
                            if($('#discount_amount').val()!='0.00') {
                              var new_amount = parseFloat(get_old_total)+parseFloat(get_old_discount);
                              $('#total_amount').val(new_amount.toFixed(2)); $('#total').html(new_amount.toFixed(2));
                            }
                           //inside form
                           $('#discount_details').val('');
                           $('#discount_amount').val(data.amount);                        
                        //voucher coupon
                    return false;
                }                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                 alert("Error: " + errorThrown + " , Please try again");
            }
          });

        });
        //coupon voucher div




        $("#previous_page").click(function () {
            var url = "{{ url()->previous() }}";
            window.history.back();
        });
                
        $("#submit_button").hide();
        $("#existing_guest").change(function () {
            //alert($(this).val());
            $('#show_guest_name').html($(this).find('option:selected').html());
            $('#guest_id').val($(this).find('option:selected').val());            
            $('.btn-close').click();
            $("#submit_button").show();
        });

        $("#create_guest_user").click(function () {
           if ($('#create').valid()) {
              saveGuestUser();
            }else{
                return false;
            }
        });

        $("#submit_button").click(function () {
           if($('#total_amount').val()==0.00){
              location.reload();
           }else{
            if(!$('#guest_id').val()){
                //alert('please select gust user');
                toastr.clear();
                toastr.error('please select guest user');
                return false;
            }
              var hasInput=true;
              var selectedData = [];

              $('.check_room_no').each(function () {
                //console.log($(this).find('option:selected').val());
               if(!$(this).find('option:selected').val()) {
                $(this).closest('td').find('.select2').css("border","1px solid red");
                hasInput=false;
               }else {
                $(this).closest('td').find('.select2').css("border","");
               }
                selectedData.push($(this).find('option:selected').val());
              });
              //console.log(hasInput);
              //return false;
              if(!hasInput){
                //alert('Please select all rooms');
                toastr.clear();
                toastr.warning('Please select all rooms');
                return false;
               }            
            var duplicates = $.grep(selectedData, function(element, index){
                return $.inArray(element, selectedData) !== index;
            });
            if (duplicates.length !== 0) {
                toastr.clear();
                toastr.warning('Duplicate room numbers is selected');
                return false;
            }
            $('#checkout_form').submit();
           }
           //$('#checkout_form').submit();
        });

        

        function saveGuestUser(form) {
            var name = $("input[name='name']").val();
            var address = $("#address").val();
            var ic_passport_no = $("input[name='ic_passport_no']").val();            
            var country = $("#country").val();
            var email = $("input[name='email']").val();
            var state = $("#state").val();
            var password = $("#password").val();
            var city = $("#city").val();
            var contact_number = $("#contact_number").val();
            var postcode = $("#postcode").val();
            var status = $("#status").val();            
            $.ajax({
            url: site_url + '/admin/bookinglisting/newbooking/save_guest_user',
            type: 'post',
            dataType: 'json',
            data: {
              name:name,address:address,ic_passport_no:ic_passport_no,country:country,email:email,state:state,password:password,city:city,contact_number:contact_number,postcode:postcode,status:status,_token:'{{csrf_token()}}'
              //selectedData: selectedData,
              //_token: '{{csrf_token()}}'
            },
            success: function(data) {
                if(data.response==0) {
                    $('#show_guest_name').html(data.message);
                    $('#guest_id').val(data.user_id);
                    $('.btn-close').click();
                    $("#submit_button").show();
                    $("#create")[0].reset();
                    return false;
                } else {
                    //alert(data.message);
                    toastr.clear();
                    toastr.error(data.message);
                    if($('#guest_id').val()==''){
                       $("#submit_button").hide();
                    }                    
                   /* $('#show_guest_name').html('');
                    $('#guest_id').val('');*/
                    //$('.btn-close').click();
                    return false;
                }                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                 alert("Error: " + errorThrown + " , Please try again");
            }
          });
        }

        
        var number_of_rooms = "{{\Request::get('number_of_rooms')}}";        
        $('.quantity_add').on('click', function() {
            var $qty = $(this).closest('div').find('.qty');
            var currentVal = parseInt($qty.val());            
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
            if($('#total_amount').val()==0.00){
              location.reload();
           }
            //get price and subtotal
              var $id = $(this).closest('div').find('.rowidvalue');
              var $price = $(this).closest('div').find('.amount');
              var $addon_name = $(this).closest('div').find('.addon_name');
              var tax_condition = $(this).closest('div').find('.addon_tax_condition').val();
              var id = parseInt($id.val());              
              var price = parseFloat($price.val());
              var addon_name = $addon_name.val();
              var sub_total = (price * parseInt($qty.val()));                           
              $('#addonsubtotal_'+id+'').html(sub_total.toFixed(2));
            //end price and subtotal
            addon_product_clone(id,addon_name,price,$qty.val(),tax_condition);
        });

        var myArrayId = [];
        var myArrayIdquantity = [];
        $('.quantity_minus').on('click', function() {
            var $qty = $(this).closest('div').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
                //get price
                var minus_value = currentVal-1;
               
                //start update price and subtotal
                  var $id = $(this).closest('div').find('.rowidvalue');
                  var $price = $(this).closest('div').find('.amount');
                  var $addon_name = $(this).closest('div').find('.addon_name');
                  var tax_condition = $(this).closest('div').find('.addon_tax_condition').val();
                  var id = parseInt($id.val());              
                  var price = parseFloat($price.val());
                  var addon_name = $addon_name.val();
                  var sub_total = (price * parseInt($qty.val()-1));
                //end update price and subtotal
                if(minus_value==0){
                  var $id = $(this).closest('div').find('.rowidvalue');
                  var id = parseInt($id.val());
                  $('#addonsubtotal_'+id+'').text('0.00');
                  //remove td if zero assigned values
                  //reset array
                  
                  //update quantity                               
                      var idposition = myArrayId.indexOf(id);                      
                      myArrayIdquantity[idposition] = 0;
                      myArrayIdquantity = myArrayIdquantity.filter(val => val !== 0);                      
                      myArrayId = myArrayId.filter(e => e !==id );
                      $('input[name="addon_id[]"]').val(myArrayId);
                      $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                  //update quantity
                  
                  $('#addontr_'+id).remove();
                  $('#addontr_'+id).html('');
                  //remove td if zero assigned values
                  //start update price
                    var addition_price = parseFloat($('#subtotal_amount').val()) - parseFloat(price);                    
                    $('#subtotal_amount').val(addition_price.toFixed(2));
                    $('#subtotal').html(addition_price.toFixed(2));
                    var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                    $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                    $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                    $('#total_amount').val(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));  
                    $('#total').html(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));
                  //end update price                   
                      

                }else{
                  var sub_total = (price * parseInt(minus_value));                           
                  $('#addonsubtotal_'+id+'').html(sub_total.toFixed(2));
                    //append updated price values
                    //start update td
                    var quantity = '#addon_quantity'+id;
                    $(''+quantity+'').html(minus_value);
                    var addon_amount = '#addon_amount_'+id;
                    var sub_total = (price * parseInt(minus_value));
                    $(''+addon_amount+'').html(sub_total.toFixed(2));
                    //end update td
                    
                    //start update price
                    var addition_price = parseFloat($('#subtotal_amount').val()) - parseFloat(price);
                    $('#subtotal_amount').val(addition_price.toFixed(2));                    
                    $('#subtotal').html(addition_price.toFixed(2));
                    var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                    $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                    $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                    $('#total_amount').val(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));  
                    $('#total').html(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));
                    //end update price
                    //append updated price values

                    //update quantity                
                      var idposition = myArrayId.indexOf(id);                
                      myArrayIdquantity[idposition] = parseInt($qty.val());
                      $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                    //update quantity
                }
                //end price
            }
            
        });

        
     function calculate_tax_amount(type,type_amount,sub_total,old_tax_amount,tax_condition) {
        var returnData=[];        
        if(type=='percentage'){
            if(tax_condition==1) {              
              returnData['percentage_amount'] = parseFloat(old_tax_amount);
            } else {              
               var sum = 0;
                $('.add_on_amount_sub').each(function(){
                    sum += parseFloat($(this).text());
                });              
              //console.log();
              //return false;
              returnData['percentage_amount'] = parseFloat(parseFloat(sub_total)-parseFloat(sum))*parseFloat(type_amount)/100;
            }            
            returnData['total_amount'] = parseFloat(sub_total)+parseFloat(returnData['percentage_amount']);
        }else{
            returnData['percentage_amount'] = parseFloat(old_tax_amount);
            returnData['total_amount'] = parseFloat(sub_total)+parseFloat(type_amount);
        }
        return returnData;
     }
     
     function addon_product_clone(id,addon_name,price,qty,tax_condition) {
              //start append product add
              const checkId = myArrayId.includes(id); 
              if(checkId){
                //start update td
                var quantity = '#addon_quantity'+id;
                $(''+quantity+'').html(qty);
                var addon_amount = '#addon_amount_'+id;
                var sub_total = (price * parseInt(qty));
                $(''+addon_amount+'').html(sub_total.toFixed(2));
                //end update td
            
                //start update price
                var multiple_price = (price * 1);
                var addition_price = parseFloat($('#subtotal_amount').val()) + parseFloat(multiple_price);                
                $('#subtotal_amount').val(addition_price.toFixed(2));
                $('#subtotal').html(addition_price.toFixed(2));
                var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                $('#total_amount').val(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));  
                $('#total').html(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));                
                //end update price
                
                //update quantity                
                  var idposition = myArrayId.indexOf(id);                
                  myArrayIdquantity[idposition] = parseInt(qty);
                  $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                //update quantity

              }else{

                //update price
                var multiple_price = (price * parseInt(qty));
                var addition_price = parseFloat($('#subtotal_amount').val()) + parseFloat(multiple_price);
                $('#subtotal_amount').val(addition_price.toFixed(2));
                $('#subtotal').html(addition_price.toFixed(2));
                var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val(),tax_condition);
                $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
                $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
                $('#total_amount').val(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));  
                $('#total').html(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));
                //$('#total').html(addition_price.toFixed(2));
                //update price                
                
                //insert new td
                myArrayId.push(id);
                myArrayIdquantity.push(parseInt(qty));
                var trtd = "<tr id='addontr_"+id+"'><td class='text-start'><label>ADD-ON</label><br><b id='addon_name"+id+"'>"+addon_name+"</b></td>  <td class='text-start'><label></label><br><b id='addon_quantity"+id+"'>"+qty+"</b></td>   <td class='text-end'><label>RM</label><br><span class='delete_price' id='addon_amount_"+id+"'>"+price.toFixed(2)+"</span></td>   <td class='text-end'><label></label><br><a class='btn text-danger bg-danger-transparent btn-icon py-0' data-bs-toggle='tooltip'><span class='bi bi-trash fs-13'></span></a></td>   </tr>";
                $('#checkout_table tr:last').after(trtd);
                //add on product id and quantity store array
                $('input[name="addon_id[]"]').val(myArrayId);
                $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                //add on product id and quantity store array
              }              
              //console.log(myArrayId);
            //end append product add
     }

    //click remove td
        $(document).on("click", ".bi-trash", function() {
            if($('td:contains("Room Name")').length==1) {
                if($('td:contains("ADD-ON")').length==0) {
                  //alert('Can\'t remove this room');
                  toastr.clear();
                  toastr.warning('Can\'t remove this room');
                  return false;
                }
            }
            
            var $deleteprice = $(this).closest('tr').find('.delete_price');
            //this only for addon product delete
            if($(this).closest('tr').find('.delete_price').attr('id')){                
                var reset_qty = $(this).closest('tr').find('.delete_price').attr('id');
                var removeItem = reset_qty.split("_")[2];
                //end quantity
                var idposition = myArrayId.indexOf(parseInt(removeItem));                      
                myArrayIdquantity[idposition] = 0;
                myArrayIdquantity = jQuery.grep(myArrayIdquantity, function(value) {
                  return value != 0;
                });
                $('input[name="addon_quantity[]"]').val(myArrayIdquantity);
                //end quantity
                myArrayId = jQuery.grep(myArrayId, function(value) {
                  return value != removeItem;
                });
                console.log(myArrayId);
                $('#quantity'+reset_qty.split("_")[2]).val('0')
                $('#addonsubtotal_'+reset_qty.split("_")[2]).text('0.00');
                //remove add on product id
                $('input[name="addon_id[]"]').val(myArrayId);
                //remove add on product id
            }
            //this only for addon product delete
            //start update price
            var numberOfDays = $(this).closest('tr').find('.numberOfDays').val();
            if(numberOfDays){
                var sub_amount = parseInt(numberOfDays)*parseFloat($deleteprice.html());
            }else{
                var sub_amount = parseFloat($deleteprice.html());
            }            
            //console.log(sub_amount);
            //return false;
            var addition_price = parseFloat($('#subtotal_amount').val()) - parseFloat(sub_amount);            
            $('#subtotal_amount').val(addition_price.toFixed(2));
            $('#subtotal').html(addition_price.toFixed(2));
            var getdata = calculate_tax_amount($('#tax_type').val().split('_')[0],$('#tax_type').val().split('_')[1],addition_price.toFixed(2),$('#tax_amount').val());
            $('#taxamount').html(getdata['percentage_amount'].toFixed(2));
            $('#tax_amount').val(getdata['percentage_amount'].toFixed(2));
            $('#total_amount').val(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));  
            $('#total').html(getdata['total_amount'].toFixed(2)-parseFloat($('#discount_amount').val()));
            //end update price
            $(this).closest("tr").empty();

            //remove room after voucher and coupon apply.
              var room_type_id = $('#room_type_id').val().split(",");
              var removeItem = $(this).closest('tr').find('.roomtypeid').val();
              room_type_id = jQuery.grep(room_type_id, function(value) {
                return value != removeItem;
              });
              $('#room_type_id').val(room_type_id.join(","));
              if($('#discount_details').val()!='') {
                 $("#voucher_coupon_button").trigger("click");
              }             
            //remove room after voucher and coupon apply.

        });
        //click remove td

     //start room number
        $('.chosen-select2').css('width', '100%');
        $('.chosen-select2').select2({
            placeholder: "Select",
            maximumSelectionLength:10
        });
        //end start room number

        //end new guest user
        $('.chosen-select1').css('width', '100%');
        $('.chosen-select1').select2({
            placeholder: "Select",
            dropdownParent: $('#scrollingmodal1')
        });
        //end new guest user

        //start existing guest user
        $('.chosen-select').css('width', '100%');
        $('.chosen-select').select2({
            placeholder: "Select",
            dropdownParent: $('#modaldemo8')
        });
        //end existing guest user

    $("#create").validate({
   /* submitHandler: function(form) {
      form.submit();
    },*/
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
        //minlength:8,
      },
      ic_passport_no: {
        required: true
      },
      contact_number: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name",
      },
      email: {
        required: "Please enter email",
      },
      ic_passport_no: {
        required: "Please enter passport or ic number",
      },
      contact_number: {
        required: "Please enter contact number",
      }
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "password") {
            error.appendTo("#password_error").css('color','#FF0000').css("fontSize", "14px").css('float','center');
        }else {
            error.insertAfter(element);
        }                
      },
  });

    //get city
        $('#state').on('change', function() {
          var value = $(this).val();
          $.ajax({
            url: site_url + '/admin/guestuser/ajax_get_city',
            type: 'post',
            dataType: 'json',
            data: {
              id: value,
              //selectedData: selectedData,
              _token: '{{csrf_token()}}'
            },
            success: function(data) {
                var html = '<option value="">Please select</option>';
                $.each(data.data, function (i, value) {
                    html += ('<option value="' + value.id + '">' + value.name + '</option>');
                });
                $("#city").html(html);
            }
          });
        });
        //get city

        //password hide show
        $("body").on('click', '.toggle-password', function() {
            var input = $("#password");
            if (input.attr("type") === "password") {
              $(this).removeClass("zmdi-eye");
              $(this).addClass("zmdi zmdi-eye-off");
              input.attr("type", "text");
            } else {
              $(this).addClass("zmdi zmdi-eye");
              $(this).removeClass("zmdi-eye-off");
              input.attr("type", "password");
            }
          });
        //password hide show
    });
    
    </script>
    @endsection
