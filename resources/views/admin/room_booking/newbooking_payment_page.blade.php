@extends('admin.layouts.app')
@section('styles')
<style type="text/css">
@media print {
    #si-printer {
        display :  none !important;
    }#si-printer1 {
        display :  none !important;
    }
}
.print:last-child {
     page-break-after: auto;
}
</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Payment</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Payment</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                            <div class="col-lg-6">
                                            <div class=" row mb-2">
                                                <label for="inputName" class="col-md-3 form-label">Customer Name</label>
                                                <div class="col-md-9 new_booking_customer_details">
                                                    {{ucwords(@$booking->guest->name)}}
                                                </div>
                                            </div>
                                            <div class=" row mb-2">
                                                <label for="inputName" class="col-md-3 form-label">Check-in Date</label>
                                                <div class="col-md-9 new_booking_customer_details">
                                                    {{@$booking->checkin_date()}}
                                                </div>
                                            </div>
                                            <div class=" row mb-2">
                                                <label for="inputName" class="col-md-3 form-label">Check-out Date</label>
                                                <div class="col-md-9 new_booking_customer_details">
                                                    {{@$booking->checkout_date()}}
                                                </div>
                                            </div>
                                            <div class=" row mb-2">
                                                <label for="inputName" class="col-md-3 form-label">Booking No</label>
                                                <div class="col-md-9 new_booking_customer_details">
                                                    {{@$booking->booking_number}}
                                                </div>
                                            </div>
                                            <div class=" row mb-2">
                                                <label for="inputName" class="col-md-3 form-label">Date</label>
                                                <div class="col-md-9 new_booking_customer_details">                                                    
                                                    {{\Carbon\Carbon::parse(@$booking->created_at)->format('d-m-Y')}}
                                                </div>
                                            </div>                                            
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> --> 
                                    <div class="row">
                                    <div class="col-md-12 col-xl-7">                                       
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th class="text-center">#</th>
                                                        <th>Description</th>
                                                        <th class="text-center">Rate (RM)/ Night(s)</th>
                                                        <th class="text-end">Total (RM)</th>
                                                    </tr>
                                                @foreach($booking_details as $key=>$val)
                                                    <tr>
                                                        <td class="text-center">{{$key+1}}</td>
                                                        <td>
                                                            {{$val->rooms->room_type}}-{{@$val->rooms_number->room_no}}
                                                        </td>
                                                        <td class="text-center">{{$val->room_price}} / {{$val->number_of_days}}</td>
                                                        <td class="text-end">{{$val->subtotal_amount}}</td>
                                                    </tr>
                                                @endforeach
                                                @foreach($booking_addon_details as $key=>$val)
                                                    <tr>
                                                        <td class="text-center"></td>
                                                        <td>{{@$val->getaddon->add_on_name}}</td>
                                                        <td class="text-center">{{@$val->quantity}} Qty</td>
                                                        <td class="text-end">{{$val->total_amount}}</td>
                                                    </tr>
                                                @endforeach                                                
                                                    <tr>
                                                        <td colspan="3" class="fw-bold text-uppercase text-end">Service Charge 
                                                        @if(@$booking->service_tax_details!=null)
                                                            @if(explode('_',@$booking->service_tax_details)[0]!='amount')
                                                              @ {{explode('_',@$booking->service_tax_details)[1]}} %
                                                            @else
                                                              @ {{explode('_',@$booking->service_tax_details)[1]}} MYR
                                                            @endif
                                                        @endif </td>
                                                        <td class="text-end">MYR {{@$booking->service_tax_amount}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="fw-bold text-uppercase text-end">Discount</td>
                                                        <td class="text-end">MYR -{{@$booking->discount_amount}}</td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td colspan="3" class="fw-bold text-uppercase text-end">Total</td>
                                                        <td class="fw-bold text-end h4">MYR {{@$booking->total_amount}}</td>
                                                    </tr>                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                 <div class="col-md-12 col-xl-5">   
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Payment Information</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-pay">
                                            <ul class="tabs-menu nav">
                                                
                                                <li class="tab_check"><a href="#tab21" data-bs-toggle="tab" class="active"><i class="fa fa-money"></i> Others</a></li>
                                                <li class="tab_check"><a href="#tab20" data-bs-toggle="tab"><i class="fa fa-credit-card"></i> Credit Card</a></li>
                                                <li class="tab_check"><a href="#tab22" data-bs-toggle="tab" class=""><i class="fa fa-credit-card"></i>Debit Card</a></li>
                                            </ul>
                                            <form action="{{url('admin/bookinglisting/newbooking/payment/'.Request::segment(5))}}" name="" method="post" id="payment_page">
                                                @csrf
                                            <div class="tab-content">
                                                
                                                <div class="tab-pane active show" id="tab21">
                                                    <label class="custom-control custom-radio-md">
                                                            <input type="radio" class="custom-control-input" id="payment_type1" name="payment_type" value="1" checked>
                                                            <span class="custom-control-label">Cash</span>
                                                    </label>
                                                    <label class="custom-control custom-radio-md">
                                                            <input type="radio" class="custom-control-input" id="payment_type2" name="payment_type" value="2">
                                                            <span class="custom-control-label">Credit Bill</span>
                                                    </label>
                                                    <label class="custom-control custom-radio-md">
                                                            <input type="radio" class="custom-control-input" id="payment_type3" name="payment_type" value="3">
                                                            <span class="custom-control-label">Bank-in</span>
                                                    </label>
                                                     <input type="text" class="form-control" name="bank_name" value="" placeholder="Bank Name" id="bank_name"><br>
                                                </div>
                                                <div class="tab-pane" id="tab20">
                                                   <input type="radio" class="custom-control-input" name="payment_type" id="payment_type4" value="4">
                                                    <div class="form-group">
                                                        <label class="form-label">CardHolder Name</label>
                                                        <input type="text" name="cardholder_name" class="form-control" placeholder="Name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Card number</label>
                                                        <div class="input-group">
                                                            <input name="cardholder_number" type="text" class="form-control" placeholder="Card number">
                                                            <span class="input-group-text btn btn-secondary shadow-none">
                                                                    <i class="fa fa-cc-visa"></i> &nbsp; <i class="fa fa-cc-amex"></i> &nbsp;
                                                                    <i class="fa fa-cc-mastercard"></i>
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label class="form-label">Expiration</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" placeholder="MM" name="month">
                                                                    <input type="number" class="form-control" placeholder="YY" name="year">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label class="form-label">CVV <i class="fa fa-question-circle" data-bs-placement="top" data-bs-toggle="tooltip" title="CVV is the last three digits on the back of your credit card"></i></label>
                                                                <input type="number" class="form-control" required="">
                                                            </div>
                                                        </div> -->
                                                    </div>                                                    
                                                </div>
                                                <div class="tab-pane" id="tab22">
                                                    <input type="radio" class="custom-control-input" id="payment_type1" name="payment_type" value="5">
                                                    <div class="form-group">
                                                        <label class="form-label">CardHolder Name</label>
                                                        <input type="text" name="cardholder_name1" class="form-control" placeholder="Name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Card number</label>
                                                        <div class="input-group">
                                                            <input type="text" name="cardholder_number1" class="form-control" placeholder="Card number">
                                                            <span class="input-group-text btn btn-secondary shadow-none">
                                                                    <i class="fa fa-cc-visa"></i> &nbsp; <i class="fa fa-cc-amex"></i> &nbsp;
                                                                    <i class="fa fa-cc-mastercard"></i>
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label class="form-label">Expiration</label>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" placeholder="MM"  name="month1">
                                                                    <input type="number" class="form-control" placeholder="YY" name="year1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label class="form-label">CVV <i class="fa fa-question-circle" data-bs-placement="top" data-bs-toggle="tooltip" title="CVV is the last three digits on the back of your credit card"></i></label>
                                                                <input type="number" class="form-control" required="">
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <button type="button" class="btn  btn-lg btn-danger" onclick="deletefunction(event,{{@$booking->id}});" >Back</button>                                                
                                                <button type="button" id="submit_button" class="btn  btn-lg btn-secondary">Save</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                             </div>


                                        </div>
                                    </div>
                                    <!-- <div class="card-footer text-end">                                        
                                        <button id="si-printer" type="button" class="btn btn-danger mb-1" onclick="javascript:window.print();"><i class="si si-printer"></i> Print Invoice</button>
                                    </div> -->
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->
                <form id="remove_booking" method="POST" action="{{ url('admin/bookinglisting/cancel_booking', $booking->id) }}"> 
                    @csrf <input name="_method" type="hidden" value="POST">
                </form>

        @endsection

    @section('scripts')
    <script type="text/javascript">
        $('#bank_name').css('display','none');
        $("input[name=payment_type][value=" + 1 + "]").prop('checked', true);
        $(document).on('click','.tab_check',function(){
             if($(this).children("a").attr("href")=='#tab20'){
                 $("input[name=payment_type][value=" + 4 + "]").prop('checked', true);
             }else if($(this).children("a").attr("href")=='#tab21'){
                 $("input[name=payment_type][value=" + 1 + "]").prop('checked', true);
             }else if($(this).children("a").attr("href")=='#tab22'){
                 $("input[name=payment_type][value=" + 5 + "]").prop('checked', true);
             }
        });
        $(document).on('click','input[name=payment_type]',function(){
            if($(this).val()==3){
                $('#bank_name').prop('required',true);
                $('#bank_name').css('display','block');
            }else{
                $('#bank_name').prop('required',false);
                $('#bank_name').css('display','none');
            }
        })
        $(document).on('click','#submit_button',function(){
           if($("input[name='payment_type']:checked").val()==1 || $("input[name='payment_type']:checked").val()==2){
             $("#payment_page").submit();
           }else if($("input[name='payment_type']:checked").val()==3){
                var valid = true,
                errorMessage = "";
                if ($('#bank_name').val() == '') {
                   errorMessage  = "please enter bank name";
                   valid = false;
                }
                if( !valid && errorMessage.length > 0){
                   //alert(errorMessage);
                   toastr.clear();
                   toastr.error(errorMessage);
                   return false;
                }
               $("#payment_page").submit();
           }else if($("input[name='payment_type']:checked").val()==4) {
                var valid = true,
                errorMessage = "";
                if ($("input[name='cardholder_name']").val() == '') {
                   errorMessage  = "please enter card name";
                   valid = false;
                }if ($("input[name='cardholder_number']").val() == '') {
                   errorMessage  = "please enter card number";
                   valid = false;
                }if ($("input[name='month']").val() == '') {
                   errorMessage  = "please enter card expire month";
                   valid = false;
                }if ($("input[name='year']").val() == '') {
                   errorMessage  = "please enter card expire year";
                   valid = false;
                }
                if( !valid && errorMessage.length > 0){
                   //alert(errorMessage);
                   toastr.clear();
                   toastr.error(errorMessage);
                   return false;
                }
                $("#payment_page").submit();
           }else if($("input[name='payment_type']:checked").val()==5) {
                var valid = true,
                errorMessage = "";
                if ($("input[name='cardholder_name1']").val() == '') {
                   errorMessage  = "please enter card name";
                   valid = false;
                }if ($("input[name='cardholder_number1']").val() == '') {
                   errorMessage  = "please enter card number";
                   valid = false;
                }if ($("input[name='month1']").val() == '') {
                   errorMessage  = "please enter card expire month";
                   valid = false;
                }if ($("input[name='year1']").val() == '') {
                   errorMessage  = "please enter card expire year";
                   valid = false;
                }
                if( !valid && errorMessage.length > 0){
                   //alert(errorMessage);
                   toastr.clear();
                   toastr.error(errorMessage);
                   return false;
                }
                $("#payment_page").submit();
           }
        });
       
        function deletefunction(event, id) {
           event.preventDefault();
           swal({
             title: "Are you sure you want to cancel this reservation?",
             text: "If you cancel this, it will be removed.",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: '#DD6B55',
             confirmButtonText: 'Delete',
             cancelButtonText: "Cancel",
             closeOnConfirm: false,
             closeOnCancel: true
           }, function(isConfirm) {
             if (isConfirm) {                
               $("#remove_booking").submit();
             } else {
               swal("Cancel", "Your data has not been removed", "error");
             }
           });
       }
    </script>



    @endsection
