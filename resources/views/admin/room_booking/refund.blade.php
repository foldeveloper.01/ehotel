@extends('admin.layouts.app')
    @section('styles')
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Refund</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Refund</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                          <h3 class="card-title">Disclaimer</h3>
                                    </div>
                                    <div class="card-body">
                                        <form id="refund" name="refund" method="post" enctype="multipart" action="{{url('admin/bookinglisting/newbooking/refund_save')}}">
                                            @csrf  
                                            <p><span class="fs-15 fw-semibold mb-0 text-warning">Warning :</span> <span class="fs-15 fw-semibold mb-0 text-secondary"> After refund, This booking will be moved to history. So room will be released for new booking.</span></p>
                                            <p><h5><b>Booking</b></h5></p>                                           

                                            <dl class="row card-body py-0">
                     <dt class="col-sm-1">Booking No</dt>
                     <dd class="col-sm-5"> {{@$booking->booking_number}} </dd>
                      <dt class="col-sm-1"><b></b></dt>
                     <dd class="col-sm-5"></dd>
                     <dt class="col-sm-1">Guest Name</dt>
                     <dd class="col-sm-5">{{ucwords(@$booking->guest->name)}}</dd>
                     <dt class="col-sm-1"><b></b></dt>
                     <dd class="col-sm-5"></dd>
                     <dt class="col-sm-1">Guest Email</dt>
                     <dd class="col-sm-5">{{ucwords(@$booking->guest->email)}}</dd>
                     </dl>                         
                                            <div><br>
                                                <p class=""><b>Discount Type:</b>
                                                  <div class="custom-radio custom-control-inline">
                                                    <label class="custom-control custom-radio-md">
                                                      <input type="radio" class="custom-control-input" name="discount_type" value="0" checked>
                                                      <span class="custom-control-label">Cancel booking with No Refund</span>
                                                    </label>
                                                  </div>
                                                  <div class="custom-control custom-radio custom-control-inline">
                                                    <label class="custom-control custom-radio-md">
                                                      <input type="radio" class="custom-control-input" name="discount_type" value="1">
                                                      <span class="custom-control-label">Cancel booking with Refund</span>
                                                    </label>
                                                  </div>
                                                </p>
                                                <div class="refund_div">
                                                    <div class="form-group">
                                                       Refund Amount (RM)
                                                       <input type="text" class="form-control col-lg-2" name="refund_amount" value="{{@$booking->total_amount}}">
                                                    </div>
                                                    <div class="form-group">
                                                       Refund Via
                                                        <select name="refund_payment_type" class="form-control col-lg-2"> 
                                                            <option value="1">Cash</option>
                                                            <option value="2">Credit Bill</option>
                                                            <option value="3">Bank-In</option>
                                                            <option value="4">Credit Card</option>
                                                            <option value="5">Debit card</option>
                                                            <option value="6">Cheque</option>
                                                        </select>
                                                    </div>                                                        
                                                </div>
                                            </div>
                                            <label class="custom-control custom-checkbox-md">
                                                  <input type="checkbox" class="custom-control-input" name="email_to_customer" value="1">
                                                  <span class="custom-control-label">Please enable checkbox, if you want to send email to customer for cancellation</span>
                                           </label>
                                            <input type="hidden" name="booking_id" value="{{\Request::segment(5)}}">
                                            <div class="card-footer">
                                                <a href="{{url('admin/bookinglisting')}}" class="btn btn-danger my-1">Back</a>
                                                <button id="button" type="button" class="btn btn-success my-1">Save</button>
                                            </div>                                        
                                        </form>
                                    </div>                                   
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')
 <script type="text/javascript">
    $('.refund_div').hide();  
    $(document).on('click','input[name="discount_type"]',function() {          
          if($(this).val()==1) {
            $('.refund_div').show();
          }else {
            $('.refund_div').hide();
          }        
     });
    $(document).on('click','#button',function() {
        if($('input[name="discount_type"]:checked').val()==1) {
             if($('input[name="refund_amount"]').val() =='' || $('input[name="refund_amount"]').val()==0) {
                toastr.clear();
                if($('input[name="refund_amount"]').val()==''){                  
                  toastr.warning('Refund amount field is required');
                }else{
                  toastr.warning('Refund amount is greater than zero');
                }
                return false;
            }
        }        
        $('#refund').submit();
    });
 </script>


    @endsection
