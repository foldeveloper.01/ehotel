@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Invoice</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                    <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img logo-3" alt="logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{@$setting->address}}
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <h3>Booking No-{{@$booking->booking_number}}</h3>
                                                <h5><b>Check-in:</b> {{\Carbon\carbon::parse(@$booking->check_in)->format('d-m-Y')}}</h5>
                                                <h5><b>Check-out:</b> {{\Carbon\carbon::parse(@$booking->check_out)->format('d-m-Y')}}</h5>
                                                <h5><b>Date:</b>{{\Carbon\Carbon::parse(@$booking_payment->created_at)->format('d-m-Y')}}</h5>
                                                @if(@$booking->payment_status==0)
                                                <h5><b>Payment: </b> {{$booking->get_paymentstatus_text(0)}} </h5>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="h3">Invoice To:</p>
                                                <p class="fs-15 fw-semibold mb-0">{{ucwords(@$booking->guest->name)}}</p>                                  
                                                <address>
                                                        {{@$booking->guest->address}}<br>
                                                        @if(@$booking->guest->city)
                                                         {{@$booking->guest->city}},
                                                        @endif
                                                        @if(@$booking->guest->state)
                                                         {{@$booking->guest->state}},
                                                        @endif
                                                         <br>
                                                        @if(@$booking->guest->country)
                                                         {{@$booking->guest->country}},
                                                        @endif
                                                        @if(@$booking->guest->postcode)
                                                         {{@$booking->guest->postcode}}
                                                        @endif
                                                        <br>
                                                        {{@$booking->guest->email}}
                                                    </address>
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> -->
                                        </div>
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap table-striped">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th>No</th>
                                                        <th>Description</th>
                                                        <th>Qty / Nights</th>
                                                        <th>Price</th>
                                                        <th class="text-end">Amount(RM)</th>
                                                    </tr>                                                                                                         
                                                        <?php $booking_details_id=[];$booking_addon_id=[]; $j=1; ?> 
                                                        @foreach($booking->booking_details as $key=>$data)
                                                          @php $booking_details_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td >{{$j++}}</td>     
                                                            <td >{{@$data->rooms->room_type}} - {{@$data->rooms_number->room_no}}
                                                                <!--  <select class="form-group select">
                                                                     <option>{{@$data->rooms_number->room_no}}</option>
                                                                 </select> -->
                                                            </td>     
                                                            <td >{{@$data->number_of_days}}</td>
                                                            <td >{{@$data->room_price}}</td>
                                                            <td class="text-end">{{@$data->room_price}}</td>
                                                        </tr>
                                                        @endforeach
                                                        @foreach($booking->booking_exsit_addon_details as $key=>$data)
                                                        @php $booking_addon_id[]=$data->id @endphp
                                                        <tr>
                                                            <td >{{$j++}}</td>     
                                                            <td >{{@$data->getaddon->add_on_name}} - {{@$data->quantity}} Qty</td>     
                                                            <td ></td>
                                                            <td >{{@$data->price}}</td>
                                                            <td class="text-end">{{@$data->total_amount}}</td>
                                                        </tr>
                                                        @endforeach
                                                          <tr>
                                                            <td colspan="4" class="fw-bold text-uppercase text-end">Service Charge@10.00%</td>
                                                            <td class="text-end">0.00</td>
                                                          </tr>                                                                                         
                                                          <tr>
                                                            <td colspan="4" class="fw-bold text-uppercase text-end">Total</td>
                                                            <td class="fw-bold text-end h4">{{number_format(@$total_amount,2)}}</td>
                                                          </tr>
                                                </tbody>
                                            </table>
                                        </div><br>
                                        This is a computer generated document. No signature is required.<br>
                                        <button id="si-printer" type="button" class="btn btn-success mb-1 text-end" onclick="javascript:window.print();"><i class="si si-printer"></i>Print</button>
                                    </div>   

                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

       
        @endsection

    @section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
      //click view new windows for print
      $(document).on('click','.click_view',function(e){
         e.preventDefault();
         window.open($(this).parent().parent().find('a').attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

      $(document).on('click','.edit_price',function(){
         $("#receipt_id").val($(this).attr('id'));
          var id = $(this).attr('id');
          $("#amount").val($(this).parent().parent().find('#rowamount_'+id+'').html());
      });

      $("#save_price_form").validate({
            submitHandler: function(form) {
              form.submit();
            },
            rules: {
              amount: {
                required: true,
                number: true
              }
            },
            messages: {
              amount: {
                required: "Please enter amount",
              }
            },    
       });

      $(document).on('click','#save_price_button',function(){        
        if ($('#save_price_form').valid()) {
                 var receipt_id = $("#receipt_id").val();
                 var amount = $("#amount").val();
                  $.ajax({
                  url: site_url + '/admin/bookinglisting/newbooking/addon_invoice/update_price',
                  type: 'post',
                  dataType: 'json',
                  data: {
                    id: receipt_id,
                    amount: amount,
                    _token: '{{csrf_token()}}'
                  },
                  success: function(data) {
                      toastr.clear();
                      toastr.success('Amount updated successfully');
                      $("#rowamount_"+receipt_id+"").html(data.amount);
                    }           
                });
                $('.btn-close').click();
            }
      });      
      
    });
</script>
    @endsection
