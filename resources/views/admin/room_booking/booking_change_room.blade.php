@extends('admin.layouts.app')
@section('styles')
@endsection
@section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
   <h1 class="page-title"></h1>
   <div>
      <ol class="breadcrumb" id="si-printer1">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Change Room</li>
      </ol>
   </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-header">
            <h4>Change Room</h4>
         </div>
         <div class="card-body">
            <div class="row pt-3">
               <div class="col-lg-4">                  
                  <dl class="row card-body py-2">
                     <dt class="col-sm-4"><b>Booking No</b></dt>
                     <dd class="col-sm-6"> {{@$booking->booking_number}} </dd>
                     <dt class="col-sm-4"><b>Guest Name</b></dt>
                     <dd class="col-sm-6"> {{@$booking->guest->name}} </dd>
                     <dt class="col-sm-4"><b>Guest Email</b></dt>
                     <dd class="col-sm-6">{{@$booking->guest->email}}</dd>
                     <dt class="col-sm-4"><b>Contact Number</b></dt>
                     <dd class="col-sm-6">+{{@$booking->guest->contact_number}}</dd>
                     <dt class="col-sm-4"><b>Check-In</b></dt>
                     <dd class="col-sm-6">{{@$booking->checkin_date()}}</dd>
                     <dt class="col-sm-4"><b>Check-Out</b></dt>
                     <dd class="col-sm-6">{{@$booking->checkout_date()}}</dd>
                     @foreach($booking_details as $key=>$value)
                     @if($key==0)
                     <dt class="col-sm-4"><b>Rooms</b></dt>
                     @else
                     <dt class="col-sm-4"><b></b></dt>
                     @endif
                     <dd class="col-sm-6">{{@$value->rooms_number->room_no}} <span class="fs-15 fw-semibold mb-0 text-danger">(RM{{@$value->room_price}})</span></dd>
                     @endforeach
                     <?php $get_total_amount = Helper::get_total_amount($booking->id);?>                                

                     <dt class="col-sm-4"><b>Service Charge</b></dt>
                     <dd class="col-sm-6"><span class="fs-15 fw-semibold mb-0 text-danger">RM{{@$booking->service_tax_amount}}</span></dd>
                     <dt class="col-sm-4"><b>Total</b></dt>
                     <dd class="col-sm-6"><span class="fs-15 fw-semibold mb-0 text-danger">
                            @if($get_total_amount < 0 )
                               -RM {{number_format(abs(@$get_total_amount),2)}}
                            @else
                               RM {{number_format(@$get_total_amount,2)}}
                            @endif
                  </span></dd>
                  </dl>
               </div>

              
               <div class="col-lg-4">
                  <form  action="{{url('admin/bookinglisting/booking_change_room/'.@$booking->id)}}" method="post">
                     @csrf
                     <input type="hidden" id="bookingid" name="bookingid" value="{{@$booking->id}}">
                     @foreach(@$booking_details as $key=>$data)
                     <p class="">
                        {{@$data->rooms_number->room_no}} -{{@$data->rooms->room_type}} Upgrade To <br>
                        <select class="col-lg-6 check_room_no" name="roombookingdetailid[{{$data->id}}]">          
                        @foreach(@$data->room_number_list['room_type'] as $key=>$val)                        
                        <optgroup label="{{@$val}}">
                           @foreach(@$data->room_number_list['room_numbers'][$key] as $k=>$v)
                           <option value="{{$k}}" @if(@$data->new_room_number_id==$k) selected @endif>{{$v}}</option>
                           @endforeach 
                        </optgroup>
                        @endforeach 
                        </select>
                     </p>
                     @endforeach
                     <button  type="submit" id="change_room" class="btn btn-success">Check Room</button>
                  </form>
               </div>
               <div class="col-lg-3">
                  <div class="change_details">
                     <p class="h4">Upgrade Amount: <span class="text-danger">RM</span> <span class="upgrade_amount text-danger">{{@$price_details['upgrade_amount']}}</span></p>
                     <p class="h4">Service Charge: <span class="text-danger">RM</span> <span class="service_amount text-danger">{{@$price_details['service_charge']}}</span> </p>
                     <p class="h4">Total Amount to pay: <span class="text-danger">RM</span> <span class="total_amount text-danger">{{@$price_details['total_amount_to_pay']}}</span></p>
                  </div>
                  <form id="change_details_form" action="{{url('admin/bookinglisting/booking_change_room/save')}}" method="post">
                     @csrf                                            
                     <input type="hidden" name="upgrade_amount" id="upgrade_amount" value="{{@$price_details['upgrade_amount']}}">
                     <input type="hidden" name="service_amount" id="service_amount" value="{{@$price_details['service_charge']}}">
                     <input type="hidden" name="total_amount" id="total_amount" value="{{@$price_details['total_amount_to_pay']}}">
                     <input type="hidden" name="bookingid" value="{{@$booking->id}}">
                     @if(@$get_selected_booking_data!='')
                     @foreach(@$get_selected_booking_data as $key=>$data)
                     <p style="display: none;">                                                    
                        <select class="col-lg-6 check_room_no" name="roombookingdetailid[{{$data->id}}]">          
                        @foreach(@$data->room_number_list['room_type'] as $key=>$val)                        
                        <optgroup label="{{@$val}}">
                           @foreach(@$data->room_number_list['room_numbers'][$key] as $k=>$v)
                           <option value="{{$k}}" @if(@$data->new_room_number_id==$k) selected @endif>{{$v}}</option>
                           @endforeach 
                        </optgroup>
                        @endforeach 
                        </select>
                     </p>
                     @endforeach
                     @endif
                     @if(@$get_selected_booking_data!='')
                     @foreach(@$get_selected_booking_data as $key=>$data)
                     <p style="display: none;">
                        <select class="col-lg-6 form-control check_room_no filter-multi" name="roombookingdetailPrice[{{$data->id}}]">
                           <option value="{{$data->update_room_price}}">{{$data->update_room_price}}</option>
                        </select>
                     </p>
                     @endforeach
                     @endif
                     <div class="refund_div">
                        <p class=""><b>Refund:</b>
                        <div class="custom-radio custom-control-inline">
                           <label class="custom-control custom-radio-md">
                           <input type="radio" class="custom-control-input" name="check_refund" value="1">
                           <span class="custom-control-label">Yes</span>
                           </label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <label class="custom-control custom-radio-md">
                           <input type="radio" class="custom-control-input" name="check_refund" value="0">
                           <span class="custom-control-label">No</span>
                           </label>
                        </div>
                        </p>
                     </div>
                     <div class="refund_amount">
                        <p class=""><b>Enter Refund Amount:</b>
                           <input type="number" class="form-control col-lg-4" name="refund_amount" id="refund_amount" value="">
                        </p>
                     </div>
                     <div class="payment_div">
                        <p class="">
                           <b>Payment Methods:</b>
                           <select id="payment_method" name="payment_method" class="form-control" style="width:50%">
                              <option value="">choose</option>
                              <option value="1">Cash</option>
                              <option value="4">Credit Card</option>
                           </select>
                        </p>
                     </div>
                     <div class="change_details">
                        <button  type="button" id="confirm_change_room" class="btn btn-success">Confirm to Change Room</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- COL-END -->
</div>
<!-- ROW-1 CLOSED -->
@endsection
@section('scripts')
<script type="text/javascript">
   $(document).ready(function() {
    $('.check_room_no').select2({matcher: matchStart});
    function matchStart(params, data) {
  // If there are no search terms, return all of the data
  if ($.trim(params.term) === '') {
    return data;
  }

  // Skip if there is no 'children' property
  if (typeof data.children === 'undefined') {
    return null;
  }

  // `data.children` contains the actual options that we are matching against
  var filteredChildren = [];
  $.each(data.children, function (idx, child) {
    if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
      filteredChildren.push(child);
    }
  });

  // If we matched any of the timezone group's children, then set the matched children on the group
  // and return the group object
  if (filteredChildren.length) {
    var modifiedData = $.extend({}, data, true);
    modifiedData.children = filteredChildren;

    // You can return modified objects from here
    // This includes matching the `children` how you want in nested data sets
    return modifiedData;
  }

  // Return `null` if the term should not be displayed
  return null;
}
});
   $(function () { 
   var checkin_date = "{{@$booking->checkin_date()}}";
   var checkout_date = "{{@$booking->checkout_date()}}";       
   
   });
   
   
   $(document).ready(function() {
   //$('#change_room').hide();
   $('.change_details').hide();
   $('.payment_div').hide();
   $('.refund_div').hide();
   $('.refund_amount').hide();
   var price_plus_or_minus = '{{@$price_details["condition"]}}';    
   var total_amount_to_pay = '{{@$price_details["total_amount_to_pay"]}}';    
   if(price_plus_or_minus!='') {
   $('#change_room').show();
   $('.change_details').show();
   if(price_plus_or_minus=='add') {
         $("input[name='check_refund']").prop('checked',false);
         if(total_amount_to_pay!='0') {
               $('.payment_div').show();
               $("#payment_method").val(1);
               $('.refund_div').hide();
               //append new payment option
                $("#payment_method option[value='8']").remove();
                $("#payment_method option[value='9']").remove();
                $('#payment_method').append($('<option>', {value: 8, text: 'Pay Later' }));
                $('#payment_method').append($('<option>', {value: 9, text: 'Free' }));
              //append new payment option
           }else{
               $('.payment_div').hide();
               $("#payment_method option:selected").prop("selected", false);
           }
    }else {
        $("input[name='check_refund'][value=0]").prop('checked',true);
        $('.payment_div').hide();
        $('.refund_div').show();
        $("#payment_method option[value='8']").remove();
        $("#payment_method option[value='9']").remove();
    }
   }  
   
   $(".check_room_no").change(function() {
   $('#change_room').show();
   $('.change_details').hide();
   $('.payment_div').hide();
   $('.refund_div').hide();
   $('.refund_amount').hide();
   });
   
   
   $(document).on('click','input[name="check_refund"]',function(){
   if($(this).val()==0){
       $('.payment_div').hide();
       $('.refund_amount').hide();
   }else{
       $('.payment_div').show();
       $('.refund_amount').show();
       $('#refund_amount').val(Math.abs($("#total_amount").val()));
   }
   });
   
   $(document).on('click','#confirm_change_room',function(){
   if($('input[name="check_refund"]:checked').val()==1){
       //refund_amount
       if(!$('#refund_amount').val()){
          toastr.clear();
          toastr.error('Please enter refund amount');
          return false;   
        }
        if(!$('#payment_method :selected').val()){               
          toastr.clear();
          toastr.error('Please choose payment type');
          return false;   
        }
       //payment_method
   }
   $('#change_details_form').submit();
   });
   
   
   });
   
</script>
@endsection