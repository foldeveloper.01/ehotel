@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Booking SMS</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Booking SMS</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/bookinglisting/newbooking/send_sms_to_customer')}}" method="post" enctype="multipart/form-data"> @csrf 
    <input type="hidden" name="booking_id" value="{{@$booking->id}}">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Booking SMS</h3>
        </div>
        <div class="card-body">
          
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="bannerimage">Booking No : {{@$booking->booking_number}}</label>                
              </div>              
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="bannerimage">Name : {{ucwords(@$booking->guest->name)}}</label>                
              </div>              
            </div>
          </div>

          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="bannerimage">Mobile : {{@$contact_number}}</label>                
              </div>              
            </div>
          </div>

          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Send To</label>
                <input type="text" name="send_to" class="form-control" placeholder="Send To" value="{{@$contact_number}}">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Message</label>
                <textarea class="content" name="message"> {!!@$message!!} </textarea>
              </div>
            </div>
          </div>

          <div class="card-footer">
             <a href="{{url('admin/bookinglisting')}}" class="btn btn-danger my-1">Back</a>
             <button class="btn btn-success my-1" value="submit">Save</button>
          </div>
          
        </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  var current_date = "{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d-m-Y')}}";
  $('#date').bootstrapMaterialDatePicker
      ({
        time: false,
        format :'DD-MM-YYYY',
        lang :'en',
        shortTime :false,
        //clearButton: true
      });
   /*$(function() {
      $('#date').datetimepicker({
        //defaultDate: defaulsst,  
        pickTime: false,
        minView: 2,
        format: 'dd-mm-yyyy',
        autoclose: true,
      });      
    });*/
   //$('#date').val(current_date);
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      title: {
        required: true
      },date: {
        required: true
      },
      image: {
        required: true
      }
    },
    messages: {
      title: {
        required: "Please enter title",
      },date: {
        required: "Please select date",
      },
      image: {
        required: "Please upload image",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection