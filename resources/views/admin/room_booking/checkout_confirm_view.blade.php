@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
 <!--  <h1 class="page-title">FAQ</h1>
  <div>
    <ol class="breadcrumb">     
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">FAQ List</li>
    </ol>
  </div> -->
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Check-out List</h3>
      </div>
      <div class="card-body">   
      <form action="{{url('admin/checkoutlisting/checkout_confirm_save/'.Request::segment(5))}}" id="check_in_submit_form" name="check_in_submit_form" method="post" enctype="multipart">
      @csrf    
        <div class="table-responsive">

            <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Name : {{ucwords(@$booking->guest->name)}} </label>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Total (include Taxes): RM {{number_format(@$booking->total_amount,2)}}</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">IC/Passport No: {{ucwords(@$booking->guest->ic_passport_no)}} </label>
              </div>
            </div>
            <?php $get_total_amount = Helper::get_total_amount($booking->id);?>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Due: @if($get_total_amount < 0 )
                                      -RM {{number_format(abs(@$get_total_amount),2)}}
                                    @else
                                      RM 0.00
                                   @endif</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Country: {{ucwords(@$booking->guest->getcountry->name)}} </label>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Balance: 
                      @if($get_total_amount < 0 )
                        -RM {{number_format(abs(@$get_total_amount),2)}}
                       @else
                        RM 0.00
                      @endif
                  </label>
              </div>
            </div>
          </div>

          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatables">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Room Type</th>
                <th class="wd-15p border-bottom-0">Room Number</th>
                <th class="wd-15p border-bottom-0">Check-in</th>
                <th class="wd-15p border-bottom-0">Check-out</th>
                <th class="wd-15p border-bottom-0">Price (RM)</th>
                <th class="wd-15p border-bottom-0">Total (RM)</th>
                <th class="wd-15p border-bottom-0">Select to Checkout?</th>
              </tr>
            </thead>
            <tbody> @foreach(@$booking->get_booking_details() as $key=>$data) @if($data->checkout_status==0) <tr>
                <td>{{@$key+1}}</td>
                <td>{{@$data->rooms->room_type}}</td>
                <td>
                    <input type="hidden" name="booking_details_ids[]" value="{{@$data->id}}">
                    <div class="form-group">                      
                      <select name="room_number[]" class="form-control select2 form-select" style="width:50%">
                        <option value="{!!@$data->rooms_number->room_no!!}" selected>{!!@$data->rooms_number->room_no!!}</option>                        
                      </select>
                    </div>
                </td>                
                <td>{{@$booking->checkin_date()}}</td>
                <td>{{@$booking->checkout_date()}}</td>
                <td>{{@$data->room_price}}</td>
                <td>{{@$data->subtotal_amount}}</td>
                <td>
                  <div class="col-xl-2 ps-1 pe-1">
                    <div class="form-group">
                      <label class="custom-control custom-checkbox-md">
                            <input type="checkbox" class="custom-control-input checkout_checkbox" name="checkout_checkbox[{{@$data->rooms_number->room_no}}]" value="1" checked>
                            <span class="custom-control-label"></span>
                     </label>
                    </div>
                  </div>
                </td>
              </tr>
              @endif
              @endforeach
              <tr>
                  <td colspan="6" class="fw-bold text-end">Booking Amount Not Paid</td>
                  <td class="h5">0.00</td>
              </tr>
              <tr>
                  <td colspan="6" class="fw-bold text-end">Total Amount to Pay</td>
                  <td class="h5">0.00</td>
              </tr>
              <tr>
                  <td colspan="6" class="fw-bold text-end">Payment Mode</td>
                  <td class="h5">
                      <select name="refund_amount_payment_mode" id="refund_amount_payment_mode" class="form-control select">
                        <option value="">Select Payment Type For Refund</option>
                        <option value="1">Cash</option>
                        <option value="2">Credit Bill</option>
                        <option value="3">Bank-In</option>
                        <option value="4">Credit Card</option>
                        <option value="5">Debit card</option>
                        <option value="6">Cheque</option>
                        <option value="7">Skip for No Payment</option>
                      </select>                    
                  </td>
                  <span id="refund_amount_payment_mode_error"></span>                  
              </tr>
              <tr>
                  <td colspan="6" class="fw-bold text-end">Early Check-out Refund Amount(Leave blank if no refund)(RM)</td>
                  <td class="h5">
                    <input type="number" name="refund_amount" id="refund_amount" class="form-control" value="">
                  </td>
                  <span id="refund_amount_error"></span>
              </tr>
              

             </tbody>
          </table>         
            
            <div class="card-footer">
              <a href="{{url('admin/checkoutlisting')}}" class="btn btn-danger my-1">Back</a>
             <button class="btn btn-success my-1" id="checkout_submit" value="submit">Check-Out</button>
            </div>

          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  
  $('#checkout_submit').on("click",function() {
       if($('.checkout_checkbox:checkbox:checked').length == 0){
          toastr.clear();
          toastr.warning('Please check atleast one checkout rooms');
          return false;
       }
       if($('#refund_amount_payment_mode :selected').val() =='' && $('#refund_amount').val() =='') {
          return true;
       }else if($('#refund_amount_payment_mode :selected').val() !='') {
          if($('#refund_amount').val()==''){
            toastr.clear();
            toastr.warning('Please enter refund amount');
            return false;            
          }
       }else if($('#refund_amount').val()!='') {
         if($('#refund_amount_payment_mode :selected').val() ==''){
            toastr.clear();
            toastr.warning('Please select payment type');
            return false;
          }
       }      
  });

</script> @endsection