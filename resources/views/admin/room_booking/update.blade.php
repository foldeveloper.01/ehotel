@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
    <h1 class="page-title">Room Type</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{url('admin')}}">Admin</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Update Room Type</li>
        </ol>
    </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row row-sm">
    <div class="col-md-12 col-xl-10">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <div class="row">
                        <div class="col-md-12 col-xl-4">
                            <div class="card text-primary bg-primary-transparent">
                                <div class="card-body">
                                    <h4 class="card-title">Total Room : {{@$room_setting->total_room}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xl-4">
                            <div class="card text-secondary bg-secondary-transparent">
                                <div class="card-body">
                                    <h4 class="card-title">Alotted Room : {{@$room_setting->alotted_room}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xl-4">
                            <div class="card text-success bg-success-transparent">
                                <div class="card-body">
                                    <h4 class="card-title">Remaining Room : {{@$room_setting->remaining_room}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="panel panel-primary">
                                        <div class="tab-menu-heading">
                                            <div class="tabs-menu">
                                                <!-- Tabs -->
                                                <ul class="nav panel-tabs panel-danger">
                                                    <li style="margin-right: 4px;">
                                                        <a href="#tab13" @if(session('active_tab')=='1' ) class="active" @endif data-bs-toggle="tab">Room Details</a>
                                                    </li>
                                                    <li style="margin-right: 4px;">
                                                        <a href="#tab14" @if(session('active_tab')=='2' ) class="active" @endif data-bs-toggle="tab">
                                                            <span></span>Pricing</a>
                                                    </li>
                                                    <li style="margin-right: 4px;">
                                                        <a href="#tab15" @if(session('active_tab')=='3' ) class="active" @endif data-bs-toggle="tab">
                                                            <span></span>Overview</a>
                                                    </li>
                                                    <li style="margin-right: 4px;">
                                                        <a href="#tab16" @if(session('active_tab')=='4' ) class="active" @endif data-bs-toggle="tab">
                                                            <span></span>Amenities</a>
                                                    </li>
                                                    <li style="margin-right: 4px;">
                                                        <a href="#tab17" @if(session('active_tab')=='5' ) class="active" @endif data-bs-toggle="tab">
                                                            <span></span>Gallery</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="panel-body tabs-menu-body">
                                            <div class="tab-content">
                                                <div class="tab-pane @if(session('active_tab')=='1' ) active @endif" id="tab13">
                                                    <div class="row">
                                                        <form id="create" action="{{url('admin/rooms/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
                                                                <input type="hidden" name="action_tab" value="room_details">
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputname">Room Type Name</label>
                                                                            <input type="text" value="{{@$data->room_type}}" name="room_type" class="form-control" id="room_type" placeholder="Room Type Name">
                                                                            @if($errors->has('room_type')) <div class="error">{{ $errors->first('room_type') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputname1">Total Rooms</label>
                                                                            <input type="text" value="{{@$data->total_room}}" name="total_room" class="form-control" id="total_room" placeholder="Total Rooms">
                                                                            @if($errors->has('total_room')) <div class="error">{{ $errors->first('total_room') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="occupants">Occupants</label>
                                                                            <input type="text" value="{{@$data->occupants}}" name="occupants" class="form-control" id="occupants" placeholder="Occupants">
                                                                            @if($errors->has('occupants')) <div class="error">{{ $errors->first('occupants') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <label for="showonline">Show Online</label>
                                                                        <br>
                                                                        <div class="custom-radio custom-control-inline">
                                                                            <label class="custom-control custom-radio-md">
                                                                                <input type="radio" class="custom-control-input" name="show_online" value="0"@if(@$data->show_online=='0') checked="" @endif>
                                                                                <span class="custom-control-label">Yes</span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                            <label class="custom-control custom-radio-md">
                                                                                <input type="radio" class="custom-control-input" name="show_online" value="1" @if(@$data->show_online=='1') checked="" @endif>
                                                                                <span class="custom-control-label">No</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="view">View (Eg: Beach or Hills)</label>
                                                                            <input type="text" value="{{@$data->view}}" name="view" class="form-control" id="view" placeholder="View">
                                                                            @if($errors->has('view')) <div class="error">{{ $errors->first('view') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="size">Size (Eg: 35 m2 / 376 ft2)</label>
                                                                            <input type="text" value="{{@$data->size}}" name="size" class="form-control" id="size" placeholder="Size">
                                                                            @if($errors->has('size')) <div class="error">{{ $errors->first('size') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="bed">Bed (Eg: King-size or twin beds)</label>
                                                                            <br>
                                                                            <input type="text" name="bed" value="{{@$data->bed}}" class="form-control" id="bed" placeholder="Bed"> @if($errors->has('bed')) <div class="error">{{ $errors->first('bed') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="description">Short Description</label>
                                                                            <br>
                                                                            <textarea name="description" class="form-control" rows="2" placeholder="Short Description">{{@$data->description}}</textarea> @if($errors->has('description')) <div class="error">{{ $errors->first('description') }}</div> @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputnumber">Room Image (450 × 300) (Max upload file size 2MB)</label>
                                                                            <input class="form-control" id="image" accept="image/*" name="image" type="file" accept='image/*'>
                                                                        </div>
                                                                    </div>@if(@$data->image!=null && @$data->image!='') <div class="col-lg-1 col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="form-label">
                                                                                <br>
                                                                            </label>
                                                                            <img height="150px;" width="150px;" class="avatar-xxl" src="{{asset('storage/images/rooms/'.@$data->image)}}" />
                                                                            <a target="_blank" href="{{asset('storage/images/rooms/'.@$data->image)}}">
                                                                                <i class="fe fe-eye "></i>
                                                                            </a>
                                                                        </div>
                                                                    </div> @endif
                                                                    
                                                                    <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="status">Status</label>
                                                                            <select name="status" id="status" class="form-control">
                                                                                <option value="0" @if(@$data->status=='0') selected @endif>Active</option>
                                                                                <option value="1" @if(@$data->status=='1') selected @endif>Suspend</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="row">
                                                                  <div class="col-lg-6 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="status">Facilities</label>
                                                                            <select multiple="multiple" name="facilities[]" id="facilities " class="form-control select2 form-select">
                                                                                <option value="">Please select</option>
                                                                                @if(@$facilities!='')
                                                                                @foreach($facilities as $key=>$value)
                                                                                <option value="{{$value->id}}" @if(in_array($value->id, $data->facilities)) selected @endif>{{$value->name}}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>                                                                    
                                                                </div>

                                                                <div class="card-footer">
                                                                    <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
                                                                    <button class="btn btn-success my-1" value="submit">Update</button>
                                                                </div>
                                                            </div>
                                                          </form>
                                                    </div>
                                                </div>
                                                <div class="tab-pane @if(session('active_tab')=='2' ) active @endif" id="tab14">
                                                    <div class="row">
                                                        <form id="price_update" action="{{url('admin/rooms/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
                                                                <input type="hidden" name="action_tab" value="room_price">
                                                                <div class="row">
                                                      <div class="col-lg-6 col-md-12">
                                                      <table class="table table-bordered">
                                                                <tbody>
                                                                  <tr>
                                                                     <td class="fw-bold"></td>
                                                                     <td><b>Price (RM)</b></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Normal</td>
                                                                    <td><input type="text" class="form-control req-field" name="normal_price" value="{{@$room_price->normal_price}}"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Weekend</td>
                                                                    <td><input type="text" class="form-control req-field" name="normal_weekend" value="{{@$room_price->normal_weekend}}"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Public Holiday</td>
                                                                    <td><input type="text" class="form-control req-field" name="normal_public_holiday" value="{{@$room_price->normal_public_holiday}}"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Promotion</td>
                                                                    <td><input type="text" class="form-control req-field" name="normal_promotion" value="{{@$room_price->normal_promotion}}"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="fw-bold">School Holiday</td>
                                                                    <td><input type="text" class="form-control req-field" name="normal_school_holiday" value="{{@$room_price->normal_school_holiday}}"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                      </div>
                                                      <div class="col-lg-6 col-md-12">
                                                      <table class="table table-bordered">
                                                                <tbody>
                                                                  <tr>
                                                                     <td class="fw-bold"></td>
                                                                     <td><b>Price (RM)</b></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Normal(1 Hour)</td>
                                                                    <td><input type="text" class="form-control req-field" name="hourly_price" value="{{@$room_price->hourly_price}}"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Weekend(1 Hour)</td>
                                                                    <td><input type="text" class="form-control req-field" name="hourly_weekend" value="{{@$room_price->hourly_weekend}}"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Public Holiday(1 Hour)</td>
                                                                    <td><input type="text" class="form-control req-field" name="hourly_public_holiday" value="{{@$room_price->hourly_public_holiday}}"></td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">Promotion(1 Hour)</td>
                                                                    <td><input type="text" class="form-control req-field" name="hourly_promotion" value="{{@$room_price->hourly_promotion}}"></td>
                                                                </tr>
                                                                  <tr>
                                                                    <td class="fw-bold">School Holiday(1 Hour)</td>
                                                                    <td><input type="text" class="form-control req-field" name="hourly_school_holiday" value="{{@$room_price->hourly_school_holiday}}"></td>
                                                                </tr>                                                                
                                                            </tbody>
                                                           </table>
                                                         </div>
                                                        </div>
                                                        <div class="form-group">
                                                          <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
                                                          <button class="btn btn-success my-1" id="price_update_submit" value="button">Update</button>
                                                          <div id="error_message" style="display: none;">Required at least one field</div>
                                                      </div>
                                                      </div>
                                                      
                                                    </form>
                                                    </div>
                                                </div>
                                                <div class="tab-pane @if(session('active_tab')=='3' ) active @endif" id="tab15">
                                                    <div class="row">
                                                        <form id="room_overview" action="{{url('admin/rooms/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
                                                                <input type="hidden" name="action_tab" value="room_overview">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputname">Text</label>                                
                                                                            <textarea name="room_overview" class="form-control content" id="room_overview" placeholder="Room Overview">{!!@$data->room_overview!!}</textarea>
                                                                            @if($errors->has('room_overview')) <div class="error">{{ $errors->first('room_overview') }}</div> @endif
                                                                        </div>
                                                                    </div>                                                                   
                                                                </div>
                                                       </div>
                                                       <div class="card-footer">
                                                                    <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
                                                                    <button class="btn btn-success my-1" value="submit">Update</button>
                                                                </div>
                                                              </form>
                                                    </div>
                                                </div>
                                                <div class="tab-pane @if(session('active_tab')=='4' ) active @endif" id="tab16">
                                                    <div class="row">
                                                        <form id="room_amenities" action="{{url('admin/rooms/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
                                                                <input type="hidden" name="action_tab" value="room_amenities">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputname">Text</label>                                
                                                                            <textarea name="room_amenities" class="form-control content2" id="room_amenities" placeholder="Room Amenities">{!!@$data->room_amenities!!}</textarea>
                                                                            @if($errors->has('room_amenities')) <div class="error">{{ $errors->first('room_amenities') }}</div> @endif
                                                                        </div>
                                                                    </div>                                                                   
                                                                </div>
                                                      </div>
                                                      <div class="card-footer">
                                                                    <a href="{{url('admin/rooms')}}" class="btn btn-danger my-1">Back</a>
                                                                    <button class="btn btn-success my-1" value="submit">Update</button>
                                                                </div>
                                                              </form>
                                                    </div>
                                                </div>
                                                <div class="tab-pane @if(session('active_tab')=='5' ) active @endif" id="tab17">
                                                  <div class="row">
                                                  <form id="room_gallery" action="{{url('admin/rooms/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf
                                                     <div class="col-xl-12">
                                                          <input type="hidden" name="action_tab" value="room_gallery">
                                                          <div class="row">
                                                              <div class="col-lg-6 col-md-12">
                                                                  <div class="form-group">
                                                                      <label for="exampleInputnumber">Room Image (450 × 300) (Max upload file size 2MB)</label>
                                                                      <input class="form-control" id="image1" accept="image/*" name="image1" type="file" accept='image/*'>                                                     
                                                                  </div>
                                                              </div>
                                                              <div class="col-lg-6 col-md-12"><br>
                                                                  <div class="form-group">
                                                                    <button class="btn btn-success my-1" value="submit">Add Gallery</button>
                                                                  </div>
                                                              </div>                                                                    
                                                          </div>
                                                      </div>
                                                    </form>

                                            @if(count(@$room_gallery)!='0')
                                            @foreach(@$room_gallery as $key=>$value)
                                            <div class="col-sm-6 col-md-6 col-xl-3 alert">
                                                <div class="card">
                                                    <div class="product-grid6">
                                                        <div class="product-image6 p-5">
                                                            <a target="_blank" href="{{asset('storage/images/rooms_gallery/'.@$value->image_name)}}" class="bg-light">
                                                                <img class="img-fluid br-7 w-100" src="{{asset('storage/images/rooms_gallery/'.@$value->image_name)}}" alt="img">
                                                            </a>
                                                        </div>
                                                        <div class="card-body pt-0">
                                                            <div class="product-content text-center">
                                                                <!-- <h1 class="title fw-bold fs-20"><a href="#">gallery name</a></h1> -->
                                                              <a onclick="deletefunction(event,'{{@$value->id}}');" href="" class="btn  btn-sm btn-danger">
                                                                <i class="fe fe-x"></i>
                                                              </a>
                                                              <form id="delete_form{{@$value->id}}" method="POST" action="{{url('admin/rooms/gallery/delete/'.@$value->id)}}"> 
                                                                @csrf
                                                                <input name="_method" type="hidden" value="POST">
                                                              </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach                                            
                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
    // just for the demos, avoids form submit
    $("#image").change(function() {
        var val = $(this).val();
        switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif':
            case 'jpg':
            case 'png':
            case 'jpeg':
            case 'svg':
                //alert("an image");
                break;
            default:
                $(this).val('');
                // error message here
                //alert("Please check on image type");
                toastr.clear();
                toastr.error("Please check on image type");
                break;
        }
    });
    $("#image1").change(function() {
        var val = $(this).val();
        switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif':
            case 'jpg':
            case 'png':
            case 'jpeg':
            case 'svg':
                //alert("an image");
                break;
            default:
                $(this).val('');
                // error message here
                toastr.clear();
                toastr.error("Please check on image type");
                break;
        }
    });
    var current_date = "{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d-m-Y')}}";
    $(function() {
        $('#date').datetimepicker({
            //defaultDate: defaulsst,  
            pickTime: false,
            minView: 2,
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
    });
    $('#date').val(current_date);
    /*jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });*/
    $("#create").validate({
        submitHandler: function(form) {
            form.submit();
        },
        rules: {
            room_type: {
                required: true
            },
            total_room: {
                required: true
            }
            /*,
                  image: {
                    required: true
                  }*/
        },
        messages: {
            room_type: {
                required: "Please enter room type name",
            },
            total_room: {
                required: "Please enter total rooms",
            }
            /*,
                  image: {
                    required: "Please upload image",
                  }*/
        }
        /*,errorPlacement: function(error, element) {
                        if (element.attr("name") == "password") {
                            error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                        }else if (element.attr("name") == "usertype") {
                            error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                        }else {
                            error.insertAfter(element);
                        }                
                       },*/
    });
    $("#room_gallery").validate({
        submitHandler: function(form) {
            form.submit();
        },
        rules: {
          image1: {
            required: true
          }
        },
        messages: {            
          image1: {
            required: "Please upload image",
          }
        }
        /*,errorPlacement: function(error, element) {
                        if (element.attr("name") == "password") {
                            error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                        }else if (element.attr("name") == "usertype") {
                            error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                        }else {
                            error.insertAfter(element);
                        }                
                       },*/
    });
    function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }

  $( document ).ready(function() {
           $(".req-field").keyup(function( event ) {
              $('.req-field').each(function () {
                  $(this).removeClass( "is-invalid" );
                  $("#error_message").css("display", "none");               
              });
           });

        $("#price_update_submit").click(function( event ) {
         event.preventDefault();
         var hasInput=false;
          $('.req-field').each(function () {
           if($(this).val()  !== ""){
            hasInput=true;
            $(this).removeClass( "is-invalid" );
           }else{
            $(this).addClass( "is-invalid" );
           }
          });
          if(!hasInput){
            $("#error_message").css("display", "block").css('color','#FF0000').css("fontSize", "14px");
           }else{
            $("#error_message").css("display", "none");            
            $( "#price_update" ).submit();
           }
      }); 
});
</script>

</script> @endsection