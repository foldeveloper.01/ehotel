@extends('admin.layouts.app')
    @section('styles')
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title"></h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Guest Details</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">                                        
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="h4"><b>Guest Details for Booking Number:</b> {{@$booking->booking_number}}</p>
                                                <p class="fs-15 fw-semibold mb-0 text-secondary">Name: {{ucwords(@$booking->guest->name)}}</p>
                                                <p class="fs-15 fw-semibold mb-0 text-secondary">Phone: {{@$booking->guest->contact_number}}
                                                <p class="fs-15 fw-semibold mb-0 text-secondary">IC No: {{@$booking->guest->ic_passport_no}}</p>
                                                <p class="fs-15 fw-semibold mb-0 text-secondary">Check-in: {{@$booking->checkin_date()}}</p>
                                                <p class="fs-15 fw-semibold mb-0 text-secondary">Check-out: {{@$booking->checkout_date()}}</p>
                                            </div>                                           
                                        </div>
                                       <form id="" name="" method="post" enctype="multipart" action="{{url('admin/bookinglisting/newbooking/save_room_guest_details/')}}">
                                        @csrf
                                        <div class="table-responsive push">
                                            <div class="col-xl-12">
                                                <div class="card">
                                                    <div class="card-body">                                   
                                                        <div class="table-responsive">
                                                            <table class="table border text-nowrap text-md-nowrap mb-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Room Type</th>
                                                                        <th>Room Number</th>
                                                                        <th>Guest Name</th>
                                                                        <th>IC/Passport No</th>
                                                                        <th>Contact No</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <input type="hidden" name="bookingid" value="{{$booking->id}}">
                                                                    @php $J=0; @endphp
                                                                    @foreach($booking_details as $key=>$value)
                                                                      <tr>
                                                                        <td>{{@$value->rooms->room_type}}</td>
                                                                        <td>{{@$value->rooms_number->room_no}}</td>                                  
                                                                        <td></td><td></td><td></td>
                                                                      </tr>
                                                                         @for ($i = 0; $i < @$value->rooms->occupants; $i++)
                                                                             <tr>
                                                                                <td></td>
                                                                                <td>Guest({{$i+1}})</td>
<td><input type="text" class="form-control" name="guestname_{{@$value->room_type_id}}[{{$i}}]" placeholder="Guest Name" value="{{@$guest_data[$J]->guest_name}}"></td>
<td><input type="text" class="form-control" name="guestic_{{@$value->room_type_id}}[{{$i}}]" placeholder="IC/Passport No" value="{{@$guest_data[$J]->guest_ic_passport_no}}"></td>
<td><input type="text" class="form-control" name="guestcontact_{{@$value->room_type_id}}[{{$i}}]" value="@if(@$guest_data[$J]->guest_contact_number!=''){{@$guest_data[$J]->guest_contact_number}}@endif" placeholder="Contact No" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$"></td>
                                                                              </tr>
                                                                             @php $J++ @endphp
                                                                         @endfor

                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                     <div class="card-footer">
        <a href="{{url('admin/bookinglisting')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
                                                </div>
                                            </div>
                                        </div>
                                      </form>
                                    </div>
                                   
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')



    @endsection
