@extends('admin.layouts.app')
    @section('styles')<style type="text/css">.flex-1{display: none;}</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Booking Listing</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Booking Listing</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row row-cards">
                            <!-- COL-END -->
                            <div class="col-xl-12 col-lg-12">
                                <div class="row">
                                    
                                        <div class="col-xl-12">
                                          <div class="card">
                                            <div class="card-header">
                                              <h3 class="card-title">New Booking</h3>
                                            </div>
                                            <form id="create" action="{{url('admin/bookinglisting/newbooking')}}" method="get" enctype="multipart/form-data"> @csrf
                                            <input type="hidden" name="type" value="{{@$booking_type}}">
                                            <div class="card-body">
                                              <div class="row">
                                                <div class="col-lg-2 col-md-2">
                                                  <div class="form-group">
                                                    <label for="exampleInputname">Check-in</label>
                                                    <input type="text" value="@if($check_condition==1){{\Request::get('checkin')}}@else{{$checkin_date}} @endif" name="checkin" class="form-control" id="checkin" placeholder="checkin">
                                                   @if($errors->has('checkin')) <div class="error">{{ $errors->first('checkin') }}</div> @endif
                                                </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2">
                                                  <div class="form-group">
                                                    <label for="exampleInputname">Check-out</label>
                                                    <input type="text" value="@if($check_condition==1){{\Request::get('checkout')}}@else{{$checkout_date}} @endif" name="checkout" class="form-control" id="checkout" placeholder="checkout">
                                                   @if($errors->has('checkout')) <div class="error">{{ $errors->first('checkout') }}</div> @endif
                                                   </div>
                                                </div>

                                                                                             
                                                <div class="col-lg-2 col-md-2">
                                                  <div class="form-group">
                                                    <label for="number_of_rooms">No of Rooms</label>
                                                    <select id="number_of_rooms_check_in_form" name="number_of_rooms" class="form-control select2 form-select">
                                                      @for ($i = 1; $i <= 20; $i++)
                                                      <option value="{{$i}}" @if(\Request::get('number_of_rooms')==$i) selected @endif>{{$i}}</option>
                                                      @endfor
                                                    </select>
                                                  </div>
                                                </div>
                                                @if($check_condition!=1)                                              
                                                <div class="col-lg-2 col-md-2">
                                                  <div class="form-group">
                                                    <label for="third_party">Third Party Booking Site (Optional)</label>
                                                    <select name="third_party" class="form-control select2 form-select">
                                                      <option value="">Choose one</option>                                                      
                                                        @foreach(@$third_party as $key=>$val)
                                                         <option value="{{@$key}}">{{@$val}}</option>
                                                        @endforeach                         
                                                    </select>
                                                  </div>
                                                </div>
                                                <div class="col-lg-1 col-md-12">
                                                  <div class="form-group">
                                                     <label for="status"></label><br>
                                                     <button class="btn btn-success my-1" value="submit" id="check">Check</button>
                                                  </div>
                                                </div>
                                                @else                                                 
                                                 <input type="hidden" name="third_party" value="">
                                                 <div class="col-lg-1 col-md-12">
                                                  <div class="form-group">
                                                     <label for="status"></label><br>
                                                     <button id="next_button" type="button" class="btn btn-primary my-2">Next</button>
                                                  </div>
                                                </div>
                                                @endif                                           

                                              </div>
                                            </div>
                                            </form>
                                                <!-- <div class="card-footer">
                                                  <a href="{{url('admin/roomfacilities')}}" class="btn btn-danger my-1">Back</a>
                                                  <button class="btn btn-success my-1" value="submit">Next</button>
                                                </div> -->

                                          @if($check_condition==1)
                                          <form action="{{url('admin/bookinglisting/newbooking/view')}}" id="form_next" method="get" enctype="multipart">
                                          <div class="row">
                                              <!-- <h3 class="p-3 mb-5">Rooms List</h3> -->
                                                @if(!@$list->isEmpty())
                                                <?php 
                                                 $start_date = \Request::get('checkin');
                                                 $end_date = \Request::get('checkout');
                                                  ?>
                                        @foreach(@$list as $key=>$value)
                                        <?php //print_r($value->id); ?>
                                            <div class="col-sm-6 col-md-6 col-xl-3 alert">
                                                <div class="card">
                                                    <div class="product-grid6">
                                                        <div class="product-image6 p-5">
                                                            <ul class="icons-wishlist">
                                                                <span class="bg-danger text-white border-danger border" data-bs-dismiss="alert" aria-hidden="true">RM{{@$value->get_rooms_price($value->id,$start_date,$end_date,null)}}/night</span>
                                                            </ul>
                                                            <a href="#" class="bg-light">
                                                                <img style="height: 306px;" class="img-fluid br-7 w-100" src="{{asset('storage/images/rooms/'.@$value->image)}}" alt="img">
                                                            </a>
                                                        </div>
                                                        
                                                        <div class="card-body pt-0">
                                                            <div class="product-content text-center">
                                                                <h1 class="title fw-bold fs-20"><a href="#">{{@$value->room_type}}</a></h1>         
                                                                <!-- <div class="price">$16,599</div> -->
                                                            </div>
                                                        </div>

                                                        <div class="row row-sm">
                                                            <div class="col room_quantity">
                                                                <div class="mb-2 me-2 sizes">
                                                                <span class="fw-bold me-4">Quantity</span>
                                                                <div class="input-group input-indec input-indec1 w-40 w-sm-50 mt-2">
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="quantity_minus btn btn-white btn-number btn-icon br-4 ">
                                                                            <i class="fa fa-minus text-muted"></i>
                                                                        </button>
                                                                    </span>
                                                                    <input type="hidden" class="form-control text-center total_room" value="{{@$value->total_room}}" readonly>
                                                                    <input type="hidden" name="room_id[]" class="form-control text-center room_id" value="{{@$value->id}}" readonly>
                                                                    <input type="text" name="quantity[]" class="form-control text-center qty" value="0" readonly>
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="quantity-right-plus btn btn-white btn-number btn-icon br-4 quantity_add">
                                                                            <i class="fa fa-plus text-muted"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach                                            
                                            @endif
                                          </div>
                                          @csrf
                                            <input type="hidden" name="checkin" value="{{\Request::get('checkin')}}">
                                            <input type="hidden" name="checkout" value="{{\Request::get('checkout')}}">
                                            <input type="hidden" id="type" name="type" value="{{@$booking_type}}">
                                            <input type="hidden" name="number_of_rooms" value="{{\Request::get('number_of_rooms')}}">
                                            <input type="hidden" name="third_party" value="{{\Request::get('third_party')}}">                                            
                                        </form>
                                          @endif
                                          
                                        </div>    
                                    
                                  </div>                               
                                </div>                               
                                <!-- COL-END -->
                            </div>
                            <!-- ROW-1 CLOSED -->
                        </div>
                        <!-- ROW-1 END-->
        @if(@$check_condition==5)
            <div class="row">
               <div class="col-12">
               <!-- start loop -->
               <div class="p-4 bg-light border border-bottom-0"><h1>No Record</h1></div>
                 <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">                                
                  </ul>
               </nav><br>
              </div>
             <!-- COL-END -->
           </div>
        @endif

        @endsection

    @section('scripts')
    <script type="text/javascript">
       
       

        $(function () {
            var current_date = "{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('DD-MM-YYYY')}}";
            var checkout_date = "{{$checkout_date}}";
            var dateToday = new Date();        
            $("#checkin").datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            //todayBtn:  1,
            autoclose: true,
            startDate: 'today'
        }).on('changeDate', function (selected) {
            var checkout_date = "{{\Request::get('checkout')}}";            
            var minDate = new Date(selected.date.valueOf());
            if(checkout_date){
              /*var checkout_date = new Date(checkout_date);
              const diffInMs   = new Date(checkout_date) - new Date(minDate)
              var different_days = diffInMs / (1000 * 60 * 60 * 24);
              if(different_days==0){
                 different_days=1;
              }*/
              var newDate = addDays(minDate,1);
            }else{
              var newDate = addDays(minDate,1);
            }
            $('#checkout').datepicker('setStartDate', newDate);
            $('#checkout').val(moment(newDate).format("DD-MM-YYYY"));
            //$('#checkout').focus();
            if(checkout_date){
               $('#create').submit();                  
            }            
        });
        $("#checkout").datepicker({
            format: 'dd-mm-yyyy',
            //todayBtn:  1,
            autoclose: true,
            //todayHighlight: true,
            startDate: checkout_date,
        }).on('changeDate', function (selected) {
            var checkin_date = "{{\Request::get('checkin')}}";
                if(checkin_date){
                   $('#create').submit();
                }
        });
        function addDays(date, days) {
           const dateCopy = new Date(date);
           dateCopy.setDate(date.getDate() + days);
           return dateCopy;
        }       
    });


        var number_of_rooms = "{{\Request::get('number_of_rooms')}}";
        var rooms_data = new Array();
        $('.quantity_add').on('click', function() {            
            var $qty = $(this).closest('div').find('.qty');
            var currentVal = parseInt($qty.val());
            $("#next_button").css("display", "block");
            var check_count = 0;
            $(".qty").each(function (){
                check_count += parseInt( $(this).val());
            });

            //check total rooms condition
              var $total_room = $(this).closest('div').find('.total_room');
              var total_room = parseInt($total_room.val());
              if(total_room<currentVal+1){
                  //alert('you can\'t book this room more than '+total_room+'');
                  toastr.clear();
                  toastr.warning('you can\'t book this room more than '+total_room+'');
                  return false;
              }            
            //check total rooms condition

            if(check_count>=number_of_rooms){
                //alert('already reached out number of rooms');
                toastr.clear();
                toastr.warning('Already reached out number of rooms');
                return false;
            }
            if ( (!isNaN(currentVal)) &&  (check_count<=number_of_rooms) ) {
                $qty.val(currentVal + 1);
            }
            //get room_id
              var $room_id = $(this).closest('div').find('.room_id');
              var room_id = parseInt($room_id.val());
            //end room_id            
            
        });


        $('#next_button').on('click', function() {
            $('#form_next').submit();
        });

        $('.quantity_minus').on('click', function() {
            var $qty = $(this).closest('div').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
            }
            var check_count = 0;
            $(".qty").each(function (){
                check_count += parseInt( $(this).val());
            });
            if(check_count==0){
                $("#next_button").css("display", "none");
            }
        });
     $("#next_button").css("display", "none");        


     $("#create").validate({       
    rules: {
      checkin: {
        required: true
      },
      checkout: {
        required: true,
      }
    },
    messages: {
      checkin: {
        required: "Please select checkin date",
      },
      checkout: {
        required: "Please select checkout date",
      }
    },
    submitHandler: function(form) {
          form.submit();
        },
    /*errorPlacement: function(error, element) {
        if (element.attr("name") == "") {
            error.appendTo("#").css('color','#FF0000').css("fontSize", "14px").css('float','center');
        }else {
            error.insertAfter(element);
        }                
      },*/
  });


$(function() {
    $("#number_of_rooms_check_in_form").change(function() {
        if("{{\Request::get('checkin')}}"){
           $('#create').submit();
        }
    });
});

</script>
    @endsection
