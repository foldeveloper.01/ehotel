@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
 <!--  <h1 class="page-title">FAQ</h1>
  <div>
    <ol class="breadcrumb">     
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">FAQ List</li>
    </ol>
  </div> -->
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Check-in for Booking Number {{@$booking->booking_number}}</h3>
      </div>
      <div class="card-body">   
      <form action="{{url('admin/checkinlisting/checkin_confirm_save/'.Request::segment(5))}}" id="check_in_submit_form" name="check_in_submit_form" method="post" enctype="multipart">
      @csrf    
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatables">
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Room Type</th>
                <th class="wd-15p border-bottom-0">Room Number</th>
                <th class="wd-15p border-bottom-0">Check-in this Room?</th>
              </tr>
            </thead>
            <tbody> @foreach(@$booking->get_booking_details() as $key=>$data) @if($data->checkin_status==0) <tr>
                <td>{{@$key+1}}</td>
                <td>{{@$data->rooms->room_type}}</td>
                <td>
                    <input type="hidden" name="booking_details_ids[]" value="{{@$data->id}}">
                    <div class="form-group">                      
                      <select name="room_number[]" class="form-control select2 form-select" style="width:50%">
                        <option value="{!!@$data->rooms_number->room_no!!}" selected>{!!@$data->rooms_number->room_no!!}</option>                        
                      </select>
                    </div>
                </td>
                <td>
                  <div class="col-xl-2 ps-1 pe-1">
                    <div class="form-group">
                      <label class="custom-control custom-checkbox-md">
                            <input type="checkbox" id="{{@$data->id}}" class="custom-control-input checkin_checkbox" name="checkin_checkbox[{{@$data->rooms_number->room_no}}]" value="1" checked>
                            <span class="custom-control-label"></span>
                     </label>
                    </div>
                  </div>
                </td>
              </tr>@endif @endforeach 
              <td></td>
              <td>                
                  <div class="form-group">
                    <label class="custom-control custom-checkbox-md">
                          <input type="checkbox" class="custom-control-input" name="collect_deposit" value="1" @if(@$booking->deposit=='yes') checked @endif>
                          <span class="custom-control-label">Collect Deposit</span>
                   </label>
                   <span id="error"></span>
                  </div>
              </td>
              <td> 
                  <div class="form-group">
                    <label for="exampleInputname">Deposit Amount(RM)</label>
                    <?php $deposit_amount='50.00'; if($booking->deposit_amount!='0.00'){$deposit_amount=$booking->deposit_amount;}?>
                    <input type="number" value="{{$deposit_amount}}" name="deposit_amount" class="form-control" id="deposit_amount" placeholder="Deposit Amount" style="width:50%">
                   </div>
              </td>
              <td>
                  <div class="form-group">
                      <label for="payment_type">Payment Mode</label><br>
                      <select name="payment_type" class="form-control select2 form-select" style="width:50%"> 
                        <option value="1" @if(@$booking->deposit_payment_status==1) selected @endif>Cash</option>
                        <option value="2" @if(@$booking->deposit_payment_status==2) selected @endif>Credit Bill</option>
                        <option value="3" @if(@$booking->deposit_payment_status==3) selected @endif>Bank-In</option>
                        <option value="4" @if(@$booking->deposit_payment_status==4) selected @endif>Credit Card</option>
                        <option value="5" @if(@$booking->deposit_payment_status==5) selected @endif>Debit card</option>
                        <option value="6" @if(@$booking->deposit_payment_status==6) selected @endif>Cheque</option>
                      </select>
                  </div>                
              </td>              
             </tbody>
          </table>
          <p> <span class="text-red">Note:</span> Please Check the box to collect deposit</p>
            
            <div class="card-footer">
              <a href="{{url('admin/bookinglisting')}}" class="btn btn-danger my-1">Back</a>
             <button type="button" class="btn btn-success my-1" id="checkin_submit" value="submit">Check-in</button>
            </div>

          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  
  $('#checkin_submit').on("click",function() {
       var booking_details_id = [];
       if($('.checkin_checkbox:checkbox:checked').length == 0){
         //alert('Please check atleast one checkin rooms');
         toastr.clear();
         toastr.warning('Please check atleast one checkin rooms');
         return false;
       }
      //store id
      $('.checkin_checkbox:checkbox:checked').each( function(i,e) {            
            booking_details_id.push($(e).attr('id'));
      });
      //check housekeeping
        $.ajax({
            url: site_url + '/admin/checkinlisting/checkin_confirm/check_housekeeping_room',
            type: 'post',
            dataType: 'json',
            data: {
              id: booking_details_id,
              _token: '{{csrf_token()}}'
            },
            success: function(data) {
              if(data.response==1){
                 toastr.clear();
                 toastr.warning(data.message);
                 return false;
               }else{
                 $("#check_in_submit_form").submit();
               }
            }
        });
      //check housekeeping
      //console.log(booking_details_id);
  }); 
  $("#check_in_submit_form").validate({
    /*submitHandler: function(form) {
      form.submit();
    },*/
    rules: {
      collect_deposit: {
        required: true
      },
      deposit_amount: {
        required: true
      }
    },
    messages: {
      collect_deposit: {
        required: "Please collect deposit",
      },
      deposit_amount: {
        required: "Please enter deposit amount",
      }
    },errorPlacement: function(error, element) {
      if (element.attr("name") == "collect_deposit") {
          error.appendTo("#error").css('color','#FF0000').css("fontSize", "14px").css('float','center');
      }else {
          error.insertAfter(element);
      }                
     },
  });
</script> @endsection