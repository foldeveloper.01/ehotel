@extends('admin.layouts.app')
@section('styles')
@endsection
@section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
   <h1 class="page-title"></h1>
   <div>
      <ol class="breadcrumb" id="si-printer1">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Extend Date</li>
      </ol>
   </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-header">
            <h4>Extend Date</h4>
         </div>
         <div class="card-body">
            <div class="row pt-3">
               <div class="col-lg-4">
                  <!-- <p class="h4"><b>Booking No:</b> {{@$booking->booking_number}}</p>
                     <p class="h4"><b>Name:</b> {{@$booking->guest->name}}</p>
                     <p class="h4"><b>Email:</b> {{@$booking->guest->email}}</p>
                     <p class="h4"><b>Contact Number:</b> {{@$booking->guest->contact_number}}</p>
                     <p class="h4"><b>Check-In:</b> {{@$booking->checkin_date()}}</p>
                     <p class="h4"><b>Check-Out:</b> {{@$booking->checkout_date()}}</p>
                     @foreach($booking_details as $key=>$value)
                     <p class="h4"><b>Room:</b> {{@$value->rooms_number->room_no}} <span class="fs-15 fw-semibold mb-0 text-danger">(RM{{@$value->room_price}})</span></p>
                     @endforeach
                     <p class="h4"><b>Service Charge:</b> <span class="fs-15 fw-semibold mb-0 text-danger">RM{{@$booking->service_tax_amount}}</span></p>
                     <p class="h4"><b>Total:</b> <span class="fs-15 fw-semibold mb-0 text-danger">RM{{@$total}}</span></p> -->
                  <dl class="row card-body py-2">
                     <dt class="col-sm-4"><b>Booking No</b></dt>
                     <dd class="col-sm-6"> {{@$booking->booking_number}} </dd>
                     <dt class="col-sm-4"><b>Guest Name</b></dt>
                     <dd class="col-sm-6"> {{@$booking->guest->name}} </dd>
                     <dt class="col-sm-4"><b>Guest Email</b></dt>
                     <dd class="col-sm-6">{{@$booking->guest->email}}</dd>
                     <dt class="col-sm-4"><b>Contact Number</b></dt>
                     <dd class="col-sm-6">+{{@$booking->guest->contact_number}}</dd>
                     <dt class="col-sm-4"><b>Check-In</b></dt>
                     <dd class="col-sm-6">{{@$booking->checkin_date()}}</dd>
                     <dt class="col-sm-4"><b>Check-Out</b></dt>
                     <dd class="col-sm-6">{{@$booking->checkout_date()}}</dd>
                     @foreach($booking_details as $key=>$value)
                     @if($key==0)
                     <dt class="col-sm-4"><b>Rooms</b></dt>
                     @else
                     <dt class="col-sm-4"><b></b></dt>
                     @endif
                     <dd class="col-sm-6">{{@$value->rooms_number->room_no}} <span class="fs-15 fw-semibold mb-0 text-danger">(RM{{@$value->room_price}})</span></dd>
                     @endforeach
                     <dt class="col-sm-4"><b>Service Charge</b></dt>
                     <dd class="col-sm-6"><span class="fs-15 fw-semibold mb-0 text-danger">RM{{@$booking->service_tax_amount}}</span></dd>
                     <dt class="col-sm-4"><b>Total</b></dt>
                     <?php $get_total_amount = Helper::get_total_amount($booking->id);?>
                     <dd class="col-sm-6"><span class="fs-15 fw-semibold mb-0 text-danger">
                           @if($get_total_amount < 0 )
                               -RM {{number_format(abs(@$get_total_amount),2)}}
                           @else
                               RM {{number_format(@$get_total_amount,2)}}
                           @endif</span></dd>
                  </dl>
               </div>
               <div class="col-lg-3">
                  <input type="hidden" id="bookingid" name="bookingid" value="{{@$booking->id}}">
                  <div class="col-lg-6 col-md-12">
                     <p>Check-In : {{@$booking->checkin_date()}}</p>
                  </div>
                  <div class="col-lg-6 col-md-12">
                     <div class="form-group">
                        <label for="exampleInputname">Check-out</label>
                        <input type="text" value="{{@$checkout_date}}" name="checkout" class="form-control" id="checkout" placeholder="checkout" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-12">                                                
                     <a href="{{url('admin/bookinglisting')}}" class="btn btn-danger my-1">Back</a>
                     <button class="btn btn-success my-1" value="button" id="check_date_button">Check</button>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="change_details">
                     @foreach(@$booking_details as $key=>$data)
                     <p class="h4">Room: {{@$data->rooms_number->room_no}} (RM<span class="{{@$data->rooms_number->room_no}}"></span>)</p>
                     @endforeach
                     <p class="h4">Upgrade Amount: <span class="text-danger">RM</span> <span class="upgrade_amount text-danger"></span></p>
                     <p class="h4">Service Charge: <span class="text-danger">RM</span> <span class="service_amount text-danger"></span> </p>
                     <p class="h4">Total Amount to pay: <span class="text-danger">RM</span> <span class="total_amount text-danger"></span></p>
                  </div>
                  <form id="change_details_form" action="{{url('admin/bookinglisting/booking_extend_date/save')}}" method="post">
                     @csrf
                     <input type="hidden" name="upgrade_amount" id="upgrade_amount">
                     <input type="hidden" name="service_amount" id="service_amount">
                     <input type="hidden" name="total_amount" id="total_amount">
                     <input type="hidden" name="checkout_date" id="checkout_date">
                     <input type="hidden" id="bookingid" name="bookingid" value="{{@$booking->id}}">
                     <div class="refund_div">
                        <p class=""><b>Refund:</b>
                        <div class="custom-radio custom-control-inline">
                           <label class="custom-control custom-radio-md">
                           <input type="radio" class="custom-control-input" name="check_refund" value="1">
                           <span class="custom-control-label">Yes</span>
                           </label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <label class="custom-control custom-radio-md">
                           <input type="radio" class="custom-control-input" name="check_refund" value="0">
                           <span class="custom-control-label">No</span>
                           </label>
                        </div>
                        </p>
                     </div>
                     <div class="refund_amount">
                        <p class=""><b>Enter Refund Amount:</b>
                           <input type="number" class="form-control col-lg-4" name="refund_amount" id="refund_amount" value="">
                        </p>
                     </div>
                     <div class="payment_div">
                        <p class="">
                           <b>Payment Methods:</b>
                           <select id="payment_method" name="payment_method" class="form-control" style="width:50%">
                              <option value="">choose</option>
                              <option value="1">Cash</option>
                              <option value="4">Credit Card</option>
                           </select>
                        </p>
                     </div>
                  </form>
                  <div class="change_details">
                     <button  type="button" id="confirm_change_date" class="btn btn-success">Confirm to Extend Date</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- COL-END -->
</div>
<!-- ROW-1 CLOSED -->
@endsection
@section('scripts')
<script type="text/javascript">
   $(function () { 
   var checkin_date = "{{@$booking->checkin_date()}}";
   var checkout_date = "{{@$checkout_date}}";
   var dateToday = new Date();        
   $("#checkin").datepicker({
   format: 'dd-mm-yyyy',
   todayHighlight: true,
   //todayBtn:  1,
   autoclose: true,
   startDate: 'today',
   orientation: "bottom left",
   startDate: checkin_date,
   }).on('changeDate', function (selected) {
       var minDate = new Date(selected.date.valueOf());
       const newDate = addDays(minDate,1);
       $('#checkout').datepicker('setStartDate', newDate);
       $('#checkout').val(moment(newDate).format("DD-MM-YYYY"));
       //$('#checkout').focus();
   });
   $("#checkout").datepicker({
   format: 'dd-mm-yyyy',
   //todayBtn:  1,
   autoclose: true,
   //todayHighlight: true,
   startDate: checkout_date,
   orientation: "bottom left"
   }).on('changeDate', function (selected) {
       var minDate = new Date(selected.date.valueOf());           
       $('#checkout_date').val(moment(minDate).format("DD-MM-YYYY"));
   });
   $("#checkout").datepicker('setDate', "{{@$checkout_date}}");
   function addDays(date, days) {
     const dateCopy = new Date(date);
     dateCopy.setDate(date.getDate() + days);
     return dateCopy;
   }       
   });
   
   $('.payment_div').hide();
   $('.refund_div').hide();
   $('.refund_amount').hide();
   $(document).on('click','input[name="check_refund"]',function(){        
   if($(this).val()==0){
       $('.payment_div').hide();
       $('.refund_amount').hide();
   }else{
       $('.payment_div').show();
       $('.refund_amount').show();
       $('#refund_amount').val(Math.abs($("#total_amount").val()));
   }
   });
   
   $(document).on('click','#confirm_change_date',function(){
   if($('input[name="check_refund"]:checked').val()==1){
       //refund_amount
       if(!$('#refund_amount').val()){
          toastr.clear();
          toastr.error('Please enter refund amount');
          return false;   
        }
        if(!$('#payment_method :selected').val()){               
          toastr.clear();
          toastr.error('Please choose payment type');
          return false;   
        }
       //payment_method
   }
   $('#change_details_form').submit();
   })
   
   
   $('.change_details').hide();
   $(document).on('click','#check_date_button',function(){
           var checkin_date = $("#checkin").val();
           var checkout_date = $("#checkout").val();                 
           var bookingid = $("#bookingid").val();                 
             $.ajax({
             url: site_url + '/admin/bookinglisting/booking_extend_date',
             type: 'post',
             dataType: 'json',
             data: {
               checkin_date: checkin_date,
               checkout_date: checkout_date,
               bookingid : bookingid,
               _token: '{{csrf_token()}}'
             },
             success: function(data) {
               if(data.response==1){
                   toastr.clear();
                   toastr.warning(data.message);
                   $('.change_details').hide();
                   $('.refund_div').hide();
                   $('.refund_amount').hide();
                   $('.payment_div').hide();
                   return false;
                 }
                 $(".change_details").show();
                 $('.refund_div').show();
                 $(".service_amount").html(data.data.service_charge);
                 $("#service_amount").val(data.data.service_charge);                      
                 $(".upgrade_amount").html(data.data.upgrade_amount);
                 $("#upgrade_amount").val(data.data.upgrade_amount);                      
                 $(".total_amount").html(data.data.total_amount_to_pay);
                 $("#total_amount").val(data.data.total_amount_to_pay);
                 //refund hide
                  if(data.data.condition=='add') {
                      $("input[name='check_refund']").prop('checked',false);
                      if(data.data.total_amount_to_pay!='0.0') {
                         $('.payment_div').show();
                         $("#payment_method").val(1);
                         $('.refund_div').hide();
                          //append new payment option
                          $("#payment_method option[value='8']").remove();
                          $("#payment_method option[value='9']").remove();
                          $('#payment_method').append($('<option>', {value: 8, text: 'Pay Later' }));
                          $('#payment_method').append($('<option>', {value: 9, text: 'Free' }));
                          //append new payment option
                      }else{
                         $('.payment_div').hide();
                         $("#payment_method option:selected").prop("selected", false);
                      }                           
                   }else{
                       $("#payment_method option[value='8']").remove();
                       $("#payment_method option[value='9']").remove();
                       $("input[name='check_refund'][value=0]").prop('checked',true);
                       $('.refund_amount').hide();
                       $('.payment_div').hide();                           
                   }                   
                 //refund show
                 $.each(data.data.room_numbers, function(k, v) {
                    //console.log(k);
                    //console.log(v);
                    $("."+k+"").html(v.split('_')[1]);                         
                    //remove last input element                         
                    $('#room_details_'+v.split('_')[0]+'').remove();
                    //remove last input element                       
                    $("form#change_details_form").append("<input id='room_details_"+v.split('_')[0]+"' name='room_details_id[]' value='"+v.split('_')[0]+"_"+v.split('_')[1]+"' type='hidden'/>")
                 });
           }
       });           
   });
      
</script>
@endsection