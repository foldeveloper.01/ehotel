@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Invoice</h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                    <img src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" class="header-brand-img logo-3" alt="logo">
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{@$setting->address}}
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <h3>Booking No-{{@$booking->booking_number}}</h3>
                                                <h5><b>Check-in:</b> {{\Carbon\carbon::parse(@$booking->check_in)->format('d-m-Y')}}</h5>
                                                <h5><b>Check-out:</b> {{\Carbon\carbon::parse(@$booking->check_out)->format('d-m-Y')}}</h5>
                                                <h5><b>Date:</b>{{\Carbon\Carbon::parse(@$booking_payment->created_at)->format('d-m-Y')}}</h5>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-4">
                                                <p class="fs-15 fw-semibold mb-0">{{ucwords(@$booking->guest->name)}}</p>                                  
                                                <address>
                                                        {{@$booking->guest->address}}<br>
                                                        @if(@$booking->guest->city)
                                                         {{@$booking->guest->city}},
                                                        @endif
                                                        @if(@$booking->guest->state)
                                                         {{@$booking->guest->state}},
                                                        @endif
                                                         <br>
                                                        @if(@$booking->guest->country)
                                                         {{@$booking->guest->country}},
                                                        @endif
                                                        @if(@$booking->guest->postcode)
                                                         {{@$booking->guest->postcode}}
                                                        @endif
                                                        <br>
                                                        {{@$booking->guest->email}}
                                                    </address>
                                            </div>
                                            <!-- <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>                                                
                                            </div> -->
                                        </div>
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap table-striped">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th>Date</th>
                                                        <th>Description </th>
                                                        <!-- <th>Rate / Night(s)</th> -->
                                                        <th class="text-end">Amount(RM)</th>
                                                    </tr>                                                                                                       
                                                        
                                                        @foreach($booking->booking_details as $key=>$data)                                                        
                                                        <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>
                                                        <td>

                                                        @if(@$data->is_booking_change_date==1)                                                            
                                                            Change Date from 
                                                              {{@$data->old_checkin_date()}} - {{@$data->old_checkout_date()}} to
                                                              {{@$data->checkin_date()}} - {{@$data->checkout_date()}} (Room No -
                                                              {{@$data->rooms_number->room_no}})                                                            
                                                        @elseif(@$data->is_booking_change_room==1)
                                                            @if(@$data->old_rooms_id=='') )
                                                               {{@$data->rooms->room_type}} - {{@$data->rooms_number->room_no}}
                                                            @else
                                                               Change Room from {{@$data->change_room_number->room_no}} (Room No -
                                                              {{@$data->rooms_number->room_no}})
                                                            @endif
                                                        @elseif(@$data->is_booking_extend_date==1)
                                                            Extend Date from {{@$data->old_checkin_extend_date()}} - {{@$data->old_checkout_extend_date()}} to
                                                              {{@$data->checkin_date()}} - {{@$data->checkout_date()}} (Room No -
                                                          {{@$data->rooms_number->room_no}})
                                                        @else
                                                           {{@$data->rooms->room_type}} - {{@$data->rooms_number->room_no}}
                                                        @endif
                                                           
                                                           <!--  @if(@$data->old_checkin_date=='' && @$data->old_rooms_id=='')
                                                               {{@$data->rooms->room_type}} - {{@$data->rooms_number->room_no}}
                                                            @else
                                                              @if(@$data->old_checkin_date!='')
                                                                Change Date from 
                                                                  {{@$data->old_checkin_date}} - {{@$data->old_checkout_date}} to
                                                                  {{@$data->check_in}} - {{@$data->check_out}} to
                                                                  {{@$data->rooms_number->room_no}}
                                                              @endif
                                                              @if(@$data->old_rooms_id!='')
                                                                Change Room from {{@$data->change_room_number->room_no}} to
                                                                  {{@$data->rooms_number->room_no}}
                                                              @endif
                                                            @endif -->                                                            
                                                           
                                                        </td>
                                                            <!-- <td> {{@$data->room_price}} / {{@$data->number_of_days}} Night(s) </td> -->
                                                            <td class="text-end">{{@$data->subtotal_amount}}</td>
                                                        </tr>                                                       
                                                        @endforeach

                                                        @foreach($booking->booking_exsit_addon_details as $key=>$data)
                                                        @php $booking_addon_id[]=$data->id @endphp
                                                        <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>     
                                                            <td >{{@$data->getaddon->add_on_name}} - {{@$data->quantity}} Qty</td>
                                                            <!-- <td class=""></td> -->
                                                            <td class="text-end">{{@$data->total_amount}}</td>
                                                        </tr>
                                                        @endforeach
                                                        
                                                        @foreach($booking->booking_payment_details as $paymentkey=>$payment)
                                                        @if(@$payment->refund_payment_check==1 && @$payment->invoice_type =='booking_date_change')
                                                          <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Change Date from {{@$payment->others}} (Refund)</td>
                                                            <!-- <td class=""></td> -->
                                                            <td class="text-end">-{{@$payment->total_amount}}</td>
                                                          </tr>
                                                          <!-- <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Refund </td>
                                                            <td class="text-end">{{@$payment->total_amount}}</td>
                                                          </tr> -->
                                                        @elseif(@$payment->refund_payment_check==1 && @$payment->invoice_type =='booking_room_change')

                                                         <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Change Room from {{@$payment->others}} (Refund)</td>
                                                            <td class="text-end">-{{@$payment->total_amount}}</td>
                                                          </tr>
                                                          <!-- <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Refund </td>
                                                            <td class="text-end">{{@$payment->total_amount}}</td>
                                                          </tr> -->

                                                        @elseif(@$payment->refund_payment_check==1 && @$payment->invoice_type !='refund')
                                                          <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Refund for booking Early Checkout </td>
                                                            <td class="text-end">-{{@$payment->total_amount}}</td>
                                                          </tr>
                                                          <!-- <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>     
                                                            <td > Refund Paid Early Checkout </td>
                                                            <td class="text-end">-{{@$payment->total_amount}}</td>
                                                          </tr> -->
                                                        @endif                                                        
                                                        @endforeach                                                        
                                                          
                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Service Charge
                                                            @if(@$booking->service_tax_details!=null)
                                                              @if(explode('_',@$booking->service_tax_details)[0]!='amount')
                                                                @ {{explode('_',@$booking->service_tax_details)[1]}} %
                                                              @else
                                                                @ {{explode('_',@$booking->service_tax_details)[1]}} MYR
                                                              @endif
                                                             @endif
                                                            </td>
                                                            <td class="text-end">MYR {{@$booking->service_tax_amount}}</td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Discount</td>
                                                            <td class="text-end">-{{number_format(@$booking->discount_amount,2)}}</td>
                                                          </tr>                                                                                         
                                                          <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Total</td>
                                                            <td class="fw-bold text-end h4">{{number_format(@$total_amount,2)}}</td>
                                                          </tr>                                                    
                                                    
                                                    @foreach($booking->booking_exsit_addon_payment_details($booking->id) as $key=>$data)
                                                      @if($data->refund_payment_check==0)
                                                        <tr> 
                                                           <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td>
                                                           <td >Payment By {{ucwords(@$data->payment_type)}}</td>
                                                           <td class="text-end">-{{@$data->total_amount}}</td>
                                                        </tr>
                                                      @endif
                                                    @endforeach
                                                    @foreach($booking->booking_change_payment_details($booking->id) as $key=>$data)
                                                      @if($data->refund_payment_check==0)
                                                        <tr> 
                                                           <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td> 
                                                           <td >Payment By {{ucwords(@$data->payment_type)}}</td>
                                                           <td class="text-end">-{{@$data->total_amount}}</td>
                                                        </tr>
                                                      @else
                                                       <tr> 
                                                           <td >{{\Carbon\Carbon::parse(@$data->created_at)->format('d-m-Y')}}</td> 
                                                           <td >Payment By {{ucwords(@$data->payment_type)}}</td>
                                                           <td class="text-end">+{{@$data->total_amount}}</td>
                                                        </tr>
                                                      @endif
                                                    @endforeach
                                                        <tr>
                                                            <td colspan="2" class="fw-bold text-uppercase text-end">Balance</td>
                                                            <td class="fw-bold text-end h4">{{number_format(@$balance_amount,2)}}</td>
                                                        </tr>
                                                    @foreach($booking->booking_payment_details as $paymentkey=>$payment)
                                                        @if(@$payment->refund_payment_check==1 && @$payment->invoice_type =='refund')
                                                          <tr> 
                                                            <td >{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>
                                                            <td class=""> Refund for booking </td>
                                                            <td class="text-end">{{@$payment->total_amount}}</td>
                                                          </tr>                                                          
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div><br>This is a computer generated document. No signature is required.
                                    <div class="card-footer">                                                                         
                                        <button id="si-printer" type="button" class="btn btn-success mb-1 text-end" onclick="javascript:window.print();"><i class="si si-printer"></i>Print</button>
                                    </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

        <div class="modal fade" id="scrollingmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                        </button>
                    </div>
                <form id="save_price_form">
                    <div class="modal-body">
                      <div class="col-lg-10 col-md-12">
                        <div class="form-group">
                            <input type="hidden" name="receipt_id" id="receipt_id">
                          <label for="remark"><b>Amount<b></label>
                          <input class="form-control mb-4" placeholder="Amount" id="amount" name="amount" type="number" pattern="^\d*(\.\d{0,2})?$">
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="button" id="save_price_button" class="btn btn-primary">Save</button>
                    </div>
                   </div>
               </form>
                </div>
            </div>
        </div>

        @endsection

    @section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
      //click view new windows for print
      $(document).on('click','.click_view',function(e){
         e.preventDefault();
         window.open($(this).parent().parent().find('a').attr('href'), "_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");         
      });
      //click view new windows for print

      $(document).on('click','.edit_price',function(){
         $("#receipt_id").val($(this).attr('id'));
          var id = $(this).attr('id');
          $("#amount").val($(this).parent().parent().find('#rowamount_'+id+'').html());
      });

      $("#save_price_form").validate({
            submitHandler: function(form) {
              form.submit();
            },
            rules: {
              amount: {
                required: true,
                number: true
              }
            },
            messages: {
              amount: {
                required: "Please enter amount",
              }
            },    
       });

      $(document).on('click','#save_price_button',function(){        
        if ($('#save_price_form').valid()) {
                 var receipt_id = $("#receipt_id").val();
                 var amount = $("#amount").val();
                  $.ajax({
                  url: site_url + '/admin/bookinglisting/newbooking/addon_invoice/update_price',
                  type: 'post',
                  dataType: 'json',
                  data: {
                    id: receipt_id,
                    amount: amount,
                    _token: '{{csrf_token()}}'
                  },
                  success: function(data) {
                      toastr.clear();
                      toastr.success('Amount updated successfully');
                      $("#rowamount_"+receipt_id+"").html(data.amount);
                    }           
                });
                $('.btn-close').click();
            }
      });      
      
    });
</script>
    @endsection
