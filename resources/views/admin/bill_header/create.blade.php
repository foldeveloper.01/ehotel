@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Bill Header</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Bill Header</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <div class="col-xl-12">
    <form id="create_user" action="{{url('admin/billheader')}}" method="post" enctype="multipart/form-data"> @csrf <div class="card">
        <div class="card-header">
          <h3 class="card-title">Bill Header</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="@if(@$data->header_image!=null && @$data->header_image!='') col-lg-7 @else col-lg-8 @endif col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Header</label>
                <input class="form-control" accept="image/*" name="header_image" type="file">
              </div>
            </div> @if(@$data->header_image!=null && @$data->header_image!='') <div class="col-lg-1 col-md-12">
              <div class="form-group">
                <label class="form-label">
                  <br>
                </label>
                <img height="150px;" width="150px;" class="avatar avatar-md br-7" src="{{asset('storage/images/bill_header/'.@$data->header_image)}}" />
                <a target="_blank" href="{{asset('storage/images/bill_header/'.@$data->header_image)}}">
                  <i class="fe fe-eye "></i>
                </a>
              </div>
            </div> @endif
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Footer Message</label>
                <textarea class="content" name="footer_message">{!!@$data->footer_message!!}</textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label class="form-label">Footer customer signature</label>
                <div class="custom-radio custom-control-inline">
                  <label class="custom-control custom-radio-md">
                    <input type="radio" class="custom-control-input" name="customer_signature" value="0" @if(@$data->customer_signature=='0') checked="" @endif> <span class="custom-control-label">Show</span>
                  </label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <label class="custom-control custom-radio-md">
                    <input type="radio" class="custom-control-input" name="customer_signature" value="1" @if(@$data->customer_signature=='1') checked="" @endif> <span class="custom-control-label">Hide</span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Update</button>
          <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script> @endsection