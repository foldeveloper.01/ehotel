@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Banner</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Banner</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <div class="col-xl-12">
    <form id="create" action="{{url('admin/banner/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Banner</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Banner Title</label>
                <input type="text" value="{{old('title')}}" name="title" class="form-control" id="title" placeholder="Title">
              </div> @if($errors->has('title')) <div class="error">{{ $errors->first('title') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Banner Title1</label>
                <input type="text" value="{{old('title1')}}" name="title1" class="form-control" id="title1" placeholder="Title1">
              </div> @if($errors->has('title1')) <div class="error">{{ $errors->first('title1') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">URL (eg: http://www.google.com) </label>
                <input type="text" value="{{old('url')}}" name="url" class="form-control" id="url" placeholder="URL">
              </div> @if($errors->has('url')) <div class="error">{{ $errors->first('url') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="position">Sort</label>
                <input type="number" value="{{old('position')}}" name="position" class="form-control" id="position" placeholder="Sort">
              </div> @if($errors->has('position')) <div class="error">{{ $errors->first('position') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="description">Banner Description</label>
                <textarea class="content" name="description">{{old('description')}}</textarea>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputnumber">Banner Image</label>
                <input class="form-control" accept="image/*" name="banner" type="file">
              </div> @if($errors->has('banner')) <div class="error">{{ $errors->first('banner') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label class="form-label">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-success my-1" value="submit">Save</button>
          <a href="{{url('admin/banner')}}" class="btn btn-danger my-1">Back</a>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  $(document).ready(function() {
    $('.summernote').summernote();
  });
  // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      title: {
        required: true
      },
      banner: {
        required: true
      }
    },
    messages: {
      title: {
        required: "Please enter title",
      },
      banner: {
        required: "Please upload banner image",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection