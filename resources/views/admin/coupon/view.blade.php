@extends('admin.layouts.app')

@section('styles')

@endsection

@section('content')

<!-- PAGE-HEADER -->
<div class="page-header">
    <h1 class="page-title">Coupon Details</h1>
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Coupon Details</li>
        </ol>
    </div>
</div>
<!-- PAGE-HEADER END -->

<!-- ROW-1 OPEN -->
<div class="row">                            
    <div class="col-xl-12 col-md-12">
        <div class="card productdesc">
            <div class="card-body">
                <div class="panel panel-primary">
                    <!-- <div class=" tab-menu-heading">
                        <div class="tabs-menu1">
                            <ul class="nav panel-tabs">
                                <li><a href="#tab5" class="active" data-bs-toggle="tab"></a></li>                                
                            </ul>
                        </div>
                    </div> -->
                    <div class="panel-body tabs-menu-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab5" style="width: 70%;">
                                <h4 class="mb-5 mt-3 fw-bold">Coupon Details :</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 70%;">
                                        <tr>
                                            <td class="fw-bold">Name</td>
                                            <td> {{@$data->name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Code</td>
                                            <td> {{@$data->code}}</td>
                                        </tr> 
                                        <tr>
                                            <td class="fw-bold">Discount Type</td>
                                            <td><?php if($data->discount_type=='amount'){
           $discount_type='By Amount <br> Amount: '.$data->amount.'';
        }elseif($data->discount_type=='percentage'){
           $discount_type='By Percentage <br><h4>'.$room_name.'</h4>Normal Price: '.$data->getCouponRoomPrice->normal_price.' (%)  <br> Weekend Price: '.$data->getCouponRoomPrice->weekend_price.' (%)<br> Promotion Price: '.$data->getCouponRoomPrice->promotion_price.' (%)<br> School Holiday Price: '.$data->getCouponRoomPrice->school_holiday_price.' (%)<br> Public Holiday Price: '.$data->getCouponRoomPrice->public_holiday_price.' (%)';
        } echo $discount_type;?>
        </td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Coupon Entitle</td>
                                            <td> Check-In Between {{\Carbon\Carbon::parse(@$data->check_in_from_date)->format('d-m-Y')}} and {{\Carbon\Carbon::parse(@$data->check_in_to_date)->format('d-m-Y')}}</td>
                                        </tr>  
                                        <tr>
                                            <td class="fw-bold">Expired</td>
                                            <td>{{\Carbon\Carbon::parse(@$data->end_date)->format('d-m-Y')}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Total Coupon</td>
                                            <td> {{@$data->total_coupon}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Used</td>
                                            <td> {{@$data->is_used}}</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Status</td>
                                            <td> @if(@$data->status=='0') Active @else Suspended @endif</td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold">Created_at</td>
                                            <td> {{\Carbon\Carbon::parse(@$data->Created_at)->format('d-m-Y')}}</td>
                                        </tr>                                                                  
                                    </table>
                                </div>
                            </div>                            
                       </div>

                   </div>
                   <div class="card-footer">
        <a href="{{url('admin/coupon')}}" class="btn btn-danger my-1">Back</a>        
      </div>
               </div>
           </div>
       </div>
   </div>
   
   
   
</div>
<!-- ROW-1 CLOSED -->

@endsection

@section('scripts')

<!-- INTERNAL SELECT2 JS -->
<script src="{{asset('assets/admin/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/admin/js/select2.js')}}"></script>

@endsection
