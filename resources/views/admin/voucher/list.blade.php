@extends('admin.layouts.app') @section('styles')@endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Voucher</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Voucher List</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Voucher List</h3>
      </div>
      <div class="card-body">
        <a href="{{url('admin/voucher/create')}}">
          <button id="table2-new-row-button" class="btn btn-primary mb-4">Create Voucher</button>
        </a>       
        <div class="table-responsive">
          <table class="table border text-nowrap text-md-nowrap mb-0" id="data" >
            <thead class="table-primary">
              <tr>
                <th class="wd-15p border-bottom-0">#</th>
                <th class="wd-15p border-bottom-0">Name</th>
                <th class="wd-15p border-bottom-0">Code</th>
                <th class="wd-15p border-bottom-0">Amount</th>
                <th class="wd-15p border-bottom-0">Expired</th>
                <th class="wd-1p border-bottom-0">Used</th>
                <th class="wd-1p border-bottom-0">Status</th>
                <th class="wd-1p border-bottom-0">Created_at</th>
                <th class="wd-1p border-bottom-0">Action</th>
              </tr>
            </thead>           
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    // DataTable

     var table = $('#data').DataTable({
         processing: true,
         serverSide: true,
         "pageLength": 10,
         "paging": true,
         ajax: "{{url('admin/voucher/getdata')}}",
         columns: [
            { data: 'id' },
            { data: 'name' },
            { data: 'code' },
            { data: 'amount' },
            { data: 'end_date' },
            { data: 'used' },
            { data: 'status' },
            { data: 'created_at' },
            { data: 'action' },
         ],'columnDefs': [ {
            'targets': [0,5,8],
            'orderable': false,
         }],         
         "order": [[7,"desc"]],
      });

     table.on('draw.dt', function () {
          var info = table.page.info();
          table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
              cell.innerHTML = i + 1 + info.start;
          });
      });

      //$('.custom-switch-input').on('change', function() {
      $('body').on('change', '.custom-switch-input', function() {
        if($(this).prop('checked')==true){
        var status_message = 'Are you sure, Do you want to Activate the data?';
      }else{
        var status_message = 'Are you sure, Do you want to Suspend the data?';
      }
      var option = confirm(status_message);    
      if (!option) {
        if ($(this).prop('checked')) {
            $(this).prop('checked', !$(this).prop('checked'));
        } else {
            $(this).prop('checked', !$(this).prop('checked'));
        }
        return false;
      }
      var isChecked = $(this).is(':checked');
      var id = $(this).attr('id');
      var selectedData;
      var $switchLabel = $('.custom-switch-indicator');
      //console.log('isChecked: ' + isChecked);
      if (isChecked) {
        selectedData = $switchLabel.attr('data-on');
      } else {
        selectedData = $switchLabel.attr('data-off');
      }
      //console.log('Selected data: ' + selectedData);
      $.ajax({
        url: site_url + '/admin/voucher/ajax_status_update',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          selectedData: selectedData,
          _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response['data'] == 1) {
            toastr.clear();
            if (selectedData == 'Yes') {
              var success = 'Status Activate';
              toastr.success(success + ' successfully');
            } else {
              var success = 'Status Suspend';
              toastr.error(success + ' successfully');
            }
          } else {
            toastr.clear();
            toastr.error('something went wrong');
          }
        }
      });
    });    
  });

  function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }
</script> @endsection