@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Voucher</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create Voucher</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/voucher/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Create Voucher</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Name</label>
                <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name" placeholder="Name">
              </div> @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
            </div> 
          </div>
          <div class="row">            
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">Code Starts with (Ex: Newyear)</label>
                <input type="text" value="{{old('code')}}" name="code" class="form-control" id="code" placeholder="Code">
              </div> @if($errors->has('code')) <div class="error">{{ $errors->first('code') }}</div> @endif
            </div>
          </div>
          <div class="row">            
            <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label for="Amount">Amount</label>
                  <div class="wrap-input100 validate-input input-group">
                    <button class="btn btn-info btn-pill">
                      MYR
                    </button>
                    <input class="input100 form-control" type="text" placeholder="Amount" name="amount" id="amount">
                  </div>
                  <span id="amounterror"></span>
                </div>                
              </div>
          </div>
          
          <div class="row">
             <div class="col-lg-3 col-md-6">
              <div class="form-group">
              <label for="exampleInputnumber">Start Date</label>
              <br>
              <input type="text" name="start_date" value="{{old('start_date')}}" class="form-control" id="start_date" placeholder="start date"> @if($errors->has('start_date')) <div class="error">{{ $errors->first('start_date') }}</div> @endif
            </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="form-group">
                <label for="status">End Date</label>
                <input type="text" name="end_date" value="{{old('end_date')}}" class="form-control" id="end_date" placeholder="end date"> @if($errors->has('end_date')) <div class="error">{{ $errors->first('end_date') }}</div> @endif
              </div>
            </div>                      
          </div>

          <div class="row">           
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Total Voucher (Ex: 5)</label>
                <input type="number" name="total_voucher" value="{{old('total_voucher')}}" class="form-control" id="total_voucher" placeholder="total voucher"> @if($errors->has('total_voucher')) <div class="error">{{ $errors->first('total_voucher') }}</div> @endif
              </div>
            </div>            
          </div>
        
          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
             
          </div>
        </div>
        <div class="card-footer">
        <a href="{{url('admin/voucher')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  
   // just for the demos, avoids form submit
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
   var current_date = "{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d-m-Y')}}";
   $('#start_date').bootstrapMaterialDatePicker
      ({
        time: false,
        format :'DD-MM-YYYY',
        lang :'en',
        shortTime :false,
        minDate :current_date,
        //maxDate :null,
        currentDate :current_date,
        //weekStart : 0,
        //shortTime :false,
        //'cancelText' :'Cancel',
        //'okText' :'OK'
      });
    $('#end_date').bootstrapMaterialDatePicker
      ({
        time: false,
        date :true,
        minDate :current_date,
        //maxDate :null,
        currentDate :current_date,
        //weekStart : 0,
        //shortTime :false,
        //'cancelText' :'Cancel',
        //'okText' :'OK'
        format :'DD-MM-YYYY',
        lang :'en',
        shortTime :false,
      });

    $('#start_date').bootstrapMaterialDatePicker().on('change',function(e, date){
      var date    = new Date(date),
      yr      = date.getFullYear(),
      month   = (date.getMonth() + 1),
      day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
      month     = month < 10 ? '0' + month  : month,
      newDate = day + '-' + month + '-' + yr;
      $('#end_date').val(newDate);
      $('#end_date').bootstrapMaterialDatePicker('setMinDate',newDate);       
    });

  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      name: {
        required: true
      },
      code: {
        required: true
      },
      start_date: {
        required: true
      },
      end_date: {
        required: true
      },
      total_voucher: {
        required: true
      },
      amount: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name",
      },
      code: {
        required: "Please enter code",
      },
      start_date: {
        required: "Please select start date",
      },
      end_date: {
        required: "Please select end date",
      },
      total_voucher: {
        required: "Please enter total voucher",
      },
      amount: {
        required: "Please enter amount",
      }
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") == "amount") {
            error.appendTo("#amounterror").css('color','#FF0000').css("fontSize", "14px").css('float','center');
        }else {
            error.insertAfter(element);
        }                
      },
  });
</script> @endsection