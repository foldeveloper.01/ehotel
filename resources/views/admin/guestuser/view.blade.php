@extends('admin.layouts.app')

    @section('styles')
<style type="text/css">
/*@media print {
    #si-printer {
        display :  none !important;
    }#si-printer1 {
        display :  none !important;
    }
}
.print:last-child {
     page-break-after: auto;
}*/
</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title"></h1>
                            <div>
                                <ol class="breadcrumb" id="si-printer1">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)"></a></li>
                                    <li class="breadcrumb-item active" aria-current="page"></li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
  <form id="create" action="{{url('admin/guestuser/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{ucwords(@$data->name)}} Info</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">Guest Name</label>
                <input type="text" value="{{@$data->name}}" name="name" class="form-control" id="name" placeholder="Guest Name">
              </div> @if($errors->has('name')) <div class="error">{{ $errors->first('name') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="exampleInputnumber">Address</label>
              <br>
              <textarea name="address" class="form-control" rows="1" placeholder="Enter Address">{!!@$data->address!!}</textarea> @if($errors->has('address')) <div class="error">{{ $errors->first('address') }}</div> @endif
            </div>
            </div>
          </div>
          <div class="row">            
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputname1">IC/Passport No</label>
                <input type="text" value="{{@$data->ic_passport_no}}" name="ic_passport_no" class="form-control" id="ic_passport_no" placeholder="IC/Passport No">
              </div> @if($errors->has('ic_passport_no')) <div class="error">{{ $errors->first('ic_passport_no') }}</div> @endif
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Country</label>
                <select id="country" name="country" class="form-control select2 form-select">
                  @foreach(@$country as $key=>$value)
                  <option value="{{$value->id}}">{{$value->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            
          </div>
          
          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="exampleInputnumber">Email</label>
              <br>
              <input type="email" name="email" value="{{@$data->email}}" class="form-control" id="email" placeholder="Email"> @if($errors->has('email')) <div class="error">{{ $errors->first('email') }}</div> @endif
            </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">State</label>
                <select id="state" name="state" class="form-control select2 form-select">
                  <option value="">Please select</option>
                  @foreach(@$state as $key=>$val)
                  <option value="{{$val->id}}" @if($data->state==$val->id) selected @endif>{{$val->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Password</label>
                <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                  <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                    <i class="zmdi zmdi-eye text-muted fa fa-fw fa-eye field_icon toggle-password" aria-hidden="true"></i>
                  </a>
                  <input class="input100 form-control" type="password" placeholder="Password" name="password" id="password">
                </div> <span id="password_error"></span> @if($errors->has('password')) <div class="error">{{ $errors->first('password') }}</div> @endif
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">City</label>
                <select id="city" name="city" class="form-control select2 form-select">
                  <option value="">Please select</option>
                  @foreach(@$city as $key=>$val)
                  <option value="{{$val->id}}" @if($data->city==$val->id) selected @endif>{{$val->name}}</option>
                  @endforeach                
                </select>
              </div>
            </div>            
          </div>

          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="form-group">
              <label for="exampleInputnumber">Contact Number</label>
              <br>
              <input type="text" name="contact_number" value="{{@$data->contact_number}}" class="form-control" id="contact_number" placeholder="Contact Number" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$"> @if($errors->has('contact_number')) <div class="error">{{ $errors->first('contact_number') }}</div> @endif
            </div>
            </div>             
             <div class="col-lg-6 col-md-12">
              <label for="exampleInputnumber">Postcode</label>
              <br>
              <input type="text" name="postcode" value="{{@$data->postcode}}" class="form-control" id="postcode" placeholder="Postcode"> @if($errors->has('postcode')) <div class="error">{{ $errors->first('postcode') }}</div> @endif
            </div>
          </div>
          <div class="row">            
             <div class="col-lg-6 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" @if(@$data->status=="0") selected @endif>Active</option>
                  <option value="1" @if(@$data->status=="1") selected @endif>Suspend</option>
                </select>
              </div>
            </div>             
          </div>
          <div class="card-footer">
        <a href="{{url('admin/guestuser')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
        </div>
      </div>
      
    </div>
    </form>
</div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')



    @endsection
