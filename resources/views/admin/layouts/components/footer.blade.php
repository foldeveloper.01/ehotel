        <!-- FOOTER -->
        <footer class="footer">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-md-12 col-sm-12 text-center">
                       All Rights Reserved © 2010 - {{date('Y')}} / e-Hotel Powered by First Online
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER CLOSED -->
