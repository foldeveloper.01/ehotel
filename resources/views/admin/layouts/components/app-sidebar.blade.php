            <!--app-sidebar-->
            <div class="sticky">
              <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
              <div class="app-sidebar">
                <div class="side-header">
                  <a class="header-brand1" href="{{url('admin')}}">
                    <img class="header-brand-img light-logo1" alt="logo" src="{{asset('storage/images/logo/'.@$site_logo->logo)}}" />
                  </a>
                  <!-- LOGO -->
                </div>
                <div class="main-sidemenu">
                  <div class="slide-left disabled" id="slide-left">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24">
                      <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" />
                    </svg>
                  </div>
                  <ul class="side-menu">
                    <li class="sub-category">
                      <h3>Dashboard</h3>
                    </li> @if(Auth()->guard('admin')->user()->admin_type_id=='1') <li class="slide">
                      <a class="side-menu__item @if(Request::segment(1)=='admin' && Request::segment(2)=='') active @endif" data-bs-toggle="slide" href="{{url('admin')}}">
                        <i class="side-menu__icon fe fe-home"></i>
                        <span class="side-menu__label">Dashboard</span>
                      </a>
                    </li>
                    <li class="slide">
                      <a class="side-menu__item @if(Request::segment(2)=='news') active @endif" href="{{url('admin/news')}}">
                        <i class="side-menu__icon fe fe-message-square"></i>
                        <span class="side-menu__label">News</span>
                      </a>
                    </li>
                    <li class="slide">
                      <a class="side-menu__item @if(Request::segment(2)=='guestuser') active @endif" href="{{url('admin/guestuser')}}">
                        <i class="side-menu__icon fe fe-users"></i>
                        <span class="side-menu__label">Guest Profile</span>
                      </a>
                    </li>
                    <!-- <li class="slide">
                      <a class="side-menu__item @if(Request::segment(2)=='rooms') active @endif" href="{{url('admin/rooms')}}">
                        <i class="side-menu__icon fe fe-users"></i>
                        <span class="side-menu__label">Room Settings</span>
                      </a>
                    </li> -->


                    <li class="slide @if(Request::segment(2)=='bookinglisting' || Request::segment(2)=='checkinlisting' || Request::segment(2)=='checkoutlisting' || Request::segment(2)=='depositlisting' || Request::segment(2)=='bookinghistory') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='bookinglisting') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fa fa-bullhorn"></i>
                        <span class="side-menu__label">Booking</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='bookinglisting' || Request::segment(2)=='checkinlisting' || Request::segment(2)=='checkoutlisting' || Request::segment(2)=='depositlisting' || Request::segment(2)=='bookinghistory') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Booking</a>
                        </li>
                        <li>
                          <a href="{{url('admin/bookinglisting')}}" class="slide-item @if(Request::segment(2)=='bookinglisting') active @endif">Booking Listing</a>
                        </li>
                        <li>
                          <a href="{{url('admin/checkinlisting')}}" class="slide-item @if(Request::segment(2)=='checkinlisting') active @endif">Check-in Listing</a>
                        </li>
                        <li>
                          <a href="{{url('admin/checkoutlisting')}}" class="slide-item @if(Request::segment(2)=='checkoutlisting') active @endif">Check-out Listing</a>
                        </li>
                        <li>
                          <a href="{{url('admin/depositlisting')}}" class="slide-item @if(Request::segment(2)=='depositlisting') active @endif">Deposit Listing</a>
                        </li>
                        <li>
                          <a href="{{url('admin/receiptlisting')}}" class="slide-item @if(Request::segment(2)=='receiptlisting') active @endif">Receipt Listing</a>
                        </li>
                        <li>
                          <a href="{{url('admin/bookinghistorylisting')}}" class="slide-item @if(Request::segment(2)=='bookinghistory') active @endif">Booking History</a>
                        </li>
                      </ul>
                    </li>


                    <li class="slide @if(Request::segment(2)=='voucher' || Request::segment(2)=='coupon') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='voucher') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon ti-wallet"></i>
                        <span class="side-menu__label">Promotion</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='voucher') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Promotion</a>
                        </li>
                        <li>
                          <a href="{{url('admin/voucher')}}" class="slide-item @if(Request::segment(2)=='voucher') active @endif">Voucher</a>
                        </li>
                        <li>
                          <a href="{{url('admin/coupon')}}" class="slide-item @if(Request::segment(2)=='coupon') active @endif">Coupon</a>
                        </li>
                      </ul>
                    </li>

                    <li class="slide @if(Request::segment(2)=='housekeepinglist') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='housekeepinglist') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon ti-brush-alt"></i>
                        <span class="side-menu__label">Housekeeping</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='housekeepinglist') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Housekeeping</a>
                        </li>
                        <li>
                          <a href="{{url('admin/housekeepinglist')}}" class="slide-item @if(Request::segment(2)=='housekeepinglist') active @endif">Housekeeping</a>
                        </li>
                      </ul>
                    </li>

                    <li class="slide @if(Request::segment(2)=='invoice') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='invoice') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fa fa-save"></i>
                        <span class="side-menu__label">Invoice</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='invoice') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Invoice</a>
                        </li>
                        <li>
                          <a href="{{url('admin/paidinvoice')}}" class="slide-item @if(Request::segment(2)=='paidinvoice') active @endif">Invoice</a>
                        </li>
                       <!--  <li>
                          <a href="{{url('admin/unpaidinvoice')}}" class="slide-item @if(Request::segment(2)=='housekeepinglist') active @endif">Unpaid Invoice</a>
                        </li> -->
                      </ul>
                    </li>

                    <li class="slide @if(Request::segment(2)=='guest_stay_report') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='guest_stay_report') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon mdi mdi-file-chart"></i>
                        <span class="side-menu__label">Reports</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='guest_stay_report') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Reports</a>
                        </li>
                        <li>
                          <a href="{{url('admin/guest_stay_report')}}" class="slide-item @if(Request::segment(2)=='guest_stay_report') active @endif">Guest Stay Report</a>
                        </li>
                        <li>
                          <a href="{{url('admin/sales_report_monthly')}}" class="slide-item @if(Request::segment(2)=='sales_report_monthly') active @endif">Sales Report Monthly</a>
                        </li>                       
                      </ul>
                    </li>

                    <li class="slide @if(Request::segment(2)=='rooms' || Request::segment(2)=='roomseasontype' || Request::segment(2)=='roomfacilities' || Request::segment(2)=='roomsseason' || Request::segment(2) =='roomsmanage') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='rooms') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fa fa-bed"></i>
                        <span class="side-menu__label">Room Settings</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='rooms') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Rooms</a>
                        </li>
                        <li>
                          <a href="{{url('admin/roomseasontype')}}" class="slide-item @if(Request::segment(2)=='roomseasontype') active @endif">Room Season Types</a>
                        </li>
                        <li>
                          <a href="{{url('admin/roomfacilities')}}" class="slide-item @if(Request::segment(2)=='roomfacilities') active @endif">Facilities</a>
                        </li>
                        <li>
                          <a href="{{url('admin/rooms')}}" class="slide-item @if(Request::segment(2)=='rooms' || Request::segment(2)=='roomsseason' || Request::segment(2) =='roomsmanage') active @endif">Rooms</a>
                        </li>
                      </ul>
                    </li>

                    <li class="slide @if(Request::segment(2)=='billheader' || Request::segment(2)=='banner'|| Request::segment(2)=='gallery'|| Request::segment(2)=='subgallery'|| Request::segment(2)=='faq'|| Request::segment(2)=='sitecontent') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='billheader') active @endif header_tit" data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fa fa-cogs"></i>
                        <span class="side-menu__label">CMS Settings</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='billheader') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">CMS Settings</a>
                        </li>
                        <li>
                          <a href="{{url('admin/logo')}}" class="slide-item @if(Request::segment(2)=='logo') active @endif">Logo</a>
                        </li>
                        <li>
                          <a href="{{url('admin/billheader')}}" class="slide-item @if(Request::segment(2)=='billheader') active @endif">Bill Header</a>
                        </li>
                        <li>
                          <a href="{{url('admin/banner')}}" class="slide-item @if(Request::segment(2)=='banner') active @endif">Banner</a>
                        </li>
                        <li>
                          <a href="{{url('admin/gallery')}}" class="slide-item @if(Request::segment(2)=='gallery' || Request::segment(2)=='subgallery') active @endif">Gallery</a>
                        </li>
                        <li>
                          <a href="{{url('admin/sitecontent')}}" class="slide-item @if(Request::segment(2)=='sitecontent') active @endif">Site Content</a>
                        </li>
                        <li>
                          <a href="{{url('admin/faq')}}" class="slide-item @if(Request::segment(2)=='faq') active @endif">FAQ</a>
                        </li>
                      </ul>
                    </li>
                    <li class="slide @if(Request::segment(2)=='settings' || Request::segment(2)=='addonproduct' || Request::segment(2)=='banktransfer'|| Request::segment(2)=='bookingwindows'|| Request::segment(2)=='paymentstype'|| Request::segment(2)=='taxsettings' || Request::segment(2)=='thirdpartytype') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='settings' || Request::segment(2)=='thirdpartytype') active @endif header_tit" data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fe fe-settings fa-spin"></i>
                        <span class="side-menu__label">Settings</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='admins' || Request::segment(2)=='addonproduct' || Request::segment(2)=='banktransfer' || Request::segment(2)=='bookingwindows' || Request::segment(2)=='thirdpartytype' || Request::segment(2)=='taxsettings') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Settings</a>
                        </li>
                        <li>
                          <a href="{{url('admin/addonproduct')}}" class="slide-item @if(Request::segment(2)=='addonproduct') active @endif">Add-on Product</a>
                        </li>
                        <li>
                          <a href="{{url('admin/banktransfer')}}" class="slide-item @if(Request::segment(2)=='banktransfer') active @endif">Bank Transfer</a>
                        </li>
                        <li>
                          <a href="{{url('admin/settings')}}" class="slide-item @if(Request::segment(2)=='settings') active @endif">Site Settings</a>
                        </li>
                        <li>
                          <a href="{{url('admin/smtpsettings')}}" class="slide-item @if(Request::segment(2)=='smtpsettings') active @endif">SMTP Setting</a>
                        </li>
                        <li>
                          <a href="{{url('admin/taxsettings')}}" class="slide-item @if(Request::segment(2)=='taxsettings') active @endif">Tax Setting</a>
                        </li>
                        <li>
                          <a href="{{url('admin/bookingwindows')}}" class="slide-item @if(Request::segment(2)=='bookingwindows') active @endif">Booking Window</a>
                        </li>
                        <li>
                          <a href="{{url('admin/paymentstype')}}" class="slide-item @if(Request::segment(2)=='paymentstype') active @endif">Payments</a>
                        </li>
                        <li>
                          <a href="{{url('admin/thirdpartytype')}}" class="slide-item @if(Request::segment(2)=='thirdpartytype') active @endif">Third Party</a>
                        </li>
                      </ul>
                    </li>
                    <li class="slide @if(Request::segment(2)=='admins') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='admins') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon fe fe-users"></i>
                        <span class="side-menu__label">Admin</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='admins') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Admin</a>
                        </li>
                        <li>
                          <a href="{{url('admin/admins')}}" class="slide-item @if(Request::segment(2)=='admins') active @endif"> Admin Listing</a>
                        </li>
                        <li>
                          <a href="{{url('admin/useractivity')}}" class="slide-item @if(Request::segment(2)=='useractivity') active @endif">Audit Trail</a>
                        </li>
                        <li>
                          <a href="{{url('admin/loginhistroy')}}" class="slide-item @if(Request::segment(2)=='loginhistroy') active @endif">Login History</a>
                        </li>
                        <!-- <li><a href="{{url('admin/accesslevel')}}" class="slide-item @if(Request::segment(2)=='accesslevel') active @endif">Admin Access Level</a></li> -->
                      </ul>
                    </li> @else
                     
                     @if(Auth()->guard('admin')->user()->admin_type_id=='2')
                    <li class="slide @if(Request::segment(2)=='housekeepinglist') is-expanded @endif">
                      <a class="side-menu__item @if(Request::segment(2)=='housekeepinglist') active @endif " data-bs-toggle="slide" href="javascript:void(0)">
                        <i class="side-menu__icon ti-brush-alt"></i>
                        <span class="side-menu__label">Housekeeping</span>
                        <i class="angle fe fe-chevron-right"></i>
                      </a>
                      <ul class="slide-menu @if(Request::segment(2)=='housekeepinglist') open @endif">
                        <li class="side-menu-label1">
                          <a href="javascript:void(0)">Housekeeping</a>
                        </li>
                        <li>
                          <a href="{{url('admin/housekeepinglist')}}" class="slide-item @if(Request::segment(2)=='housekeepinglist') active @endif">Housekeeping</a>
                        </li>
                      </ul>
                    </li>
                    @endif


                    <!-- other logedin user -->
                    @endif
                  </ul>
                  <div class="slide-right" id="slide-right">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24">
                      <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z" />
                    </svg>
                  </div>
                </div>
              </div>
              <!--/APP-SIDEBAR-->
            </div>
            <!--app-sidebar-->