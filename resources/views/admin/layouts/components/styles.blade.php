    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('storage/images/logo/'.@$site_logo->favicon)}}" />

    <!-- BOOTSTRAP CSS -->
    <link id="style" href="{{asset('assets/admin/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- STYLE CSS -->
    <link href="{{asset('assets/admin/css/style.css?v=').time()}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/dark-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/transparent-style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/css/skin-modes.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/sweetalert.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/toastr.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/admin/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <!--  datepicker -->    
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-material-datetimepicker.css')}}"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- datepicker  -->
    

    @yield('styles')

    <!--- FONT-ICONS CSS -->
    <link href="{{asset('assets/admin/plugins/icons/icons.css')}}" rel="stylesheet" />

    <!-- COLOR SKIN CSS -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="{{asset('assets/admin/css/color1.css')}}" />
