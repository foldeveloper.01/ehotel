    <!-- BACK-TO-TOP -->
    <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- JQUERY JS -->
    <script> var site_url = '{{url('/')}}';</script>
    <script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
     <script src="{{asset('assets/admin/plugins/jquery/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery/additional-methods.min.js')}}"></script>

    <!-- BOOTSTRAP JS -->
    <script src="{{asset('assets/admin/plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- INPUT MASK JS-->
    <script src="{{asset('assets/admin/plugins/input-mask/jquery.mask.min.js')}}"></script>

    <!-- SIDE-MENU JS -->
    <script src="{{asset('assets/admin/plugins/sidemenu/sidemenu.js')}}"></script>

    <!-- SIDEBAR JS -->
    <script src="{{asset('assets/admin/plugins/sidebar/sidebar.js')}}"></script>

    <!-- Perfect SCROLLBAR JS-->
    <script src="{{asset('assets/admin/plugins/p-scroll/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/p-scroll/pscroll.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/p-scroll/pscroll-1.js')}}"></script>
    <script src="{{asset('assets/admin/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-datetimepicker.min.js')}}"></script>

    <script src="{{asset('assets/admin/js/toastr.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/sweetalert.min.js')}}"></script>
    <!-- SPARKLINE JS-->
    <script src="{{asset('assets/admin/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- CHART-CIRCLE JS-->
    <script src="{{asset('assets/admin/plugins/circle-progress/circle-progress.min.js')}}"></script>
    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/admin/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/select2.js')}}"></script>
    <!-- PIETY CHART JS-->
    <script src="{{asset('assets/admin/plugins/peitychart/jquery.peity.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/peitychart/peitychart.init.js')}}"></script>
    <!-- INTERNAL CHARTJS CHART JS-->
    <script src="{{asset('assets/admin/plugins/chart/Chart.bundle.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/chart/rounded-barchart.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/chart/utils.js')}}"></script>
    <!-- INTERNAL Data tables js-->
    <script src="{{asset('assets/admin/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/table-data.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
       <script src="{{asset('assets/admin/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <!-- INTERNAL APEXCHART JS -->
    <script src="{{asset('assets/admin/js/apexcharts.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/apexchart/irregular-data-series.js')}}"></script>
    <!-- C3 CHART JS -->
    <script src="{{asset('assets/admin/plugins/charts-c3/d3.v5.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/charts-c3/c3-chart.js')}}"></script>
    <!-- CHART-DONUT JS -->
    <script src="{{asset('assets/admin/js/charts.js')}}"></script>
    <!-- INTERNAL Flot JS -->
    <script src="{{asset('assets/admin/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/flot/chart.flot.sampledata.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/flot/dashboard.sampledata.js')}}"></script>
    <!-- INTERNAL Vector js -->
    <script src="{{asset('assets/admin/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- INTERNAL INDEX JS -->
    <script src="{{asset('assets/admin/js/index.js')}}"></script>
    <script src="{{asset('assets/admin/js/index1.js')}}"></script>
    <!-- SWEET-ALERT JS -->
    <script src="{{asset('assets/admin/plugins/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/sweet-alert.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery-steps/jquery.steps.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/form-wizard.js?v='.time())}}"></script>
    <!-- jquery datepicker custom -->
    <script type="text/javascript" src="{{asset('assets/admin/js/material.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/moment-with-locales.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- jquery datepicker custom -->
    @yield('scripts')
    <!-- Color Theme js -->
    <script src="{{asset('assets/admin/js/themeColors.js')}}"></script>
    <!-- Sticky js -->
    <script src="{{asset('assets/admin/js/sticky.js')}}"></script>
    <!-- CUSTOM JS -->
    <script src="{{asset('assets/admin/js/custom.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/summernote/summernote1.js')}}"></script>
    <script src="{{asset('assets/admin/js/summernote.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/wysiwyag/jquery.richtext.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/wysiwyag/wysiwyag.js')}} "></script>
    <script src="{{asset('assets/admin/plugins/p-scroll/pscroll.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/p-scroll/pscroll-1.js')}}"></script> 
    <script src="{{asset('assets/admin/js/js-year-calendar.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>    
    <script src="{{asset('assets/admin/js/bootstrap-datepicker5.min.js')}}"></script>    

    <script type="text/javascript">
     @if(Session::has('success'))
      toastr.options =
      {
        "closeButton" : true,
        "progressBar" : true
      }
            toastr.success("{{ session('success') }}");
      @endif

      @if(Session::has('error'))
      toastr.options =
      {
        "closeButton" : true,
        "progressBar" : true
      }
            toastr.error("{{ session('error') }}");
      @endif

      @if(Session::has('info'))
      toastr.options =
      {
        "closeButton" : true,
        "progressBar" : true
      }
            toastr.info("{{ session('info') }}");
      @endif

      @if(Session::has('warning'))
      toastr.options =
      {
        "closeButton" : true,
        "progressBar" : true
      }
            toastr.warning("{{ session('warning') }}");
      @endif
  </script>


