<!doctype html>
<html lang="en" dir="ltr">

    <head>

        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="{{@$setting->meta_descriptions}}">
        <meta name="author" content="{{@$setting->meta_author}}">
        <meta name="keywords" content="{{@$setting->meta_keywords}}">

        <!-- title -->
        <title>{{@$setting->site_name}}</title>

        @include('admin.layouts.components.custom-styles')

    </head>

    <body class="">

        @yield('class')

            <!-- global-loader -->
            <div id="global-loader">
                <img src="{{asset('assets/admin/images/loader.svg')}}" class="loader-img" alt="Loader">
            </div>
            <!-- global-loader closed -->

                <!-- PAGE -->
                <div class="page">
                    <div class="">

                        @yield('content')

                    </div>
                </div>
                <!-- End PAGE -->

        </div>
        <!-- BACKGROUND-IMAGE CLOSED -->

        @include('admin.layouts.components.custom-scripts')

    </body>

</html>
