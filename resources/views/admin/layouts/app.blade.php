<!doctype html>
<html lang="en" dir="ltr">

<head>

    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{{@$setting->meta_descriptions}}">
    <meta name="author" content="{{@$setting->meta_author}}">
    <meta name="keywords" content="{{@$setting->meta_keywords}}">

    <!-- title -->
    <title>{{@$setting->site_name}}</title>

    @include('admin.layouts.components.styles')

</head>

<body class="app sidebar-mini ltr">

        <!-- global-loader -->
        <div id="global-loader">
            <img src="{{asset('assets/admin/images/loader.svg')}}" class="loader-img" alt="Loader">
        </div>
        <!-- global-loader closed -->

        <!-- page -->
        <div class="page">
            <div class="page-main">

                @include('admin.layouts.components.app-header')

                @include('admin.layouts.components.app-sidebar')

                    <!--app-content open-->
                    <div class="main-content app-content mt-0">
                        <div class="side-app">

                            <!-- container -->
                            <div class="main-container container-fluid">                                

                                @include('sweetalert::alert')

                                @yield('content')

                            </div>
                            <!-- container-closed -->
                        </div>
                    </div>
                    <!--app-content closed-->
                </div>
                <!-- page-main closed -->

            @include('admin.layouts.components.sidebar-right')

            @include('admin.layouts.components.modal')

            @yield('modal')

            @include('admin.layouts.components.footer')

        </div>
        <!-- page -->

        @include('admin.layouts.components.scripts')

    </body>

</html>
