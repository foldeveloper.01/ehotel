@extends('admin.layouts.app')
    @section('styles')<style type="text/css">.flex-1{display: none;}</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">News</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">News</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row row-cards">
                            <!-- COL-END -->
                            <div class="col-xl-12 col-lg-12">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="card p-0">
                                            <div class="card-body p-4">
                                                <div class="row">
                                                    <div class="col-xl-5 col-lg-6 col-md-5 col-sm-6">
                                                        <form action="{{url('admin/news')}}" method="post" role="search" >
                                                            @csrf
                                                          <div class="input-group d-flex w-100 float-start my-1">
                                                            <input type="text" name="search" class="form-control" placeholder="Search ..." value="{{@$search}}">
                                                            <button type="submit" class="input-group-text btn btn-primary">
                                                                <!-- <i class="fa fa-search fs-16" aria-hidden="true"> -->Search<!-- </i> -->
                                                            </button>
                                                          </div>
                                                        </form>
                                                    </div>
                                                    <div class="col-xl-7 col-lg-6 col-md-7 col-sm-6">
                                                        <div class="float-sm-end float-none my-1">
                                                            <a href="{{url('admin/news/create')}}" class="btn btn-primary me-2">Add News</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-11">
                                        <div class="row">


                                        @if(count(@$list)!='0')
                                        @foreach(@$list as $key=>$value)
                                            <div class="col-sm-6 col-md-6 col-xl-3 alert">
                                                <div class="card">
                                                    <div class="product-grid6">
                                                        <div class="product-image6 p-5">
                                                            <!-- <ul class="icons-wishlist">
                                                                <li><a href="javascript:void(0)" class="bg-danger text-white border-danger border" data-bs-dismiss="alert" aria-hidden="true"><i class="fe fe-heart"></i></a></li>
                                                            </ul> -->
                                                            <a href="#" class="bg-light">
                                                                <img style="height: 306px;" class="img-fluid br-7 w-100" src="{{asset('storage/images/news/'.@$value->image)}}" alt="img">
                                                            </a>
                                                        </div>
                                                        <div class="card-body pt-0">
                                                            <div class="product-content text-center">
                                                                <h1 class="title fw-bold fs-20"><a href="#">{{@$value->title}}</a></h1>
                                                                <!-- <div class="mb-2 text-warning">
                                                                    <i class="fa fa-star text-warning"></i>
                                                                    <i class="fa fa-star text-warning"></i>
                                                                    <i class="fa fa-star text-warning"></i>
                                                                    <i class="fa fa-star-half-o text-warning"></i>
                                                                    <i class="fa fa-star-o text-warning"></i>
                                                                </div>
                                                                <div class="price mb-2">$16,599<span class="ms-4">$19,799</span></div>
                                                                <span class="text-success fs-18 fw-semibold">In Stock</span> -->
                                                                <a href="{{url('admin/news/update/'.@$value->id)}}" class="btn btn-sm btn-primary">
                                                                <i class="fe fe-edit"></i>
                                                              </a>
                                                              <a onclick="deletefunction(event,1);" href="" class="btn  btn-sm btn-danger">
                                                                <i class="fe fe-x"></i>
                                                              </a>
                                                              <form id="delete_form1" method="POST" action="{{url('admin/news/delete/'.@$value->id)}}"> 
                                                                @csrf
                                                                <input name="_method" type="hidden" value="POST">
                                                              </form>
                                                            </div>
                                                        </div>
                                                       <!--  <div class="card-footer text-center">
                                                            <a href="{{url('cart')}}" class="btn btn-primary mb-1"><i class="fe fe-shopping-cart me-2"></i>Add to cart</a>
                                                            <a href="javascript:void(0)" class="btn btn-light ms-2 mb-1"><i class="fe fe-share-2 me-2 text-dark"></i>Share</a>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            <div class="card-body pt-0">
                                                            <div class="product-content text-center">
                                                        No Result!
                                                    </div>
                                                </div>
                                            @endif





                                           


                                        </div>
                                        <div class="mb-5">
                                            <div class="float-end">
                                                <ul class="pagination mb-5">
                                                   {{ @$list->links() }}
                                                </ul>
                                                <!-- <ul class="pagination mb-5">
                                                    <li class="page-item page-prev disabled">
                                                        <a class="page-link" href="javascript:void(0)" tabindex="-1">Prev</a>
                                                    </li>
                                                    <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                                                    <li class="page-item"><a class="page-link" href="javascript:void(0)">5</a></li>
                                                    <li class="page-item page-next">
                                                        <a class="page-link" href="javascript:void(0)">Next</a>
                                                    </li>
                                                </ul> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- COL-END -->
                            </div>
                            <!-- ROW-1 CLOSED -->
                        </div>
                        <!-- ROW-1 END-->

        @endsection

    @section('scripts')
    <script type="text/javascript">function deletefunction(event, id) {
    event.preventDefault();
    swal({
      title: "Are you sure you want to delete this record?",
      text: "If you delete this, it will be gone forever.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Delete',
      cancelButtonText: "Cancel",
      closeOnConfirm: false,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $("#delete_form" + id).submit();
      } else {
        swal("Cancel", "Your data has not been removed", "error");
      }
    });
  }</script>
    @endsection
