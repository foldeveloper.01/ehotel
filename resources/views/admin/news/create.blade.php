@extends('admin.layouts.app') @section('styles') @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">News</h1>
  <div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{url('admin')}}">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Create News</li>
    </ol>
  </div>
</div>
<!-- PAGE-HEADER END -->
<!-- ROW-1 OPEN -->
<div class="row">
  <form id="create" action="{{url('admin/news/save')}}" method="post" enctype="multipart/form-data"> @csrf <div class="col-xl-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Add News</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">News Date</label>
                <input type="text" name="date" class="form-control" id="date" placeholder="date" autocomplete="off">
              </div> @if($errors->has('date')) <div class="error">{{ $errors->first('date') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">News Title</label>
                <input type="text" value="" name="title" class="form-control" id="title" placeholder="Title">
              </div> @if($errors->has('title')) <div class="error">{{ $errors->first('title') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="bannerimage">News Image (Max upload file size 2MB)</label>
                <input class="form-control" accept="image/*" name="image" type="file">
              </div>
              @if($errors->has('image')) <div class="error">{{ $errors->first('image') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="exampleInputname">News Content</label>
                <textarea class="content" name="content"></textarea>
              </div> @if($errors->has('content')) <div class="error">{{ $errors->first('content') }}</div> @endif
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-8 col-md-12">
              <div class="form-group">
                <label for="status">Status</label>
                <select name="status" class="form-control select2 form-select">
                  <option value="0" selected>Active</option>
                  <option value="1">Suspend</option>
                </select>
              </div>
            </div>
          </div>
          <div class="card-footer">
        <a href="{{url('admin/news')}}" class="btn btn-danger my-1">Back</a>
        <button class="btn btn-success my-1" value="submit">Save</button>
      </div>
        </div>
      </div>
      
    </div>
</div>
</form>
</div>
<!-- ROW-1 CLOSED --> @endsection @section('scripts') <script type="text/javascript">
  // just for the demos, avoids form submit
  var current_date = "{{ \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('d-m-Y')}}";
  /*$('#date').bootstrapMaterialDatePicker
      ({
        time: false,
        format :'DD-MM-YYYY',
        lang :'en',
        shortTime :false,
        //clearButton: true
      });*/
   $(function() {
      $('#date').datepicker({
        //defaultDate: defaulsst,  
        pickTime: false,
        minView: 2,
        format: 'dd-mm-yyyy',
        autoclose: true,
      });      
    });
   //$('#date').val(current_date);
  /*jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
  });*/
  $("#create").validate({
    submitHandler: function(form) {
      form.submit();
    },
    rules: {
      title: {
        required: true
      },date: {
        required: true
      },
      image: {
        required: true
      }
    },
    messages: {
      title: {
        required: "Please enter title",
      },date: {
        required: "Please select date",
      },
      image: {
        required: "Please upload image",
      }
    }
    /*,errorPlacement: function(error, element) {
                    if (element.attr("name") == "password") {
                        error.appendTo("#errorpassword").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else if (element.attr("name") == "usertype") {
                        error.appendTo("#errorusertype").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                    }else {
                        error.insertAfter(element);
                    }                
                   },*/
  });
</script> @endsection