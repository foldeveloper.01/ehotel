<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\Admin\{AdminAuthController,SettingsController,DashboardController,UserLoginHistroyController,UserActivityController,AccessLevelController,DomainController,UserController,PaymentHistroyController,Add_on_product_Controller,Bank_Transfer_Controller,Booking_Windows_Controller,Payments_Type_Controller,Bill_Header_Controller,Faq_Controller,Banner_Controller,Logo_Controller,Gallery_Controller,SubGallery_Controller,Roomfacilities_Controller,RoomseasonType_Controller,Rooms_Controller,GuestUser_Controller,News_Controller,Sitecontent_Controller,Voucher_Controller,Coupon_Controller,Rooms_season_manage_Controller,Rooms_season_Controller,Room_Booking_listing_Controller,Housekeepinglist_Controller,ThirdParty_Controller, Invoice_Controller, Receipt_listing_Controller, Deposit_listing_Controller,Booking_history_listing_Controller,Checkin_booking_listing_Controller,Checkout_booking_listing_Controller,Guest_Stay_Report_Controller};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

       
    //Clear Cache facade value:
    Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
    });

    //Reoptimized class loader:
    Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
    });

    //Route cache:
    Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
    });

    //Clear Route cache:
    Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
    });

    //Clear View cache:
    Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
    });

    //Clear Config cache:
    Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
    });

    Route::get('generate', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
    });   
    
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/login', [AdminAuthController::class, 'getLogin'])->name('adminLogin');
    Route::post('/login', [AdminAuthController::class, 'postLogin'])->name('adminLoginPost');
    Route::get('/signup', [AdminAuthController::class, 'signup'])->name('signup');
    Route::get('/registerr', [AdminAuthController::class, 'register'])->name('registerr');
    Route::get('/logout', [AdminAuthController::class, 'getLogout']);
    Route::get('/forgotpassword', [AdminAuthController::class, 'forgotPassword'])->name('forgotpassword');
    Route::post('/forgotpassword', [AdminAuthController::class, 'gentrateUserPassword'])->name('forgotpassword');

    Route::group(['middleware' => 'adminauth'], function () {
        //dashboard
        Route::get('/', [DashboardController::class, 'dashboard'])->name('adminDashboard');
        Route::post('/', [DashboardController::class, 'searchDomain'])->name('adminDashboard');
        //user list
        Route::get('/admins', [UserController::class, 'userslist']);
        Route::get('/admins/create', [UserController::class, 'createuser']);
        Route::post('/admins/save', [UserController::class, 'saveuser']);
        Route::get('/admins/update/{id}', [UserController::class, 'updateuser']);
        Route::post('/admins/update/{id}', [UserController::class, 'edituser']);

        Route::get('/admins/profile/{id}', [UserController::class, 'viewprofile']);
        Route::post('/admins/profile/{id}', [UserController::class, 'updateprofile']);

        Route::any('/admins/delete/{id}', [UserController::class, 'deleteuser']);
        Route::get('/admins/view/{id}', [UserController::class, 'viewuser']);
        Route::any('/admins/officerpassword/{id}', [UserController::class, 'officerUserpassword']);
        Route::any('/admins/emailSend/{id}', [UserController::class, 'ResetUserPasswordToMail']);
        Route::post('/admins/emailSend1/{id}', [UserController::class, 'ResetUserPasswordToMail1']);
        Route::post('/admins/myaccountupdate/{id}', [UserController::class, 'myaccountupdate']);
        Route::get('/admins/checkCurrentPassword/{id}/{password}', [UserController::class, 'checkCurrentPassword']);
        Route::post('/admins/ajax_status_update', [UserController::class, 'ajax_status_update']);
        //settings
        Route::get('/settings', [SettingsController::class, 'settings']);
        Route::post('/settings', [SettingsController::class, 'savesettings']);
        Route::get('/smtpsettings', [SettingsController::class, 'smtpsettings']);
        Route::post('/smtpsettings', [SettingsController::class, 'savesmtpsettings']);
        Route::get('/taxsettings', [SettingsController::class, 'taxsettings']);
        Route::post('/taxsettings', [SettingsController::class, 'savetaxsettings']);
       
        //user menu access leval
        Route::get('/accesslevel', [AccessLevelController::class, 'listdata']);
        Route::post('/accesslevel/useraccesslevel', [AccessLevelController::class, 'savedata']);
        //end       
        //user login histroy
        Route::get('/loginhistroy', [UserLoginHistroyController::class, 'listdata']);
        //user login activity
        Route::get('/useractivity', [UserActivityController::class, 'listdata']);
        //add-on product
        Route::get('/addonproduct', [Add_on_product_Controller::class, 'listdata']);
        Route::get('/addonproduct/create', [Add_on_product_Controller::class, 'createdata']);
        Route::post('/addonproduct/save', [Add_on_product_Controller::class, 'savedata']);
        Route::post('/addonproduct/ajax_status_update', [Add_on_product_Controller::class, 'ajax_status_update']);
        Route::get('/addonproduct/update/{id}', [Add_on_product_Controller::class, 'updatedata']);
        Route::post('/addonproduct/update/{id}', [Add_on_product_Controller::class, 'dataupdate']);
        Route::post('/addonproduct/delete/{id}', [Add_on_product_Controller::class, 'deletedata']);
        //bank transfer
        Route::get('/banktransfer', [Bank_Transfer_Controller::class, 'listdata']);
        Route::get('/banktransfer/create', [Bank_Transfer_Controller::class, 'createdata']);
        Route::post('/banktransfer/save', [Bank_Transfer_Controller::class, 'savedata']);
        Route::get('/banktransfer/update/{id}', [Bank_Transfer_Controller::class, 'updatedata']);
        Route::post('/banktransfer/update/{id}', [Bank_Transfer_Controller::class, 'dataupdate']);
        Route::post('/banktransfer/delete/{id}', [Bank_Transfer_Controller::class, 'deletedata']);
        Route::post('/banktransfer/ajax_status_update', [Bank_Transfer_Controller::class, 'ajax_status_update']);
        //booking windows
        Route::get('/bookingwindows', [Booking_Windows_Controller::class, 'listdata']);
        Route::get('/bookingwindows/create', [Booking_Windows_Controller::class, 'createdata']);
        Route::post('/bookingwindows/save', [Booking_Windows_Controller::class, 'savedata']);
        Route::get('/bookingwindows/update/{id}', [Booking_Windows_Controller::class, 'updatedata']);
        Route::post('/bookingwindows/update/{id}', [Booking_Windows_Controller::class, 'dataupdate']);
        Route::post('/bookingwindows/delete/{id}', [Booking_Windows_Controller::class, 'deletedata']);
        Route::post('/bookingwindows/ajax_status_update', [Booking_Windows_Controller::class, 'ajax_status_update']);
        //payment settings
        Route::get('/paymentstype', [Payments_Type_Controller::class, 'listdata']);
        Route::get('/paymentstype/create', [Payments_Type_Controller::class, 'createdata']);
        Route::post('/paymentstype/save', [Payments_Type_Controller::class, 'savedata']);
        Route::post('/paymentstype/ajax_status_update', [Payments_Type_Controller::class, 'ajax_status_update']);
        Route::get('/paymentstype/update/{id}', [Payments_Type_Controller::class, 'updatedata']);
        Route::post('/paymentstype/update/{id}', [Payments_Type_Controller::class, 'dataupdate']);
        Route::post('/paymentstype/delete/{id}', [Payments_Type_Controller::class, 'deletedata']);
        //cms settings
        //bill header
        Route::get('/billheader', [Bill_Header_Controller::class, 'billheader']);
        Route::post('/billheader', [Bill_Header_Controller::class, 'savebillheader']);
        //faq
        Route::get('/faq', [Faq_Controller::class, 'listdata']);
        Route::get('/faq/create', [Faq_Controller::class, 'createdata']);
        Route::post('/faq/save', [Faq_Controller::class, 'savedata']);
        Route::post('/faq/ajax_status_update', [Faq_Controller::class, 'ajax_status_update']);
        Route::get('/faq/update/{id}', [Faq_Controller::class, 'updatedata']);
        Route::post('/faq/update/{id}', [Faq_Controller::class, 'dataupdate']);
        Route::post('/faq/delete/{id}', [Faq_Controller::class, 'deletedata']);
        //banner
        Route::get('/banner', [Banner_Controller::class, 'listdata']);
        Route::get('/banner/create', [Banner_Controller::class, 'createdata']);
        Route::post('/banner/save', [Banner_Controller::class, 'savedata']);
        Route::post('/banner/ajax_status_update', [Banner_Controller::class, 'ajax_status_update']);
        Route::get('/banner/update/{id}', [Banner_Controller::class, 'updatedata']);
        Route::post('/banner/update/{id}', [Banner_Controller::class, 'dataupdate']);
        Route::post('/banner/delete/{id}', [Banner_Controller::class, 'deletedata']);
        //logo
        Route::get('/logo', [Logo_Controller::class, 'logo']);
        Route::post('/logo', [Logo_Controller::class, 'savelogo']);
        //gallery category
        Route::get('/gallery', [Gallery_Controller::class, 'listdata']);
        Route::get('/gallery/create', [Gallery_Controller::class, 'createdata']);
        Route::post('/gallery/save', [Gallery_Controller::class, 'savedata']);
        Route::post('/gallery/ajax_status_update', [Gallery_Controller::class, 'ajax_status_update']);
        Route::get('/gallery/update/{id}', [Gallery_Controller::class, 'updatedata']);
        Route::post('/gallery/update/{id}', [Gallery_Controller::class, 'dataupdate']);
        Route::post('/gallery/delete/{id}', [Gallery_Controller::class, 'deletedata']);
        //gallery subcategory
        Route::get('/gallery/{id}/subgallery', [SubGallery_Controller::class, 'listdata']);
        Route::get('/subgallery/{id}/create', [SubGallery_Controller::class, 'createdata']);
        Route::post('/subgallery/{id}/save', [SubGallery_Controller::class, 'savedata']);
        Route::post('/subgallery/ajax_status_update', [SubGallery_Controller::class, 'ajax_status_update']);
        Route::get('/subgallery/update/{id}', [SubGallery_Controller::class, 'updatedata']);
        Route::post('/subgallery/update/{id}', [SubGallery_Controller::class, 'dataupdate']);
        Route::post('/subgallery/delete/{id}', [SubGallery_Controller::class, 'deletedata']);
        //site content
        Route::get('/sitecontent', [Sitecontent_Controller::class, 'listdata']);
        Route::get('/sitecontent/create', [Sitecontent_Controller::class, 'createdata']);
        Route::post('/sitecontent/save', [Sitecontent_Controller::class, 'savedata']);
        Route::post('/sitecontent/ajax_status_update', [Sitecontent_Controller::class, 'ajax_status_update']);
        Route::get('/sitecontent/update/{id}', [Sitecontent_Controller::class, 'updatedata']);
        Route::post('/sitecontent/update/{id}', [Sitecontent_Controller::class, 'dataupdate']);
        Route::post('/sitecontent/delete/{id}', [Sitecontent_Controller::class, 'deletedata']);
        //news
        Route::any('/news', [News_Controller::class, 'listdata']);
        Route::get('/news/create', [News_Controller::class, 'createdata']);
        Route::post('/news/save', [News_Controller::class, 'savedata']);
        Route::post('/news/ajax_status_update', [News_Controller::class, 'ajax_status_update']);
        Route::get('/news/update/{id}', [News_Controller::class, 'updatedata']);
        Route::post('/news/update/{id}', [News_Controller::class, 'dataupdate']);
        Route::post('/news/delete/{id}', [News_Controller::class, 'deletedata']);
        //Gust User
        Route::get('/guestuser', [GuestUser_Controller::class, 'listdata']);
        Route::get('/ajax_guestuser', [GuestUser_Controller::class, 'ajax_guestuser']);
        Route::get('/guestuser/create', [GuestUser_Controller::class, 'createdata']);
        Route::post('/guestuser/save', [GuestUser_Controller::class, 'savedata']);
        Route::post('/guestuser/ajax_status_update', [GuestUser_Controller::class, 'ajax_status_update']);
        Route::get('/guestuser/update/{id}', [GuestUser_Controller::class, 'updatedata']);
        Route::post('/guestuser/update/{id}', [GuestUser_Controller::class, 'dataupdate']);
        Route::get('/guestuser/view/{id}', [GuestUser_Controller::class, 'view_data']);
        Route::post('/guestuser/delete/{id}', [GuestUser_Controller::class, 'deletedata']);
        Route::get('/guestuser/export', [GuestUser_Controller::class, 'exportGuestuser']);
        Route::post('/guestuser/ajax_get_city', [GuestUser_Controller::class, 'ajax_get_city']);
           //Room_Setting
        Route::get('/roomfacilities', [Roomfacilities_Controller::class, 'listdata']);
        Route::get('/roomfacilities/create', [Roomfacilities_Controller::class, 'createdata']);
        Route::post('/roomfacilities/save', [Roomfacilities_Controller::class, 'savedata']);
        Route::post('/roomfacilities/ajax_status_update', [Roomfacilities_Controller::class, 'ajax_status_update']);
        Route::get('/roomfacilities/update/{id}', [Roomfacilities_Controller::class, 'updatedata']);
        Route::post('/roomfacilities/update/{id}', [Roomfacilities_Controller::class, 'dataupdate']);
        Route::post('/roomfacilities/delete/{id}', [Roomfacilities_Controller::class, 'deletedata']);
           //Room Season type
        Route::get('/roomseasontype', [RoomseasonType_Controller::class, 'listdata']);
        Route::post('/roomseasontype', [RoomseasonType_Controller::class, 'listdata']);
        Route::get('/roomseasontype/create', [RoomseasonType_Controller::class, 'createdata']);
        Route::post('/roomseasontype/save', [RoomseasonType_Controller::class, 'savedata']);
        Route::post('/roomseasontype/ajax_status_update', [RoomseasonType_Controller::class, 'ajax_status_update']);
        Route::get('/roomseasontype/update/{id}', [RoomseasonType_Controller::class, 'updatedata']);
        Route::post('/roomseasontype/update/{id}', [RoomseasonType_Controller::class, 'dataupdate']);
        Route::post('/roomseasontype/delete/{id}', [RoomseasonType_Controller::class, 'deletedata']);
           //rooms
        Route::get('/rooms', [Rooms_Controller::class, 'listdata']);
        Route::get('/rooms/create', [Rooms_Controller::class, 'createdata']);
        Route::post('/rooms/save', [Rooms_Controller::class, 'savedata']);
        Route::post('/rooms/ajax_status_update', [Rooms_Controller::class, 'ajax_status_update']);
        Route::get('/rooms/update/{id}', [Rooms_Controller::class, 'updatedata']);
        Route::post('/rooms/update/{id}', [Rooms_Controller::class, 'dataupdate']);
        Route::post('/rooms/delete/{id}', [Rooms_Controller::class, 'deletedata']);
        Route::post('/rooms/gallery/delete/{id}', [Rooms_Controller::class, 'deletegallery']);
           //rooms season
        Route::get('/roomsseason', [Rooms_season_Controller::class, 'listdata']);        
        Route::post('/roomsseason', [Rooms_season_Controller::class, 'listdata']);        
        Route::post('/roomsseason/removeEvent', [Rooms_season_Controller::class, 'removeEvent']);        
           //rooms manage
        Route::get('/roomsmanage/{id}', [Rooms_season_manage_Controller::class, 'listdata']);
        Route::post('/roomsmanage/ajax_status_update', [Rooms_season_manage_Controller::class, 'ajax_status_update']);
        Route::get('/roomsmanage/create/{id}', [Rooms_season_manage_Controller::class, 'createdata']);
        Route::post('/roomsmanage/save', [Rooms_season_manage_Controller::class, 'savedata']);
        Route::get('/roomsmanage/update/{id}', [Rooms_season_manage_Controller::class, 'updatedata']);
        Route::post('/roomsmanage/update/{id}', [Rooms_season_manage_Controller::class, 'dataupdate']);
        Route::post('/roomsmanage/delete/{id}', [Rooms_season_manage_Controller::class, 'deletedata']);
           //rooms number season
        Route::get('roomsmanage/rooms/{rooms_id}', [Rooms_season_manage_Controller::class, 'list_room_number_data']);        
        Route::post('roomsmanage/rooms/{rooms_id}', [Rooms_season_manage_Controller::class, 'list_room_number_data']);        
        Route::post('roomsmanage/removeEvent', [Rooms_season_manage_Controller::class, 'removeEvent']);
           //Promotion voucher
        Route::get('/voucher', [Voucher_Controller::class, 'listdata']);
        Route::get('/voucher/create', [Voucher_Controller::class, 'createdata']);
        Route::post('/voucher/save', [Voucher_Controller::class, 'savedata']);
        Route::post('/voucher/ajax_status_update', [Voucher_Controller::class, 'ajax_status_update']);
        Route::get('/voucher/update/{id}', [Voucher_Controller::class, 'updatedata']);
        Route::post('/voucher/update/{id}', [Voucher_Controller::class, 'dataupdate']);
        Route::post('/voucher/delete/{id}', [Voucher_Controller::class, 'deletedata']);
        Route::get('voucher/getdata', [Voucher_Controller::class, 'getdataforajax']);
           //Promotion coupon
        Route::get('/coupon', [Coupon_Controller::class, 'listdata']);
        Route::get('/coupon/create', [Coupon_Controller::class, 'createdata']);
        Route::post('/coupon/save', [Coupon_Controller::class, 'savedata']);
        Route::post('/coupon/ajax_status_update', [Coupon_Controller::class, 'ajax_status_update']);
        Route::get('/coupon/update/{id}', [Coupon_Controller::class, 'updatedata']);
        Route::post('/coupon/update/{id}', [Coupon_Controller::class, 'dataupdate']);
        Route::post('/coupon/delete/{id}', [Coupon_Controller::class, 'deletedata']);
        Route::get('coupon/getdata', [Coupon_Controller::class, 'getdataforajax']);
        Route::get('coupon/view/{id}', [Coupon_Controller::class, 'viewdata']);
        Route::post('coupon_voucher/applied', [Coupon_Controller::class, 'coupon_voucher_applied']);
           //booking listing
        Route::get('/bookinglisting', [Room_Booking_listing_Controller::class, 'listdata']);
        Route::post('/bookinglisting/remove_due_booking', [Room_Booking_listing_Controller::class, 'remove_due_booking']);
        Route::post('/bookinglisting/cancel_booking/{roomid}', [Room_Booking_listing_Controller::class, 'cancel_booking']);
        Route::post('/bookinglisting/pay_due_amount', [Room_Booking_listing_Controller::class, 'pay_due_amount']);
        Route::get('/bookinglisting/newbooking', [Room_Booking_listing_Controller::class, 'newbooking']);
        Route::get('/bookinglisting/newbooking/view', [Room_Booking_listing_Controller::class, 'newbookingview']);
        Route::post('/bookinglisting/newbooking/save_guest_user', [Room_Booking_listing_Controller::class, 'save_guest_user']);
        Route::post('/bookinglisting/newbooking/save', [Room_Booking_listing_Controller::class, 'newbooking_save']);
        Route::get('/bookinglisting/newbooking/payment/{id}', [Room_Booking_listing_Controller::class, 'newbooking_payment']);
        Route::post('/bookinglisting/newbooking/payment/{id}', [Room_Booking_listing_Controller::class, 'newbooking_payment_save']);
        Route::get('/bookinglisting/newbooking/get_room_guest_details/{id}', [Room_Booking_listing_Controller::class, 'get_room_guest_details']);
        Route::post('/bookinglisting/newbooking/save_room_guest_details', [Room_Booking_listing_Controller::class, 'save_room_guest_details']);
        Route::get('/bookinglisting/newbooking/productaddon/{id}', [Room_Booking_listing_Controller::class, 'get_product_addon']);
        Route::get('/bookinglisting/newbooking/addon_invoice/{bookingid}/{booking_payment_id}/{invoice_type}', [Room_Booking_listing_Controller::class, 'get_addon_invoice']);
        Route::post('/bookinglisting/newbooking/addon_invoice/update_price', [Room_Booking_listing_Controller::class, 'addon_invoice_price_update']);
        Route::get('/bookinglisting/newbooking/receipt_view/{receipt_id}', [Room_Booking_listing_Controller::class, 'view_on_receipt']);
        Route::get('/bookinglisting/newbooking/billing_view/{booking_id}', [Room_Booking_listing_Controller::class, 'view_on_billing']);
        Route::get('/bookinglisting/newbooking/edit_billing_view/{booking_id}', [Room_Booking_listing_Controller::class, 'edit_on_billing_view']);
        Route::post('/bookinglisting/newbooking/edit_delete_billing_view', [Room_Booking_listing_Controller::class, 'edit_delete_billing_view']);
        Route::get('/bookinglisting/newbooking/billing_print_view/{booking_id}', [Room_Booking_listing_Controller::class, 'view_on_billing_print']);
        Route::get('/bookinglisting/newbooking/booking_view/{booking_id}', [Room_Booking_listing_Controller::class, 'booking_print_view']);
        Route::post('/bookinglisting/newbooking/saveproductaddon/{id}', [Room_Booking_listing_Controller::class, 'save_product_addon']);       
           //change room date
        Route::get('/bookinglisting/booking_change_date/{id}', [Room_Booking_listing_Controller::class, 'booking_change_date']);       
        Route::post('/bookinglisting/booking_change_date/save', [Room_Booking_listing_Controller::class, 'booking_change_date_save']);       
        Route::get('/bookinglisting/booking_change_date', [Room_Booking_listing_Controller::class, 'get_rooms_details_using_date']);
           //extend room date
        Route::get('/bookinglisting/booking_extend_date/{id}', [Room_Booking_listing_Controller::class, 'booking_extend_date']);       
        Route::post('/bookinglisting/booking_extend_date/save', [Room_Booking_listing_Controller::class, 'booking_extend_date_save']);
        Route::post('/bookinglisting/booking_extend_date', [Room_Booking_listing_Controller::class, 'get_rooms_details_using_extenddate']);
           //change room
        Route::get('/bookinglisting/booking_change_room/{id}', [Room_Booking_listing_Controller::class, 'booking_change_room']);       
        Route::post('/bookinglisting/booking_change_room/save', [Room_Booking_listing_Controller::class, 'booking_change_room_save']);       
        Route::post('/bookinglisting/booking_change_room/{id}', [Room_Booking_listing_Controller::class, 'booking_change_room_getprice']);
           //refund view
        Route::get('/bookinglisting/newbooking/refund_view/{id}', [Room_Booking_listing_Controller::class, 'booking_refund_view']);
        Route::post('/bookinglisting/newbooking/refund_save', [Room_Booking_listing_Controller::class, 'booking_refund_save']);
           //email to customer regards pending pament.
        Route::get('/bookinglisting/newbooking/send_email_to_customer/{id}', [Room_Booking_listing_Controller::class, 'send_email_to_customer']);
        Route::post('/bookinglisting/newbooking/send_email_to_customer', [Room_Booking_listing_Controller::class, 'mail_send_to_customer_pending_payment']);
           //send sendphone
        Route::get('/bookinglisting/newbooking/send_sms_to_customer/{id}', [Room_Booking_listing_Controller::class, 'send_sms_to_customer']);
        Route::post('/bookinglisting/newbooking/send_sms_to_customer', [Room_Booking_listing_Controller::class, 'sms_send_to_customer_pending_payment']);

           //housekeeping listing
        Route::get('/housekeepinglist', [Housekeepinglist_Controller::class, 'listdata']);
        Route::post('/housekeepinglist', [Housekeepinglist_Controller::class, 'listdata']);
        Route::get('housekeepinglist/getdata', [Housekeepinglist_Controller::class, 'getdataforajax']);
        Route::get('/housekeepinglist/booking_print_view/{booking_id}', [Housekeepinglist_Controller::class, 'view_on_billing_print']);
           //third party type
        Route::get('/thirdpartytype', [ThirdParty_Controller::class, 'listdata']);
        Route::get('/thirdpartytype/create', [ThirdParty_Controller::class, 'createdata']);
        Route::post('/thirdpartytype/save', [ThirdParty_Controller::class, 'savedata']);
        Route::post('/thirdpartytype/ajax_status_update', [ThirdParty_Controller::class, 'ajax_status_update']);
        Route::get('/thirdpartytype/update/{id}', [ThirdParty_Controller::class, 'updatedata']);
        Route::post('/thirdpartytype/update/{id}', [ThirdParty_Controller::class, 'dataupdate']);
        Route::post('/thirdpartytype/delete/{id}', [ThirdParty_Controller::class, 'deletedata']);
           //paid invoice
        Route::get('/paidinvoice', [Invoice_Controller::class, 'listdata']);
        Route::post('/paidinvoice', [Invoice_Controller::class, 'ajax_data']);
        Route::get('/paidinvoice/print_invoice/{id}', [Invoice_Controller::class, 'print_invoice']);
        Route::post('/invoiceremark/get', [Invoice_Controller::class, 'get_invoice_remark']);
        Route::post('/invoiceremark/create', [Invoice_Controller::class, 'create_invoice_remark']);
        Route::post('/invoiceremark/remove', [Invoice_Controller::class, 'remove_invoice_remark']);
           //receipt listing
        Route::get('/receiptlisting', [Receipt_listing_Controller::class, 'listdata']);
           //deposit listing
        Route::get('/depositlisting', [Deposit_listing_Controller::class, 'listdata']);
        Route::post('/depositremark/get', [Deposit_listing_Controller::class, 'get_deposit_remark']);
        Route::post('/depositremark/create', [Deposit_listing_Controller::class, 'create_deposit_remark']);
        Route::post('/depositremark/remove', [Deposit_listing_Controller::class, 'remove_deposit_remark']);
        Route::get('/depositremark/receipt_view/{receipt_id}', [Deposit_listing_Controller::class, 'view_on_receipt']);
        Route::get('/deposit_refund/{type}/{booking_id}', [Deposit_listing_Controller::class, 'deposit_refund']);
           //booking history listing
        Route::get('/bookinghistorylisting', [Booking_history_listing_Controller::class, 'listdata']);
           //booking checkin listing
        Route::get('/checkinlisting', [Checkin_booking_listing_Controller::class, 'listdata']);
        Route::post('/checkinlisting/remove_due_booking', [Checkin_booking_listing_Controller::class, 'remove_due_booking']);            
        Route::get('/checkinlisting/checkin_confirm/view/{booking_id}', [Checkin_booking_listing_Controller::class, 'checkin_confirm_view']);
        Route::post('/checkinlisting/checkin_confirm/check_housekeeping_room', [Checkin_booking_listing_Controller::class, 'check_housekeeping_room']);
        Route::post('/checkinlisting/checkin_confirm_save/{id}', [Checkin_booking_listing_Controller::class, 'checkin_confirm_save']);
           //booking checkout listing
        Route::get('/checkoutlisting', [Checkout_booking_listing_Controller::class, 'listdata']);
        Route::post('/checkoutlisting/remove_due_booking', [Checkout_booking_listing_Controller::class, 'remove_due_booking']);
        Route::get('/checkoutlisting/checkout_confirm/view/{booking_id}', [Checkout_booking_listing_Controller::class, 'checkout_confirm_view']);
        Route::post('/checkoutlisting/checkout_confirm_save/{id}', [Checkout_booking_listing_Controller::class, 'checkout_confirm_save']);
        /*Route::post('/depositremark/get', [Deposit_listing_Controller::class, 'get_deposit_remark']);
        Route::post('/depositremark/create', [Deposit_listing_Controller::class, 'create_deposit_remark']);*/
        //invoice edit
        Route::get('/paidinvoice/invoice_edit/{id}', [Invoice_Controller::class, 'invoice_edit']);
        Route::post('/paidinvoice/invoice_save', [Invoice_Controller::class, 'invoice_save']);
           //invoice billing edit
        Route::get('/paidinvoice/invoice_booking_edit/{id}', [Invoice_Controller::class, 'invoice_booking_edit']);
        Route::post('/paidinvoice/invoice_booking_save', [Invoice_Controller::class, 'invoice_booking_save']);

        //reports
        //Guest stay Report        
          Route::get('/guest_stay_report', [Guest_Stay_Report_Controller::class, 'listdata']);
          Route::post('/guest_stay_report/ajax_guest_stay_report', [Guest_Stay_Report_Controller::class, 'ajax_guest_stay_report']);
          Route::get('/guest_stay_report/export', [Guest_Stay_Report_Controller::class, 'export']);
          Route::get('/sales_report_monthly', [Guest_Stay_Report_Controller::class, 'saleslistdata']);
        //guest  stay Report


    });
});
Auth::routes();
//front end
Route::get('/', [FrontController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');