<?php

namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;

trait Multitenantable {

    protected static function boot()
    {
        parent::boot();
        static::saving(function($model)
        {
            if(auth()->guard('admin')->user()->admin_type_id!=1)
        	    $model->user_id= auth()->guard('admin')->user()->id;
        });
    }

    protected static function bootMultitenantable()
    {
        if (auth()->guard('admin')->check()) {
        	if(auth()->guard('admin')->user()->admin_type_id!=1){
            static::addGlobalScope('user_id', function (Builder $builder) {
                $builder->where($builder->qualifyColumn('user_id'),auth()->guard('admin')->user()->id);
            });
        }
      }
    }

}
