<?php
namespace App\Traits;

use App\Models\UserActivity;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

/**
 * Class ModelEventLogger
 * @package App\Traits
 *
 *  Automatically Log Add, Update, Delete events of Model.
 */
trait ModelEventLogger {

    /**
     * Automatically boot with Model, and register Events handler.
     */
     public static function bootModelEventLogger()
    {
        @$user_id = @auth()->guard('admin')->user()->id;
        foreach (static::getRecordActivityEvents() as $eventName) {          
          static::$eventName(function (Model $model) use ($user_id, $eventName) {
            $reflect = new \ReflectionClass($model);
            $data = new UserActivity();
            $data->user_id = $user_id;
            $data->contentId =  $model->id;
            $data->contentType =  get_class($model);
            $data->details =  json_encode($model->getDirty());
            $data->action =  static::getActionName($eventName);
            $data->description =  ucfirst($eventName) . " a " . $reflect->getShortName();
            $data->save();
          });
    }    
}

    /**
     * Set the default events to be recorded if the $recordEvents
     * property does not exist on the model.
     *
     * @return array
     */
    protected static function getRecordActivityEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }
        return [
            'created',
            'updated',
            'deleted',
        ];
    }

    /**
     * Return Suitable action name for Supplied Event
     *
     * @param $event
     * @return string
     */
    protected static function getActionName($event)
    {
        switch (strtolower($event)) {
            case 'created':
                return 'create';
                break;
            case 'updated':
                return 'update';
                break;
            case 'deleted':
                return 'delete';
                break;
            default:
                return 'unknown';
        }
    }
} 