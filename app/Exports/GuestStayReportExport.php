<?php

namespace App\Exports;

use App\Models\{User, Room_booking_details};
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Carbon\Carbon;

class GuestStayReportExport implements FromCollection, WithMapping, WithHeadings, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Room_booking_details::join('ehotel_guest_user','ehotel_rooms_booking_details.customer_id','=','ehotel_guest_user.id')
                                   ->join('ehotel_rooms_number','ehotel_rooms_booking_details.room_number_id','=','ehotel_rooms_number.id')
                                   ->where('ehotel_rooms_booking_details.checkout_status',1)->where('ehotel_rooms_booking_details.is_change_data',0)->where('ehotel_rooms_booking_details.deleted_at',null)
                                   ->orderBy('ehotel_rooms_booking_details.id','desc')
                                   ->get();
    }

    public function map($emp): array
    {
        return [
             $emp->id,               
             $emp->room_no,          
             $emp->name,          
             Carbon::parse($emp->checkin_date_time)->format('d-m-Y g:i A'),          
             Carbon::parse($emp->checkout_date_time)->format('d-m-Y'),
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Room Number',
            'Guest Name',
            'Check-in',
            'Check-out'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
}
