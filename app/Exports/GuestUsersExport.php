<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Carbon\Carbon;

class GuestUsersExport implements FromCollection, WithMapping, WithHeadings, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('deleted_at',null)->get();
    }

    public function map($emp): array
    {
        return [
             $emp->id,
             ($emp->created_at!='' && $emp->created_at!=null && $emp->created_at!='0000-00-00')?Carbon::parse($emp->created_at)->format('d-m-Y'):'',     
             $emp->name,          
             $emp->ic_passport_no,          
             $emp->contact_number,          
             $emp->email,
             $emp->address,
             /*($emp->designation!=''&&$emp->designation!=0)?Designation::find($emp->designation)->name :'',
             ($emp->status==0) ? 'De Active' : 'Active',
             ($emp->login_status==0) ? 'No' : 'Yes',*/
                       
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Registration Date',
            'Name',
            'IC/Passport No',
            'Contact Number',
            'Email',            
            'Location'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:N1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
}
