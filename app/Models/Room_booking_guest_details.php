<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Invoice;
use App\Models\Room_booking_details;
use App\Models\Rooms;
use App\Models\{Admin,Add_on_product};
use Carbon\Carbon;
class Room_booking_guest_details extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */    

    protected $table = 'ehotel_rooms_booking_guest_details';
    protected $fillable = [
        'booking_id', 'guest_name', 'room_type_id', 'room_number_id', 'guest_ic_passport_no', 'guest_contact_number', 'created_at', 'updated_at', 'deleted_at'
    ];   
   
}
