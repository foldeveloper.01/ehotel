<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;

class Room_price extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_room_price';
    protected $fillable = [
        'room_id', 'normal_price', 'normal_weekend', 'normal_public_holiday', 'normal_promotion', 'normal_school_holiday', 'hourly_price', 'hourly_weekend',
        'hourly_public_holiday', 'hourly_promotion', 'hourly_school_holiday', 'created_at', 'updated_at', 'deleted_at'
    ];
   
}
