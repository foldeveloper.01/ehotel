<?php
namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;

class Rooms_manage extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_rooms_number';
    protected $fillable = [
        'room_id', 'room_no', 'building_name', 'floor_name', 'position', 'lock_id', 'status', 'created_at', 'updated_at', 'deleted_at'];
      
    public function room_type()
    {
        return $this->hasOne(Rooms::class,'id','room_id');
    }
}
