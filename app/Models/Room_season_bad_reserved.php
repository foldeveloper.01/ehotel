<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;

class Room_season_bad_reserved extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_rooms_season_bad_reserved';
    protected $fillable = [
        'title', 'start_date', 'end_date', 'room_number_id', 'manage_year', 'type', 'created_at', 'updated_at', 'deleted_at'
    ];
    public function getrooms_season_type($id){
        $data = Room_season_type::find($id);
        if(is_object($data)){
          return strtoupper($data->color_code);
        }
        return '';
    }
   
}
