<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Invoice;
use App\Models\{Rooms,Room_booking_details,Room_booking_addon,Room_booking_receipt};
class Room_booking_payment extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_rooms_booking_payment';
    protected $fillable = [
        'booking_id', 'total_amount', 'booking_details_id', 'payment_details', 'invoice_type', 'booking_addon_id', 'payment_status', 'payment_type', 'refund_payment_check', 'created_at', 'others', 'updated_at', 'deleted_at'
    ];    
    //payment_type    1=cash, 2=Credit Bill, 3=Bank-in, 4=credit card, 5=debit card, 6=cheque, 7=skip for no payment, 8=Pay Later, 9=Free
    //refund_payment_check   0=default(no refund amount), 1=yes(refund amount)

    public function getPaymentTypeAttribute($value) {
        if($this->attributes['payment_type']==1){
            return 'cash';
        }if($this->attributes['payment_type']==2){
            return 'credit bill';
        }if($this->attributes['payment_type']==3){
            return 'bank in';
        }if($this->attributes['payment_type']==4){
            return 'credit card';
        }if($this->attributes['payment_type']==5){
            return 'debit card';
        }if($this->attributes['payment_type']==6){
            return 'cheque';
        }if($this->attributes['payment_type']==7){
            return 'skip for no payment';
        }if($this->attributes['payment_type']==8){
            return 'pay later';
        }if($this->attributes['payment_type']==9){
            return 'free';
        }       
    }

    public function booking_details_id(): BelongsToMany
      {
          return $this->belongsToMany(Room_booking_details::class);
      }
   public function booking_addon_id(): BelongsToMany
      {
          return $this->belongsToMany(Room_booking_addon::class);
      }

   public function room_booking_receipt() {
            return $this->hasOne(Room_booking_receipt::class,'payment_table_id','id');
    }
   
}
