<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;

class Add_on_product extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_add_on_product';
    protected $fillable = [
        'add_on_name', 'add_on_code', 'add_on_amount', 'position', 'service_charge_enable', 'show_online_enable', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
   
}
