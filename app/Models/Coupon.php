<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Coupon_Room_Price;

class Coupon extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_coupon';
    protected $fillable = [
        'name', 'code', 'discount_type', 'start_date', 'end_date', 'amount', 'check_in_from_date', 'check_in_to_date', 'total_coupon', 'is_used', 'is_remark','is_remark_text', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getCouponRoomPrice()
    {
        return $this->hasOne(Coupon_Room_Price::class, 'coupon_id');
    }
   
}
