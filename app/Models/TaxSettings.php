<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;

class TaxSettings extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //tax_type 0=percentage, 1=amount
    protected $table = 'ehotel_tax_settings';
    protected $fillable = [
        'tax_type', 'tax_percentage', 'tax_amount', 'created_at', 'updated_at',
    ];
   
}
