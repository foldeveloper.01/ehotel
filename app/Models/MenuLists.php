<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuLists extends Model
{
	protected $table = 'ehotel_menu_lists';
    protected $fillable = ['id', 'menu_name', 'label_name', 'menu_url','created_at','updated_at'];
    public $timestamps = false;
}
