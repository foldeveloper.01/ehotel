<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;

class Bank_Transfer extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_bank_transfer';
    protected $fillable = [
        'bank_name', 'account_name', 'account_number', 'swiftcode', 'waiting_days', 'booking_automatically_deleted', 'status', 'created_at', 'updated_at', 'image', 'deleted_at'
    ];
   
}
