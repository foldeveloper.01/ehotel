<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use App\Models\Gallery;

class Sub_Gallery extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_subgallery';
    protected $fillable = [
        'category_id', 'title', 'title1', 'image', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    public function getGallery()
    {
        return $this->belongsTo(Gallery::class, 'category_id');
    }
   
}
