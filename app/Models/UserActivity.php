<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;

class UserActivity extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_user_activity';
    protected $fillable = [
        'user_id', 'contentId', 'action', 'description', 'details', 'created_at', 'updated_at', 'contentType',
    ];

    public function userDetail()
    {
        return $this->hasOne(Admin::class,'id','user_id');
    }

    public function GetAdminType($value)
    {
       return Admintypes::findOrFail($value)->type;
    }
    
   
}
