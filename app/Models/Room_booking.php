<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Invoice;
use App\Models\Room_booking_details;
use App\Models\Rooms;
use App\Models\{Admin, Room_booking_addon,Room_booking_payment};
use Carbon\Carbon;
class Room_booking extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //payment_status  0=pending , 1=paid, 2=due, 3=refund,
    //booking_type    walk-in , online
    //deposit_payment_status 0=default, 1=cash, 2=Credit Bill, 3=Bank-In, 4=Credit Card, 5=Debit card, 6=Cheque,
    //booking_status 0=default, 1=check-in, 2=check-out, 3=refund, 4=cancelled
    //is_booking_type :  normal, midnight, monthly
    protected $table = 'ehotel_rooms_booking';
    protected $fillable = [
        'customer_id', 'check_in', 'check_out', 'number_of_rooms', 'received_amount', 'pending_amount', 'service_tax_details', 'service_tax_amount', 'service_tax_deleted', 'subtotal_amount', 'total_amount', 'booking_number', 'payment_status', 'booking_status', 'booking_type', 'admin_user_id', 'deposit', 'deposit_amount', 'deposit_payment_status', 'deposit_refund_amount', 'deposit_refund', 'refund', 'refund_amount', 'created_at', 'updated_at', 'deleted_at', 'is_booking_type' , 'discount_details', 'discount_amount', 'discount_deleted'
    ];

    public function setBookingNumberAttribute()
    {
        $this->attributes['booking_number'] = $this->generateRandomString(2);
    }
    public function generateRandomString($length = 10)
    {
          $characters = '0123456789';
          $stamp = date("ymds");
          $randomString = '';
          for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
          }
          return $stamp.$randomString;
    }
    public function get_booking_status()
    {
        if($this->booking_status==0){            
            if($this->payment_status==0){
                return 'Pending';
            }
            return 'Confirmed';
        }elseif($this->booking_status==1){
            return 'Check-In';
        }elseif($this->booking_status==2){
            return 'Check-Out';
        }elseif($this->booking_status==3){
            return 'Refunded';
        }elseif($this->booking_status==4){
            return 'Cancelled';
        }
    }

    public function guest() {
            return $this->hasOne(User::class,'id','customer_id');
    }
    public function admin() {
            return $this->hasOne(Admin::class,'id','admin_user_id');
    }
    
    public function booking_details() {
            return $this->hasMany(Room_booking_details::class,'booking_id','id')->where('deleted_at','=',null);
    }
    public function get_booking_details() {
        return $this->booking_details()->where('is_change_data','=',0)->where('deleted_at','=',null)->get();
    }
    public function checkin_rooms_count() {
        return $this->booking_details()->where('checkin_status','=',0)->where('deleted_at','=',null)->count();
    }
    public function checkout_rooms_count() {
        return $this->booking_details()->where('checkin_status','=',1)->where('checkout_status','=',0)->where('deleted_at','=',null)->count();
    }
    public function get_checkin_rooms_data() {
        return $this->booking_details()->where('checkin_status','=',1)->where('deleted_at','=',null)->get();
    }
    public function booking_addon_details() {
            return $this->hasMany(Room_booking_addon::class,'booking_id','id')->where('deleted_at','=',null);
    }     
    public function booking_exsit_addon_details() {
        return $this->booking_addon_details()->where('invoice_type','=','exist')->where('deleted_at','=',null);
    }
    public function booking_new_addon_details() {
        return $this->booking_addon_details()->where('invoice_type','=','new')->where('invoice_type_latest_old','=','latest')->where('deleted_at','=',null);
    }
    public function booking_exsit_addon_payment_details($bookingid) {
        return Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist'])->where('deleted_at','=',null)->get();
    }
    public function booking_change_payment_details($bookingid) {
        return Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['booking_date_change','booking_room_change', 'booking_date_extend'])->where('deleted_at','=',null)->get();
    }
    public function booking_payment_details() {
            return $this->hasMany(Room_booking_payment::class,'booking_id','id')->where('deleted_at','=',null);
    }
    /*public function setDepositPaymentStatusAttribute($value) {
        if($value=='Cash'){
           $this->attributes['deposit_payment_status'] = 1;
        }elseif($value=='Card'){
           $this->attributes['deposit_payment_status'] = 2;
        }elseif($value=='Bank-In'){
           $this->attributes['deposit_payment_status'] = 3;
        }elseif($value=='Credit Card'){
           $this->attributes['deposit_payment_status'] = 4;
        }elseif($value=='Debit Card'){
           $this->attributes['deposit_payment_status'] = 5;
        }elseif($value=='Cheque'){
           $this->attributes['deposit_payment_status'] = 6;
        }
    }*/  
    public function getDepositPaymentStatusAttribute() {
        if($this->attributes['deposit_payment_status']==1){
           return 'Cash';
        }elseif($this->attributes['deposit_payment_status']==2){
           return 'Card';
        }elseif($this->attributes['deposit_payment_status']==3){
           return 'Bank-In';
        }elseif($this->attributes['deposit_payment_status']==4){
           return 'Credit Card';
        }elseif($this->attributes['deposit_payment_status']==5){
           return 'Debit Card';
        }elseif($this->attributes['deposit_payment_status']==6){
           return 'Cheque';
        }
    } 
    public function get_paymentstatus_text($value) {        
        if($value==0){            
           return 'Due';
        }/*elseif($this->attributes['payment_status']==1){
           return $this->attributes['payment_status'] = 'paid';
        }elseif($this->attributes['payment_status']==2){
           return $this->attributes['payment_status'] = 'others';
        }*/
    }  
    public function checkin_date_booking() {            
            return explode(',',Carbon::parse($this->check_in)->format('d,F,l'));
    }
    public function checkout_date_booking() {
            return explode(',',Carbon::parse($this->check_out)->format('d,F,l'));
    }
    public function checkin_date() {
            return Carbon::parse($this->check_in)->format('d-m-Y');
    }
    public function checkout_date() {
            return Carbon::parse($this->check_out)->format('d-m-Y');
    }

    public function check_single_rooms_using_date($rooms_id, $start_date, $end_date){        
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $get_room_details = Room_booking_details::where('room_number_id',$rooms_id)
                                                ->where('checkout_status',0)
                                                ->where('is_change_data',0)
                                                ->where('deleted_at',null)
                                                /*->whereDate('check_in','<=',$start_date)
                                                ->whereDate('check_out','>=',$end_date)*/
                                                ->where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                /*->orwhereBetween('check_in',array($start_date,$end_date))
                                                ->orWhereBetween('check_out',array($start_date,$end_date))*/
                                                ->count();
        //dd($get_room_details);
        if($get_room_details==0){
            $check_bad_reserve_id = Room_season_bad_reserved::where('deleted_at',null)
                                                /*->whereDate('start_date','<=',$start_date)
                                                ->whereDate('end_date','>=',$end_date)*/
                                                ->where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                                                ->orwhereBetween('start_date',array($start_date,$end_date))
                                                ->orWhereBetween('end_date',array($start_date,$end_date))                                                 
                                                ->whereIn('room_number_id',[$rooms_id])                                                
                                                ->count();
            if($check_bad_reserve_id==0){
                return $response=0;//success
            }
        }
        return $response=1;//failed
    }

    public function check_single_room_available($room_type_id, $rooms_id, $start_date, $end_date){        
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $get_room_details = Room_booking_details::where('room_type_id',$room_type_id)
                                                ->where('room_number_id',$rooms_id)
                                                ->where('checkout_status',0)
                                                ->where('is_change_data',0)
                                                ->where('deleted_at',null)
                                                /*->whereDate('check_in','<=',$start_date)
                                                ->whereDate('check_out','>=',$end_date)*/
                                                ->where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                /*->orwhereBetween('check_in',array($start_date,$end_date))
                                                ->orWhereBetween('check_out',array($start_date,$end_date))*/
                                                ->count();
        //dd($get_room_details);
        if($get_room_details==0){
            $check_bad_reserve_id = Room_season_bad_reserved::where('deleted_at',null)
                                                /*->whereDate('start_date','<=',$start_date)
                                                ->whereDate('end_date','>=',$end_date)*/
                                                ->where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                                                ->orwhereBetween('start_date',array($start_date,$end_date))
                                                ->orWhereBetween('end_date',array($start_date,$end_date))                                                      
                                                ->whereIn('room_number_id',[$rooms_id])
                                                ->count();
            if($check_bad_reserve_id==0){
                return $response=0;//success
            }
        }
        return $response=1;//failed
    }
    public function check_extend_date_available($rooms_id, $start_date, $end_date){        
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');        
        $get_room_details = Room_booking_details::where('room_number_id',$rooms_id)
                                                ->where('checkout_status',0)
                                                ->where('is_change_data',0)
                                                ->where('deleted_at',null)
                                                /*->whereDate('check_in','<=',$start_date)
                                                ->whereDate('check_out','>=',$end_date)*/
                                                ->where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                ->whereBetween('check_in',array($start_date,$end_date))
                                                ->WhereBetween('check_out',array($start_date,$end_date))
                                                ->count();
        //dd($get_room_details);
        if($get_room_details==0) {
            $check_bad_reserve_id = Room_season_bad_reserved::where('deleted_at',null)
                                                /*->whereDate('start_date','<=',$start_date)
                                                ->whereDate('end_date','>=',$end_date)*/
                                                ->where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                                                ->whereBetween('start_date',array($start_date,$end_date))
                                                ->whereBetween('end_date',array($start_date,$end_date))                                               
                                                ->whereIn('room_number_id',[$rooms_id])                                                
                                                ->count();
            if($check_bad_reserve_id==0) {
                return $response=0;//success
            }
        }
        return $response=1;//failed
    }
    public function get_single_room_available($room_type_id, $rooms_id, $start_date, $end_date){
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $get_room_details = Room_booking_details::where('room_type_id',$room_type_id)
                                                ->where('room_number_id',$rooms_id)
                                                ->where('checkout_status',0)
                                                ->where('is_change_data',0)
                                                ->where('deleted_at',null)
                                                /*->whereDate('check_in','<=',$start_date)
                                                ->whereDate('check_out','>=',$end_date)*/
                                                ->where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                ->orwhereBetween('check_in',array($start_date,$end_date))
                                                ->orWhereBetween('check_out',array($start_date,$end_date))
                                                ->count();
        if($get_room_details==0){
            $check_bad_reserve_id = Room_season_bad_reserved::where('deleted_at',null)
                                                /*->whereDate('start_date','<=',$start_date)
                                                ->whereDate('end_date','>=',$end_date)*/
                                                ->where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                                                ->orwhereBetween('start_date',array($start_date,$end_date))
                                                ->orWhereBetween('end_date',array($start_date,$end_date))                                               
                                                ->whereIn('room_number_id',[$rooms_id])                                                
                                                ->count();
            if($check_bad_reserve_id==0){
                return $response=0;//success
            }
        }
        return $response=1;//failed
    }
   
}
