<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserAccessLevel extends Model
{
    protected $table = 'ehotel_user_access_level';
    protected $fillable = ['id', 'admin_type_id', 'menu_id', 'permission_view', 'permission_delete', 'permission_update', 'permission_create', 'created_at','updated_at'];
    public $timestamps = false;
}
