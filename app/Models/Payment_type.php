<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;

class Payment_type extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_payment_type';
    protected $fillable = [
        'payment_name', 'merchant_id', 'merchant_code', 'merchant_key', 'merchant_password', 'api_key', 'collection_id', 'id_type', 'id_number', 'redirect_url', 'success_url', 'failure_url', 'status', 'image', 'created_at', 'updated_at', 'deleted_at'
    ];
   
}
