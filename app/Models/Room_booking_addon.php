<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Invoice;
use App\Models\Room_booking_details;
use App\Models\Rooms;
use App\Models\{Admin,Add_on_product};
use Carbon\Carbon;
class Room_booking_addon extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //payment_status  0=pending , 1=paid, 2=others
    //booking_type    0=defauld , 1=cash, 2=Credit Bill, 3=Bank-in, 4=credit card, 5=debit card,

    protected $table = 'ehotel_rooms_booking_addon';
    protected $fillable = [
        'booking_id', 'product_addon_id', 'price', 'quantity', 'service_tax', 'subtotal_amount', 'total_amount', 'invoice_type', 'invoice_type_latest_old', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getaddon() {
            return $this->hasOne(Add_on_product::class,'id','product_addon_id');
    }
   
}
