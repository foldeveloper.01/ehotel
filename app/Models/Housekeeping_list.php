<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Rooms_manage;
use App\Models\Admintypes;

class Housekeeping_list extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_housekeeping_list';
    protected $fillable = [
        'booking_number', 'room_type_id', 'room_number_id', 'check_in', 'check_out', 'release_date', 'status', 'user_id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getRoomData(){
        return $this->hasOne(Rooms_manage::class,'id','room_number_id');
    }

    public function getUserData(){
        return $this->hasOne(Admin::class,'id','user_id');
    }
     public function booking_data(){
        return $this->hasOne(Room_booking::class,'booking_number','booking_number');
    }
   
}
