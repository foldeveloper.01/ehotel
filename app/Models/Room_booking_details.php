<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\{Rooms,Rooms_manage,Invoice,User,Room_season_bad_reserved};
use Carbon\Carbon;

class Room_booking_details extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //checkin_status 0=not checkin; 1=checkin
    //checkout_status 0=not checkout; 1=checkout
    protected $table = 'ehotel_rooms_booking_details';
    protected $fillable = [
        'booking_id', 'room_type_id', 'checkin_status', 'checkout_status', 'room_number_id', 'customer_id', 'check_in', 'check_out', 'checkin_date_time', 'checkout_date_time', 'room_quantity', 'room_price', 'subtotal_amount', 'status', 'is_booking_change_date', 'old_checkin_date', 'old_checkout_date', 'old_rooms_id', 'old_checkin_extend_date', 'old_checkout_extend_date', 'number_of_days', 'is_change_data', 'is_booking_change_room', 'is_booking_extend_date', 'created_at', 'updated_at', 'deleted_at'
    ];
    
    public function guest() {
            return $this->hasOne(User::class,'id','customer_id');
    }
    /*public function invoice() {
            return $this->hasOne(Invoice::class,'booking_id','id');
    }*/
    public function rooms() {
            return $this->hasOne(Rooms::class,'id','room_type_id');
    }
    public function rooms_number() {
            return $this->hasOne(Rooms_manage::class,'id','room_number_id');
    }
    public function change_room_number() {
            return $this->hasOne(Rooms_manage::class,'id','old_rooms_id');
    }
    public function checkin_date() {
            return Carbon::parse($this->check_in)->format('d-m-Y');
    }
    public function checkout_date() {
            return Carbon::parse($this->check_out)->format('d-m-Y');
    }
    public function old_checkin_date() {
            return Carbon::parse($this->old_checkin_date)->format('d-m-Y');
    }
    public function old_checkout_date() {
            return Carbon::parse($this->old_checkout_date)->format('d-m-Y');
    }
    public function old_checkin_extend_date() {
            return Carbon::parse($this->old_checkin_extend_date)->format('d-m-Y');
    }
    public function old_checkout_extend_date() {
            return Carbon::parse($this->old_checkout_extend_date)->format('d-m-Y');
    }
    public function get_rooms_number($id) {
            return Rooms_manage::findOrFail($id);
    }
    
   
}
