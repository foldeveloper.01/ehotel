<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;

class Settings extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_settings';
    protected $fillable = [
        'site_name', 'site_link', 'email', 'phone', 'fax', 'website_maintenance', 'meta_author', 'meta_keywords', 'meta_descriptions', 'header', 'logo', 'favicon', 'address', 'created_at', 'updated_at','whatsapp','facebook','instagram','twitter','youtube', 'business_registration',
    ];
   
}
