<?php
namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use App\Models\{Rooms_manage, Room_booking_details, Room_price, Room_season_bad_reserved, Room_season_manage, Room_season_type};
use Carbon\Carbon;

class Rooms extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_rooms';
    protected $fillable = [
        'room_type', 'total_room', 'image', 'occupants', 'show_online', 'view', 'size', 'bed', 'description', 'facilities', 'status', 'created_at', 'updated_at', 'deleted_at', 'room_overview', 'room_amenities', 'room_price_update_status'
    ];
    public function getFacilitiesAttribute($value)
    {
        return unserialize($value);
    }    

    public function roomprice()
    {
        return $this->hasOne(Room_price::class,'room_id','id');
    }
    public function rooms_number()
    {
        return $this->hasMany(Rooms_manage::class,'room_id','id');
    }
    public static function get_rooms_price($room_type_id, $start_date, $end_date, $number_of_days) {
        $room_normal_price = Rooms::findOrFail($room_type_id);
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $room_season_manage_data = Room_season_manage::where('deleted_at')
                                                ->whereDate('start_date','>=',$start_date)
                                                ->whereDate('end_date','<=',$end_date)
                                                ->pluck('room_season_type_id')->toArray();
        if(count($room_season_manage_data)){
            $get_session_type = Room_season_type::whereIn('id',$room_season_manage_data)->get();            
            $get_price_details = [];
            foreach($get_session_type as $key=>$val){
                if($val->id==1){
                    //Weekend                    
                    $get_price_details[$val->name]=$room_normal_price->roomprice->normal_weekend;                    
                }elseif($val->id==2){
                    //Promotion
                    $get_price_details[$val->name]=$room_normal_price->roomprice->normal_promotion;
                }elseif($val->id==3){
                    //School Holiday
                    $get_price_details[$val->name]=$room_normal_price->roomprice->normal_school_holiday;
                }elseif($val->id==4){
                    //Public Holiday
                    $get_price_details[$val->name]=$room_normal_price->roomprice->normal_public_holiday;
                }
            }
            return max($get_price_details);
        }else{
           return $room_normal_price->roomprice->normal_price;
        }
    }
    
    public function get_available_rooms_number($room_type_id,$start_date, $end_date){
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $getRoomsNumber = Rooms_manage::where('room_id',$room_type_id)->where('deleted_at',null)->pluck('room_no','id')->toArray();
        //dd($getRoomsNumber);       
        $IgnoreRoomsNumber=[];
        $get_room_details = Room_booking_details::
                                               /* ->whereDate('check_in','>=',$start_date)
                                                ->whereDate('check_out','<=',$end_date) */
                                                 where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                ->whereBetween('check_in',array($start_date,$end_date))
                                                ->WhereBetween('check_out',array($start_date,$end_date))
                                                ->where('room_type_id',$room_type_id)
                                                ->where('checkout_status',0)
                                                ->where('is_change_data',0)
                                                ->where('deleted_at',null)                                          
                                                ->pluck('room_number_id','room_number_id')->toArray();        
        $feature_Booking_RoomsId = array_diff_key($getRoomsNumber,$get_room_details);
        $check_bad_reserve_id = Room_season_bad_reserved::where('deleted_at',null)
                                    /*->whereDate('start_date','>=',$start_date)
                                    ->whereDate('end_date','<=',$end_date)*/
                                    ->where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                                    ->whereBetween('start_date',array($start_date,$end_date))
                                    ->whereBetween('end_date',array($start_date,$end_date))                                              
                                    ->whereIn('room_number_id',array_keys($feature_Booking_RoomsId))
                                    ->pluck('room_number_id','room_number_id')
                                    ->toArray();
        $available_id = array_diff_key($feature_Booking_RoomsId,$check_bad_reserve_id);
        //dd($available_id);
        /*if(count($available_id)!=0) {
           $feature_Booking_RoomsId_condition_added=[]; 
           foreach($available_id as $key=>$value) {
                   $feature_Booking_RoomsId_condition_added[$value]=$feature_Booking_RoomsId[$value];
           }
           return $feature_Booking_RoomsId_condition_added;
        }
        dd($feature_Booking_RoomsId);*/
        return $available_id;
    }
    public static function get_only_room_type_availables($start_date, $end_date) {
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $finalData=[]; $IgnoreRoomsTypeId=[];
        $get_room_details = Room_booking_details::where('checkout_status',0)
                                                ->where('is_change_data',0)
                                                ->where('deleted_at',null)
                                                /*->whereDate('check_in','>=',$start_date)
                                                ->whereDate('check_out','<=',$end_date)*/
                                                ->where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                ->whereBetween('check_in',array($start_date,$end_date))
                                                ->whereBetween('check_out',array($start_date,$end_date))
                                                ->get();
        if(count($get_room_details)){            
            foreach($get_room_details as $key=>$data){
                $room_data = Rooms::findOrFail($data->room_type_id);
                $finalData['total_rooms_count'][$data->room_type_id]=$room_data->total_room;
                $finalData['rooms_numbers'][$data->room_type_id][]=$data->room_number_id;
            }            
            foreach($finalData['total_rooms_count'] as $fk=>$val){
                if(count($finalData['rooms_numbers'][$fk])==$val){
                     $IgnoreRoomsTypeId[]=$fk;
                }
            }
        }
        return $IgnoreRoomsTypeId;
    }
   
}
