<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\Admin;
class Room_invoice_remark extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_rooms_invoice_remark';
    protected $fillable = [
        'room_booking_id', 'remarks', 'admin_user_id', 'created_at', 'updated_at', 'deleted_at'
    ];  

    public function guest() {
            return $this->hasOne(Admin::class,'id','admin_user_id');
    }
   
}
