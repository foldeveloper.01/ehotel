<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\{User, Room_booking, Coupon_Room_Price, Admin, Admintypes, Room_booking_receipt};

class Invoice extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //invoice_status paid=paid, void=void
    protected $table = 'ehotel_rooms_invoice';
    protected $fillable = [
        'booking_id', 'receipt_no', 'invoice_number', 'invoice_date', 'invoice_customer_id', 'invoice_status', 'admin_user_id'
    ];

    public function guest() {
            return $this->hasOne(User::class,'id','invoice_customer_id');
    }
    public function admin() {
            return $this->hasOne(Admin::class,'id','admin_user_id');
    }
    public function receipt() {
            return $this->hasOne(Room_booking_receipt::class,'receipt_no','receipt_no');
    }
    public function booking_details() {
            return $this->hasOne(Room_booking::class,'id','booking_id');
    }

    public function invoiceNumber()
    {
        $latest = Invoice::latest()->first();
        $startNumber=100;
        if (! $latest) {
            return ++$startNumber;
        }else{
            return ++$latest->invoice_number;
        }
    }
   
}
