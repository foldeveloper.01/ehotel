<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;
use Illuminate\Support\Str;
use App\Models\{User,Invoice,Rooms,Room_booking,Room_booking_payment};
class Room_booking_receipt extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_rooms_booking_receipt';
    protected $fillable = [
        'booking_id', 'payment_table_id', 'receipt_no', 'receipt_amount', 'payment_status', 'payment_type', 'created_at', 'updated_at', 'deleted_at'
    ];    
    //payment_type    1=cash, 2=Credit Bill, 3=Bank-in, 4=credit card, 5=debit card, 
   
    public function setReceiptNoAttribute($value){
        $this->attributes['receipt_no'] = Room_booking_receipt::count()+1;
    }
    public function getbooking_data() {
            return $this->hasOne(Room_booking::class,'id','booking_id');
    }
    public function get_payment_data() {
            return $this->hasOne(Room_booking_payment::class,'id','payment_table_id');
    }
    public function getPaymentTypeAttribute($value) {
        if($this->attributes['payment_type']==1){
            return 'cash';
        }if($this->attributes['payment_type']==2){
            return 'credit bill';
        }if($this->attributes['payment_type']==3){
            return 'bank in';
        }if($this->attributes['payment_type']==4){
            return 'credit card';
        }if($this->attributes['payment_type']==5){
            return 'debit card';
        }       
    }
    public function get_payment_type($value) {
        if($value==1){
            return 'cash';
        }elseif($value==2){
            return 'credit bill';
        }elseif($value==3){
            return 'bank in';
        }elseif($value==4){
            return 'credit card';
        }elseif($value==5){
            return 'debit card';
        }       
    }
}
