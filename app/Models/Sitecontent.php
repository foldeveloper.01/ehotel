<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\ModelEventLogger;

class Sitecontent extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_sitecontent';
    protected $fillable = [
        'title', 'menu_name', 'image', 'image1', 'description', 'short_description', 'meta_title', 'meta_keyword', 'meta_description', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
   
}
