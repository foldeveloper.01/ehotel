<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Rooms;
use App\Models\Admintypes;

class Coupon_Room_Price extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ehotel_coupon_room_price';
    protected $fillable = [
        'coupon_id', 'room_id', 'normal_price', 'weekend_price', 'promotion_price', 'school_holiday_price', 'public_holiday_price', 'created_at', 'updated_at'
    ];
    /*protected $casts = [
      'room_id' => 'array'
    ];*/

    public function getRoomIdAttribute($value)  {
        return @unserialize($value) !== false ? unserialize($value) : $value;
    }

    public function getRoomData($id)
    {
        return Rooms::whereIn('id',$id)->get();
    }
   
}
