<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Settings;
use App\Models\Logo;
use PDO;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //header and footer values get
         View::composer('*', function ($view) {
          $sitesetting = Settings::first();          
          $logo = Logo::first();          
          $view->with('setting',$sitesetting)->with('site_logo',$logo);
         });
    }
}
