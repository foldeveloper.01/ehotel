<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Settings;
use App\Models\Admin;
use App\Models\UserActivity;
use App\Models\SmtpSettings;
use App\Models\UserLoginHistroy;
use App\Policies\SettingsPolicy;
use App\Policies\SmtpSettingsPolicy;
use App\Policies\AdminPolicy;
use App\Policies\UserActivityPolicy;
use App\Policies\UserLoginHistroyPolicy;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
         Settings::class => SettingsPolicy::class,
         Admin::class => AdminPolicy::class,
         UserActivity::class => UserActivityPolicy::class,
         UserLoginHistroy::class => UserLoginHistroyPolicy::class,
         SmtpSettings::class => SmtpSettingsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
