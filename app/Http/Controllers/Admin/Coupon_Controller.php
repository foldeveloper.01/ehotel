<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Rooms,Voucher,Coupon_Room_Price,Coupon,City,State,Country,User,Admintypes,Admin};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use DB;
use App\Exports\GuestUsersExport;
use Excel;

class Coupon_Controller extends Controller
{
    //index
    public function listdata(Request $request){
        $request->session()->forget('active_tab');
        $list = Coupon::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.coupon.list',compact('list'));
    }
    public function createdata(Request $request){
        if(!$request->session()->has('active_tab')){
          $request->session()->put('active_tab',1);
        }
       $room_types = Rooms::where('status',0)->where('deleted_at',null)->get();
       return view('admin.coupon.create',compact('room_types'));
    }
    public function savedata(Request $request){
        //dd($request->all());
        //dd(is_array($request->room));
        $request->session()->put('active_tab',1);
        $validated = Validator::make($request->all(),[
            'name' => 'max:200|unique:ehotel_coupon,name,NULL,id,deleted_at,NULL',
            'code' => 'required|max:100',
            'discount_type' => 'required|max:100',
            'amount_start_date' => 'required|max:100',
            'percentage_start_date' => 'required|max:100',
            'from_date' => 'required|max:100',            
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                if($request->discount_type=='percentage'){
                    $request->session()->put('active_tab',2);
                }
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $check_in_from_date = Carbon::parse(trim(explode('/',str_replace(" - "," / ",trim($request->from_date)))[0]))->format('Y-m-d');
        $check_in_to_date = Carbon::parse(trim(explode('/',str_replace(" - "," / ",trim($request->from_date)))[1]))->format('Y-m-d');
        $coupon = new Coupon();
        if($request->discount_type=='amount'){
          $start_date = Carbon::parse(trim(explode('/',$request->amount_start_date)[0]))->format('Y-m-d');
          $end_date = Carbon::parse(trim(explode('/',$request->amount_start_date)[1]))->format('Y-m-d');
          $coupon->start_date = $start_date;
          $coupon->end_date = $end_date;
          $coupon->amount = $request->amount;          
        }elseif($request->discount_type=='percentage'){
          $start_date = Carbon::parse(trim(explode('/',$request->percentage_start_date)[0]))->format('Y-m-d');
          $end_date = Carbon::parse(trim(explode('/',$request->percentage_start_date)[1]))->format('Y-m-d');
          $coupon->start_date = $start_date;
          $coupon->end_date = $end_date;
          $request->session()->put('active_tab',2);
        }
        $coupon->name = $request->name;
        $coupon->code = $request->code;
        $coupon->discount_type = $request->discount_type;        
        $coupon->check_in_from_date = $check_in_from_date;
        $coupon->check_in_to_date = $check_in_to_date;        
        $coupon->is_remark = $request->is_remark;
        $coupon->is_remark_text = $request->is_remark_text;
        $coupon->total_coupon = $request->total_coupon;
        $coupon->status = $request->status;
        $coupon->save();
        //save prices
        if($request->discount_type=='percentage'){
            if(is_array($request->room)){
                /*foreach($request->room as $key=>$value){*/
                    $data = new Coupon_Room_Price();
                    $data->coupon_id = $coupon->id;
                    $data->room_id = serialize(array_filter($request->room));
                    $data->normal_price = $request->normal_price;
                    $data->weekend_price = $request->weekend_price;
                    $data->promotion_price = $request->promotion_price;
                    $data->school_holiday_price = $request->school_holiday_price;
                    $data->public_holiday_price = $request->public_holiday_price;
                    $data->save();
               /* }*/
            }          
        }
        //save prices
        return redirect('admin/coupon')->with('success','Added sucessfully.');
    }    
    public function updatedata(Request $request,$id){
        $data = Coupon::findOrFail($id);
        $room_types = Rooms::where('status',0)->where('deleted_at',null)->get();
        $get_selected_roomid=[];
        if(Coupon_Room_Price::where('coupon_id',$id)->count()){
         $room_types_id = Coupon_Room_Price::where('coupon_id',$id)->pluck('room_id');
         $get_selected_roomid = $room_types_id[0];
        }
        if($data->discount_type=='amount'){ $tab_number=1; }else{ $tab_number=2; };
        if(!$request->session()->has('active_tab')){
          $request->session()->put('active_tab',$tab_number);
        }
        return view('admin.coupon.update', compact('data','room_types','get_selected_roomid'));
    }
    public function dataupdate(Request $request, $id){        
            $request->session()->put('active_tab',1);
            $validated = Validator::make($request->all(),[
               'name' => 'max:200|unique:ehotel_coupon,name,'.$id.',id,deleted_at,NULL',
               'code' => 'required|max:100',
               'discount_type' => 'required|max:100',
               'amount_start_date' => 'required|max:100',
               'percentage_start_date' => 'required|max:100',
               'from_date' => 'required|max:100',            
               'status' => 'required',
             ]);
        if ($validated->fails()) {
                if($request->discount_type=='percentage'){
                    $request->session()->put('active_tab',2);
                }
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $check_in_from_date = Carbon::parse(trim(explode('/',str_replace(" - "," / ",trim($request->from_date)))[0]))->format('Y-m-d');
        $check_in_to_date = Carbon::parse(trim(explode('/',str_replace(" - "," / ",trim($request->from_date)))[1]))->format('Y-m-d');
        $coupon = Coupon::findOrFail($id);
        if($request->discount_type=='amount'){
          $start_date = Carbon::parse(trim(explode('/',$request->amount_start_date)[0]))->format('Y-m-d');
          $end_date = Carbon::parse(trim(explode('/',$request->amount_start_date)[1]))->format('Y-m-d');
          $coupon->start_date = $start_date;
          $coupon->end_date = $end_date;
          $coupon->amount = $request->amount;
          if(is_object(Coupon_Room_Price::where('coupon_id',$coupon->id)->first())){
             Coupon_Room_Price::where('coupon_id',$coupon->id)->delete();
          }         
        }elseif($request->discount_type=='percentage'){
          $start_date = Carbon::parse(trim(explode('/',$request->percentage_start_date)[0]))->format('Y-m-d');
          $end_date = Carbon::parse(trim(explode('/',$request->percentage_start_date)[1]))->format('Y-m-d');
          $coupon->start_date = $start_date;
          $coupon->end_date = $end_date;
          $coupon->amount = $request->amount;
          $request->session()->put('active_tab',2);
        }
        $coupon->name = $request->name;
        $coupon->code = $request->code;
        $coupon->discount_type = $request->discount_type;        
        $coupon->check_in_from_date = $check_in_from_date;
        $coupon->check_in_to_date = $check_in_to_date;        
        $coupon->is_remark = $request->is_remark;
        $coupon->is_remark_text = $request->is_remark_text;
        $coupon->total_coupon = $request->total_coupon;
        $coupon->status = $request->status;
        $coupon->save();
        //save prices
        if($request->discount_type=='percentage'){
            if(is_array($request->room)){
                if(is_object(Coupon_Room_Price::where('coupon_id',$coupon->id)->first())){
                $data = Coupon_Room_Price::where('coupon_id',$coupon->id)->first();
                $data->room_id = serialize(array_filter($request->room));
                $data->normal_price = $request->normal_price;
                $data->weekend_price = $request->weekend_price;
                $data->promotion_price = $request->promotion_price;
                $data->school_holiday_price = $request->school_holiday_price;
                $data->public_holiday_price = $request->public_holiday_price;
                $data->save();
                }else{
                $data = new Coupon_Room_Price();
                $data->coupon_id = $coupon->id;
                $data->room_id = serialize(array_filter($request->room));
                $data->normal_price = $request->normal_price;
                $data->weekend_price = $request->weekend_price;
                $data->promotion_price = $request->promotion_price;
                $data->school_holiday_price = $request->school_holiday_price;
                $data->public_holiday_price = $request->public_holiday_price;
                $data->save();
              }
            }          
        }
        //save prices
        return redirect('admin/coupon')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
         $data = Coupon::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/coupon')->with('success','Deleted sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Coupon::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }

    //index
    public function getdataforajax(Request $request){   
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $totalRecords = Coupon::where('deleted_at',null)->select('count(*) as allcount')->count();
     $totalRecordswithFilter = Coupon::where('deleted_at',null)
                                  ->where(function($query) use($searchValue) {
                                    $query->where('name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('code', 'LIKE', '%'.$searchValue.'%')                                          
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })
                                  ->select('count(*) as allcount')
                                  ->count();
     // Fetch records
    $records = Coupon::where('deleted_at',null)
                                  ->where(function($query) use($searchValue) {
                                    $query->where('name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('code', 'LIKE', '%'.$searchValue.'%')                                         
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();
     $data_arr = array();     
     foreach($records as $key=>$record){        
        $action = "<a href=".url('admin/coupon/update/'.$record->id)." class='btn btn-sm btn-primary'>
                    <i class='fe fe-edit'></i>
                  </a>
                  <a href=".url('admin/coupon/view/'.$record->id)." class='btn btn-sm btn-success'>
                    <i class='fe fe-eye'></i>
                  </a>
                  <a onclick='deletefunction(event,$record->id);' href='' class='btn btn-sm btn-danger'>
                    <i class='fe fe-x'></i>
                  </a>
                  <form id='delete_form$record->id' method='POST' action=".url('admin/coupon/delete', $record->id)."> <input type='hidden' name='_token' value='".csrf_token()."'> <input name='_method' type='hidden' value='POST'>
                  </form>";
        if($record->status=='0'){ $status_check ='checked'; }else{ $status_check ='notchecked'; }
        $status = "<div class='col-xl-2 ps-1 pe-1'>
                    <div class='form-group'>
                      <label class='custom-switch form-switch mb-0'>
                        <input type='checkbox' id='status_$record->id' name='status' class='custom-switch-input' $status_check> <span class='custom-switch-indicator custom-switch-indicator-md' data-on='Yes' data-off='No'></span>
                      </label>
                    </div>
                  </div>";
        $discount_type='';
        if($record->discount_type=='amount'){
           $discount_type='By Amount';
        }elseif($record->discount_type=='percentage'){
           $discount_type='By Percentage';
        }
        $data_arr[] = array(
          "id" => null,
          "name" => $record->name,
          "code" => $record->code,
          "discount_type" => $discount_type,
          "end_date" => Carbon::parse($record->end_date)->format('d-m-Y'),
          "check_in_from_date" => 'Check-In Between '.Carbon::parse($record->check_in_from_date)->format('d-m-Y').' and '.Carbon::parse($record->check_in_to_date)->format('d-m-Y'),
          "total_coupon" => $record->total_coupon,
          "used" => ($record->is_used==0) ? 'No' : 'Yes',
          "status" => $status,
          "created_at" => Carbon::parse($record->created_at)->format('d-m-Y H:i'),
          "action" => $action
        );
     }
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }

    public function viewdata(Request $request,$id){
        $data = Coupon::findOrFail($id);
        $room_name='';
        if($data->getCouponRoomPrice){
           foreach($data->getCouponRoomPrice->getRoomData($data->getCouponRoomPrice->room_id) as $key=>$val){
                   $room_name.=$val->room_type.', ';
           }
           $room_name=substr($room_name, 0, strlen($room_name) - 2);
        }
        return view('admin.coupon.view', compact('data','room_name'));
    }

   public function coupon_voucher_applied(Request $request) {
        //dd($request->all());
        if($request->checkin_date!='') {
            $data=[];           
            $current_date = Carbon::now();
            $checkin_date = $request->checkin_date;
            $code = $request->code;            
            $data['details']  = '';
            $data['amount']   = '0.00';                  
            $data['message']  = "Voucher/Coupon Invalid";                  
            $data['response'] = 1;
            $data['type'] ='';
            //check voucher   
            $voucher =  Voucher::where(DB::raw('BINARY `code`'),$code)
                                  ->whereDate('start_date','<=',$current_date)
                                  ->whereDate('end_date','>=',$current_date)
                                  ->where('deleted_at',null)
                                  ->first();               
            if(is_object($voucher)) {
                 $total_voucher_count = $voucher->total_voucher;
                 $used_voucher_count  = $voucher->is_used;
                 if($used_voucher_count >= $total_voucher_count) {                    
                    $data['details']  = '';
                    $data['amount']   = '0.00';                  
                    $data['message']  = "voucher expired";
                    $data['response'] = 1;
                    $data['type'] = 'voucher';
                 } else {
                    $amount = $voucher->amount;
                    $data['details']  = 'voucher_'.$voucher->id.'_'.$amount;
                    $data['amount']   = $amount;
                    $data['message']  = "voucher applied";                  
                    $data['response'] = 0;
                    $data['type'] = 'voucher';                  
                 }                
                return response()->json($data); exit;
              }
            //check voucher

            //check coupon
            $coupon =  Coupon::where(DB::raw('BINARY `code`'),$code)
                                  ->whereDate('start_date','<=',$current_date)
                                  ->whereDate('end_date','>=',$current_date)
                                  ->where('deleted_at',null)
                                  ->first();
            if(is_object($coupon)) {
                $check_in_from_date = $coupon->check_in_from_date;
                $check_in_to_date = $coupon->check_in_to_date; 
                $check = Carbon::parse($checkin_date)->between($check_in_from_date, $check_in_to_date);              
                if($check){
                    $total_coupon_count = $coupon->total_coupon;
                    $used_coupon_count  = $coupon->is_used;
                     if($coupon->discount_type=='amount') {                        
                        if($used_coupon_count >= $total_coupon_count) {
                        $data['details']  = '';
                        $data['amount']   = '0.00';                  
                        $data['message']  = "coupon expired";
                        $data['response'] = 1;
                        $data['type'] = 'coupon';
                        }else{
                        $amount = $coupon->amount;
                        $data['details']  = 'coupon_'.$coupon->id.'_'.$amount;
                        $data['amount']   = $amount;
                        $data['message']  = "coupon applied";                  
                        $data['response'] = 0;
                        $data['type'] = 'coupon';
                       }
                    }elseif($coupon->discount_type=='percentage') {
                      $amount = Helper::getRoomTypeDiscountPrice($request,$coupon->id);
                      //dd($amount);
                      $data['details']  = 'coupon_'.$coupon->id.'_'.$amount;
                      $data['amount']   = $amount;
                      $data['message']  = "coupon applied";                  
                      $data['response'] = 0;
                      $data['type'] = 'coupon_percentage';
                    }
                }else{
                    $data['details']  = '';
                    $data['amount']   = '0.00';                  
                    $data['message']  = "The coupon is not available for this date";
                    $data['response'] = 1;
                    $data['type'] = 'coupon';
                }
            }            
            //check coupon
            return response()->json($data); exit;
        }
        $data['response'] = 3;
        $data['message'] = 'Please check parameters';        
        return response()->json($data); exit;
    }

}