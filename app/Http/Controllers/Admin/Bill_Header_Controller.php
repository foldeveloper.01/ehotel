<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Bill_header;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;

class Bill_Header_Controller extends Controller
{
    public function billheader(){
        $data = Bill_header::first();
        return view('admin.bill_header.create',compact('data'));
    }    
    //create settings
    public function savebillheader(Request $request)
    {         
        $validated = Validator::make($request->all(),[        
        'header_image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error', implode(",", $validated->errors()->all()));
        }
        //create data
        $data = Bill_header::count();
        if($data==0){
           $header = new Bill_header();            
         if($request->hasFile('header_image'))
         {
            //$path = $request->file('header_image')->store('public/images/bill_header');
            $file_name = Helper::setUploadfileName($request,'billheader').'.'.$request->file('header_image')->extension();
            $path = $request->file('header_image')->storeAs('public/images/bill_header',$file_name);
            $header->header_image  = explode ("/", $path)[3];
         }
        $header->footer_message = $request->footer_message;        
        $header->customer_signature = $request->customer_signature;        
        $header->save();
        }else{
        $header = Bill_header::first();
         if($request->hasFile('header_image'))
         {
            //delete old files            
            if(Storage::disk('public')->exists('images/bill_header/'.$header->header_image)){
               Storage::disk('public')->delete('images/bill_header/'.$header->header_image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'billheader').'.'.$request->file('header_image')->extension();
            $path = $request->file('header_image')->storeAs('public/images/bill_header',$file_name);
            $header->header_image  = explode ("/", $path)[3];
         }
        $header->footer_message = $request->footer_message;        
        $header->customer_signature = $request->customer_signature;        
        $header->save();
        }
        return redirect('admin/billheader')->with('success','updated sucessfully.');
    }

}