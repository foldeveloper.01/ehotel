<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Sub_Gallery;
use App\Models\Rooms;
use App\Models\Room_Setting;
use App\Models\Room_facilities;
use App\Models\Room_gallery;
use App\Models\Room_price;
use App\Models\Rooms_manage;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;

class Rooms_Controller extends Controller
{
    public function listdata(Request $request){
        $request->session()->forget('active_tab');
        $list = Rooms::where('deleted_at',null)->orderBy('id','DESC')->paginate(10);
        $room_setting = Room_Setting::first();        
        return view('admin.rooms.list',compact('list','room_setting'));
    }    
    public function createdata(){
        $facilities = Room_facilities::where('status','0')->where('deleted_at',null)->get();
        $room_setting = Room_Setting::first();
        return view('admin.rooms.create',compact('facilities','room_setting'));
    }
    public function savedata(Request $request) {
        $helper_response = Helper::RoomSetting($request,'create','');
        if($helper_response['data']==3) {
            return redirect()->back()->withInput()->with('error',$helper_response['message']);
        }elseif($helper_response['data']==2) {
            return redirect()->back()->withInput()->with('error',$helper_response['message']);
        }
        $validated = Validator::make($request->all(),[
            'room_type' => 'required|max:200',
            'total_room' => 'required|max:200',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $rooms = new Rooms();
        if($request->hasFile('image'))
         {
            $file_name = Helper::setUploadfileName($request,'rooms').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/rooms',$file_name);
            $rooms->image  = explode ("/", $path)[3];
         }
        $rooms->room_type = $request->room_type;
        $rooms->total_room = $request->total_room;
        $rooms->occupants = $request->occupants;
        //$rooms->show_online = $request->show_online;
        $rooms->show_online = 1;
        $rooms->view = $request->view;
        $rooms->size = $request->size;
        $rooms->bed = $request->bed;
        $rooms->description = $request->description;
        $rooms->facilities = (is_array($request->facilities)) ? serialize(array_filter($request->facilities)) : '';
        $rooms->status = $request->status;
        $rooms->save();
        //save room manage data
        $start = ($request->room_number_start_with) ? $request->room_number_start_with : '100';
        $limit = $request->total_room;        
        for ($i=0; $i < $limit; $i++) {
            $data = new Rooms_manage();
            $data->room_id = $rooms->id;
            $data->room_no = $start+$i;
            $data->status = 0;
            $data->save();
        }
        //save room manage data
        $request->session()->put('active_tab',2);
        return redirect('admin/rooms/update/'.$rooms->id)->with('success','Added sucessfully.');
    }    
    public function updatedata(Request $request,$id){
        if(!$request->session()->has('active_tab')){
          $request->session()->put('active_tab',1);
        }
        $this_room_fecilities = [];
        $data = Rooms::findOrFail($id);
        $faci_lities = Room_facilities::where('status','0')->where('deleted_at',null)->get();
        $room_gallery = Room_gallery::where('deleted_at',null)->orderBy('id','desc')->get();
        $room_price = Room_price::where('room_id',$id)->first();
        $room_setting = Room_Setting::first();        
        if(is_array($data->facilities)) {
            $this_room_fecilities = $data->facilities;
        }        
        return view('admin.rooms.update', compact('data','faci_lities','room_gallery', 'room_price','room_setting','this_room_fecilities'));
    }
    public function dataupdate(Request $request, $id){
            if($request->action_tab=='room_details'){
                $validated = Validator::make($request->all(),[
                'room_type' => 'required|max:200',
                'total_room' => 'required|max:200',
                'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required',
                 ]);
                if ($validated->fails()) {                    
                    return redirect()
                                ->back()
                                ->withErrors($validated)
                                ->withInput()
                                ->with('error', implode(",", $validated->errors()->all()));
                }
                $res = $this->rooms_details_save($request,$id);
                $request->session()->put('active_tab',1);
                if($res['code']==1){
                    return redirect('admin/rooms/update/'.$id)->with('error',$res['message']);
                }
            }elseif($request->action_tab=='room_price') {
                $room = Rooms::findOrFail($id);
                if($room->room_price_update_status==0) {
                   $room->show_online = 0;
                }
                $room->room_price_update_status = 1;             
                
                $room_price = Room_price::where('room_id',$id)->first();
                if(is_object($room_price)) {
                   $room_price->room_id = $id;$room_price->normal_price = $request->normal_price;$room_price->normal_weekend = $request->normal_weekend;$room_price->normal_public_holiday = $request->normal_public_holiday;$room_price->normal_promotion = $request->normal_promotion;$room_price->normal_school_holiday = $request->normal_school_holiday;$room_price->hourly_price = $request->hourly_price;
                   $room_price->hourly_weekend = $request->hourly_weekend;$room_price->hourly_public_holiday = $request->hourly_public_holiday;
                   $room_price->hourly_promotion = $request->hourly_promotion;$room_price->hourly_school_holiday = $request->hourly_school_holiday;
                   $room_price->save();
                }else{
                   $rooms = new Room_price();
                   $rooms->room_id = $id;$rooms->normal_price = $request->normal_price;$rooms->normal_weekend = $request->normal_weekend;$rooms->normal_public_holiday = $request->normal_public_holiday;$rooms->normal_promotion = $request->normal_promotion;$rooms->normal_school_holiday = $request->normal_school_holiday;$rooms->hourly_price = $request->hourly_price;
                   $rooms->hourly_weekend = $request->hourly_weekend;$rooms->hourly_public_holiday = $request->hourly_public_holiday;
                   $rooms->hourly_promotion = $request->hourly_promotion;$rooms->hourly_school_holiday = $request->hourly_school_holiday;
                   $rooms->save();
                   $room->show_online = 0;
                }
                $room->save();              
                $request->session()->put('active_tab',2);
            }elseif($request->action_tab=='room_overview') {
                $rooms = Rooms::findOrFail($id);
                $rooms->room_overview = $request->room_overview;
                $rooms->save();
                $request->session()->put('active_tab',3);
            }elseif($request->action_tab=='room_amenities') {
                $rooms = Rooms::findOrFail($id);
                $rooms->room_amenities = $request->room_amenities;
                $rooms->save();
                $request->session()->put('active_tab',4);
            }elseif($request->action_tab=='room_gallery') {
                $validated = Validator::make($request->all(),[
                'image1' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048'
                 ]);
                if ($validated->fails()) {
                    return redirect()->back()->withErrors($validated)->withInput()->with('error', implode(",", $validated->errors()->all()));
                }
                $room_gallery = new Room_gallery();
                $file_name = Helper::setUploadfileName($request,'rooms_gallery').'.'.$request->file('image1')->extension();
                $path = $request->file('image1')->storeAs('public/images/rooms_gallery',$file_name);
                $room_gallery->image_name  = explode ("/", $path)[3];
                $room_gallery->room_id = $id;
                $room_gallery->save();
                $request->session()->put('active_tab',5);
            }               
         return redirect('admin/rooms/update/'.$id)->with('success','Updated sucessfully.');
    }
    public function rooms_details_save($request, $id){
        $rooms = Rooms::findOrFail($id);
        $helper_response = Helper::RoomSetting($request,'update',$rooms->total_room);
        if($helper_response['data']==3 || $helper_response['data']==2){
            return ['code'=>'1','message'=>$helper_response['message']];
        }        
        if($request->hasFile('image'))
         {
            //delete old files            
            if(Storage::disk('public')->exists('images/rooms/'.$rooms->image)){
               Storage::disk('public')->delete('images/rooms/'.$rooms->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'rooms').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/rooms',$file_name);
            $rooms->image  = explode ("/", $path)[3];
         }
        $rooms->room_type = $request->room_type;
        $rooms->total_room = $request->total_room;
        $rooms->occupants = $request->occupants;
        $rooms->show_online = $request->show_online;
        $rooms->view = $request->view;
        $rooms->size = $request->size;
        $rooms->bed = $request->bed;
        $rooms->description = $request->description;
        $rooms->facilities = (is_array($request->facilities)) ? serialize(array_filter($request->facilities)) : '';
        $rooms->status = $request->status;
        $rooms->save();
        return ['code'=>'0','message'=>'success'];
    }
    public function deletedata(Request $request, $id){
         $data = Rooms::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
         //calculate rooms data
         $old_total_rooms = $data->total_room;
         $room_setting = Room_Setting::first();
         $room_setting->alotted_room = ($room_setting->alotted_room-$old_total_rooms);
         $room_setting->remaining_room = ($room_setting->remaining_room+$old_total_rooms);
         $room_setting->save();
         //calculate rooms data
         if(is_object(Rooms_manage::where('room_id',$id)->first())){
            Rooms_manage::where('room_id',$id)->update(['deleted_at' => Carbon::now()]);
         }if(is_object(Room_gallery::where('room_id',$id)->first())){
            Room_gallery::where('room_id',$id)->update(['deleted_at' => Carbon::now()]);
         }if(is_object(Room_price::where('room_id',$id)->first())){
           Room_price::where('room_id',$id)->update(['deleted_at' => Carbon::now()]);
         }
         return redirect('admin/rooms')->with('success','Deleted sucessfully.');
    }
    public function deletegallery(Request $request, $id){      
         $data = Room_gallery::findOrFail($id);
         $data->deleted_at= Carbon::now();
         $data->save();
         //delete old files            
         /*if(Storage::disk('public')->exists('images/rooms/'.$data->image_name)){
           Storage::disk('public')->delete('images/rooms/'.$data->image_name);
         }*/
         //delete old files
         $request->session()->put('active_tab',5);
         return redirect('admin/rooms/update/'.$data->room_id)->with('success','Deleted sucessfully.');
    }

}