<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Bank_Transfer;
use App\Models\Sitecontent;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

class Sitecontent_Controller extends Controller
{
    //index
    public function listdata(){
        $list = Sitecontent::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.sitecontent.list',compact('list'));
    }
    public function createdata(){        
        return view('admin.sitecontent.create');
    }
    public function savedata(Request $request){
            $validated = Validator::make($request->all(),[            
            'menu_name' => 'max:200|unique:ehotel_sitecontent,menu_name,NULL,id,deleted_at,NULL',
            'title' => 'required|max:200',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image1' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $sitecontent = new Sitecontent();
        if($request->hasFile('image'))
        {
            $file_name = Helper::setUploadfileName($request,'sitecontent').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/sitecontent',$file_name);
            $sitecontent->image  = explode ("/", $path)[3];
        }
        if($request->hasFile('image1'))
        {
            $file_name = Helper::setUploadfileName($request,'sitecontent').'.'.$request->file('image1')->extension();
            $path = $request->file('image1')->storeAs('public/images/sitecontent',$file_name);
            $sitecontent->image1  = explode ("/", $path)[3];
        }
        $sitecontent->title = $request->title;
        $sitecontent->menu_name = $request->menu_name;
        $sitecontent->description = $request->description;
        $sitecontent->short_description = $request->short_description;
        $sitecontent->status = $request->status;
        $sitecontent->save();
        return redirect('admin/sitecontent')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
       //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Sitecontent::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }/*elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Sitecontent::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }*/
        return response()->json($res_data); exit;
    }
    public function updatedata($id){       
         $data = Sitecontent::findOrFail($id);
        return view('admin.sitecontent.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){    
            $validated = Validator::make($request->all(),[             
            'menu_name' => 'max:200|unique:ehotel_sitecontent,menu_name,'.$id.',id,deleted_at,NULL',
            'title' => 'required|max:200',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image1' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $sitecontent = Sitecontent::findOrFail($id);
        if($request->hasFile('image'))
        {   //delete old files            
            if(Storage::disk('public')->exists('images/sitecontent/'.$sitecontent->image)){
               Storage::disk('public')->delete('images/sitecontent/'.$sitecontent->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'sitecontent').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/sitecontent',$file_name);
            $sitecontent->image  = explode ("/", $path)[3];
        }
        if($request->hasFile('image1'))
        {  //delete old files            
            if(Storage::disk('public')->exists('images/sitecontent/'.$sitecontent->image1)){
               Storage::disk('public')->delete('images/sitecontent/'.$sitecontent->image1);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'sitecontent').'.'.$request->file('image1')->extension();
            $path = $request->file('image1')->storeAs('public/images/sitecontent',$file_name);
            $sitecontent->image1  = explode ("/", $path)[3];
        }
        $sitecontent->title = $request->title;
        $sitecontent->menu_name = $request->menu_name;
        $sitecontent->description = $request->description;
        $sitecontent->short_description = $request->short_description;
        $sitecontent->status = $request->status;
        $sitecontent->save();
        return redirect('admin/sitecontent')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
         $data = Sitecontent::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/sitecontent')->with('success','Deleted sucessfully.');
    }

}