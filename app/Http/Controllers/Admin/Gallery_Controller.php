<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Gallery;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class Gallery_Controller extends Controller
{
    public function listdata(){
        $list = Gallery::where('deleted_at',null)->orderBy('id','DESC')->get();
        return view('admin.gallery.list',compact('list'));
    }    
    public function createdata(){       
        return view('admin.gallery.create');
    }
    public function savedata(Request $request){        
        $validated = Validator::make($request->all(),[            
            'category_name' => 'max:200|unique:ehotel_gallery,category_name,NULL,id,deleted_at,NULL',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $gallery = new Gallery();
        $gallery->category_name = $request->category_name;
        $gallery->position = $request->position;
        $gallery->status = $request->status;
        $gallery->save();
        return redirect('admin/gallery')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Gallery::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        } elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Gallery::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
         $data = Gallery::findOrFail($id);
        return view('admin.gallery.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){        
            $validated = Validator::make($request->all(),[
            'category_name' => 'max:200|unique:ehotel_gallery,category_name,'.$id.',id,deleted_at,NULL',
            'status' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $gallery = Gallery::findOrFail($id);
        $gallery->category_name = $request->category_name;
        $gallery->position = $request->position;
        $gallery->status = $request->status;
        $gallery->save();
        return redirect('admin/gallery')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){       
         $data = Gallery::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/gallery')->with('success','Deleted sucessfully.');
    }

}