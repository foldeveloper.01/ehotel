<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\User;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use DB;
use App\Exports\GuestUsersExport;
use Excel;

class GuestUser_Controller extends Controller
{
    //index
    public function listdata() {
        //$list = User::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.guestuser.list');
    }
    public function createdata() {
        $country = Country::where('id',132)->get();     
        $state = State::where('country_id',132)->get();    
        //$city = City::where('state_id',1935)->get();
        return view('admin.guestuser.create',compact('country','state'));
    }
    public function savedata(Request $request) {    
        $validated = Validator::make($request->all(),[
            'ic_passport_no' => 'max:20|unique:ehotel_guest_user,ic_passport_no,NULL,id,deleted_at,NULL',
            'email' => 'nullable|email|max:100|unique:ehotel_guest_user,email,NULL,id,deleted_at,NULL',
            'name' => 'required|max:100',
            //'password' => 'required|min:8',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $User = new User();
        $User->name = $request->name;
        $User->email = $request->email;
        $User->ic_passport_no = $request->ic_passport_no;
        $User->password = bcrypt($this->rand_string(8));
        $User->company_name = $request->company_name;
        $User->contact_number = $request->contact_number;
        $User->vehicle_number = $request->vehicle_number;
        $User->address = $request->address;
        $User->country = $request->country;
        $User->state = $request->state;
        $User->city = $request->city;
        $User->postcode = $request->postcode;
        $User->remarks = $request->remarks;
        $User->status = $request->status;
        $User->save();
        return redirect('admin/guestuser')->with('success','Added sucessfully.');
    }

    public function rand_string( $length ) {
       $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
       return substr(str_shuffle($chars),0,$length);
    }
    
    public function ajax_get_city(Request $request){
       $city = City::where('state_id',$request->id)->get();
       return response()->json(['data' => $city]); exit;
    }
    public function updatedata($id){
        $data = User::findOrFail($id);
        $country = Country::where('id',$data->country)->get();
        //if($data->state!=null){ $state = $data->state;}else{ $state =$data->country;}
        //if($data->city!=null){ $city = $data->city;}else{ $city =$data->state;}
        $state = State::where('country_id',$data->country)->get();    
        $city = City::where('state_id',$data->state)->get();
        return view('admin.guestuser.update', compact('data', 'country', 'state', 'city'));
    }
    public function view_data($guest_id) {
        $data = User::findOrFail($guest_id);
        $country = Country::where('id',$data->country)->get();
        $state = State::where('country_id',$data->country)->get();    
        $city = City::where('state_id',$data->state)->get();
        return view('admin.guestuser.view', compact('data', 'country', 'state', 'city'));
    }
    public function dataupdate(Request $request, $id){
            $validated = Validator::make($request->all(),[
            'ic_passport_no' => 'max:20|unique:ehotel_guest_user,ic_passport_no,'.$id.',id,deleted_at,NULL',
            'email' => 'nullable|email|max:100|unique:ehotel_guest_user,email,'.$id.',id,deleted_at,NULL',
            'name' => 'required|max:100',
            //'password' => 'nullable|min:8',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $User = User::findOrFail($id);
        if($request->password!=''){
            $User->password = bcrypt($request->password);
        }
        $User->name = $request->name;
        $User->email = $request->email;
        $User->ic_passport_no = $request->ic_passport_no;
        $User->company_name = $request->company_name;
        $User->contact_number = $request->contact_number;
        $User->vehicle_number = $request->vehicle_number;
        $User->address = $request->address;
        $User->country = $request->country;
        $User->state = $request->state;
        $User->city = $request->city;
        $User->postcode = $request->postcode;
        $User->remarks = $request->remarks;
        $User->status = $request->status;
        $User->save();
        return redirect('admin/guestuser')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){        
         $data = User::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/guestuser')->with('success','Deleted sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            User::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }

    //index
    public function ajax_guestuser(Request $request){
    /*for ($i=1; $i < 20 ; $i++) { 
            $User = new User();
            $User->name = 'abcd'.$i;
            $User->email = 'abcd'.$i.'@gmail.com';
            $User->ic_passport_no = rand(10,1000).$i;
            $User->contact_number = rand(1000,10000).$i;
            $User->country = $i;
            $User->status = 0;
            $User->save();
        }  */    
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     // Total records
     //dd($columnSortOrder);
     $totalRecords = User::where('deleted_at',null)->select('count(*) as allcount')->count();
     $totalRecordswithFilter = User::join('ehotel_country','ehotel_country.id','=','ehotel_guest_user.country')
                                  ->where('ehotel_guest_user.deleted_at',null)
                                  ->where(function($query) use($searchValue) {
                                    $query->where('ehotel_guest_user.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.ic_passport_no', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.contact_number', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_country.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(ehotel_guest_user.created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })
                                  ->select('count(*) as allcount')
                                  ->count();
     // Fetch records
    $records = User::join('ehotel_country','ehotel_country.id','=','ehotel_guest_user.country')
                                  ->where('ehotel_guest_user.deleted_at',null)
                                  ->where(function($query) use($searchValue) {
                                    $query->where('ehotel_guest_user.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.ic_passport_no', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.contact_number', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_country.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(ehotel_guest_user.created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })
                                  ->select('ehotel_country.name AS country_name','ehotel_guest_user.*')
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();
     $data_arr = array();     
     foreach($records as $key=>$record){        
        $action = "<a href=".url('admin/guestuser/update/'.$record->id)." class='btn btn-sm btn-primary'>
                    <i class='fe fe-edit'></i>
                  </a>
                  <a onclick='deletefunction(event,$record->id);' href='' class='btn btn-sm btn-danger'>
                    <i class='fe fe-x'></i>
                  </a>
                  <form id='delete_form$record->id' method='POST' action=".url('admin/guestuser/delete', $record->id)."> <input type='hidden' name='_token' value='".csrf_token()."'> <input name='_method' type='hidden' value='POST'>
                  </form>";
        if($record->status=='0'){ $status_check ='checked'; }else{ $status_check ='notchecked'; }
        $status = "<div class='col-xl-2 ps-1 pe-1'>
                    <div class='form-group'>
                      <label class='custom-switch form-switch mb-0'>
                        <input type='checkbox' id='status_$record->id' name='status' class='custom-switch-input' $status_check> <span class='custom-switch-indicator custom-switch-indicator-md' data-on='Yes' data-off='No'></span>
                      </label>
                    </div>
                  </div>";
        $name = "<a href=".url('admin/guestuser/view/'.$record->id)." class='click_print custom_a'>".$record->name."                    
                  </a>";
        $data_arr[] = array(
          "id" => null,
          "name" => $name,
          "ic_passport_no" => $record->ic_passport_no,
          "contact_number" => $record->contact_number,
          "country" => @$record->country_name,
          "status" => $status,
          "created_at" => Carbon::parse($record->created_at)->format('d-m-Y H:i'),
          "action" => $action
        );
     }
     //dd($totalRecordswithFilter);
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }

    public function exportGuestuser(Request $request){
        return Excel::download(new GuestUsersExport, 'users'.date('d-m-Y_H:i:s').'.xlsx');
    }

}