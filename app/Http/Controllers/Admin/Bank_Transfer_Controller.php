<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Bank_Transfer;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

class Bank_Transfer_Controller extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $list = Bank_Transfer::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.bank_transfer.list',compact('list'));
    }
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        return view('admin.bank_transfer.create');
    }
    public function savedata(Request $request){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $validated = Validator::make($request->all(),[
            'account_name' => 'required|max:200',
            'bank_name' => 'max:200|unique:ehotel_bank_transfer,bank_name,NULL,id,deleted_at,NULL',
            'account_number' => 'required',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        //dd($request->all());
        $bank = new Bank_Transfer();
        if($request->hasFile('image'))
        {
            //$path = $request->file('image')->store('public/images/bank_transfer');
            $file_name = Helper::setUploadfileName($request,'banktransfer').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/bank_transfer',$file_name);
            $bank->image  = explode ("/", $path)[3];
        }
        $bank->bank_name = $request->bank_name;
        $bank->account_name = $request->account_name;
        $bank->account_number = $request->account_number;
        $bank->swiftcode = $request->swiftcode;
        $bank->waiting_days = $request->waiting_days;
        $bank->booking_automatically_deleted = $request->booking_automatically_deleted;
        $bank->status = $request->status;
        $bank->save();
        return redirect('admin/banktransfer')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
       //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Bank_Transfer::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Bank_Transfer::whereId($data[1])->update(['waiting_days'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Bank_Transfer::findOrFail($id);
        return view('admin.bank_transfer.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
        //dd($id);
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy         
            $validated = Validator::make($request->all(),[
             'account_name' => 'required|max:200',
            'bank_name' => 'max:200|unique:ehotel_bank_transfer,bank_name,'.$id.',id,deleted_at,NULL',
            'account_number' => 'required',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $bank = Bank_Transfer::findOrFail($id);
        if($request->hasFile('image'))
        {   
            //delete old files            
            if(Storage::disk('public')->exists('images/bank_transfer/'.$bank->image)){
               Storage::disk('public')->delete('images/bank_transfer/'.$bank->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'banktransfer').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/bank_transfer',$file_name);
            $bank->image  = explode ("/", $path)[3];
        }
        $bank->bank_name = $request->bank_name;
        $bank->account_name = $request->account_name;
        $bank->account_number = $request->account_number;
        $bank->swiftcode = $request->swiftcode;
        $bank->waiting_days = $request->waiting_days;
        $bank->booking_automatically_deleted = $request->booking_automatically_deleted;
        $bank->status = $request->status;
        $bank->save();
        return redirect('admin/banktransfer')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Bank_Transfer::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/banktransfer')->with('success','Deleted sucessfully.');
    }

}