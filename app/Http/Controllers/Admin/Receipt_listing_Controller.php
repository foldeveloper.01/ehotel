<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin,Admintypes,Sub_Gallery,Rooms,Room_Setting,Room_facilities,Room_gallery,Room_price,Rooms_manage,Add_on_product, User, Country, State,Room_booking, Room_booking_details, Room_booking_payment,Room_booking_receipt};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;
use Illuminate\Support\Str;
use DB;
class Receipt_listing_Controller extends Controller
{
    public function listdata(Request $request) {
    //dd($request->all());
    $list=Room_booking_receipt::join('ehotel_rooms_booking_payment','ehotel_rooms_booking_payment.id','=','ehotel_rooms_booking_receipt.payment_table_id')
                                ->join('ehotel_rooms_booking','ehotel_rooms_booking.id','=','ehotel_rooms_booking_payment.booking_id')
                                ->join('ehotel_admins','ehotel_admins.id','=','ehotel_rooms_booking.admin_user_id')
                                ->join('ehotel_guest_user','ehotel_guest_user.id','=','ehotel_rooms_booking.customer_id')
                                ->when($request->filled('min_date'), function ($query) use ($request) {
                                      $min_date = Carbon::parse($request->min_date)->format('Y-m-d');
                                      $max_date = Carbon::parse($request->max_date)->format('Y-m-d');
                                      $query->whereDate('ehotel_rooms_booking_receipt.created_at','>=',$min_date)
                                            ->whereDate('ehotel_rooms_booking_receipt.created_at','<=',$max_date);
                                 })
                                ->when($request->filled('payment_type'), function ($query) use ($request) {                                      
                                      $query->where('ehotel_rooms_booking_payment.payment_type',$request->payment_type);
                                 })
                                ->when($request->filled('issued_by'), function ($query) use ($request) {                                      
                                      $query->where('ehotel_rooms_booking.admin_user_id',$request->issued_by);
                                 })
                                ->when($request->filled('search'), function ($query) use ($request) {
                                      $query->where('ehotel_guest_user.name', 'LIKE', '%'.$request->search.'%')
                                          ->orWhere('ehotel_admins.username', 'LIKE', '%'.$request->search.'%')
                                          ->orWhere('ehotel_rooms_booking.booking_number', 'LIKE', '%'.$request->search.'%')
                                          ->orWhere('ehotel_rooms_booking_receipt.receipt_no', 'LIKE', '%'.$request->search.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(ehotel_rooms_booking_receipt.created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%')
                                          ->orWhere('ehotel_rooms_booking_receipt.receipt_amount', 'LIKE', '%'.$request->search.'%');
                                 })                                
                                ->select('ehotel_rooms_booking_receipt.id as receipt_table_id',
                                  'ehotel_rooms_booking.id as book_table_id',
                                  'ehotel_rooms_booking_payment.id as book_payment_id',
                                  'ehotel_rooms_booking_receipt.created_at as payment_created_at',
                                  'ehotel_rooms_booking_receipt.receipt_no as receipt_number',
                                  'ehotel_rooms_booking_payment.refund_payment_check as refund_payment_check',
                                  'ehotel_rooms_booking.booking_number as booking_number',
                                  'ehotel_guest_user.name as customer_name',
                                  'ehotel_rooms_booking_receipt.receipt_amount as receipt_amount',
                                  'ehotel_rooms_booking_payment.payment_type as payment',
                                  'ehotel_admins.username as issued_by')
                                ->orderBy('receipt_table_id','desc')
                                ->paginate(20)->withQueryString();
       $admin_list = Admin::where('admin_type_id',1)->where('deleted_at',null)->pluck('username','id');      
       return view('admin.receipt_listing.list',compact('list','admin_list'));
    }    

}