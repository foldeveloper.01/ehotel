<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Add_on_product;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class Add_on_product_Controller extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $list = Add_on_product::where('deleted_at',null)->orderBy('position', 'ASC')->get();
        return view('admin.add_on_product.list',compact('list'));
    }
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        return view('admin.add_on_product.create');
    }
    public function savedata(Request $request){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy         
            $validated = Validator::make($request->all(),[
            'add_on_name' => 'required|max:200',
            'add_on_code' => 'max:200|unique:ehotel_add_on_product,add_on_code,NULL,id,deleted_at,NULL',
            'add_on_amount' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }        
        $product = new Add_on_product();
        $product->add_on_name = $request->add_on_name;
        $product->add_on_code = $request->add_on_code;
        $product->add_on_amount = $request->add_on_amount;
        $product->status = $request->status;
        $product->service_charge_enable = $request->service_charge;
        $product->show_online_enable = $request->show_online;
        $product->save();
        return redirect('admin/addonproduct')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
       //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='showonline'){
            if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Add_on_product::whereId($data[1])->update(['show_online_enable'=>$value]);
            $res_data['data']='1';
            $res_data['type']=$data[0];
        } elseif($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Add_on_product::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
            $res_data['type']=$data[0];
        }elseif($data[0]=='service'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Add_on_product::whereId($data[1])->update(['service_charge_enable'=>$value]);
            $res_data['data']='1';
            $res_data['type']=$data[0];
        }elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Add_on_product::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Add_on_product::findOrFail($id);
        return view('admin.add_on_product.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
        //dd($request->all());
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
            $validated = Validator::make($request->all(),[
            'add_on_name' => 'required|max:200',
            'add_on_code' => 'max:200|unique:ehotel_add_on_product,add_on_code,'.$id.',id,deleted_at,NULL',
            'add_on_amount' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        
        $product = Add_on_product::findOrFail($id);
        $product->add_on_name = $request->add_on_name;
        $product->add_on_code = $request->add_on_code;
        $product->add_on_amount = $request->add_on_amount;
        $product->status = $request->status;
        $product->service_charge_enable = $request->service_charge;
        $product->show_online_enable = $request->show_online;
        $product->save();
        return redirect('admin/addonproduct')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Add_on_product::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/addonproduct')->with('success','Deleted sucessfully.');
    }

}