<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\
{
    Rooms_manage, Room_season_manage, Room_price, Room_gallery, Room_facilities, Room_Setting, Rooms, Sub_Gallery, Admintypes, Admin, Room_season_type, Room_season_bad_reserved
};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;

class Rooms_season_manage_Controller extends Controller
{
    public function listdata(Request $request, $id)
    {
        $room = Rooms::findOrFail($id);
        //dd($room->total_room);
        $list = Rooms_manage::where("room_id", $id)->where("deleted_at", null)
            ->orderBy("position", "DESC")
            ->get();
        $limit_rooms = $room->total_room;
        $get_id = [];
        $get_update_id = [];
        foreach ($list as $key => $data)
        {
            if ($key < $limit_rooms)
            {
                $get_id[] = $data->id;
            }
            else
            {
                $get_update_id[] = $data->id;
            }
        }
        Rooms_manage::whereIn("id", $get_id)->update(["status" => 0]);
        Rooms_manage::whereIn("id", $get_update_id)->update(["status" => 1, "position" => 0, ]);
        $room_setting = Room_Setting::first();
        $list = Rooms_manage::where("room_id", $id)->where("deleted_at", null)
            ->orderBy("position", "desc")
            ->get();
        return view("admin.rooms_manage.list", compact("list", "room_setting", "room"));
    }
    public function createdata(Request $request, $id)
    {
        $room = Rooms::findOrFail($id);
        $rooms_manage_count = Rooms_manage::where("room_id", $id)->where("deleted_at", null)
            ->orderBy("position", "DESC")
            ->count();
        $remaining_room = $room->total_room - $rooms_manage_count;
        return view("admin.rooms_manage.create", compact("remaining_room", "room", "rooms_manage_count"));
    }

    public function savedata(Request $request)
    {
        //dd($request->room_id);
        $validated = Validator::make($request->all() , ["name" => "max:200|unique:ehotel_rooms_number,room_no," . $request->room_id . ",id,deleted_at,NULL", "total_number" => "required", ]);
        if ($validated->fails())
        {
            return redirect()
                ->back()
                ->withErrors($validated)->withInput()
                ->with('error', implode(",", $validated->errors()->all()));
        }
        $start = $request->name;
        $limit = $request->total_number;
        for ($i = 0;$i < $limit;$i++)
        {
            $data = new Rooms_manage();
            $data->room_id = $request->room_id;
            $data->room_no = $start + $i;
            $data->status = 0;
            $data->save();
        }
        return redirect("admin/roomsmanage/" . $request->room_id)
            ->with("success", "Created sucessfully.");
    }

    public function ajax_status_update(Request $request)
    {
        $data = explode("_", $request->id);
        $res_data["data"] = "0";
        $res_data["message"] = "";
        $res_data["type"] = $data[0];
        if ($data[0] == "status")
        {
            //dd($data);
            if ($request->selectedData == "Yes")
            {
                $value = 0;
            }
            else
            {
                $value = 1;
            }
            Rooms_manage::whereId($data[1])->update(["status" => $value]);
            $res_data["data"] = "1";
        }
        elseif ($data[0] == "position")
        {
            if ($request->selectedData == null)
            {
                $value = 0;
            }
            else
            {
                $value = $request->selectedData;
            }
            //Rooms_manage::whereId($data[1])->update(['position'=>$value]);
            $roommanage = Rooms_manage::findOrFail($data[1]);
            $roommanage->position = $value;
            $roommanage->save();
            $room = Rooms::findOrFail($roommanage->room_id);
            $limit_rooms = $room->total_room;
            $get_all_data = Rooms_manage::where("room_id", $roommanage->room_id)
                ->orderBy("position", "desc")
                ->where("deleted_at", null)
                ->get();
            $get_id = [];
            $get_update_id = [];
            foreach ($get_all_data as $key => $data)
            {
                if ($key < $limit_rooms)
                {
                    $get_id[] = $data->id;
                }
                else
                {
                    $get_update_id[] = $data->id;
                }
            }
            Rooms_manage::whereIn("id", $get_id)->update(["status" => 0]);
            Rooms_manage::whereIn("id", $get_update_id)->update(["status" => 1, "position" => 0, ]);
            $res_data["data"] = "1";
        }
        elseif ($data[0] == "roomno")
        {
            if ($request->selectedData == null)
            {
                $value = 0;
            }
            else
            {
                $value = $request->selectedData;
            }
            $count = Rooms_manage::where("room_id", $data[1])->where("room_no", $value)->where("deleted_at", null)
                ->count();
            if ($count == 0)
            {
                Rooms_manage::whereId($data[1])->update(["room_no" => $value]);
                $res_data["data"] = "1";
            }
            else
            {
                $res_data["data"] = "1";
                $res_data["message"] = "room number already exists";
            }
        }
        elseif ($data[0] == "buildingname")
        {
            if ($request->selectedData == null)
            {
                $value = 0;
            }
            else
            {
                $value = $request->selectedData;
            }
            Rooms_manage::whereId($data[1])->update(["building_name" => $value, ]);
            $res_data["data"] = "1";
        }
        elseif ($data[0] == "floorname")
        {
            if ($request->selectedData == null)
            {
                $value = 0;
            }
            else
            {
                $value = $request->selectedData;
            }
            Rooms_manage::whereId($data[1])->update(["floor_name" => $value]);
            $res_data["data"] = "1";
        }
        elseif ($data[0] == "lockid")
        {
            if ($request->selectedData == null)
            {
                $value = 0;
            }
            else
            {
                $value = $request->selectedData;
            }
            Rooms_manage::whereId($data[1])->update(["lock_id" => $value]);
            $res_data["data"] = "1";
        }
        return response()->json($res_data);
        exit();
    }

    public function deletedata(Request $request, $id)
    {
        $data = Rooms_manage::findOrFail($id);
        $data->status = 1;
        $data->deleted_at = Carbon::now();
        $data->save();
        //delete bad/reserve
        Room_season_bad_reserved::where('room_number_id',$id)->delete();
        //delete bad/reserve
        return redirect("admin/roomsmanage/" . $data->room_id)
            ->with("success", "Deleted sucessfully.");
    }

    public function events_update_function($request, $rooms_id, $rooms_manage){
        // two dates between condition check
                $start_date = Carbon::createFromFormat("d/m/Y", $request->input("event-start-date"))->format('Y-m-d');
                $end_date = Carbon::createFromFormat("d/m/Y", $request->input("event-end-date"))->format('Y-m-d');
                $event_name = $request->input("event-name");
                Room_season_bad_reserved::findOrFail($request->input("event-index"))->delete();
                $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("start_date", "<=", $start_date)->whereDate("end_date", ">=", $end_date)->first();
                    if (is_object($dateCheck))
                    {
                        $newDate = [];
                        $dateCheck->start_date;
                        $dateCheck->end_date;
                        $both_check_condition = 0;
                        $betwen_check_condition = 0;
                        $j = 0;
                        //update if old start date match for new start and end date
                        if ($dateCheck->start_date == $start_date && $dateCheck->start_date == $end_date)
                        {
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        elseif ($dateCheck->end_date == $start_date && $dateCheck->end_date == $end_date)
                        {
                            //update if old end date match for new start and end date
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($dateCheck->end_date . "-1 day"));
                        }
                        elseif ($dateCheck->start_date == $start_date && $dateCheck->end_date == $end_date)
                        {
                            //update if old end date match for new start and end date
                            $both_check_condition = 1;
                            $dateCheck->title = $request->input("event-name");
                            $dateCheck->type = $request->input("season_type");
                            $dateCheck->save();
                        }
                        elseif ($dateCheck->start_date == $start_date)
                        {
                            // if choose old start date and new start date same,
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        elseif ($dateCheck->end_date == $end_date)
                        {
                            //old & new start date different but old & new end date same
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                        }
                        else
                        {
                            $betwen_check_condition = 1;
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $j++;
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        if ($both_check_condition == 0)
                        {
                            if ($both_check_condition == 0)
                            {
                                $dateCheck->start_date = $newDate[0]["start_date"];
                                $dateCheck->end_date = $newDate[0]["end_date"];
                                $dateCheck->save();
                            }
                            if ($betwen_check_condition == 1)
                            {
                                $newrow = new Room_season_bad_reserved();
                                $newrow->title = $dateCheck->title;
                                $newrow->start_date = $newDate[1]["start_date"];
                                $newrow->end_date = $newDate[1]["end_date"];
                                $newrow->manage_year = $dateCheck->manage_year;
                                $newrow->room_number_id = $rooms_id;
                                $newrow->type = $dateCheck->type;
                                $newrow->save();
                            }
                            //start new data create
                            $book = new Room_season_bad_reserved();
                            $book->title = $request->input("event-name");
                            $book->start_date = $start_date;
                            $book->end_date = $end_date;
                            $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                            $book->room_number_id = $rooms_id;
                            $book->type = $request->input("season_type");
                            $book->save();
                            //end new data create
                            
                        }
                        //end two date betwen condition
                        
                    }
                    else
                    {
                        //start&end date betwen date get
                        $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("start_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->get();
                        $get_old_id = [];
                        if (count($dateCheck))
                        {
                            //dd('222');
                            foreach ($dateCheck as $key => $val)
                            {
                                $get_old_id[] = $val->id;
                            }
                            Room_season_bad_reserved::whereIn("id", $get_old_id)->delete();
                        }
                        //start&end date betwen date get
                        //check end date betwen condition
                        $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("start_date", ">=", $start_date)->whereDate("start_date", "<=", $end_date)->orderBy("id", "desc")
                            ->first();
                        if (is_object($dateCheck))
                        {
                            //dd('333');
                            $dateCheck->start_date = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $dateCheck->save();
                        }
                        //check end date betwen condition
                        //check start date betwen condition
                        $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("end_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->orderBy("id", "desc")
                            ->first();
                        if (is_object($dateCheck))
                        {
                            //dd('444');
                            $dateCheck->end_date = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $dateCheck->save();
                        }
                        //check start date betwen condition
                        $book = new Room_season_bad_reserved();
                        $book->title = $request->input("event-name");
                        $book->start_date = $start_date;
                        $book->end_date = $end_date;
                        $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                        $book->room_number_id = $rooms_id;
                        $book->type = $request->input("season_type");
                        $book->save();
                    }
                    /*$message = ucwords($request->input("season_type")) . " updated sucessfully";*/
            return true;
    }
    public function list_room_number_data(Request $request, $rooms_id)
    {
        $rooms_manage = Rooms_manage::findOrFail($rooms_id);
        if ($request->get("season_year_update"))
        {
            $current_year = $request->get("season_year_update");
        }
        else
        {
            $current_year = date("Y");
        }
        if ($request->isMethod("post"))
        {
            if ($request->input("season_type"))
            {
                $start_date = Carbon::createFromFormat("d/m/Y", $request->input("event-start-date"))->format('Y-m-d');
                $end_date = Carbon::createFromFormat("d/m/Y", $request->input("event-end-date"))->format('Y-m-d');
                $event_name = $request->input("event-name");
                //start update data
                if ($request->input("event-index"))
                {
                    /*$book = Room_season_bad_reserved::findOrFail($request->input("event-index"));
                    $book->title = $request->input("event-name");
                    $book->start_date = $start_date;
                    $book->end_date = $end_date;
                    $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                    $book->room_number_id = $rooms_id;
                    $book->type = $request->input("season_type");
                    $book->save();
                    $message = "Updated sucessfully";*/
                    $this->events_update_function($request, $rooms_id, $rooms_manage);
                    $message = ucwords($request->input("season_type")) . " updated sucessfully";
                }
                else
                {
                    // two dates between condition check
                    $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("start_date", "<=", $start_date)->whereDate("end_date", ">=", $end_date)->first();
                    if (is_object($dateCheck))
                    {
                        $newDate = [];
                        $dateCheck->start_date;
                        $dateCheck->end_date;
                        $both_check_condition = 0;
                        $betwen_check_condition = 0;
                        $j = 0;
                        //update if old start date match for new start and end date
                        if ($dateCheck->start_date == $start_date && $dateCheck->start_date == $end_date)
                        {
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        elseif ($dateCheck->end_date == $start_date && $dateCheck->end_date == $end_date)
                        {
                            //update if old end date match for new start and end date
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($dateCheck->end_date . "-1 day"));
                        }
                        elseif ($dateCheck->start_date == $start_date && $dateCheck->end_date == $end_date)
                        {
                            //update if old end date match for new start and end date
                            $both_check_condition = 1;
                            $dateCheck->title = $request->input("event-name");
                            $dateCheck->type = $request->input("season_type");
                            $dateCheck->save();
                        }
                        elseif ($dateCheck->start_date == $start_date)
                        {
                            // if choose old start date and new start date same,
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        elseif ($dateCheck->end_date == $end_date)
                        {
                            //old & new start date different but old & new end date same
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                        }
                        else
                        {
                            $betwen_check_condition = 1;
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $j++;
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        if ($both_check_condition == 0)
                        {
                            if ($both_check_condition == 0)
                            {
                                $dateCheck->start_date = $newDate[0]["start_date"];
                                $dateCheck->end_date = $newDate[0]["end_date"];
                                $dateCheck->save();
                            }
                            if ($betwen_check_condition == 1)
                            {
                                $newrow = new Room_season_bad_reserved();
                                $newrow->title = $dateCheck->title;
                                $newrow->start_date = $newDate[1]["start_date"];
                                $newrow->end_date = $newDate[1]["end_date"];
                                $newrow->manage_year = $dateCheck->manage_year;
                                $newrow->room_number_id = $rooms_id;
                                $newrow->type = $dateCheck->type;
                                $newrow->save();
                            }
                            //start new data create
                            $book = new Room_season_bad_reserved();
                            $book->title = $request->input("event-name");
                            $book->start_date = $start_date;
                            $book->end_date = $end_date;
                            $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                            $book->room_number_id = $rooms_id;
                            $book->type = $request->input("season_type");
                            $book->save();
                            //end new data create
                            
                        }
                        //end two date betwen condition
                        
                    }
                    else
                    {
                        //dd($request->all());
                        //start&end date betwen date get
                        $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("start_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->get();
                        $get_old_id = [];
                        if (count($dateCheck))
                        {
                            //dd('222');
                            foreach ($dateCheck as $key => $val)
                            {
                                $get_old_id[] = $val->id;
                            }
                            Room_season_bad_reserved::whereIn("id", $get_old_id)->delete();
                        }
                        //start&end date betwen date get
                        //check end date betwen condition
                        $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("start_date", ">=", $start_date)->whereDate("start_date", "<=", $end_date)->orderBy("id", "desc")
                            ->first();
                        if (is_object($dateCheck))
                        {
                            //dd('333');
                            $dateCheck->start_date = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $dateCheck->save();
                        }
                        //check end date betwen condition
                        //check start date betwen condition
                        $dateCheck = Room_season_bad_reserved::where("room_number_id", $rooms_id)->whereDate("end_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->orderBy("id", "desc")
                            ->first();
                        if (is_object($dateCheck))
                        {
                            //dd('444');
                            $dateCheck->end_date = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $dateCheck->save();
                        }
                        //check start date betwen condition
                        $book = new Room_season_bad_reserved();
                        $book->title = $request->input("event-name");
                        $book->start_date = $start_date;
                        $book->end_date = $end_date;
                        $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                        $book->room_number_id = $rooms_id;
                        $book->type = $request->input("season_type");
                        $book->save();
                    }
                    $message = ucwords($request->input("season_type")) . " created sucessfully";
                }
                $current_year = Carbon::parse($start_date)->format("Y");
                return redirect()
                    ->back()
                    ->with("success", $message);
            }
        }
        else
        {
            $get_current_year_managedata = Room_season_bad_reserved::where("room_number_id", $rooms_id)->where("manage_year", "like", "%" . $current_year . "%")->get();
            $final_data = [];
            if (!$get_current_year_managedata->isEmpty())
            {
                foreach ($get_current_year_managedata as $key => $val)
                {
                    $final_data[$key]["id"] = $val->id;
                    $final_data[$key]["name"] = $val->title;
                    $final_data[$key]["type"] = $val->type;
                    $final_data[$key]["color"] = $val->type == "bad" ? "#e82646" : "#f7b731";
                    $final_data[$key]["startDate"] = Carbon::parse($val->start_date)
                        ->format("Y, m, d");
                    $final_data[$key]["endDate"] = Carbon::parse($val->end_date)
                        ->format("Y, m, d");
                }
            }
        }
        return view("admin.rooms_manage.rooms_number_list", compact("current_year", "final_data", "rooms_manage"));
    }
    public function removeEvent(Request $request)
    {
        $res_data["data"] = 0;
        if ($request->id)
        {
            $get_data = Room_season_bad_reserved::find($request->id);
            if (is_object($get_data))
            {
                $res_data["data"] = 1;
                $get_data->delete();
            }
        }
        return response()
            ->json($res_data);
        exit();
    }
}

