<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Sub_Gallery;
use App\Models\Gallery;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;

class SubGallery_Controller extends Controller
{
    public function listdata($id){
        $list = Sub_Gallery::whereIn('category_id',[$id])->where('deleted_at',null)->orderBy('id','DESC')->get();
        $category = Gallery::findOrFail($id);
        return view('admin.subgallery.list',compact('list','category'));
    }    
    public function createdata($id){
        $category = Gallery::findOrFail($id);
        return view('admin.subgallery.create',compact('category'));
    }
    public function savedata(Request $request,$id){
        $validated = Validator::make($request->all(),[
            'category_id' => 'exists:ehotel_gallery,'.$id,          
            'title' => 'max:200|unique:ehotel_subgallery,title,NULL,id,deleted_at,NULL',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $gallery = new Sub_Gallery();
        if($request->hasFile('image'))
         {
            $file_name = Helper::setUploadfileName($request,'subgallery').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/subgallery',$file_name);
            $gallery->image  = explode ("/", $path)[3];
         }
        $gallery->category_id = $id;
        $gallery->title = $request->title;
        $gallery->title1 = $request->title1;
        $gallery->status = $request->status;
        $gallery->position = $request->position;
        $gallery->save();
        return redirect('admin/gallery/'.$id.'/subgallery')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Sub_Gallery::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        } elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Sub_Gallery::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
         $data = Sub_Gallery::findOrFail($id);
         $category = Gallery::findOrFail($data->category_id);
        return view('admin.subgallery.update', compact('data','category'));
    }
    public function dataupdate(Request $request, $id){
            $validated = Validator::make($request->all(),[
            //'category_id' => 'exists:ehotel_gallery,'.$request->category_id.',id',          
            'title' => 'max:200|unique:ehotel_subgallery,title,'.$id.',id,deleted_at,NULL',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $gallery = Sub_Gallery::findOrFail($id);
        if($request->hasFile('image'))
         {  //delete old files            
            if(Storage::disk('public')->exists('images/subgallery/'.$gallery->image)){
               Storage::disk('public')->delete('images/subgallery/'.$gallery->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'subgallery').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/subgallery',$file_name);
            $gallery->image  = explode ("/", $path)[3];
         }
        $gallery->category_id = $request->category_id;
        $gallery->title = $request->title;
        $gallery->title1 = $request->title1;
        $gallery->status = $request->status;
        $gallery->position = $request->position;
        $gallery->save();
        return redirect('admin/gallery/'.$request->category_id.'/subgallery')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){        
         $data = Sub_Gallery::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
         return redirect('admin/gallery/'.$data->category_id.'/subgallery')->with('success','Deleted sucessfully.');
    }

}