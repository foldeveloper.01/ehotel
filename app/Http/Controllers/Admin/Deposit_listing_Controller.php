<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin,Admintypes,Sub_Gallery,Rooms,Room_Setting,Room_facilities,Room_gallery,Room_price,Rooms_manage,Add_on_product, User, Country, State,Room_booking, Room_booking_details, Room_booking_payment,Room_deposit_remark};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;
use Illuminate\Support\Str;
use DB;
class Deposit_listing_Controller extends Controller
{
    public function listdata(Request $request){    
    if($request->search !=''|| $request->deposite_status !='') {
            $searchValue = $request->search;           
            $list = Room_booking::with('guest')->when($request->deposite_status==1, function ($query) use ($request) {                                
                               $query->where('deposit', 'yes')->where('deposit_refund', 'no');
                            })->when($request->deposite_status==2, function ($query) use ($request,) {                                
                               $query->where('deposit', 'yes')->where('deposit_refund', 'yes');
                            })
                            ->whereHas('booking_details.rooms_number', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('room_no', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->orWhereHas('guest', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('name', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('ic_passport_no', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('contact_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('booking_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('deposit_amount', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere(DB::raw("(DATE_FORMAT(check_in,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%')
                                    ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%')                                    
                                    ;
                               });
                            })                            
                            ->where('deleted_at',null)->orderBy('id','desc')
                            ->paginate(20)->withQueryString();
        }else {
                $list = Room_booking::where('deposit','yes')->where('deleted_at',null)
                           ->orderBy('updated_at','desc')                            
                           ->paginate(20);
        }

    return view('admin.deposit_listing.list',compact('list'));
    }
    public function view_on_receipt($booking_id){
         $receipt = Room_booking::findOrFail($booking_id);
         return view('admin.deposit_listing.view_on_receipt',compact('receipt'));
    }
    public function get_deposit_remark(Request $request){
        $value = [];
        if($request->id){
           $finalData=[];
           $data=Room_booking::findOrFail($request->id);
           $value['booking_id']=$data->booking_number;
           $value['guest_name']=@$data->guest->name;
           $value['guest_email']=@$data->guest->email;
           $value['guest_number']=@$data->guest->contact_number;
           $value['amount']=$data->total_amount;
           $value['booking_date']=Carbon::parse($data->check_in)->format('d-m-Y');          
           $getolddata= Room_deposit_remark::where('room_booking_id',$request->id)->select('id','room_booking_id','remarks','admin_user_id')->get();
           if(count($getolddata)!=0){              
              foreach($getolddata as $key=>$val){
                $finalData[$key]['id']= $val->id;
                $finalData[$key]['remarks']= $val->remarks;
                $finalData[$key]['created_at']= Carbon::parse($val->created_at)->format('d-m-Y g:i A');
                $finalData[$key]['admin_user_id']= $val->guest->username.'('.$val->guest->email.')';                
              }
              $value['tablerow']=$finalData;
           }          
        }
       return response()->json($value); exit;
     }
     public function create_deposit_remark(Request $request){
        if($request->remark!=''){
            //dd($request->all());
            $data = new Room_deposit_remark();
            $data->room_booking_id = $request->bookingid;
            $data->remarks = $request->remark;
            $data->admin_user_id = Auth::guard('admin')->user()->id;
            $data->save();
            return redirect()->back()->with('success','Remark updated for Booking No : '.@$request->booking_no.'');           
        }
           return redirect()->back()->with('error','Please check inputs.');
     }
     public function remove_deposit_remark(Request $request){
        $response=0;
        if($request->id){
           Room_deposit_remark::findOrFail($request->id)->delete();
           $response=1;
        }
       return response()->json($response); exit;
     }
     public function deposit_refund(Request $request, $type, $bookingid){        
        if($type !='' && $bookingid !=''){
            $data=Room_booking::findOrFail($bookingid);
            if($type=='refund'){
              $data->deposit_refund = 'yes';
              $data->deposit_refund_amount = $data->deposit_amount;
              $message = 'Deposit for '.$data->guest->name.' has been Refunded';
            }else{
              $data->deposit_refund = 'no';
              $data->deposit_refund_amount =0;
              $message = 'Deposit for '.$data->guest->name.' has not been Refunded';
            }
            $data->save();                       
        }
        return redirect()->back()->with('success',$message);
     }

}