<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Booking_windows;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class Booking_Windows_Controller extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $list = Booking_windows::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.booking_windows.list',compact('list'));
    }
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        return view('admin.booking_windows.create');
    }
    public function savedata(Request $request){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $validated = Validator::make($request->all(),[            
            'website_url' => 'max:200|unique:ehotel_booking_windows,website_url,NULL,id,deleted_at,NULL',
            'status' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        //dd($request->all());
        $booking = new Booking_windows();
        $booking->website_url = $request->website_url;
        $booking->status = $request->status;
        $booking->save();
        return redirect('admin/bookingwindows')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Booking_windows::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Booking_windows::findOrFail($id);
        return view('admin.booking_windows.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy         
            $validated = Validator::make($request->all(),[
            'website_url' => 'max:200|unique:ehotel_booking_windows,website_url,'.$id.',id,deleted_at,NULL',
            'status' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $booking = Booking_windows::findOrFail($id);
        $booking->website_url = $request->website_url;
        $booking->status = $request->status;
        $booking->save();
        return redirect('admin/bookingwindows')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Booking_windows::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/bookingwindows')->with('success','Deleted sucessfully.');
    }

}