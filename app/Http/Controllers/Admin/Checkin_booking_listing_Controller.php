<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin,Admintypes,Sub_Gallery,Rooms,Room_Setting,Room_facilities,Room_gallery,Room_price,Rooms_manage,Add_on_product, User, Country, State,Room_booking, Room_booking_details, Room_booking_payment,Room_booking_addon,Housekeeping_list};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;
use Illuminate\Support\Str;

class Checkin_booking_listing_Controller extends Controller
{
    public function listdata(Request $request) {
        if($request->search !=''|| $request->booking_status !='' || $request->payment !='') {
            $searchValue = $request->search;
            $booking_status = [0,1];
            $payment_status = [1,2,3,4];            
            $todayCheck_in='';
                if($request->booking_status==1) {
                    $booking_status = [1];
                }elseif($request->booking_status==2) {
                    $booking_status = [0,1];
                    $todayCheck_in = Carbon::today();
                }
            //booking status
           

            //dd($booking_status);
            $list = Room_booking::whereIn('booking_status',$booking_status)
                            ->whereIn('payment_status',$payment_status)
                            ->when($request->booking_status==2, function ($query) use ($request,$todayCheck_in) {
                               $query->whereDate('check_in', $todayCheck_in);
                            })
                            ->whereHas('booking_details.rooms_number', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('room_no', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->orWhereHas('guest', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('name', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('ic_passport_no', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('contact_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('booking_number', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->where('deleted_at',null)->orderBy('id','desc')
                            ->paginate(20)->withQueryString();
        } else {
        $list=Room_booking::whereIn('booking_status',[0,1])
                           ->WhereIn('payment_status',[1,2,3,4])                           
                           ->where('deleted_at',null)
                           ->orderBy('id','desc')
                           ->paginate(20);
        }
        return view('admin.checkin_booking_list.list',compact('list'));
    }
    public function remove_due_booking(Request $request){
       Room_booking::whereIn('id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_details::whereIn('booking_id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_addon::whereIn('booking_id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       return ['response'=>0,'message'=>'success'];
    }
    public function check_housekeeping_room(Request $request){
       $booking_details =  Room_booking_details::whereIn('id', $request->id)->pluck('room_number_id','room_type_id');
       $store_id=[];
       foreach($booking_details as $key=>$data){
         $check_room = Housekeeping_list::where('room_type_id',$key)->where('room_number_id',$data)->where('status','!=','completed')->count();
         if($check_room!=0){
            $store_id[]=$data;
         }
         //completed
       }
       $response=0;
       $message='';
       if(count($store_id)){
        $response=1;
        $message='Room number '.implode(',',$store_id).' is under housekeeping. Please release before check-in.';      
       }
       return ['response'=>$response,'message'=>$message];
    }
    public function checkin_confirm_view($bookingid){         
         $booking = Room_booking::findOrFail($bookingid);
         if(count($booking->get_booking_details()->where('checkin_status',0))==0) {
            return redirect('admin/checkinlisting')->with('error','Already Check-In'); 
         }
         return view('admin.room_booking.checkin_confirm_view',compact('booking'));
     }
     public function checkin_confirm_save(Request $request,$bookingid){
         //dd($request->all());
         $updatedID=[];$updatedRoom=[];
         foreach($request->checkin_checkbox as $key=>$val){            
            $roomnumber_key = array_search ($key, $request->room_number);
            $details_id = $request->booking_details_ids[$roomnumber_key];
            $updatedRoom[]=$key;
            $updatedID[]=$details_id;
         }
         $numberOfCheckin_count = Room_booking_details::where('booking_id',$bookingid)->where('checkin_status',0)->where('deleted_at',null)->count();
         $booking = Room_booking_details::whereIn('id',$updatedID)->update(['checkin_status'=>1,'checkin_date_time'=>Carbon::now()]);
         if($request->collect_deposit==1){
            $booking_data = Room_booking::findOrFail($bookingid);            
                $deposit_payment_status=$booking_data->deposit_payment_status;
             if($booking_data->deposit=='no'){
                $deposit_payment_status=$request->payment_type;
               }
               if(!is_numeric($deposit_payment_status)) {
                 $deposit_payment_status = $request->payment_type;
               }               
               $booking_data->deposit = 'yes';
               $booking_data->deposit_amount = $request->deposit_amount;
               $booking_data->deposit_payment_status = $deposit_payment_status;
                //update booking status
                 //if($numberOfCheckin_count==count($updatedID)){                   
                   $booking_data->booking_status = 1;                  
                 //}
                 //update booking status
               $booking_data->save();            
         }
        if($numberOfCheckin_count==count($updatedID)){          
              return redirect('admin/checkoutlisting')->with('success','Check-in sucessfully for this rooms '.implode(',',$updatedRoom).'');
        }
        return redirect('admin/checkinlisting')->with('success','Check-in sucessfully for this rooms '.implode(',',$updatedRoom).'');  
     }

}