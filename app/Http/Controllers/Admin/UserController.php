<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\DomainTypes;
use App\Models\Country;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Notifications\UserPasswordEmailNotification;
use App\Models\SmtpSettings;
use Helper;

class UserController extends Controller
{
    public function userslist(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Admin::class);
        //policy
        $userlist = Admin::where('deleted_at',null)->orderBy('id','desc')->get();
        return view('admin.admin.lists',compact('userlist'));
    }
    public function createuser(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Admin::class);
        //policy
        $admintypes =  Admintypes::select('id','type')->where('status','0')->get();
        return view('admin.admin.create',compact('admintypes'));
    }
    public function updateuser($id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy
        $getuserData = Admin::findOrFail($id);
        $admintypes =  Admintypes::select('id','type')->where('status','0')->get();
        $country =  Country::select('id','name')->get();
        return view('admin.admin.update',compact('admintypes','getuserData','country'));
    }
    public function viewprofile($id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy
        $getuserData = Admin::findOrFail($id);
        $admintypes =  Admintypes::select('id','type')->where('status','0')->get();
        $country =  Country::select('id','name')->get();
        return view('admin.admin.profileupdate',compact('admintypes','getuserData','country'));
    }     

    public function viewuser($id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Admin::class);
        //policy        
        $userData = Admin::findOrFail($id);
        return view('admin.admin.view',compact('userData'));
    } 
    public function deleteuser(Request $request, $id){
         //policy
         $user = auth()->guard('admin')->user();
         $user->can('delete', Admin::class);
        //policy
        $getuserData = Admin::findOrFail($id);
        $getuserData->status = 1;
        $getuserData->deleted_at = Carbon::now();
        $getuserData->save();
        return redirect('admin/admins')->with('success','Deleted sucessfully.');
    }
    //create user
    public function saveuser(Request $request)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('create', Admin::class);
        //policy
        $validated = Validator::make($request->all(),[
        'firstname' => 'required|max:100',
        'username' => 'unique:ehotel_admins,username,NULL,id,deleted_at,NULL',
        'email' => 'email|unique:ehotel_admins,email,NULL,id,deleted_at,NULL',
        'usertype' => 'required',
        'status' => 'required',
        'password' => 'required',
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        //create data        
        $user = new Admin();
        $user->firstname = $request->firstname;
        $user->username = $request->username;
        $user->phone_number    = ($request->phone_number) ? $request->phone_number :'';
        $user->email     = $request->email;
        $user->password  = bcrypt($request->password);
        $user->admin_type_id  = $request->usertype;
        $user->status  = '1';
        $user->save();
        //$user->passwords = $request->password;
        //mail sending to user
        //$user->notify(new UserPasswordEmailNotification($user));
        return redirect('admin/admins')->with('success','Added sucessfully.');
    }
    //create user
    public function edituser(Request $request, $id)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy         
        $validated = Validator::make($request->all(),[
        'firstname' => 'required|max:100',
        'username' => 'unique:ehotel_admins,username,'.$id.',id,deleted_at,NULL',
        'email' => 'email|unique:ehotel_admins,email,'.$id.',id,deleted_at,NULL',
        'usertype' => 'required',
        'status' => 'required',
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        
        //update data
        $user = Admin::find($id);
        if(isset($request->password) && $request->password!=''){ $user->password = bcrypt($request->password); }
        $user->firstname = $request->firstname;
        $user->username = $request->username;
        $user->phone_number    = ($request->phone_number) ? $request->phone_number :'';
        $user->email     = $request->email;
        $user->admin_type_id  = $request->usertype;
        $user->status  = $request->status;
        $user->save();
        return redirect()->back()->with('success','Updated sucessfully.');
    }

    public function updateprofile(Request $request, $id)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy         
        $validated = Validator::make($request->all(),[        
        'username' => 'unique:ehotel_admins,username,'.$id.',id,deleted_at,NULL',
        'email' => 'email|unique:ehotel_admins,email,'.$id.',id,deleted_at,NULL',
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        
        //update data
        $user = Admin::find($id);
        if(isset($request->password) && $request->password!=''){ $user->password = bcrypt($request->password); }        
        if(isset($request->officer_password) && $request->officer_password!=''){ $user->officer_password = bcrypt($request->officer_password); }        
        $user->username = $request->username;
        $user->phone_number    = ($request->phone_number) ? $request->phone_number :'';
        $user->email     = $request->email;
        $user->save();
        return redirect()->back()->with('success','Updated sucessfully.');
    }

     public function myaccountupdate(Request $request, $id)
    {    //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy
        $validated = Validator::make($request->all(),[
        'name' => 'required|max:100',
        'address' => 'required|max:100',
        'phone_number' => 'required|max:100',
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        
        //update data
        $user = Admin::find($id);
        $user->firstname = $request->name;
        $user->address    = $request->address;
        $user->phone_number     = $request->phone_number;
        $user->organization  = $request->organization;
        $user->additional_details  = $request->additional_details;
        $user->save();
        return redirect()->back()->with('success','Updated sucessfully.');
    }

    public function officerUserpassword(Request $request, $id)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy
         $validated = Validator::make($request->all(),[
        'password' => 'required|max:100',
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Please check errors');
        }
        $user = Admin::find($id);
        $user->officer_password  = bcrypt($request->password);
        $user->save();        
        return redirect('admin/admins')->with('success','Updated sucessfully.');
    }

    public function ResetUserPasswordToMail(Request $request, $id)
    {

    //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy
        $check_smtp = Helper::getSMTPdata();
        if($check_smtp==0){
           return redirect('admin/admins')->with('error','Please check smtp configuration');
        }
        $password = $this->randomPassword();
        $user = Admin::find($id);
        $user->password  = bcrypt($password);
        $user->save();
        $user->notify(new UserPasswordEmailNotification($user,$password));
        return redirect('admin/admins')->with('success','Mail sent sucessfully.');
    }

    public function ResetUserPasswordToMail1(Request $request, $id)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Admin::class);
        //policy
        $check_smtp = Helper::getSMTPdata();
        if($check_smtp==0){
           return redirect()->back()->with('error','Please check smtp configuration');
        }
        $password = $this->randomPassword();
        $user = Admin::find($id);
        $user->password  = bcrypt($password);
        $user->save();
        $user->notify(new UserPasswordEmailNotification($user,$password));
        return redirect()->back()->with('success','Mail sent sucessfully.');
    }

    function randomPassword() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
        //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?"; 
        $length = rand(10, 16); 
        $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, 8 );
         return $password;
    }

    public function checkCurrentPassword($id,$password){
        $data['data'] = Admin::find($id);
        $data['data'] = Hash::check($password, $data['data']->password);
        return response()->json($data); exit;
    }
    public function ajax_status_update(Request $request){
       //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Admin::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
            $res_data['type']=$data[0];
        }
        return response()->json($res_data); exit;
    }

}