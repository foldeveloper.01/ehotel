<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin,Admintypes,User,Country,State,City,Invoice,Room_booking,Room_invoice_remark,Room_booking_details,Room_booking_addon};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use DB;
use App\Exports\GuestUsersExport;
use Excel;

class Invoice_Controller extends Controller
{
    //index
    public function listdata(Request $request) {
        //dd($request->all());
        $invoice_status='paid';
        if($request->status==2){
         $invoice_status='void';
        }
        //dd($invoice_status);
        $list = Invoice::join('ehotel_rooms_booking','ehotel_rooms_booking.id','=','ehotel_rooms_invoice.booking_id')
                                  ->where('ehotel_rooms_invoice.invoice_status',$invoice_status)
                                  ->whereHas('booking_details', function ($query) use ($request) {
                                    $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('booking_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere(DB::raw("(DATE_FORMAT(check_in,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%')
                                    ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%');
                                     });
                                   })
                                  ->orWhereHas('receipt', function ($query) use ($request) {
                                    $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('receipt_amount', 'LIKE', '%'.$request->search.'%');
                                     });
                                   })
                                  ->orWhereHas('guest', function ($query) use ($request) {
                                    $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('name', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('ic_passport_no', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('contact_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('invoice_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere(DB::raw("(DATE_FORMAT(invoice_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%')
                                    /*->orWhere('deposit_amount', 'LIKE', '%'.$request->search.'%')                                    
                                    ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$request->search.'%')*/      
                                    ;
                                      });
                                   })
                                  ->orderBy('ehotel_rooms_invoice.invoice_id','desc')
                                  ->paginate(20)->withQueryString();
        return view('admin.invoice.paidlist',compact('list'));
    }
    //index
    public function ajax_data(Request $request){
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     // Total records
     //dd($columnSortOrder);
     $totalRecords = Invoice::where('invoice_status',0)->select('count(*) as allcount')->count();
     $totalRecordswithFilter = Invoice::join('ehotel_rooms_booking','ehotel_rooms_booking.id','=','ehotel_rooms_invoice.booking_id')
                                  ->where('ehotel_rooms_invoice.invoice_status',0)
                                  /*->where(function($query) use($searchValue) {
                                    $query->where('ehotel_guest_user.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.ic_passport_no', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.contact_number', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_country.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(ehotel_guest_user.created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })*/
                                  ->select('count(*) as allcount')
                                  ->count();
     // Fetch records
    $records = Invoice::join('ehotel_rooms_booking','ehotel_rooms_booking.id','=','ehotel_rooms_invoice.booking_id')
                                  ->where('ehotel_rooms_invoice.invoice_status',0)
                                  /*->where(function($query) use($searchValue) {
                                    $query->where('ehotel_guest_user.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.ic_passport_no', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_guest_user.contact_number', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('ehotel_country.name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(ehotel_guest_user.created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })*/
                                  //->select('ehotel_country.name AS country_name','ehotel_guest_user.*')
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();
     $data_arr = array();     
     foreach($records as $key=>$record){
        /*$action = "<a href=".url('admin/guestuser/update/'.$record->id)." class='btn btn-sm btn-primary'>
                    <i class='fe fe-edit'></i>
                  </a>
                  <a onclick='deletefunction(event,$record->id);' href='' class='btn btn-sm btn-danger'>
                    <i class='fe fe-x'></i>
                  </a>
                  <form id='delete_form$record->id' method='POST' action=".url('admin/guestuser/delete', $record->id)."> <input type='hidden' name='_token' value='".csrf_token()."'> <input name='_method' type='hidden' value='POST'>
                  </form>";*/
        /*if($record->status=='0'){ $status_check ='checked'; }else{ $status_check ='notchecked'; }*/
        $status = "<button id=".@$record->id." class='btn btn-success bg-success-gradient btn-sm click_status' data-bs-toggle='modal' data-bs-target='#scrollingmodal'>".ucwords(@$record->invoice_status)."</button>";
        $invoice_number = " <a target='_blank' href='".url('admin/paidinvoice/print_invoice/'.@$record->id)."' class='text-blue click_view'>".@$record->invoice_number."</a>";
        $booking_number = " <a target='_blank' href='".url('admin/paidinvoice/print_invoice/'.@$record->id)."' class='text-blue click_view'>".@$record->booking_number."</a>";
        $data_arr[] = array(
          "id" => @$record->id,
          "invoice_date" => Carbon::parse(@$record->invoice_date)->format('d-m-Y g:i A'),
          "invoice_number" => $invoice_number,
          "booking_number" => @$booking_number,
          "guest" => @$record->guest->name.'<br>'.@$record->guest->ic_passport_no.'<br>'.@$record->guest->contact_number,
          "check_in" => Carbon::parse(@$record->check_in)->format('d-m-Y'),
          "check_out" => Carbon::parse(@$record->check_out)->format('d-m-Y'),
          "total_amount" => @$record->total_amount,
          "invoice_status" => $status,
          "action" => 'action'
        );
     }
     //dd($totalRecordswithFilter);
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }

    public function get_invoice_remark(Request $request){
        $value = [];
        if($request->id){
           $finalData=[];
           $data=Room_booking::findOrFail($request->id);
           $value['booking_id']=$data->booking_number;
           $value['guest_name']=@$data->guest->name;
           $value['guest_email']=@$data->guest->email;
           $value['guest_number']=@$data->guest->contact_number;
           $value['amount']=$data->total_amount;
           $value['booking_date']=Carbon::parse($data->check_in)->format('d-m-Y');          
           $getolddata= Room_invoice_remark::where('room_booking_id',$request->id)->select('id','room_booking_id','remarks','admin_user_id')->get();
           if(count($getolddata)!=0){              
              foreach($getolddata as $key=>$val){
                $finalData[$key]['id']= $val->id;
                $finalData[$key]['remarks']= $val->remarks;
                $finalData[$key]['created_at']= Carbon::parse($val->created_at)->format('d-m-Y g:i A');
                $finalData[$key]['admin_user_id']= $val->guest->username.'('.$val->guest->email.')';                
              }
              $value['tablerow']=$finalData;
           }          
        }
       return response()->json($value); exit;
     }
     public function create_invoice_remark(Request $request){     
        if($request->status!='' && $request->remark!='') {            
            if($request->status!='') {             
             $status = ($request->status==1) ? 'void' : 'paid';             
             Invoice::whereIn('invoice_id',[$request->invoice_table_id])->update(['invoice_status'=>$status]);
            }            
            $data = new Room_invoice_remark();
            $data->room_booking_id = $request->bookingid;
            $data->remarks = $request->remark;
            $data->admin_user_id = Auth::guard('admin')->user()->id;
            $data->save();
            return redirect()->back()->with('success','Remark updated for Booking No : '.@$request->booking_no.'');
        }        
        return redirect()->back()->with('error','Please check inputs.');
     } 

     public function print_invoice(Request $request,$invoice_id){
         $invoice = Invoice::where('invoice_id',$invoice_id)->first();
         $bookingid = $invoice->booking_id;
         $booking = Room_booking::findOrFail($bookingid);
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');
         $total_amount = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount)+$room_booking_addon;         
         $addon_details_sum_tax = ($room_booking_details->sum('subtotal_amount')+$room_booking_addon)+$booking->service_tax_amount;
         $balance_amount = number_format($booking->total_amount-$addon_details_sum_tax,2);
         return view('admin.invoice.invoice',compact('booking','invoice','balance_amount','total_amount'));
     } 

     public function remove_invoice_remark(Request $request){
        $response=0;
        if($request->id){
           Room_invoice_remark::findOrFail($request->id)->delete();
           $response=1;
        }
       return response()->json($response); exit;
     }

    public function invoice_edit($invoice_id){       
       $invoice = Invoice::where('invoice_id',$invoice_id)->first();       
       $booking = $invoice->booking_details;
       return view('admin.invoice.invoice_edit',compact('invoice','booking'));
    }
    public function invoice_save(Request $request) {       
       foreach($request->roomlist as $key=>$val){
           $data = Room_booking_details::findOrFail($key);
           $data->room_number_id = $request->roomlist[$key];
           $data->check_in = Carbon::parse($request->checkin_date[$key])->format('Y-m-d');
           $data->check_out = Carbon::parse($request->checkout_date[$key])->format('Y-m-d');
           $data->room_price = $request->room_price[$key];
           $data->subtotal_amount = $request->subtotal_amount[$key];
           $data->save();           
       }       
       return redirect()->back()->with('success','Successfully Updated');
    }
    public function invoice_booking_edit($invoice_id) {       
       $invoice = Invoice::where('invoice_id',$invoice_id)->first();       
       $booking = $invoice->booking_details;       
       return view('admin.invoice.booking_edit',compact('invoice','booking'));
    }
    public function invoice_booking_save(Request $request) {
       foreach($request->roomlist as $key=>$val) {
           $data = Room_booking_details::findOrFail($key);
           $data->room_number_id = $request->roomlist[$key];
           $data->check_in = Carbon::parse($request->checkin_date[$key])->format('Y-m-d');
           $data->check_out = Carbon::parse($request->checkout_date[$key])->format('Y-m-d');
           $data->room_price = $request->room_price[$key];
           $data->subtotal_amount = $request->subtotal_amount[$key];
           $data->save();           
       }
       return redirect()->back()->with('success','Successfully Updated');
    }


}