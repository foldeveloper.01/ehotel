<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Housekeeping_list,Room_booking,Room_booking_payment,Room_booking_addon,Room_booking_details,Add_on_product,Admintypes,Admin};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use DB;

class Housekeepinglist_Controller extends Controller
{
    //index
    public function listdata(Request $request){
        if ($request->isMethod('post')) {
                if(!$request->status){
                $check = Housekeeping_list::findOrFail(explode('/',$request->id)[0]);
                if($check->user_id==0){
                    $check->user_id = $request->user_id;
                    $check->status = 'cleaning';
                    $check->save();
                    return redirect('admin/housekeepinglist')->with('success','Added sucessfully.');
                }elseif (isset(explode('/',$request->id)[1])) {
                    if(Auth()->guard('admin')->user()->id!=$request->user_id){
                       $check->user_id = $request->user_id;
                       $check->save();
                    }else{
                      return redirect('admin/housekeepinglist')->with('error','Some one is taking this one.');
                    }                    
                }
                else{
                    return redirect('admin/housekeepinglist')->with('error','Some one is taking this one.');
                }
            }else{
                 $check = Housekeeping_list::findOrFail($request->status_id);
                 if($request->status=='Cleaning'){
                    $status = 'completed';
                 }elseif($request->status=='Completed'){
                    $status = 'cleaning';
                 }if($check->status=='cleaning'){
                    $check->status = $status;
                 }elseif($check->status=='completed'){
                    $check->status = $status;
                 }                
                 $check->release_date=Carbon::now();
                 $check->save();
                 return redirect('admin/housekeepinglist')->with('success','House Keeping done for Room Number '.$check->getRoomData->room_no.'');
            }
         }
        $housekeeping_user_list = Admin::where('admin_type_id',2)->where('status',0)->where('deleted_at',null)->get();
        return view('admin.housekeepinglist.list',compact('housekeeping_user_list'));
    } 

    //index
    public function getdataforajax(Request $request){
        //houskeeper login
     if(Auth()->guard('admin')->user()->admin_type_id==2){
        $this->housekeeperdata($request);
     }
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value

     $totalRecords = Housekeeping_list::select('count(*) as allcount')->count();
     $totalRecordswithFilter = Housekeeping_list::whereHas('booking_data', function($q){
                                        $q->where('deleted_at',null);
                                    })
                                    ->where(function($query) use($searchValue) {
                                    $query->where('booking_number', 'LIKE', '%'.$searchValue.'%')
                                          /*->orWhere('room_number_id','LIKE', '%'.$searchValue.'%')*/
                                          ->orWhere('status','LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_in,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(release_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhereHas('getRoomData', function ($query) use ($searchValue) {
                                                        $query->where('room_no', 'like', '%'.$searchValue.'%');
                                            })->orWhereHas('getRoomData', function ($query) use ($searchValue) {
                                                        $query->where('room_no', 'like', '%'.$searchValue.'%');
                                            })
                                          ->orWhereHas('getUserData', function ($query) use ($searchValue) {
                                                $query->where('username', 'like', '%'.$searchValue.'%');
                                            });
                                    })
                                  ->select('count(*) as allcount')
                                  ->count();
     // Fetch records
    $records = Housekeeping_list::whereHas('booking_data', function($q){
                                        $q->where('deleted_at',null);
                                    })
                                    ->where(function($query) use($searchValue) {
                                    $query->where('booking_number', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('room_number_id','LIKE', '%'.$searchValue.'%')
                                          ->orWhere('status','LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_in,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(release_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhereHas('getRoomData', function ($query) use ($searchValue) {
                                                        $query->where('room_no', 'like', '%'.$searchValue.'%');
                                            })
                                          ->orWhereHas('getUserData', function ($query) use ($searchValue) {
                                                $query->where('username', 'like', '%'.$searchValue.'%');
                                            });
                                    })
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();
     $data_arr = array();     
     foreach($records as $key=>$record){        
        if(@$record->getUserData->username){
            //$userData = @$record->getUserData->username;
            if($record->status=="cleaning"){
              $status = "<button class='btn btn-info btn-sm update_status' onclick='update_status(event,".$record->id.");'>Cleaning</button>";
            }elseif($record->status=="completed"){
              $status = "<button class='btn btn-success btn-sm update_status' onclick='update_status(event,".$record->id.");'>Completed</button>";  
            }            
            $userData = "<span id=".$record->id.'/'.$record->getUserData->id."><button id='user_assign_update' class='btn btn-success bg-success-gradient btn-sm' data-bs-toggle='modal' data-bs-target='#scrollingmodal'>".$record->getUserData->username."</button></span";
        }else{
            $status = "<button class='btn btn-secondary btn-sm'>Ready to clean</button>";
            $userData = "<span id=".$record->id."><button id='user_assign' class='btn btn-danger bg-danger-gradient btn-sm' data-bs-toggle='modal' data-bs-target='#scrollingmodal'>Click To Assign</button></span";
        }
        $booking_number = "<a href=".url('admin/housekeepinglist/booking_print_view/'.@$record->booking_data->id)."  class='click_print custom_a' data-bs-placement='top' data-bs-toggle='tooltip' title='Booking Details'>".$record->booking_number."</a>";
        $action = "<a id=".@$record->booking_data->id." class='btn btn-success btn-sm bg-success-gradient click_status text-white' data-bs-placement='top' title='Add Remark' data-bs-toggle='modal' data-bs-target='#add_remark'><i class='fa fa-sticky-note-o'></i></a>";
        $data_arr[] = array(
          "id" => null,
          "booking_number" => $booking_number,
          "room_numbers" => @$record->getRoomData->room_no,
          "check_in" => Carbon::parse($record->check_in)->format('d-m-Y'),
          "check_out" => Carbon::parse($record->check_out)->format('d-m-Y'),          
          "created_at" => Carbon::parse($record->created_at)->format('d-m-Y'),          
          "release_date" => Carbon::parse($record->release_date)->format('d-m-Y g:i A'),
          "user_id" => $userData,
          "status" => $status,
          "update_user_id" => @$record->getUserData->id,
          "action" => $action,
        );
     }
     //dd($data_arr);
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }  

    public function housekeeperdata(Request $request){     
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value

     $totalRecords = Housekeeping_list::whereHas('booking_data', function($q){
                                        $q->where('deleted_at',null);
                                       })->whereIn('user_id',[0,Auth()->guard('admin')->user()->id])
                                       ->select('count(*) as allcount')->count();
     $totalRecordswithFilter = Housekeeping_list::whereHas('booking_data', function($q){
                                        $q->where('deleted_at',null);
                                    })->where(function($query) use($searchValue) {
                                       $query->where('booking_number', 'LIKE', '%'.$searchValue.'%')
                                          /*->orWhere('room_number_id','LIKE', '%'.$searchValue.'%')*/
                                          ->orWhere('status','LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_in,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(release_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhereHas('getRoomData', function ($query) use ($searchValue) {
                                                        $query->where('room_no', 'like', '%'.$searchValue.'%');
                                            });
                                    })
                                  ->whereIn('user_id',[0,Auth()->guard('admin')->user()->id])
                                  ->select('count(*) as allcount')
                                  ->count();
     // Fetch records
    $records = Housekeeping_list::whereHas('booking_data', function($q){
                                        $q->where('deleted_at',null);
                                    })->where(function($query) use($searchValue) {
                                    $query->where('booking_number', 'LIKE', '%'.$searchValue.'%')
                                          /*->orWhere('room_number_id','LIKE', '%'.$searchValue.'%')*/
                                          ->orWhere('status','LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_in,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(check_out,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(release_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhereHas('getRoomData', function ($query) use ($searchValue) {
                                                        $query->where('room_no', 'like', '%'.$searchValue.'%');
                                            });
                                    })
                                  ->whereIn('user_id',[0,Auth()->guard('admin')->user()->id])
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();
     $data_arr = array();     
     foreach($records as $key=>$record){        
        if(@$record->getUserData->username){
            $userData = Auth()->guard('admin')->user()->username;
            if($record->status=="cleaning"){
              $status = "<button class='btn btn-info btn-sm update_status' onclick='update_status(event,".$record->id.");'>Cleaning</button>";
            }elseif($record->status=="completed"){
              $status = "<button class='btn btn-success btn-sm update_status'>Completed</button>";  
            }  
        }else{
            $status = "<button class='btn btn-secondary btn-sm'>Ready to clean</button>";
            $userData = "<span id=".$record->id."><button class='btn btn-danger bg-danger-gradient btn-sm' onclick='AssignRoom(event,".$record->id.",".Auth()->guard('admin')->user()->id.");'>Assign to me</button></span";
        }
       $action = "<a id=".@$record->booking_data->id." class='btn btn-success btn-sm bg-success-gradient click_status text-white' data-bs-placement='top' title='Add Remark' data-bs-toggle='modal' data-bs-target='#add_remark'><i class='fa fa-sticky-note-o'></i></a>";
        $data_arr[] = array(
          "id" => null,
          "booking_number" => $record->booking_number,
          "room_numbers" => @$record->getRoomData->room_no,
          "check_in" => Carbon::parse($record->check_in)->format('d-m-Y'),
          "check_out" => Carbon::parse($record->check_out)->format('d-m-Y'),          
          "created_at" => Carbon::parse($record->created_at)->format('d-m-Y'),          
          "release_date" => Carbon::parse($record->release_date)->format('d-m-Y g:i A'),
          "user_id" => $userData,          
          "status" => $status,
          "action" => $action,
        );
     }
     //dd($data_arr);
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }

     public function view_on_billing_print($bookingid) {        
         $booking = Room_booking::findOrFail($bookingid);         
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');
         $subtotal = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount);
         $discount = $booking->discount_amount;
         $total_amount = ($subtotal-$discount)+$room_booking_addon;         
         /*$addon_details_sum_tax = ($room_booking_details->sum('subtotal_amount')+$room_booking_addon)+$booking->service_tax_amount;
         $balance_amount = number_format($booking->total_amount-$addon_details_sum_tax,2); */
         $no_refund_payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change', 'booking_date_extend'])->where('refund_payment_check',0)->sum('total_amount');
         $refund_payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change', 'booking_date_extend'])->where('refund_payment_check',1)->sum('total_amount');         
         $balance_amount = $total_amount-($no_refund_payment-$refund_payment);
         //dd($balance_amount);
         return view('admin.housekeepinglist.billing_print_view',compact('booking','total_amount','balance_amount'));
     }

}