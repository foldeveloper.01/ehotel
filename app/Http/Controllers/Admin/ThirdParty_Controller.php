<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Bank_Transfer;
use App\Models\Third_party;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

class ThirdParty_Controller extends Controller
{
    //index
    public function listdata(){    
        $list = Third_party::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.third_party.list',compact('list'));
    }
    public function createdata(){        
        return view('admin.third_party.create');
    }
    public function savedata(Request $request){
       //dd($request->all());
        $validated = Validator::make($request->all(),[            
            'name' => 'max:200|unique:ehotel_third_party,name,NULL,id,deleted_at,NULL',
            'logo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'payto' => 'required',
            'position' => 'required',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $thirdparty = new Third_party();
        if($request->hasFile('logo'))
        {            
            $file_name = Helper::setUploadfileName($request,'thirdparty').'.'.$request->file('logo')->extension();
            $path = $request->file('logo')->storeAs('public/images/thirdparty',$file_name);
            $thirdparty->logo  = explode ("/", $path)[3];
        }
        $thirdparty->name = $request->name;
        $thirdparty->pay_to = $request->payto;
        $thirdparty->position = $request->position;
        $thirdparty->status = $request->status;
        $thirdparty->save();
        return redirect('admin/thirdpartytype')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
       //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Third_party::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Third_party::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){       
         $data = Third_party::findOrFail($id);
        return view('admin.third_party.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){        
            $validated = Validator::make($request->all(),[             
            'name' => 'max:200|unique:ehotel_third_party,name,'.$id.',id,deleted_at,NULL',
            'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'payto' => 'required',
            'position' => 'required',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $thirdparty = Third_party::findOrFail($id);
        if($request->hasFile('logo'))
        {            
            $file_name = Helper::setUploadfileName($request,'thirdparty').'.'.$request->file('logo')->extension();
            $path = $request->file('logo')->storeAs('public/images/thirdparty',$file_name);
            $thirdparty->logo  = explode ("/", $path)[3];
        }
        $thirdparty->name = $request->name;
        $thirdparty->pay_to = $request->payto;
        $thirdparty->position = $request->position;
        $thirdparty->status = $request->status;
        $thirdparty->save();
        return redirect('admin/thirdpartytype')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){        
         $data = Third_party::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/thirdpartytype')->with('success','Deleted sucessfully.');
    }

}