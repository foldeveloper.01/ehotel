<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Room_season_type;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class RoomseasonType_Controller extends Controller
{
    public function listdata(){
        $list = Room_season_type::where('deleted_at',null)->orderBy('position','asc')->get();
        return view('admin.room_season_type.list',compact('list'));
    }    
    public function createdata(){        
        return view('admin.room_season_type.create');
    }
    public function savedata(Request $request){
        $validated = Validator::make($request->all(),[            
            'name' => 'max:200|unique:ehotel_rooms_season_type,name,NULL,id,deleted_at,NULL',
            'button_class_name' => 'required',
            'color_code' => 'required',
            'status' => 'required',
            'position' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $room_season_type = new Room_season_type();
        $room_season_type->name = $request->name;
        $room_season_type->button_class_name = $request->button_class_name;
        $room_season_type->status = $request->status;
        $room_season_type->color_code = $request->color_code;
        $room_season_type->position = $request->position;
        $room_season_type->save();
        return redirect('admin/roomseasontype')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Room_season_type::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Room_season_type::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){        
         $data = Room_season_type::findOrFail($id);
        return view('admin.room_season_type.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
            //dd($request->all());
        $validated = Validator::make($request->all(),[
        'name' => 'max:200|unique:ehotel_rooms_season_type,name,'.$id.',id,deleted_at,NULL',
        'button_class_name' => 'required',
        'status' => 'required',
        'color_code' => 'required',
        'position' => 'required'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error', implode(",", $validated->errors()->all()));
        }       
        $room_season_type = Room_season_type::findOrFail($id);
        $room_season_type->name = $request->name;
        $room_season_type->button_class_name = $request->button_class_name;
        $room_season_type->status = $request->status;
        $room_season_type->color_code = $request->color_code;
        $room_season_type->position = $request->position;
        $room_season_type->save();
        return redirect('admin/roomseasontype')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
         $data = Room_season_type::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/roomseasontype')->with('success','Deleted sucessfully.');
    }

}