<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\User;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Voucher;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use DB;
use App\Exports\GuestUsersExport;
use Excel;

class Voucher_Controller extends Controller
{
    //index
    public function listdata(){
        $list = Voucher::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.voucher.list',compact('list'));
    }
    public function createdata(){
        return view('admin.voucher.create');
    }
    public function savedata(Request $request){
        $validated = Validator::make($request->all(),[
            'name' => 'max:200|unique:ehotel_voucher,name,NULL,id,deleted_at,NULL',
            'code' => 'required|max:100',
            'start_date' => 'required|date|max:100',
            'end_date' => 'required|date|max:100',
            'amount' => 'required|numeric|between:1,99999999999999',
            'total_voucher' => 'required|numeric',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $voucher = new Voucher();
        $voucher->name = $request->name;
        $voucher->code = $request->code;
        $voucher->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $voucher->end_date = Carbon::parse($request->end_date)->format('Y-m-d');
        $voucher->amount = $request->amount;
        $voucher->total_voucher = $request->total_voucher;
        $voucher->status = $request->status;
        $voucher->save();
        return redirect('admin/voucher')->with('success','Added sucessfully.');
    }    
    public function updatedata($id){
        $data = Voucher::findOrFail($id);
        //$data->start_date = Carbon::parse($data->start_date)->format('d-m-Y');
        //$data->end_date = Carbon::parse($data->end_date)->format('d-m-Y');
        return view('admin.voucher.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
            $validated = Validator::make($request->all(),[
            'name' => 'max:200|unique:ehotel_voucher,name,'.$id.',id,deleted_at,NULL',
            'code' => 'required|max:100',
            'start_date' => 'required|date|max:100',
            'end_date' => 'required|date|max:100',
            'amount' => 'required|numeric|between:1,99999999999999',
            'total_voucher' => 'required|numeric',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $voucher = Voucher::findOrFail($id);        
        $voucher->name = $request->name;
        $voucher->code = $request->code;
        $voucher->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $voucher->end_date = Carbon::parse($request->end_date)->format('Y-m-d');
        $voucher->amount = $request->amount;
        $voucher->total_voucher = $request->total_voucher;
        $voucher->status = $request->status;
        $voucher->save();
        return redirect('admin/voucher')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
         $data = Voucher::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/voucher')->with('success','Deleted sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Voucher::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }

    //index
    public function getdataforajax(Request $request){   
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     $totalRecords = Voucher::where('deleted_at',null)->select('count(*) as allcount')->count();
     $totalRecordswithFilter = Voucher::where('deleted_at',null)
                                  ->where(function($query) use($searchValue) {
                                    $query->where('name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('code', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('amount', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('is_used', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(start_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(end_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })
                                  ->select('count(*) as allcount')
                                  ->count();
     // Fetch records
    $records = Voucher::where('deleted_at',null)
                                  ->where(function($query) use($searchValue) {
                                    $query->where('name', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('code', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('amount', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere('is_used', 'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(created_at,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(start_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                          ->orWhere(DB::raw("(DATE_FORMAT(end_date,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%');
                                    })
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();
     $data_arr = array();     
     foreach($records as $key=>$record){        
        $action = "<a href=".url('admin/voucher/update/'.$record->id)." class='btn btn-sm btn-primary'>
                    <i class='fe fe-edit'></i>
                  </a>
                  <a onclick='deletefunction(event,$record->id);' href='' class='btn btn-sm btn-danger'>
                    <i class='fe fe-x'></i>
                  </a>
                  <form id='delete_form$record->id' method='POST' action=".url('admin/voucher/delete', $record->id)."> <input type='hidden' name='_token' value='".csrf_token()."'> <input name='_method' type='hidden' value='POST'>
                  </form>";
        if($record->status=='0'){ $status_check ='checked'; }else{ $status_check ='notchecked'; }
        $status = "<div class='col-xl-2 ps-1 pe-1'>
                    <div class='form-group'>
                      <label class='custom-switch form-switch mb-0'>
                        <input type='checkbox' id='status_$record->id' name='status' class='custom-switch-input' $status_check> <span class='custom-switch-indicator custom-switch-indicator-md' data-on='Yes' data-off='No'></span>
                      </label>
                    </div>
                  </div>";
        $data_arr[] = array(
          "id" => null,
          "name" => $record->name,
          "code" => $record->code,
          "amount" => $record->amount.' MYR',
          "end_date" => Carbon::parse($record->end_date)->format('d-m-Y'),
          "used" => ($record->is_used==0) ? 'No' : 'Yes',
          "status" => $status,
          "created_at" => Carbon::parse($record->created_at)->format('d-m-Y H:i'),
          "action" => $action
        );
     }
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }

}