<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Faq_admin;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class Faq_Controller extends Controller
{
    public function listdata(){
        $list = Faq_admin::where('deleted_at',null)->orderBy('id','DESC')->get();
        return view('admin.faq.list',compact('list'));
    }    
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        return view('admin.faq.create');
    }
    public function savedata(Request $request){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $validated = Validator::make($request->all(),[            
            'question' => 'max:200|unique:ehotel_faq,question,NULL,id,deleted_at,NULL',
            'status' => 'required',
            'answer' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $faq = new Faq_admin();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->status = $request->status;
        $faq->save();
        return redirect('admin/faq')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Faq_admin::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Faq_admin::findOrFail($id);
        return view('admin.faq.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy         
            $validated = Validator::make($request->all(),[
            'question' => 'max:200|unique:ehotel_faq,question,'.$id.',id,deleted_at,NULL',
            'status' => 'required',
            'answer' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $faq = Faq_admin::findOrFail($id);
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->status = $request->status;
        $faq->save();
        return redirect('admin/faq')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Faq_admin::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/faq')->with('success','Deleted sucessfully.');
    }

}