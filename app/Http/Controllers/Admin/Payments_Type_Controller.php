<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Payment_type;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

class Payments_Type_Controller extends Controller
{
    //index
    public function listdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        $list = Payment_type::where('deleted_at',null)->orderBy('id', 'DESC')->get();
        return view('admin.payment_type.list',compact('list'));
    }
    public function createdata(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
        return view('admin.payment_type.create');
    }
    public function savedata(Request $request){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy         
            $validated = Validator::make($request->all(),[
            'payment_name' => 'max:200|unique:ehotel_payment_type,payment_name,NULL,id,deleted_at,NULL'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }        
        $payment = new Payment_type();
        if($request->hasFile('image'))
        {
            //$path = $request->file('image')->store('public/images/payment_type');
            $file_name = Helper::setUploadfileName($request,'paymenttype').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/payment_type',$file_name);
            $payment->image  = explode ("/", $path)[3];
        }
        $payment->payment_name = $request->payment_name;
        $payment->merchant_id = $request->merchant_id;
        $payment->merchant_code = $request->merchant_code;
        $payment->merchant_key = $request->merchant_key;
        $payment->merchant_password = $request->merchant_password;        
        $payment->api_key = $request->api_key;        
        $payment->collection_id = $request->collection_id;        
        $payment->id_type = $request->id_type;        
        $payment->id_number = $request->id_number;        
        $payment->redirect_url = $request->redirect_url;        
        $payment->success_url = $request->success_url;        
        $payment->failure_url = $request->failure_url;        
        $payment->status = $request->status;        
        $payment->save();
        return redirect('admin/paymentstype')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Payment_type::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
            $res_data['type']=$data[0];
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Payment_type::findOrFail($id);
        return view('admin.payment_type.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
            $validated = Validator::make($request->all(),[
            'payment_name' => 'max:200|unique:ehotel_payment_type,payment_name,'.$id.',id,deleted_at,NULL'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        
        $payment = Payment_type::findOrFail($id);
        if($request->hasFile('image'))
        {
            //delete old files            
            if(Storage::disk('public')->exists('images/payment_type/'.$payment->image)){
               Storage::disk('public')->delete('images/payment_type/'.$payment->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'paymenttype').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/payment_type',$file_name);
            $payment->image  = explode ("/", $path)[3];
        }
        $payment->payment_name = $request->payment_name;
        $payment->merchant_id = $request->merchant_id;
        $payment->merchant_code = $request->merchant_code;
        $payment->merchant_key = $request->merchant_key;
        $payment->merchant_password = $request->merchant_password;        
        $payment->api_key = $request->api_key;        
        $payment->collection_id = $request->collection_id;        
        $payment->id_type = $request->id_type;        
        $payment->id_number = $request->id_number;        
        $payment->redirect_url = $request->redirect_url;        
        $payment->success_url = $request->success_url;        
        $payment->failure_url = $request->failure_url;        
        $payment->status = $request->status;        
        $payment->save();
        return redirect('admin/paymentstype')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Payment_type::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/paymentstype')->with('success','Deleted sucessfully.');
    }

}