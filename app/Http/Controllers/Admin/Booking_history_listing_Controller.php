<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin,Admintypes,Sub_Gallery,Rooms,Room_Setting,Room_facilities,Room_gallery,Room_price,Rooms_manage,Add_on_product, User, Country, State,Room_booking, Room_booking_details, Room_booking_payment,Room_deposit_remark};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;
use Illuminate\Support\Str;
use DB;
class Booking_history_listing_Controller extends Controller
{
    public function listdata(Request $request){
        if($request->search !='') {
            $searchValue = $request->search;           
            $list = Room_booking::whereIn('booking_status',[2,3,4])
                            ->whereHas('booking_details.rooms_number', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('room_no', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->orWhereHas('guest', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('name', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('ic_passport_no', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('contact_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('booking_number', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->where('deleted_at',null)->orderBy('id','desc')
                            ->paginate(20)->withQueryString();
        } else {
           $list=Room_booking::whereIn('booking_status',[2,3,4])                           
                           ->where('deleted_at',null)->orderBy('id','desc')
                           ->paginate(20);
        }
        return view('admin.room_booking_history.list',compact('list'));
    }
    
}