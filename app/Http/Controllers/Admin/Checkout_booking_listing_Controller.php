<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin,Admintypes,Sub_Gallery,Rooms,Room_Setting,Room_facilities,Room_gallery,Room_price,Rooms_manage,Add_on_product, User, Country, State,Room_booking, Room_booking_details, Room_booking_payment,Room_booking_addon,Housekeeping_list,Room_booking_receipt,Invoice};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;
use Illuminate\Support\Str;

class Checkout_booking_listing_Controller extends Controller
{
    public function listdata(Request $request){
        if($request->search !=''|| $request->booking_status !='' || $request->payment !='') {
            $searchValue = $request->search;
            $booking_status = [1];
            $payment_status = [1];
            //booking status
            $todayCheck_out='';
            if($request->booking_status==3) {
                $booking_status = [1];
            }elseif($request->booking_status==4) {
                $booking_status = [1];
                $todayCheck_out = Carbon::today();
            }
            //booking status
            
            $list = Room_booking::whereIn('booking_status',$booking_status)
                            ->whereIn('payment_status',$payment_status)
                            ->when($request->booking_status==4, function ($query) use ($request,$todayCheck_out) {                                
                               $query->whereDate('check_out', $todayCheck_out);
                            })
                            ->whereHas('booking_details.rooms_number', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('room_no', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->orWhereHas('guest', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('name', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('ic_passport_no', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('contact_number', 'LIKE', '%'.$request->search.'%')
                                    ->orWhere('booking_number', 'LIKE', '%'.$request->search.'%');
                               });
                            })
                            ->where('deleted_at',null)->orderBy('id','desc')
                            ->paginate(20)->withQueryString();
        }else {
        $list=Room_booking::where('booking_status',1)                          
                           ->where('deleted_at',null)->orderBy('id','desc')
                           ->paginate(20);
        }
        return view('admin.checkout_booking_list.list',compact('list'));
    }
    public function remove_due_booking(Request $request){
       Room_booking::whereIn('id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_details::whereIn('booking_id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_addon::whereIn('booking_id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       return ['response'=>0,'message'=>'success'];
    }
    public function checkout_confirm_view($bookingid){
         $booking = Room_booking::findOrFail($bookingid);
         /*$currentDate = Carbon::now();
         $dateToCompare = Carbon::createFromFormat('Y-m-d',$booking->check_out);
         $earlyCheckOut=0;
         if($currentDate->lt($dateToCompare)){
            $earlyCheckOut=1;
         }*/         
         return view('admin.room_booking.checkout_confirm_view',compact('booking'));
     }
     public function checkout_confirm_save(Request $request,$bookingid){
         //dd($request->all());
         $updatedID=[];$updatedRoom=[];
         foreach($request->checkout_checkbox as $key=>$val){
            $roomnumber_key = array_search ($key, $request->room_number);
            $details_id = $request->booking_details_ids[$roomnumber_key];
            $updatedRoom[]=$key;
            $updatedID[]=$details_id;
         }         
         $numberOfCheckin_count = Room_booking_details::where('booking_id',$bookingid)->where('checkout_status',0)->where('deleted_at',null)->count();
         Room_booking_details::whereIn('id',$updatedID)->update(['checkout_status'=>1,'checkout_date_time'=>Carbon::now()]);
         
         $booking = Room_booking::findOrFail($bookingid);
         //update booking status
         if($numberOfCheckin_count==count($updatedID)){          
          $booking->booking_status = 2;
          $booking->save();
         }
         //update booking status

         //save housekeeping part
         foreach($updatedID as $key=>$data){
            $getData = Room_booking_details::findOrFail($data);
            $hkeeping = new Housekeeping_list();            
            $hkeeping->booking_number = $booking->booking_number;
            $hkeeping->room_type_id = $getData->room_type_id;
            $hkeeping->room_number_id = $getData->room_number_id;
            $hkeeping->check_in = $getData->check_in;
            $hkeeping->check_out = $getData->check_out;
            $hkeeping->save();
         }
         //save housekeeping part
        
            //Room_booking_payment
         if($request->refund_amount) {
          $booking_payment = new Room_booking_payment();
          $booking_payment->booking_id = $bookingid;
          $booking_payment->total_amount = $request->refund_amount;
          $booking_payment->payment_status = $booking->payment_status;
          $booking_payment->payment_type = $request->refund_amount_payment_mode;
          $booking_payment->booking_details_id = implode(',',[]);
          $booking_payment->booking_addon_id = implode(',',[]);
          $booking_payment->refund_payment_check = 1;
          $booking_payment->save();
            //Room_booking_payment
            //room booking receipt
          $booking_receipt = new Room_booking_receipt();
          $booking_receipt->booking_id= $bookingid;
          $booking_receipt->receipt_no='';
          $booking_receipt->payment_table_id=$booking_payment->id;
          $booking_receipt->receipt_amount=$request->refund_amount;
          $booking_receipt->payment_status=$booking->payment_status;
          $booking_receipt->payment_type=$request->refund_amount_payment_mode;
          $booking_receipt->save();
         }
           //room booking receipt
        if($numberOfCheckin_count==count($updatedID)){
             $receipt_number = Room_booking_receipt::where('booking_id',$bookingid)->orderBy('id', 'desc')->first();
             //save invoice
               $invoice = new Invoice();
               $invoice->booking_id = $bookingid;
               $invoice->receipt_no = $receipt_number->receipt_no;
               $invoice->admin_user_id = Auth::guard('admin')->user()->id;
               $invoice->invoice_customer_id = $booking->customer_id;
               $invoice->invoice_number = $invoice->invoiceNumber();
               $invoice->invoice_date = Carbon::now();
               $invoice->invoice_status = 'paid';
               $invoice->save();
             //save invoice
            return redirect('admin/bookinghistorylisting')->with('success','Check-out sucessfully for this rooms '.implode(',',$updatedRoom).'');
         }
            return redirect('admin/checkoutlisting')->with('success','Check-out sucessfully for this rooms '.implode(',',$updatedRoom).'');  
     }

}