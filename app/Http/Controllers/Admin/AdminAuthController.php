<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\UserLoginHistroy;
use Hash;
use Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use App\Notifications\UserPasswordEmailNotification;
use App\Models\SmtpSettings;
use Helper;

class AdminAuthController extends Controller
{
    public function getLogin(){
        if(auth()->guard('admin')->check()){
            return redirect('admin');
        }
        return view('admin.login');
    }
    public function signup(){
        return view('admin.signup');
    }

    public function register()
    {
        $pass = '1234567';
        return Admin::create([
            'firstname' => 'Admin',
            'email' => 'admin3@gmail.com',
            'status' => '0',
            'admin_type_id' => '1',
            'password' => Hash::make($pass),
        ]);
    }

    public function postLogin(Request $request)
    {
        $validated = Validator::make($request->all(),[
        'email' => 'required|max:100',
        'password' => 'required',
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()->with('error','Login failed');
        }
        $input = $request->all();
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(auth()->guard('admin')->attempt([$fieldType => $request->input('email'),  'password' => $request->input('password')])){
            $user = auth()->guard('admin')->user();            
            if($user->status==1){
             auth()->guard('admin')->logout();
             return redirect()->back()->with('error','Your account has been suspend please contact our admin.');
            }
            UserLoginHistroy::create(['user_id'=> $user->id,'last_login_at'=> Carbon::now()->toDateTimeString(),'last_login_ip'=> $request->getClientIp()]);
            return redirect()->route('adminDashboard')->with('success','You are Logged in sucessfully.');      
        }else {
            //Alert::error('Error', 'Whoops! invalid email and password.');
            return back()->withErrors($validated)
                        ->withInput()
                        ->with('error','Whoops! invalid email and password.');
        }
    }

    public function forgotPassword(){
        return view('admin.forgotpassword');
    }

    public function gentrateUserPassword(Request $request)
    {        
        $validated = Validator::make($request->all(),[
        'email' => 'required|email|max:100'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error','Whoops! invalid email.');
        }
        $user_check_email = Admin::where('email', $request->email)->first();
        if(is_object($user_check_email)){
            if($user_check_email->status==1){
              $messages = 'Your account is Suspend please contact your administration';
              return view('admin.forgotpassword',compact('messages'));
            }
            $check_smtp = Helper::getSMTPdata();
            if($check_smtp==0){
               $messages = 'Please check smtp configuration';
               return view('admin.forgotpassword',compact('messages'));
               //return redirect('admin/admins')->with('error','Please check smtp configuration');
            }
            //success
            $password = $this->randomPassword();
            $user = Admin::find($user_check_email->id);
            $user->password  = bcrypt($password);
            $user->email  = $user_check_email->email;
            $user->save();
            $user->notify(new UserPasswordEmailNotification($user,$password));
            $messages = 'please check your email to reset your password';
            return view('admin.forgotpassword',compact('messages'));
         }
        $messages = 'Email Account does not exist';
        return view('admin.forgotpassword',compact('messages'));
      
    }

    function randomPassword() {
        //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?"; 
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
        $length = rand(10, 16); 
        $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, 8 );
         return $password;
    }

    public function getLogout(Request $request)
    {
        auth()->guard('admin')->logout();
        Session::flush();
        return redirect(route('adminLogin'))->with('success','You are logout sucessfully.');
    }
}