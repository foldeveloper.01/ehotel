<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\{Room_booking,Room_booking_addon,Room_booking_details,Room_booking_guest_details,Room_booking_payment,Room_booking_receipt,Rooms,Rooms_manage};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use DB;
use App\Exports\GuestStayReportExport;
use Excel;

class Guest_Stay_Report_Controller extends Controller
{
    
    //index
    public function listdata(){       
        //$list = User::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.reports.guest_stay_reports');
    }

    public function ajax_guest_stay_report(Request $request) {
    //dd($request->all());
     $draw = $request->get('draw');
     $start = $request->get("start");
     $rowperpage = $request->get("length");
     $columnIndex_arr = $request->get('order');
     $columnName_arr = $request->get('columns');
     $order_arr = $request->get('order');
     $search_arr = $request->get('search');
     $columnIndex = $columnIndex_arr[0]['column']; // Column index
     $columnName = $columnName_arr[$columnIndex]['data']; // Column name
     $columnSortOrder = $order_arr[0]['dir']; // asc or desc
     $searchValue = $search_arr['value']; // Search value
     // Total records  
     if($columnName=='id'){
       $columnName='ehotel_rooms_booking_details.id';
     }elseif($columnName=='rooms'){
       $columnName='ehotel_rooms_number.room_no';
     }elseif($columnName=='guest'){
       $columnName='ehotel_guest_user.name';
     }elseif($columnName=='checkin_date_time'){
       $columnName='ehotel_rooms_booking_details.checkin_date_time';
     }elseif($columnName=='checkout_date_time'){
       $columnName='ehotel_rooms_booking_details.checkout_date_time';
     }
     $minDate = Carbon::now()->subYear(5);
     $maxDate = Carbon::now()->addYears(5);     
     if($request->filled('minDate')) {
       $minDate = Carbon::parse($request->minDate)->format('Y-m-d');
     }
     if($request->filled('maxDate')) {
       $maxDate = Carbon::parse($request->maxDate)->format('Y-m-d');
     }
     $totalRecords = Room_booking_details::where('checkout_status',1)->where('is_change_data',0)->where('deleted_at',null)->select('count(*) as allcount')->count();
     $totalRecordswithFilter = Room_booking_details::join('ehotel_guest_user','ehotel_rooms_booking_details.customer_id','=','ehotel_guest_user.id')
                                   ->join('ehotel_rooms_number','ehotel_rooms_booking_details.room_number_id','=','ehotel_rooms_number.id')
                                    ->where(function($query) use($request,$searchValue,$minDate,$maxDate) {
                                      $query->where('ehotel_guest_user.name', 'LIKE', '%'.$searchValue.'%')
                                      ->orWhere('ehotel_rooms_number.room_no', 'LIKE', '%'.$searchValue.'%')
                                      ->orWhere(DB::raw("(DATE_FORMAT(ehotel_rooms_booking_details.checkin_date_time,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                      ->orWhere(DB::raw("(DATE_FORMAT(ehotel_rooms_booking_details.checkout_date_time,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                      ;
                                    })
                                    ->where('ehotel_rooms_booking_details.checkout_status',1)->where('ehotel_rooms_booking_details.is_change_data',0)->where('ehotel_rooms_booking_details.deleted_at',null)
                                    ->whereBetween('ehotel_rooms_booking_details.checkin_date_time',[$minDate, $maxDate])                                
                                    ->select('count(*) as allcount')
                                    ->count();
     // Fetch records
    $records = Room_booking_details::join('ehotel_guest_user','ehotel_rooms_booking_details.customer_id','=','ehotel_guest_user.id')
                                   ->join('ehotel_rooms_number','ehotel_rooms_booking_details.room_number_id','=','ehotel_rooms_number.id')
                                   ->where(function($query) use($request,$searchValue,$minDate,$maxDate) {
                                      $query->where('ehotel_guest_user.name', 'LIKE', '%'.$searchValue.'%')
                                      ->orWhere('ehotel_rooms_number.room_no', 'LIKE', '%'.$searchValue.'%')
                                      ->orWhere(DB::raw("(DATE_FORMAT(ehotel_rooms_booking_details.checkin_date_time,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                      ->orWhere(DB::raw("(DATE_FORMAT(ehotel_rooms_booking_details.checkout_date_time,'%d-%m-%Y %H:%i'))"),'LIKE', '%'.$searchValue.'%')
                                      ;
                                    })
                                  ->where('ehotel_rooms_booking_details.checkout_status',1)->where('ehotel_rooms_booking_details.is_change_data',0)->where('ehotel_rooms_booking_details.deleted_at',null)
                                  ->whereBetween('ehotel_rooms_booking_details.checkin_date_time',[$minDate, $maxDate])                               
                                  ->orderBy($columnName,$columnSortOrder)
                                  ->skip($start)
                                  ->take($rowperpage)
                                  ->get();

                                  

     $data_arr = array();     
     foreach($records as $key=>$record) {
        $action = "<a href=".url('admin/guestuser/view/'.$record->guest->id)." class='btn btn-sm btn-primary click_print'>".$record->guest->name."                    
                  </a>";
        $details = "<a href=".url('admin/bookinglisting/newbooking/billing_print_view/'.$record->booking_id)." class='btn btn-success btn-sm bg-success-gradient text-white click_print'>".@$record->rooms_number->room_no."</a>";
        $data_arr[] = array(
          "id" => null,
          "rooms" => $details,
          "guest" => $action,          
          "checkin_date_time" => Carbon::parse($record->checkin_date_time)->format('d-m-Y g:i A'),
          "checkout_date_time" => Carbon::parse($record->checkout_date_time)->format('d-m-Y'),
        );
     }     
     $response = array(
        "draw" => intval($draw),
        "iTotalRecords" => $totalRecords,
        "iTotalDisplayRecords" => $totalRecordswithFilter,
        "aaData" => $data_arr
     );
     echo json_encode($response);
     exit;
    }

    public function export(Request $request){
        return Excel::download(new GuestStayReportExport, 'GuestStayReport'.date('d-m-Y_H:i:s').'.xlsx');
    }
    public function saleslistdata(Request $request)
    {
        $years=2023;    
        $list = Room_booking_payment::where('deleted_at',null)->where('refund_payment_check',0)
       ->select(
            DB::raw('sum(total_amount) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%M') as months"))
       ->whereYear('created_at', '=', $years)->groupBy('months')->get();
       $amount=array();
       $months=array();
       foreach($list as $k => $v){
        $amount[$k]=$v->sums;
        $months[$k]=$v->months;
       }
       $total_Amount= array_sum($amount);     
       return view('admin.reports.sales_monthly_reports',compact('amount','months','years','list','total_Amount'));
    }
}