<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Admin, Admintypes, Sub_Gallery, Rooms, Room_Setting, Room_facilities, Room_gallery, Room_price, Rooms_manage, Add_on_product, User, Country, State, Room_booking, Room_booking_details, Room_booking_payment, Room_booking_addon, Room_booking_guest_details, Room_booking_receipt, Housekeeping_list, Invoice, Email_Content, Settings, Voucher, Coupon, Third_party};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use App\Notifications\CustomerForPendingPayment;
use Input;
use Illuminate\Support\Str;
use Mail;

class Room_Booking_listing_Controller extends Controller
{
    public function listdata(Request $request){
        if($request->search !=''|| $request->booking_status !='' || $request->payment !='') {
            $searchValue = $request->search;
            $booking_status = [0,1];
            $payment_status = [1];
            //booking status
                $todayCheck_in=''; $todayCheck_out='';
                if($request->booking_status==1) {
                    $booking_status = [0];
                }elseif($request->booking_status==2) {
                    $booking_status = [0];
                    $todayCheck_in = Carbon::today();
                }elseif($request->booking_status==3) {
                    $booking_status = [1];
                }elseif($request->booking_status==4) {
                    $booking_status = [1];
                    $todayCheck_out = Carbon::today();
                }
            //booking status

            //payment status
                if($request->payment!='') {
                    $payment_status = [$request->payment];
                }            
            //payment status
             $list = Room_booking::with('booking_details')->with('guest')
                            ->whereIn('booking_status',$booking_status)
                            ->whereIn('payment_status',$payment_status)
                            ->when($request->booking_status==2, function ($query) use ($request,$todayCheck_in) {                                
                               $query->whereDate('check_in', $todayCheck_in);
                            })->when($request->booking_status==4, function ($query) use ($request,$todayCheck_out) {                                
                               $query->whereDate('check_out', $todayCheck_out);
                            })
                            /*->whereHas('booking_details.rooms_number', function ($query) use ($request) {
                                $query->when($request->filled('search'), function ($query) use ($request) {
                                    $query->where('room_no', 'LIKE', '%'.$request->search.'%');
                               });
                            })*/
                            ->when($request->filled('search'), function ($query) use ($request) {
                                    $query->whereHas('guest', function ($query) use ($request) {                                
                                     $query->where('name', 'LIKE', '%'.$request->search.'%');                               
                                    })->orWhereHas('booking_details.rooms_number', function ($query) use ($request) {                                
                                       $query->where('room_no', 'LIKE', '%'.$request->search.'%');
                                    });
                             })
                            ->where('deleted_at',null)->orderBy('id','desc')
                            ->paginate(20)->withQueryString();
        }else {
            $list = Room_booking::whereIn('booking_status',[0,1])                          
                           ->where('deleted_at',null)->orderBy('id','desc')
                           ->paginate(20);
        }                                  
        return view('admin.room_booking.list',compact('list'));
    }
    public function newbooking(Request $request) {        
        $check_condition = 0;
        $list ='';
        $booking_type = $request->type;
        if($booking_type=='normal') {
           $checkin_date = Carbon::now()->format('d-m-Y');
           $checkout_date = Carbon::now()->addDays(1)->format('d-m-Y');
        }elseif($booking_type=='midnight') {
           $checkin_date = Carbon::now()->subDays(1)->format('d-m-Y');
           $checkout_date = Carbon::now()->format('d-m-Y');
        }elseif($booking_type=='monthly') {
           $checkin_date = Carbon::now()->format('d-m-Y');
           $checkout_date = Carbon::now()->addDays(30)->format('d-m-Y');
        }        
        if($request->get('checkin') && $request->get('checkout')) {
           if(Rooms::where('deleted_at',null)->count()==0) {
              return redirect()->back()->with('error','Please make a new rooms');
           }
           if(Rooms::where('room_price_update_status',1)->where('deleted_at',null)->count()==0) {
              return redirect()->back()->with('error','Please update room price');
           }
           $get_room_types_id = Rooms::get_only_room_type_availables($request->get('checkin'),$request->get('checkout'));
           $check_condition = 1;
           $list = Rooms::whereNotIn('id',$get_room_types_id)->where('show_online',0)->where('room_price_update_status',1)->where('deleted_at',null)->orderBy('id','DESC')->get();
           if(count($list)==0) {
             $check_condition = 5;
             return view('admin.room_booking.newbooking',compact('check_condition','list','checkin_date','checkout_date', 'booking_type'));
             //return redirect()->back()->with('error','No rooms available for selected date');
           }          
        }
        $third_party = Third_party::where('deleted_at',null)->pluck('name','id');
        //dd($third_party);
        return view('admin.room_booking.newbooking',compact('check_condition','list','checkin_date','checkout_date', 'booking_type','third_party'));
    }
    public function newbookingview(Request $request) {
        //dd($request->type);
        if($request->quantity) {
            $startDate = $request->checkin;
            $EndDate = $request->checkout;
            $numberOfDays = Carbon::parse($startDate)->diffInDays(Carbon::parse($EndDate));
            $finalData=[];
            $room_type_id=[];
            $amount=0;
            $j=0;
            foreach($request->quantity as $key=>$value){
                if($value!=0){
                    for ($i=1; $i <= $value; $i++) {
                         $room_type_id[]=$request->room_id[$key].'_'.$j;
                         $room = Rooms::findOrFail($request->room_id[$key]);
                         $getRoomsIds = $room->get_available_rooms_number($request->room_id[$key],$startDate,$EndDate);
                         $storeRoomsId[$request->room_id[$key]][]=$getRoomsIds;
                         if(count($storeRoomsId[$request->room_id[$key]]) >1){
                            $getRoomsIds=array_slice($getRoomsIds,count($storeRoomsId[$request->room_id[$key]])-1,null, true);
                         }
                         $room->availableRoomsNumber = $getRoomsIds;
                         //dd($room->availableRoomsNumber);
                         $room->roomsprice = $room->get_rooms_price($request->room_id[$key],$startDate,$EndDate,$numberOfDays);
                         //dd($room->roomsprice);
                         $amount += ($numberOfDays*$room->roomsprice);
                         $finalData[]=$room;
                         $j++;
                    }
                }
            }
        }
        //dd($request->room_id);
        $totalroomprice = $amount;
        $list_add_on_product = Add_on_product::where('status',0)->where('show_online_enable',0)->orderBy('add_on_name','asc')->get();
        $guest_list = User::where('deleted_at',null)->where('status',0)->orderBy('name','asc')->get();
        $country = Country::where('id',132)->get();     
        $state = State::where('country_id',132)->get();
        $booking_type = $request->type;
        $room_type_id = implode(',',$room_type_id);
        //coupon voucher count check
        $coupon_count  = Coupon::where('deleted_at',null)->count();
        $voucher_count = Voucher::where('deleted_at',null)->count();
        $coupon_voucher_count = 1;
        if($coupon_count==0 && $voucher_count==0) {
           $coupon_voucher_count = 0;
        }
        //coupon voucher count check
        return view('admin.room_booking.newbooking_view',compact('list_add_on_product','guest_list','country','state','finalData','totalroomprice','numberOfDays', 'booking_type', 'room_type_id','coupon_voucher_count'));
    }
    public function newbooking_payment(Request $request,$id){
         $booking = Room_booking::findOrFail($id);
         $booking_details = $booking->booking_details;
         $booking_addon_details = $booking->booking_addon_details;
         //dd($booking_addon_details);
         return view('admin.room_booking.newbooking_payment_page',compact('booking','booking_details','booking_addon_details'));
    }
    public function newbooking_payment_save(Request $request,$id){
        //dd($request->all());
        $booking = Room_booking::findOrFail($id);        
        $booking->payment_status = 1;
        $booking->save();
        $booking_details_id = [];
        foreach($booking->booking_details as $key=>$val){
                $booking_details_id[]=$val->id;
        }
        $booking_addon_id = [];
        foreach($booking->booking_addon_details as $key=>$val){
                $booking_addon_id[]=$val->id;
        }
         //Room_booking_payment
         $fields = ['_token'];
         $booking_payment = new Room_booking_payment();
         $booking_payment->booking_id = $id;
         $booking_payment->total_amount = $booking->total_amount;
         $booking_payment->payment_status = $booking->payment_status;
         $booking_payment->payment_type = $request->payment_type;
         $booking_payment->booking_details_id = implode(',',$booking_details_id);
         $booking_payment->booking_addon_id = implode(',',$booking_addon_id);
         $booking_payment->invoice_type = 'exist';
         $booking_payment->payment_details = json_encode($request->except($fields));
         $booking_payment->others = 'booking payment '.$booking->total_amount.'';
         $booking_payment->save();
         //room booking receipt
         $booking_receipt = new Room_booking_receipt();
         $booking_receipt->booking_id=$id;
         $booking_receipt->receipt_no='';
         $booking_receipt->payment_table_id=$booking_payment->id;
         $booking_receipt->receipt_amount=$booking->total_amount;
         $booking_receipt->payment_status=$booking->payment_status;
         $booking_receipt->payment_type=$request->payment_type;
         $booking_receipt->save();
         return redirect('admin/bookinglisting/')->with('success','Payment submit sucessfully for this Booking ID:'.$booking->booking_number.'');
    }
    public function newbooking_save(Request $request){
        //dd($request->all());
         //if tax mismatching
        $get_tax_setting = Helper::getTaxSetting();
        if($get_tax_setting['type']!=explode('_',$request->tax_type)[0] || $get_tax_setting['value']!=explode('_',$request->tax_type)[1]){
           return redirect()->back()->with('error','Tax settings updated, please try again.');
        }
        $startDate = $request->checkin;
        $EndDate = $request->checkout;
        $numberOfDays = Carbon::parse($startDate)->diffInDays(Carbon::parse($EndDate)); 
        //dd($numberOfDays);
         //if tax mismatching  
         //add voucher and coupon count++
          if($request->discount_details!='') {
               $get_type_id =  explode('_',$request->discount_details);
               if($get_type_id[0]=='voucher') {
                $voucher_id = $get_type_id[1];
                $voucher = Voucher::findOrFail($voucher_id);
                $voucher->is_used = ($voucher->is_used+1);
                $voucher->save();               
               }elseif($get_type_id[0]=='coupon') {
                 $coupon_id = $get_type_id[1]; 
                $coupon = Coupon::findOrFail($coupon_id);
                $coupon->is_used = ($coupon->is_used+1);
                $coupon->save();                
               }
          }
         //add voucher and coupon count++     
         $booking = new Room_booking();
         $booking->customer_id = $request->guest_id;
         $booking->check_in = Carbon::parse($request->checkin)->format('Y-m-d');
         $booking->check_out = Carbon::parse($request->checkout)->format('Y-m-d');
         $booking->number_of_rooms = array_sum(array_map("count", $request->roomname));
         $booking->service_tax_details = $request->tax_type;
         $booking->service_tax_amount = $request->tax_amount;
         $booking->subtotal_amount = $request->subtotal_amount;
         $booking->total_amount = $request->total_amount;
         $booking->is_booking_type = $request->booking_type;
         $booking->booking_number = '';
         $booking->payment_status = 0;
         $booking->discount_details = $request->discount_details;
         $booking->discount_amount = $request->discount_amount;
         $booking->admin_user_id = Auth::guard('admin')->user()->id;
         $booking->save();
         foreach($request->roomname as $key=>$value){
            foreach($value as $keys=>$val) {
            $booking_details = new Room_booking_details();
            $booking_details->room_type_id = $key;
            $booking_details->room_number_id = $request->roomsnumber[$key][$keys];
            $booking_details->customer_id = $request->guest_id;
            $booking_details->check_in = Carbon::parse($request->checkin)->format('Y-m-d');
            $booking_details->check_out = Carbon::parse($request->checkout)->format('Y-m-d');
            $booking_details->room_price = $request->roomprice[$key][$keys];
            $booking_details->room_quantity = 1;            
            $booking_details->number_of_days = $numberOfDays;            
            $booking_details->subtotal_amount = ($numberOfDays*$request->roomprice[$key][$keys]);
            $booking_details->booking_id = $booking->id;
            $booking_details->status = 0;
            $booking_details->save();
          }
        }
        if($request->addon_id[0]!=null){
              $add_on = explode(',',$request->addon_id[0]);
              $add_on_quantity = explode(',',$request->addon_quantity[0]);
              foreach($add_on as $k=>$add){
                 $getData = Add_on_product::findOrFail($add);
                 $booking_addon = new Room_booking_addon();
                 $booking_addon->product_addon_id = $add;
                 $booking_addon->booking_id = $booking->id;
                 $booking_addon->price = number_format($getData->add_on_amount,2);
                 $booking_addon->quantity = $add_on_quantity[$k];
                 $booking_addon->invoice_type = 'exist';
                 $booking_addon->total_amount = number_format($getData->add_on_amount*$add_on_quantity[$k],2);
                 $booking_addon->save();
              }
         }
        return redirect('admin/bookinglisting/newbooking/payment/'.$booking->id);
        /*return redirect('admin/bookinglisting/newbooking/payment/'.$booking->id)->with('success','Booking ID:'.$booking->booking_number.' generated sucessfully.');*/
    }
    function getBcRound($number, $precision = 0)
    {
        $precision = ($precision < 0)
                   ? 0
                   : (int) $precision;
        if (strcmp(bcadd($number, '0', $precision), bcadd($number, '0', $precision+1)) == 0) {
            return bcadd($number, '0', $precision);
        }
        if ($this->getBcPresion($number) - $precision > 1) {
            $number = getBcRound($number, $precision + 1);
        }
        $t = '0.' . str_repeat('0', $precision) . '5';
        return $number < 0
               ? bcsub($number, $t, $precision)
               : bcadd($number, $t, $precision);
    }
    function getBcPresion($number) {
        $dotPosition = strpos($number, '.');
        if ($dotPosition === false) {
            return 0;
        }
        return strlen($number) - strpos($number, '.') - 1;
    }
    public function save_guest_user(Request $request) {
        //dd($this->rand_string(8));
        $validated = Validator::make($request->all(),[
            'ic_passport_no' => 'max:20|unique:ehotel_guest_user,ic_passport_no,NULL,id,deleted_at,NULL',
            'email' => 'nullable|email|max:100|unique:ehotel_guest_user,email,NULL,id,deleted_at,NULL',
            'name' => 'required|max:100',
            //'password' => 'required|min:8',
             ]);
            if ($validated->fails()) {
               $message=$validated->messages()->all()[0];
               return ['response'=>1,'message'=>$message];                
            }
        $User = new User();
        $User->name = $request->name;
        $User->email = $request->email;
        $User->ic_passport_no = $request->ic_passport_no;
        $User->password = bcrypt($this->rand_string(8));
        $User->company_name = $request->company_name;
        $User->contact_number = $request->contact_number;
        $User->vehicle_number = $request->vehicle_number;
        $User->address = $request->address;
        $User->country = $request->country;
        $User->state = $request->state;
        $User->city = $request->city;
        $User->postcode = $request->postcode;
        $User->remarks = $request->remarks;
        $User->status = $request->status;
        $User->save();
        return ['response'=>0,'user_id'=>$User->id,'message'=>$User->name.' ('.$User->ic_passport_no.')'];
    }
    public function rand_string( $length ) {
       $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
       return substr(str_shuffle($chars),0,$length);
    }
    public function remove_due_booking(Request $request){
       Room_booking::whereIn('id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_details::whereIn('booking_id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_addon::whereIn('booking_id', [$request->id])->update(['deleted_at' =>Carbon::now()]);
       return ['response'=>0,'message'=>'success'];
    }
    public function cancel_booking($booking_id) {
       $booking = Room_booking::findOrFail($booking_id);
       $booking->deleted_at = Carbon::now();
       $booking->save();
       Room_booking_details::whereIn('booking_id', [$booking_id])->update(['deleted_at' =>Carbon::now()]);
       Room_booking_addon::whereIn('booking_id', [$booking_id])->update(['deleted_at' =>Carbon::now()]);
       return redirect('admin/bookinglisting')->with('success','This Booking ID:'.$booking->booking_number.' is cancelled');
    }
    public function pay_due_amount(Request $request) {
            $validated = Validator::make($request->all(), [
                'payment_booking_id' => 'max:11|exists:ehotel_rooms_booking,id',
                'amount' => 'required',
            ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error','Please check your inputs');
            }         
         $fields = ['_token', 'payment_booking_id', 'amount'];        
         $booking = Room_booking::findOrFail($request->payment_booking_id);
         //Room_booking_payment
         $booking_payment = new Room_booking_payment();
         $booking_payment->booking_id = $booking->id;
         $booking_payment->booking_details_id = implode(',',[]);
         $booking_payment->booking_addon_id = implode(',',[]);
         $booking_payment->payment_status = $booking->payment_status; 
         $booking_payment->payment_type = $request->payment_method;
         $booking_payment->total_amount = $request->amount;
         $booking_payment->invoice_type = 'exist';
         $booking_payment->payment_details = json_encode($request->except($fields));
         $booking_payment->others = 'due amount '.$request->amount.'';
         $booking_payment->save();
         //room booking receipt
          $booking_receipt = new Room_booking_receipt();
          $booking_receipt->booking_id = $booking->id;
          $booking_receipt->receipt_no = '';
          $booking_receipt->payment_table_id = $booking_payment->id;
          $booking_receipt->receipt_amount = $request->amount;
          $booking_receipt->payment_status = $booking->payment_status;
          $booking_receipt->payment_type = $request->payment_method;
          $booking_receipt->save();
       return redirect()->back()->with('success','Payment submit sucessfully for this Booking ID:'.$booking->booking_number.'');
    }
    public function edit_delete_billing_view(Request $request){
       //dd($request->all());
       if($request->type=='booking_details') {
             if(explode('_',$request->id)[0]=='delete') {              
              $room_booking_details = Room_booking_details::findOrFail(explode('_',$request->id)[1]);
              $room_booking_details->deleted_at = Carbon::now();
              $room_booking_details->save();
              $booking = Room_booking::findOrFail($room_booking_details->booking_id);
              $subtotal_amount = ($booking->subtotal_amount-$request->amount);
              $total_amount = ($subtotal_amount+$booking->service_tax_amount);
              $booking->subtotal_amount = $subtotal_amount;
              $booking->total_amount = $total_amount;
              $booking->save();
              return ['response'=>0,'message'=>'successfully removed'];
           }           
           $Room_booking_details = Room_booking_details::findOrFail(explode('_',$request->id)[1]);
           $Room_booking_details->room_price = $request->amount;
           $Room_booking_details->subtotal_amount = $request->amount;
           $Room_booking_details->save();
           return ['response'=>0,'message'=>'Amount updated successfully'];
       }if($request->type=='service_charge'){
          $room_booking = Room_booking::findOrFail(explode('_',$request->id)[1]);
          if(explode('_',$request->id)[0]=='delete') {             
             $room_booking->service_tax_deleted=1;
             $room_booking->service_tax_amount=0.00;
             $room_booking->subtotal_amount = ($room_booking->subtotal_amount-$request->amount);
             $room_booking->total_amount = ($room_booking->total_amount-$request->amount);
             $room_booking->save();
             return ['response'=>0,'message'=>'Tax amount successfully removed'];
          }          
          if($room_booking->service_tax_amount == $request->amount) {
            return ['response'=>0,'message'=>'Amount updated successfully'];
          }
          if($room_booking->service_tax_amount < $request->amount) {
            //increase
            //$room_booking->service_tax_amount = $request->amount;
            $diff_tax_amount = abs($room_booking->service_tax_amount-$request->amount);
            $tax_amount = $room_booking->service_tax_amount+$diff_tax_amount;
            $sub_total = $room_booking->subtotal_amount+$diff_tax_amount;
            $total_amount = $room_booking->total_amount+$diff_tax_amount;            
            $room_booking->service_tax_amount=$tax_amount;
            $room_booking->subtotal_amount = $sub_total;
            $room_booking->total_amount = $total_amount;
          }else {
            //decrease
            $diff_tax_amount = abs($room_booking->service_tax_amount-$request->amount);
            $tax_amount = ($room_booking->service_tax_amount-$diff_tax_amount);
            $sub_total = ($room_booking->subtotal_amount-$diff_tax_amount);
            $total_amount = ($room_booking->total_amount-$diff_tax_amount);            
            $room_booking->service_tax_amount = $tax_amount;
            $room_booking->subtotal_amount = $sub_total;
            $room_booking->total_amount = $total_amount;
          }
          $room_booking->save();
          return ['response'=>0,'message'=>'Tax amount updated successfully'];
       }
       if($request->type=='payment'){
          if(explode('_',$request->id)[0]=='delete') {
             Room_booking_payment::findOrFail(explode('_',$request->id)[1])->update(['deleted_at'=>Carbon::now()]);
             return ['response'=>0,'message'=>'successfully removed'];
          }
          $payment = Room_booking_payment::findOrFail(explode('_',$request->id)[1]);
          $payment->total_amount = $request->amount;          
          $payment->save();
          return ['response'=>0,'message'=>'Amount updated successfully'];
       }
       if($request->type=='add_on') {
         $booking_addon = Room_booking_addon::findOrFail(explode('_',$request->id)[1]);
         $room_booking = Room_booking::findOrFail($booking_addon->booking_id);
          if(explode('_',$request->id)[0]=='delete') {
            $booking_addon->deleted_at = Carbon::now();
            $booking_addon->save();            
            $room_booking->subtotal_amount = ($room_booking->subtotal_amount-$request->amount);
            $room_booking->total_amount = ($room_booking->total_amount-$request->amount);
            $room_booking->save();           
            return ['response'=>0,'message'=>'successfully removed'];
          }
          if($booking_addon->total_amount < $request->amount) {
            $diff_tax_amount = abs($booking_addon->total_amount-$request->amount);            
            $sub_total = $room_booking->subtotal_amount+$diff_tax_amount;
            $total_amount = $room_booking->total_amount+$diff_tax_amount;            
            $room_booking->subtotal_amount = $sub_total;
            $room_booking->total_amount = $total_amount;            
          }else {
            $diff_tax_amount = abs($booking_addon->total_amount-$request->amount);            
            $sub_total = $room_booking->subtotal_amount-$diff_tax_amount;
            $total_amount = $room_booking->total_amount-$diff_tax_amount;            
            $room_booking->subtotal_amount = $sub_total;
            $room_booking->total_amount = $total_amount;            
          }
          $room_booking->save();
          $booking_addon->total_amount = $request->amount;
          $booking_addon->save();
          return ['response'=>0,'message'=>'Amount updated successfully'];
       }       
    }

    public function get_room_guest_details(Request $request,$id){
         $booking = Room_booking::findOrFail($id);         
         $booking_details = $booking->get_booking_details();
         $guest_data = Room_booking_guest_details::where('booking_id',$id)->get();
         //dd($booking_details[0]->rooms->occupants);
         return view('admin.room_booking.guest_details',compact('booking','booking_details','guest_data'));
    }
    public function save_room_guest_details(Request $request){         
         $check_booking_id = Room_booking_guest_details::where('booking_id',$request->bookingid)->first();
         if(is_object($check_booking_id)){
            //dd($request->all());
           $update_data = Room_booking_guest_details::where('booking_id',$request->bookingid)->get();
            $getArray=[];
            foreach($update_data as $key=>$val){
                $getArray[$val->room_type_id][]=$val->id;
            }
            foreach($getArray as $k=>$value){
                 for($i=0;$i<count($value);$i++){
                    $name = 'guestname_'.$k;                    
                    $ic_num = 'guestic_'.$k;
                    $contact_no = 'guestcontact_'.$k;
                    //dd($request->all());
                    $guest_data = Room_booking_guest_details::findOrFail($value[$i]);                    
                    $guest_data->guest_name = @$request->$name[$i];
                    $guest_data->guest_ic_passport_no = @$request->$ic_num[$i];
                    $guest_data->guest_contact_number = @$request->$contact_no[$i];
                    $guest_data->save();
                }
            }            
            return redirect()->back()->with('success','Guest details updated sucessfully.');
         }
         $booking = Room_booking::findOrFail($request->bookingid);         
         $booking_details = $booking->get_booking_details();
         foreach($booking_details as $key=>$val){
             //dd($val->room_type_id);           
             for($i=0;$i<$val->rooms->occupants;$i++){
                    $name = 'guestname_'.$val->room_type_id;                    
                    $ic_num = 'guestic_'.$val->room_type_id;
                    $contact_no = 'guestcontact_'.$val->room_type_id;
                    //dd($request->$name);
                    $guest_data = new Room_booking_guest_details();
                    $guest_data->booking_id = $request->bookingid;
                    $guest_data->room_type_id = $val->room_type_id;
                    $guest_data->room_number_id = $val->room_number_id;
                    $guest_data->guest_name = $request->$name[$i];
                    $guest_data->guest_ic_passport_no = $request->$ic_num[$i];
                    $guest_data->guest_contact_number = $request->$contact_no[$i];
                    $guest_data->save();
             }
         }
        return redirect()->back()->with('success','Guest details saved sucessfully.');
    }

    public function get_product_addon(Request $request,$id){
      $list_add_on_product = Add_on_product::where('status',0)->where('show_online_enable',0)->orderBy('add_on_name','asc')->get();
      $booking = Room_booking::findOrFail($id);
      //dd($list_add_on_product);
      return view('admin.room_booking.product_add_on',compact('list_add_on_product','booking'));
    }
    public function save_product_addon(Request $request,$id){        
       if($request->addon_id[0]!=null){
              //dd($request->all());         
              $fields = ['_token','addon_id','addon_quantity','subtotal_amount','total_amount','invoice_type'];
              $add_on = explode(',',$request->addon_id[0]);
              $add_on_quantity = explode(',',$request->addon_quantity[0]);
              if($request->invoice_type=='new'){
                 $count_check = Room_booking_addon::where(['booking_id'=>$id, 'invoice_type'=>'new', 'deleted_at'=>null])
                                                  ->update(['invoice_type_latest_old'=>'old']);
              } $saveAddonId=[];             
               foreach($add_on as $k=>$add){
                 $getData = Add_on_product::findOrFail($add);
                 $booking_addon = new Room_booking_addon();
                 $booking_addon->product_addon_id = $add;
                 $booking_addon->booking_id = $id;
                 $booking_addon->price = $getData->add_on_amount;
                 $booking_addon->quantity = $add_on_quantity[$k];
                 $booking_addon->invoice_type = $request->invoice_type;
                 $booking_addon->service_tax = $request->tax_amount;
                 $booking_addon->total_amount = $getData->add_on_amount*$add_on_quantity[$k];
                 $booking_addon->save();
                 $saveAddonId[]= $booking_addon->id;                       
              }
                //price_update
                  $booking = Room_booking::findOrFail($id);
                    //price update if only come exsit invoice condition
                  if($request->invoice_type!='new'){
                         $tax_amount = $booking->service_tax_amount+$request->tax_amount;
                         $subtotal = $booking->subtotal_amount+$request->subtotal_amount;
                         $total = $booking->total_amount+$request->total_amount;
                         $booking->service_tax_amount = $tax_amount;
                         $booking->subtotal_amount = $subtotal;
                         $booking->total_amount = $total;
                   }
                    //price update if only come exsit invoice condition 
                //price_update
                //save payment details
                  $booking->payment_status=1;  
                //Room_booking_payment
                  $booking->save();
                 $invoice_type = ($request->invoice_type=='new') ? 'new' : 'exist';
                 $booking_payment = new Room_booking_payment();
                 $booking_payment->booking_id = $id;
                 $booking_payment->total_amount = $request->total_amount;
                 $booking_payment->payment_status = $booking->payment_status;
                 $booking_payment->payment_type = $request->payment_type;
                 $booking_payment->invoice_type = $invoice_type;
                 $booking_payment->booking_details_id = implode(',',[]);
                 $booking_payment->booking_addon_id = implode(',',$saveAddonId);
                 $booking_payment->payment_details = json_encode($request->except($fields));
                 $booking_payment->others = 'add on payment '.$request->total_amount.'';
                 $booking_payment->save();
                //save payment details
                 //room booking receipt
                 $booking_receipt = new Room_booking_receipt();
                 $booking_receipt->booking_id=$id;
                 $booking_receipt->receipt_no='';
                 $booking_receipt->payment_table_id=$booking_payment->id;
                 $booking_receipt->receipt_amount=$request->total_amount;
                 $booking_receipt->payment_status=$booking->payment_status;
                 $booking_receipt->payment_type=$request->payment_type;
                 $booking_receipt->save();
         }
        return redirect('admin/bookinglisting/newbooking/addon_invoice/'.$id.'/'.$booking_payment->id.'/'.$request->invoice_type)->with('success','Invoice generate sucessfully.');  
    }
    public function addon_invoice_price_update(Request $request){
        if($request->id!='' && $request->amount!=''){
            $update_amount = Room_booking_receipt::findOrFail($request->id);
            $update_amount->receipt_amount = number_format($request->amount,2);
            $update_amount->save();
            return ['response'=>0,'message'=>'success', 'amount'=>$update_amount->receipt_amount];
        }
    }  
    public function view_on_receipt($id){
         $receipt = Room_booking_receipt::findOrFail($id);     
         //dd($receipt->get_payment_data->refund_payment_check);
         return view('admin.room_booking.view_on_receipt',compact('receipt'));
    }
    public function view_on_billing($bookingid){
         $booking = Room_booking::findOrFail($bookingid);
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');
         //$total_amount = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount)+$room_booking_addon;

         $subtotal = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount);
         $discount = $booking->discount_amount;
         $total_amount = ($subtotal-$discount)+$room_booking_addon;

         /*$addon_details_sum_tax = ($room_booking_details->sum('subtotal_amount')+$room_booking_addon)+$booking->service_tax_amount;
         $balance_amount = number_format($booking->total_amount-$addon_details_sum_tax,2);*/
         $payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change', 'booking_room_change', 'booking_date_extend'])->where('refund_payment_check',0)->sum('total_amount');         
         $balance_amount = $total_amount-$payment;         
         return view('admin.room_booking.billing_view',compact('booking','total_amount','balance_amount'));
    }
    public function edit_on_billing_view($bookingid) {
         $booking = Room_booking::findOrFail($bookingid);
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');

         //$total_amount = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount)+$room_booking_addon;
         $subtotal = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount);
         $discount = $booking->discount_amount;
         $total_amount = ($subtotal-$discount)+$room_booking_addon;
         //dd($total_amount);
         /*$addon_details_sum_tax = ($room_booking_details->sum('subtotal_amount')+$room_booking_addon)+$booking->service_tax_amount;
         $balance_amount = number_format($booking->total_amount-$addon_details_sum_tax,2);*/
         $payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change', 'booking_date_extend'])->where('refund_payment_check',0)->sum('total_amount');
         
         $balance_amount = $total_amount-$payment;         
         return view('admin.room_booking.edit_billing_view',compact('booking','total_amount','balance_amount'));
    }
    public function view_on_billing_print($bookingid) {
         $booking = Room_booking::findOrFail($bookingid);
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');
         $subtotal = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount);
         $discount = $booking->discount_amount;
         $total_amount = ($subtotal-$discount)+$room_booking_addon;         
         /*$addon_details_sum_tax = ($room_booking_details->sum('subtotal_amount')+$room_booking_addon)+$booking->service_tax_amount;
         $balance_amount = number_format($booking->total_amount-$addon_details_sum_tax,2); */
         $no_refund_payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change', 'booking_date_extend'])->where('refund_payment_check',0)->sum('total_amount');
         $refund_payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change', 'booking_date_extend'])->where('refund_payment_check',1)->sum('total_amount');         
         $balance_amount = $total_amount-($no_refund_payment-$refund_payment);
         //dd($balance_amount);
         return view('admin.room_booking.billing_print_view',compact('booking','total_amount','balance_amount'));
     }
     public function booking_print_view($bookingid){    
         $booking = Room_booking::findOrFail($bookingid);
         //dd($booking->get_paymentsss_status_text(0));
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');
         $total_amount = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount)+$room_booking_addon;
         $addon_details_sum_tax = ($room_booking_details->sum('subtotal_amount')+$room_booking_addon)+$booking->service_tax_amount;
         $balance_amount = number_format($booking->total_amount-$addon_details_sum_tax,2); 
         return view('admin.room_booking.booking_view',compact('booking','total_amount','balance_amount'));
     }    
     
    public function get_addon_invoice($id,$booking_payment_id,$invoice_type){
      $booking = Room_booking::findOrFail($id);
      $invoice_type = $invoice_type;
      $room_booking_addon='';
      $balance_amount=0.00;
      $booking_payment=Room_booking_payment::findOrFail($booking_payment_id);
      if($invoice_type=='new'){
        $room_booking_addon = Room_booking_addon::where('booking_id',$id)->where('invoice_type','new')->where('invoice_type_latest_old','latest')->where('deleted_at',null)->get();        
        $get_service_charge = '';
        $balance_amount = ($room_booking_addon->sum('total_amount')+$room_booking_addon->first()->service_tax)-$booking_payment->total_amount;
        //dd(number_format($balance_amount,2));
      }else{
        $balance_amount = Room_booking_payment::where('booking_id',$id)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change'])->where('refund_payment_check',0)->where('deleted_at',null)->sum('total_amount');
      }
      //dd($balance_amount);
      return view('admin.room_booking.addon_invoice',compact('booking','invoice_type','booking_payment','room_booking_addon','balance_amount'));
    }
    public function get_rooms_details_using_date(Request $request){
        
        $booking = Room_booking::findOrFail($request->bookingid);
        if($request->checkin_date == '' || $request->checkout_date == '') {
           return ['response'=>1, 'message'=>'Please check input date'];
        }
        if($request->checkin_date == $booking->checkin_date() && $request->checkout_date == $booking->checkout_date()) {
           return ['response'=>1, 'message'=>'Please choose different date'];
        }
        $startDate = $request->checkin_date;
        $EndDate = $request->checkout_date;
        $numberOfDays = Carbon::parse($startDate)->diffInDays(Carbon::parse($EndDate));
        $old_number_of_days=0;
        //dd($numberOfDays);
        $price_details=[];
        $condition_availablity = 0;
        $getAlreadyBookedRoom = [];
        foreach($booking->get_booking_details() as $key=>$value) {
             $old_number_of_days = $value->number_of_days;
             $condition_check = $booking->check_single_room_available($value->room_type_id,$value->room_number_id,$request->checkin_date,$request->checkout_date);
             if($condition_check==1){
                $getAlreadyBookedRoom[] = $value->rooms_number->room_no;
                $condition_availablity = 1;
             }             
        }                
        if($condition_availablity==1) {
           //failed
           return ['response'=>1, 'message'=>' '.implode(',',$getAlreadyBookedRoom).' rooms are already booked. please choose different date'];
           //failed            
         }
         //success 
         $new_amount=0;
         $old_amount=0;
         foreach($booking->get_booking_details() as $key=>$val) {
                //dd($booking->get_booking_details());
                $data = Rooms::get_rooms_price($val->room_type_id,$request->checkin_date,$request->checkout_date,null);
                //dd($data);
                $price_details['room_numbers'][$val->rooms_number->room_no] = $val->id.'_'.($numberOfDays*$data);
                $price_details['upgrade_amount'] = '';
                $price_details['service_charge'] = '';
                $price_details['total_amount_to_pay'] = '';
                //$new_amount+=($numberOfDays*$data);
                //$old_amount+=$val->subtotal_amount;
                if($old_number_of_days==$numberOfDays){ 
                    $new_amount+=$data;
                    $old_amount+=$val->room_price;
                }else{                   
                    $new_amount+=($numberOfDays*$data);
                    $old_amount+=$val->room_price;
                }
                
         }
//dd($old_amount);
         //$old_amount_old_tax = number_format($old_amount+$booking->service_tax_amount,2);
         if($old_amount > $new_amount) {               
               $price_details['upgrade_amount'] = ($new_amount-$old_amount);
               $price_details['service_charge'] = Helper::getTaxAmount($new_amount-$old_amount);
               //dd($price_details['upgrade_amount']);
               $price_details['total_amount_to_pay'] = ($price_details['upgrade_amount']+$price_details['service_charge']);
               $price_details['condition'] = 'sub';               
         } else {
               $price_details['upgrade_amount'] = ($new_amount-$old_amount);
               $price_details['service_charge'] = Helper::getTaxAmount($new_amount-$old_amount);               
               $price_details['total_amount_to_pay'] = ($price_details['upgrade_amount']+$price_details['service_charge']);
               $price_details['condition'] = 'add';
         }
        //dd($price_details);
        return ['response'=>0, 'message'=>'success', 'data'=>$price_details];
    }    
    public function booking_change_date(Request $request,$bookingid) {
         $booking = Room_booking::findOrFail($bookingid);
         $booking_details = $booking->get_booking_details();
         $booking_addon_details = $booking->booking_addon_details;
         $total = ($booking_details->sum('room_price')+$booking->service_tax_amount);
         $checkin_date  = Carbon::parse($booking->checkin_date())->addDay()->format('d-m-Y');
         $checkout_date = Carbon::parse($booking->checkout_date())->addDay()->format('d-m-Y');
         //dd($booking->checkin_date());
        // dd($checkout_date);
         return view('admin.room_booking.booking_change_date',compact('booking','booking_details','booking_addon_details','total','checkout_date','checkin_date'));
    }
    public function booking_change_date_save(Request $request) {
        //dd($request->all());
        if($request->checkin_date!='' && $request->checkout_date!='' && $request->bookingid!=''){
           $booking = Room_booking::findOrFail($request->bookingid);
           //dd($booking->service_tax_amount-abs($request->service_amount));
           $checkin_old_date  = $booking->check_in;
           $checkout_old_date = $booking->check_out;
           $new_checkin_date = Carbon::parse($request->checkin_date)->format('Y-m-d');
           $new_checkout_date = Carbon::parse($request->checkout_date)->format('Y-m-d');          
           $numberOfDays = Carbon::parse($new_checkin_date)->diffInDays(Carbon::parse($new_checkout_date));
           //refund amount when amount is minus(reduce)
            if(isset($request->check_refund) && $request->check_refund==0){
                foreach($request->room_details_id as $key=>$val) {
                 $id = explode('_',$val)[0]; $price=explode('_',$val)[1];                 
                 $booking_details = Room_booking_details::findOrFail($id);                 
                 $booking_details->is_change_data = 1;
                 //$booking_details->is_booking_change_date = 1;
                 $booking_details->save();
                 $new_booking_details = new Room_booking_details();
                 $new_booking_details->booking_id=$booking_details->booking_id;
                 $new_booking_details->room_type_id=$booking_details->room_type_id;
                 $new_booking_details->room_number_id=$booking_details->room_number_id;
                 $new_booking_details->customer_id=$booking_details->customer_id;
                 $new_booking_details->check_in = $new_checkin_date;
                 $new_booking_details->check_out = $new_checkout_date;
                 $new_booking_details->old_checkin_date  = $checkin_old_date;
                 $new_booking_details->old_checkout_date = $checkout_old_date;
                 $new_booking_details->room_quantity = 1;
                 $new_booking_details->number_of_days = $numberOfDays;
                 //$new_booking_details->room_price = ($price/$numberOfDays);
                 //$new_booking_details->subtotal_amount = $price;
                 $new_booking_details->room_price = $booking_details->room_price;
                 $new_booking_details->subtotal_amount = 0;
                 $new_booking_details->checkin_status = $booking_details->checkin_status;
                 $new_booking_details->checkout_status = $booking_details->checkout_status;
                 //$new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
                 //$new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
                 $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
                 $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
                 $new_booking_details->is_booking_change_date = 1;
                 $new_booking_details->status=0;
                 $new_booking_details->save();
              }
                 $booking->check_in  = $new_checkin_date;
                 $booking->check_out = $new_checkout_date;                 
                 $booking->save();      
              return redirect('admin/bookinglisting')->with('success','Sucessfully date changed.');
            }
            if(isset($request->check_refund) && $request->check_refund==1) {
                $old_date = Carbon::parse($checkin_old_date)->format('d-m-Y').'-'.Carbon::parse($checkout_old_date)->format('d-m-Y');
                $new_date = Carbon::parse($new_checkin_date)->format('d-m-Y').'-'.Carbon::parse($new_checkout_date)->format('d-m-Y');
                $others   = $old_date.' to '.$new_date;                
                $get_booking_details_id = [];
                foreach($request->room_details_id as $key=>$val) {
                 $id = explode('_',$val)[0]; $price=explode('_',$val)[1];
                 $get_booking_details_id[]=$id;                 
                   $booking_details = Room_booking_details::findOrFail($id);                 
                   $booking_details->check_in = $new_checkin_date;
                   $booking_details->check_out = $new_checkout_date;
                   $booking_details->save();                
                 }
                   $booking->check_in  = $new_checkin_date;
                   $booking->check_out = $new_checkout_date;
                   //$booking->service_tax_amount = ($booking->service_tax_amount-abs($request->service_amount));
                   //$booking->subtotal_amount = ($booking->subtotal_amount-abs($request->upgrade_amount));
                   //$booking->total_amount = ($booking->total_amount-abs($request->total_amount));
                   $booking->save();
                    //Room_booking_payment
                    $booking_payment = new Room_booking_payment();
                    $booking_payment->booking_id = $booking->id;
                    $booking_payment->total_amount = $request->refund_amount;
                    $booking_payment->payment_status = $booking->payment_status;
                    $booking_payment->payment_type = $request->payment_method;
                    $booking_payment->booking_details_id = implode(',',$get_booking_details_id);
                    $booking_payment->booking_addon_id = implode(',',[]);
                    $booking_payment->invoice_type = 'booking_date_change';
                    $booking_payment->refund_payment_check = 1;
                    $booking_payment->others = $others;
                    $booking_payment->save();
                   //room booking receipt
                    $booking_receipt = new Room_booking_receipt();
                    $booking_receipt->booking_id = $booking->id;
                    $booking_receipt->receipt_no = '';
                    $booking_receipt->payment_table_id = $booking_payment->id;
                    $booking_receipt->receipt_amount = $booking_payment->total_amount;
                    $booking_receipt->payment_status = $booking->payment_status;
                    $booking_receipt->payment_type = $request->payment_method;
                    $booking_receipt->save();    
              return redirect('admin/bookinglisting')->with('success','Sucessfully date changed.');
            }           
            //refund amount when amount is minus(reduce)

            //if amount is free (choose payment method is free)
            if(isset($request->payment_method) && $request->payment_method==9) {
                  foreach($request->room_details_id as $key=>$val) {
                     $id = explode('_',$val)[0]; $price=explode('_',$val)[1];                 
                     $booking_details = Room_booking_details::findOrFail($id);                 
                     $booking_details->is_change_data = 1;
                     //$booking_details->is_booking_change_date = 1;
                     $booking_details->save();
                     $new_booking_details = new Room_booking_details();
                     $new_booking_details->booking_id=$booking_details->booking_id;
                     $new_booking_details->room_type_id=$booking_details->room_type_id;
                     $new_booking_details->room_number_id=$booking_details->room_number_id;
                     $new_booking_details->customer_id=$booking_details->customer_id;
                     $new_booking_details->check_in = $new_checkin_date;
                     $new_booking_details->check_out = $new_checkout_date;
                     $new_booking_details->old_checkin_date  = $checkin_old_date;
                     $new_booking_details->old_checkout_date = $checkout_old_date;
                     $new_booking_details->room_quantity = 1;
                     $new_booking_details->number_of_days = $numberOfDays;
                     //$new_booking_details->room_price = ($price/$numberOfDays);
                     //$new_booking_details->subtotal_amount = $price;
                     $new_booking_details->room_price = $booking_details->room_price;
                     $new_booking_details->subtotal_amount = 0;
                     $new_booking_details->checkin_status = $booking_details->checkin_status;
                     $new_booking_details->checkout_status = $booking_details->checkout_status;
                     //$new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
                     //$new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
                     $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
                     $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
                     $new_booking_details->is_booking_change_date = 1;
                     $new_booking_details->status=0;
                     $new_booking_details->save();
                  }
                 $booking->check_in  = $new_checkin_date;
                 $booking->check_out = $new_checkout_date;                 
                 $booking->save();      
                 return redirect('admin/bookinglisting')->with('success','Sucessfully date changed.');
           }
            //if amount is free (choose payment method is free)


           if($request->upgrade_amount < 0) {
               $old_sub_total  = $booking->subtotal_amount;
               $old_tax_amount = $booking->service_tax_amount;               
               $final_sub_total = ($old_sub_total-$request->upgrade_amount);
               //$final_tax_amount = ($old_tax_amount-$request->service_amount);
               $final_total_amount = ($final_sub_total-$booking->service_tax_amount);
           } else {               
               $old_sub_total  = $booking->subtotal_amount;
               $old_tax_amount = $booking->service_tax_amount;               
               $final_sub_total = ($old_sub_total+$request->upgrade_amount);
               //$final_tax_amount = ($old_tax_amount+$request->service_amount);
               $final_total_amount = ($final_sub_total+$booking->service_tax_amount);               
           } 
           
           $get_booking_details_id = [];
           foreach($request->room_details_id as $key=>$val) {            
             $id = explode('_',$val)[0]; $price=explode('_',$val)[1];             
             $get_booking_details_id[]=$id;
             $booking_details = Room_booking_details::findOrFail($id);
             $booking_details->is_change_data = 1;
             //$booking_details->is_booking_change_date = 1;
             $booking_details->save();
              if($request->upgrade_amount==0) {
                 $RoomPrice = ($price/$numberOfDays);
                 $RoomSubTotal = ($price-($price/$numberOfDays));
               } else {
                 $RoomPrice = $price;
                 $RoomSubTotal = ($price-$booking_details->room_price);
               }            
             $new_booking_details = new Room_booking_details();
             $new_booking_details->booking_id=$booking_details->booking_id;
             $new_booking_details->room_type_id=$booking_details->room_type_id;
             $new_booking_details->room_number_id=$booking_details->room_number_id;
             $new_booking_details->customer_id=$booking_details->customer_id;
             $new_booking_details->check_in = $new_checkin_date;
             $new_booking_details->check_out = $new_checkout_date;
             $new_booking_details->old_checkin_date  = $checkin_old_date;
             $new_booking_details->old_checkout_date = $checkout_old_date;
             $new_booking_details->room_quantity = 1;
             $new_booking_details->number_of_days = $numberOfDays;
             $new_booking_details->room_price = $RoomPrice; 
             $new_booking_details->subtotal_amount = $RoomSubTotal;
             $new_booking_details->checkin_status = $booking_details->checkin_status;
             $new_booking_details->checkout_status = $booking_details->checkout_status;
             //$new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
             //$new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
             $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
             $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
             $new_booking_details->is_booking_change_date =1;
             $new_booking_details->status=0;
             $new_booking_details->save();
           } 
           
           $booking->check_in  = $new_checkin_date;
           $booking->check_out = $new_checkout_date;
           $booking->subtotal_amount = $final_sub_total;
           $booking->total_amount = $final_total_amount;

           $booking->save();
           if($request->upgrade_amount!='0' && $request->payment_method!=null) {
               //Room_booking_payment
                if($request->payment_method!=8) { //if choose payment mode after pay, ingore this payment
                $booking_payment = new Room_booking_payment();
                $booking_payment->booking_id = $booking->id;
                $booking_payment->total_amount = $request->upgrade_amount;
                $booking_payment->payment_status = $booking->payment_status;
                $booking_payment->payment_type = $request->payment_method;
                $booking_payment->booking_details_id = implode(',',$get_booking_details_id);
                $booking_payment->booking_addon_id = implode(',',[]);
                $booking_payment->invoice_type = 'booking_date_change';
                $booking_payment->save();
               //room booking receipt
                $booking_receipt = new Room_booking_receipt();
                $booking_receipt->booking_id = $booking->id;
                $booking_receipt->receipt_no = '';
                $booking_receipt->payment_table_id = $booking_payment->id;
                $booking_receipt->receipt_amount = $booking_payment->total_amount;
                $booking_receipt->payment_status = $booking->payment_status;
                $booking_receipt->payment_type = $request->payment_method;
                $booking_receipt->save();
               } //if choose payment mode after pay, ingore this payment 
            }
        }
        return redirect('admin/bookinglisting')->with('success','Sucessfully date changed.');
    }
    public function booking_extend_date(Request $request,$bookingid) {
         $booking = Room_booking::findOrFail($bookingid);
         $booking_details = $booking->get_booking_details();
         $booking_addon_details = $booking->booking_addon_details;
         $total = number_format($booking_details->sum('subtotal_amount') + $booking->service_tax_amount,2);
         $checkout_date = Carbon::parse($booking->checkout_date())->addDay()->format('d-m-Y');         
         return view('admin.room_booking.booking_extend_date',compact('booking', 'booking_details', 'booking_addon_details', 'total','checkout_date'));
    }
    public function booking_extend_date_save(Request $request) {
        dd($request->all());
        if($request->checkout_date!='' && $request->bookingid!='') {
           $booking = Room_booking::findOrFail($request->bookingid);           
           $checkin_old_date  = $booking->check_in;
           $checkout_old_date = $booking->check_out;
           $new_checkin_date = Carbon::parse($booking->check_in)->format('Y-m-d');
           $new_checkout_date = Carbon::parse($request->checkout_date)->format('Y-m-d');          
           $numberOfDays = Carbon::parse($checkout_old_date)->diffInDays(Carbon::parse($new_checkout_date));
           $newnumberOfDays = Carbon::parse($new_checkin_date)->diffInDays(Carbon::parse($new_checkout_date));
           
            //if amount is free (choose payment method is free)
            if(isset($request->payment_method) && $request->payment_method==9) {
                  foreach($request->room_details_id as $key=>$val) {
                     $id = explode('_',$val)[0]; $price=explode('_',$val)[1];                 
                     $booking_details = Room_booking_details::findOrFail($id);                 
                     $booking_details->is_change_data = 1;
                     //$booking_details->is_booking_extend_date = 1;
                     $booking_details->save();
                     $new_booking_details = new Room_booking_details();
                     $new_booking_details->booking_id=$booking_details->booking_id;
                     $new_booking_details->room_type_id=$booking_details->room_type_id;
                     $new_booking_details->room_number_id=$booking_details->room_number_id;
                     $new_booking_details->customer_id=$booking_details->customer_id;
                     $new_booking_details->check_in = $new_checkin_date;
                     $new_booking_details->check_out = $new_checkout_date;
                     $new_booking_details->old_checkin_extend_date  = $checkin_old_date;
                     $new_booking_details->old_checkout_extend_date = $checkout_old_date;
                     $new_booking_details->room_quantity = 1;
                     $new_booking_details->number_of_days = $newnumberOfDays;
                     //$new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
                     //$new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
                     $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
                     $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
                     $new_booking_details->room_price = $booking_details->room_price;
                     $new_booking_details->subtotal_amount = 0;
                     $new_booking_details->is_booking_extend_date = 1;
                     $new_booking_details->checkin_status = $booking_details->checkin_status;
                     $new_booking_details->checkout_status = $booking_details->checkout_status;
                     $new_booking_details->status=0;
                     $new_booking_details->save();
                  }
                 $booking->check_in  = $new_checkin_date;
                 $booking->check_out = $new_checkout_date;                 
                 $booking->save();      
                 return redirect('admin/checkoutlisting')->with('success','Sucessfully date extended.');
           }
            //if amount is free (choose payment method is free)
            $get_booking_details_id = [];
            foreach($request->room_details_id as $key=>$val) {            
             $id = explode('_',$val)[0]; $price=explode('_',$val)[1];                          
             $get_booking_details_id[]=$id;
             $booking_details = Room_booking_details::findOrFail($id);
             //dd(($price-($booking_details->room_price/$numberOfDays)));
             $booking_details->is_change_data = 1;
             //$booking_details->is_booking_extend_date = 1;
             $booking_details->save();
              /*if($request->upgrade_amount==0) {
                 $RoomPrice = ($price/$numberOfDays);
                 $RoomSubTotal = ($price-($price/$numberOfDays));
               } else {*/
             $RoomPrice = $price;
             $RoomSubTotal = ($RoomPrice*$numberOfDays);
             /*}*/
             $new_booking_details = new Room_booking_details();
             $new_booking_details->booking_id=$booking_details->booking_id;
             $new_booking_details->room_type_id=$booking_details->room_type_id;
             $new_booking_details->room_number_id=$booking_details->room_number_id;
             $new_booking_details->customer_id=$booking_details->customer_id;
             $new_booking_details->check_in = $new_checkin_date;
             $new_booking_details->check_out = $new_checkout_date;
             $new_booking_details->old_checkin_extend_date  = $checkin_old_date;
             $new_booking_details->old_checkout_extend_date = $checkout_old_date;
             $new_booking_details->room_quantity = 1;
             $new_booking_details->number_of_days = $numberOfDays;
             $new_booking_details->room_price = $RoomPrice; 
             $new_booking_details->subtotal_amount = $RoomSubTotal;
             $new_booking_details->checkin_status = $booking_details->checkin_status;
             //$new_booking_details->checkout_status = $booking_details->checkout_status;
             //$new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
             //$new_booking_details->old_checkout_date = $checkout_old_date;
             $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
             $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
             $new_booking_details->is_booking_extend_date = 1;
             $new_booking_details->status=0;
             $new_booking_details->save();
           }

           $booking->check_in  = $new_checkin_date;
           $booking->check_out = $new_checkout_date;
           $booking->service_tax_amount = ($booking->service_tax_amount+$request->service_amount);
           $booking->subtotal_amount = ($booking->subtotal_amount+$request->upgrade_amount);
           $booking->total_amount = ($booking->total_amount+$request->total_amount);
           $booking->save();

           if($request->upgrade_amount!='0' && $request->payment_method!=null) {
               //Room_booking_payment
                if($request->payment_method!=8) { //if choose payment mode after pay, ingore this payment
                $booking_payment = new Room_booking_payment();
                $booking_payment->booking_id = $booking->id;
                $booking_payment->total_amount = $request->total_amount;
                $booking_payment->payment_status = $booking->payment_status;
                $booking_payment->payment_type = $request->payment_method;
                $booking_payment->booking_details_id = implode(',',$get_booking_details_id);
                $booking_payment->booking_addon_id = implode(',',[]);
                $booking_payment->invoice_type = 'booking_date_extend';
                $booking_payment->save();
               //room booking receipt
                $booking_receipt = new Room_booking_receipt();
                $booking_receipt->booking_id = $booking->id;
                $booking_receipt->receipt_no = '';
                $booking_receipt->payment_table_id = $booking_payment->id;
                $booking_receipt->receipt_amount = $booking_payment->total_amount;
                $booking_receipt->payment_status = $booking->payment_status;
                $booking_receipt->payment_type = $request->payment_method;
                $booking_receipt->save();
               } //if choose payment mode after pay, ingore this payment 
            }
        }        
        return redirect('admin/checkoutlisting')->with('success','Sucessfully date extended.');
    }

    public function get_rooms_details_using_extenddate(Request $request) {
        //dd($request->all());
        $booking = Room_booking::findOrFail($request->bookingid);
        if($request->checkout_date == '') {
           return ['response'=>1, 'message'=>'Please choose check-out date'];
        }
        if($request->checkout_date == $booking->checkout_date()) {
           return ['response'=>1, 'message'=>'Please choose different date'];
        }
        $startDate = $booking->checkout_date();
        $EndDate = $request->checkout_date;
        $numberOfDays = Carbon::parse($booking->checkout_date())->diffInDays(Carbon::parse($EndDate));        
        $old_number_of_days=0;
        $price_details=[];
        $condition_availablity = 0;
        $getAlreadyBookedRoom = [];
        foreach($booking->get_booking_details() as $key=>$value) {
             $condition_check = $booking->check_extend_date_available($value->room_number_id,$startDate,$EndDate);
             if($condition_check==1){
                $getAlreadyBookedRoom = $value->rooms_number->room_no;
                $condition_availablity = 1;
             }
        }
        
        if($condition_availablity==1) {
           //failed
           return ['response'=>1, 'message'=>'Sorry, Selected date not Available Or already added to cart'];
           //failed            
        }
         //success 
         $new_amount=0;
         /*$old_amount=0;*/
         foreach($booking->get_booking_details() as $key=>$val) {
                $data = Rooms::get_rooms_price($val->room_type_id,$request->checkin_date,$request->checkout_date,null);
                $price_details['room_numbers'][$val->rooms_number->room_no] = $val->id.'_'.$data;
                $price_details['upgrade_amount'] = '';
                $price_details['service_charge'] = '';
                $price_details['total_amount_to_pay'] = '';                
                $new_amount+=$data;
                /*$old_amount+=$val->room_price;*/ 
         }        
        $price_details['upgrade_amount'] = ($new_amount*$numberOfDays);
        $price_details['service_charge'] = Helper::getTaxAmount($new_amount*$numberOfDays);
        $price_details['total_amount_to_pay'] = ($price_details['upgrade_amount']+$price_details['service_charge']);
        $price_details['condition'] = 'add';
        //dd($price_details);
        return ['response'=>0, 'message'=>'success', 'data'=>$price_details];
    }


     public function booking_change_room(Request $request,$bookingid) {         
         $booking = Room_booking::findOrFail($bookingid);
         $booking_details = $booking->get_booking_details();
         $get_available_room_no = Helper::get_all_rooms($booking);                 
         foreach($booking_details as $key=>$data) {
                 //old room id is append
                 $exists_room_number_data = Helper::get_room_number_data($data->room_number_id);
                 $room_nuber_details = $exists_room_number_data->pluck('room_no','id')->toArray();
                 $room_type_name = $exists_room_number_data->first()->room_type->room_type;                 
                 $appends_position = array_search($room_type_name,$get_available_room_no['room_type']);
                 $get_available_room_no['room_numbers'][$appends_position]=array($data->room_number_id => $room_nuber_details[$data->room_number_id]) + $get_available_room_no['room_numbers'][$appends_position];
                 //old room id is append.
                 $data->new_room_number_id = $data->room_number_id;                 
                 $data->room_number_list= $get_available_room_no;
         }
         //dd($booking_details);
         $booking_addon_details = $booking->booking_addon_details;
         $booking_details_id = implode(',',$booking_details->pluck('room_number_id')->toArray());
         $total = number_format($booking_details->sum('subtotal_amount')+$booking->service_tax_amount,2);
         return view('admin.room_booking.booking_change_room',compact('booking','booking_details','booking_addon_details','total','booking_details_id'));
    }
    public function booking_change_room_save(Request $request) {
           //dd($request->all());
           $booking = Room_booking::findOrFail($request->bookingid);
           if(isset($request->check_refund) && $request->check_refund==0) {
                foreach($request->roombookingdetailid as $key=>$val) {
                 $id = $key; $price = $request->roombookingdetailPrice[$key];                 
                 $booking_details = Room_booking_details::findOrFail($id);
                 $new_rooms_id = $val;                 
                 $old_rooms_id = $booking_details->room_number_id;                 
                 $numberOfDays = $booking_details->number_of_days;  
                 $check_in = $booking_details->check_in;
                 $check_out = $booking_details->check_out;               
                 $booking_details->is_change_data = 1;
                 //$booking_details->is_booking_change_room = 1;
                 //$booking_details->old_rooms_id = $old_rooms_id;
                 $booking_details->save();
                 $new_booking_details = new Room_booking_details();
                 $new_booking_details->booking_id=$booking_details->booking_id;
                 $new_booking_details->room_type_id=$booking_details->room_type_id;
                 $new_booking_details->room_number_id = $new_rooms_id;
                 $new_booking_details->customer_id=$booking_details->customer_id;
                 $new_booking_details->check_in  = $check_in;
                 $new_booking_details->check_out = $check_out;
                 $new_booking_details->room_quantity = 1;
                 $new_booking_details->number_of_days = $numberOfDays;
                 $new_booking_details->room_price = $booking_details->room_price;
                 $new_booking_details->subtotal_amount = 0;
                 $new_booking_details->old_rooms_id = $old_rooms_id;
                 $new_booking_details->checkin_status = $booking_details->checkin_status;
                 $new_booking_details->checkout_status = $booking_details->checkout_status;
                 $new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
                 $new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
                 $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
                 $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
                 $new_booking_details->is_booking_change_room = 1;
                 $new_booking_details->status=0;
                 $new_booking_details->save();
              }                
              return redirect('admin/bookinglisting')->with('success','Sucessfully room changed.');
            }
            if(isset($request->check_refund) && $request->check_refund==1) {                
                $others = [];
                $get_booking_details_id = [];
                foreach($request->roombookingdetailid as $key=>$val) {
                 $id = $key; $price = $request->roombookingdetailPrice[$key];
                 $get_booking_details_id[]=$id;                 
                   $booking_details = Room_booking_details::findOrFail($id);
                   $new_rooms_id = $val;                 
                   $old_rooms_id = $booking_details->room_number_id;                  
                   $booking_details->old_rooms_id = $old_rooms_id;
                   $booking_details->room_number_id = $new_rooms_id;
                   $booking_details->save(); 
                   $others[]=  $booking_details->get_rooms_number($old_rooms_id)->room_no.' to '.$booking_details->get_rooms_number($new_rooms_id)->room_no.', ';             
                 }
                    
                    //Room_booking_payment
                    $booking_payment = new Room_booking_payment();
                    $booking_payment->booking_id = $booking->id;
                    $booking_payment->total_amount = $request->refund_amount;
                    $booking_payment->payment_status = $booking->payment_status;
                    $booking_payment->payment_type = $request->payment_method;
                    $booking_payment->booking_details_id = implode(',',$get_booking_details_id);
                    $booking_payment->booking_addon_id = implode(',',[]);
                    $booking_payment->invoice_type = 'booking_room_change';
                    $booking_payment->refund_payment_check = 1;
                    $booking_payment->others = rtrim(implode($others),',');
                    $booking_payment->save();
                   //room booking receipt
                    $booking_receipt = new Room_booking_receipt();
                    $booking_receipt->booking_id = $booking->id;
                    $booking_receipt->receipt_no = '';
                    $booking_receipt->payment_table_id = $booking_payment->id;
                    $booking_receipt->receipt_amount = $booking_payment->total_amount;
                    $booking_receipt->payment_status = $booking->payment_status;
                    $booking_receipt->payment_type = $request->payment_method;
                    $booking_receipt->save();    
              return redirect('admin/bookinglisting')->with('success','Sucessfully room changed.');
            }

            //if amount is free (choose payment method is free)
            if(isset($request->payment_method) && $request->payment_method==9) {
                  foreach($request->roombookingdetailid as $key=>$val) {
                     $id = $key; $price = $request->roombookingdetailPrice[$key];
                     $booking_details = Room_booking_details::findOrFail($id); 
                     $new_rooms_id = $val;                 
                     $old_rooms_id = $booking_details->room_number_id;                 
                     $numberOfDays = $booking_details->number_of_days;
                     $check_in = $booking_details->check_in;
                     $check_out = $booking_details->check_out;
                     $booking_details->is_change_data = 1;
                     //$booking_details->is_booking_change_room = 1;
                     $booking_details->save();
                     $new_booking_details = new Room_booking_details();
                     $new_booking_details->booking_id=$booking_details->booking_id;
                     $new_booking_details->room_type_id=$booking_details->room_type_id;
                     $new_booking_details->room_number_id = $new_rooms_id;
                     $new_booking_details->customer_id = $booking_details->customer_id;
                     $new_booking_details->check_in = $check_in;
                     $new_booking_details->check_out = $check_out;
                     $new_booking_details->old_rooms_id = $old_rooms_id;
                     $new_booking_details->room_quantity = 1;
                     $new_booking_details->number_of_days = $numberOfDays;
                     $new_booking_details->room_price = $booking_details->room_price;
                     $new_booking_details->subtotal_amount = 0;
                     $new_booking_details->checkin_status = $booking_details->checkin_status;
                     $new_booking_details->checkout_status = $booking_details->checkout_status;
                     $new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
                     $new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
                     $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
                     $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
                     $new_booking_details->is_booking_change_room = 1;
                     $new_booking_details->status=0;
                     $new_booking_details->save();
                  }
                 return redirect('admin/bookinglisting')->with('success','Sucessfully date changed.');
           }
            //if amount is free (choose payment method is free)

            //refund amount when amount is minus(reduce)
           $old_sub_total  = $booking->subtotal_amount;
           $old_tax_amount = $booking->service_tax_amount;               
           $final_sub_total = ($old_sub_total+$request->upgrade_amount);
           $final_tax_amount = ($old_tax_amount+$request->service_amount);
           $final_total_amount = ($final_sub_total+$final_tax_amount);
           $get_booking_details_id = [];
           foreach($request->roombookingdetailid as $key=>$val) {
             $id = $key; $price = $request->roombookingdetailPrice[$key];             
             $get_booking_details_id[]=$id;
             $booking_details = Room_booking_details::findOrFail($id);
             
             $new_rooms_id = $val;                 
             $old_rooms_id = $booking_details->room_number_id;                 
             $numberOfDays = $booking_details->number_of_days;
             $check_in = $booking_details->check_in;
             $check_out = $booking_details->check_out;

             $booking_details->is_change_data = 1;
             //$booking_details->is_booking_change_room = 1;
             $booking_details->save();
              if($request->upgrade_amount==0) {
                 $RoomPrice = $price;
                 $RoomSubTotal = ($price-$price);
               } else {
                 $RoomPrice = $price;
                 $RoomSubTotal = ($price-$booking_details->room_price);
               }

             $new_booking_details = new Room_booking_details();
             $new_booking_details->booking_id = $booking_details->booking_id;
             $new_booking_details->room_type_id = $booking_details->room_type_id;
             $new_booking_details->room_number_id = $new_rooms_id;
             $new_booking_details->old_rooms_id = $old_rooms_id;
             $new_booking_details->customer_id = $booking_details->customer_id;
             $new_booking_details->check_in = $check_in;
             $new_booking_details->check_out = $check_out;
             $new_booking_details->room_quantity = 1;
             $new_booking_details->number_of_days = $numberOfDays;
             $new_booking_details->room_price = $RoomPrice; 
             $new_booking_details->subtotal_amount = $RoomSubTotal;
             $new_booking_details->checkin_status = $booking_details->checkin_status;
             $new_booking_details->checkout_status = $booking_details->checkout_status;
             $new_booking_details->old_checkin_date = $booking_details->old_checkin_date;
             $new_booking_details->old_checkout_date = $booking_details->old_checkout_date;
             $new_booking_details->checkin_date_time = $booking_details->checkin_date_time;
             $new_booking_details->checkout_date_time = $booking_details->checkout_date_time;
             $new_booking_details->is_change_data = 0;
             $new_booking_details->is_booking_change_room = 1;
             $new_booking_details->status=0;
             $new_booking_details->save();
           }          
           
           $booking->service_tax_amount = $final_tax_amount;
           $booking->subtotal_amount = $final_sub_total;
           $booking->total_amount = $final_total_amount;
           $booking->save();
           if($request->upgrade_amount!='0' && $request->payment_method!=null) {
              if($request->payment_method!=8) { //if choose payment mode after pay, ingore this payment
               //Room_booking_payment
                $booking_payment = new Room_booking_payment();
                $booking_payment->booking_id = $booking->id;
                $booking_payment->total_amount = $request->total_amount;
                $booking_payment->payment_status = $booking->payment_status;
                $booking_payment->payment_type = $request->payment_method;
                $booking_payment->booking_details_id = implode(',',$get_booking_details_id);
                $booking_payment->booking_addon_id = implode(',',[]);
                $booking_payment->invoice_type = 'booking_room_change';
                $booking_payment->save();
                  //room booking receipt
                $booking_receipt = new Room_booking_receipt();
                $booking_receipt->booking_id = $booking->id;
                $booking_receipt->receipt_no = '';
                $booking_receipt->payment_table_id = $booking_payment->id;
                $booking_receipt->receipt_amount = $booking_payment->total_amount;
                $booking_receipt->payment_status = $booking->payment_status;
                $booking_receipt->payment_type = $request->payment_method;
                $booking_receipt->save();
              }
            }
        return redirect('admin/bookinglisting')->with('success','Sucessfully rooms changed.');
    }
    public function booking_change_room_getprice(Request $request,$bookingid) {
        //dd($request->all());  
        if(count($request->all())==0){
          return redirect('admin/');
        }
        $booking = Room_booking::findOrFail($bookingid);
        $booking_details = $booking->get_booking_details();
        $old_booking_details = [];
        foreach($booking_details as $key=>$val) {
            $old_booking_details[$val->id]=$val->room_number_id;
        }        
        $Check_input_changes = array_diff_assoc($old_booking_details,$request->roombookingdetailid);
        $hasDuplicates = count($request->roombookingdetailid) > count(array_unique($request->roombookingdetailid));       
        if(count($Check_input_changes)==0) {
            return redirect()->back()->with('error','Please choose upgrade or downgrade room');
        }if($hasDuplicates) {
            $room_number_id = array_unique( array_diff_assoc( $request->roombookingdetailid, array_unique( $request->roombookingdetailid ) ) );  
            $getRoomData = Rooms_manage::whereIn('id',$room_number_id)->pluck('room_no')->toArray();
            return redirect()->back()->with('error','Please check duplicate room numbers. '.implode(',',$getRoomData).''); 
        }        
        $Already_booked_Rooms = [];
        $price_details=[];
        $new_amount=0;
        $old_amount=0;
        $rooms_lists = Helper::get_all_rooms($booking);
        $rooms_list_with_price = [];
        $get_selected_booking_data = [];
        foreach($booking_details as $key=>$data) {
            if($data->room_number_id == $request->roombookingdetailid[$data->id]) {                
                $exists_room_number_data = Helper::get_room_number_data($data->room_number_id);                                
                $room_nuber_details = $exists_room_number_data->pluck('room_no','id')->toArray();                                
                $room_type_name = $exists_room_number_data->first()->room_type->room_type;
                $appends_position = array_search($room_type_name,$rooms_lists['room_type']);                                
                $rooms_lists['room_numbers'][$appends_position]=array($data->room_number_id => $room_nuber_details[$data->room_number_id]) + $rooms_lists['room_numbers'][$appends_position];
               $data->new_room_number_id = $data->room_number_id;
               $data->room_number_list = $rooms_lists;
            } else {
                //dd($request->roombookingdetailid[$data->id]);
                //update price
                $startDate = $data->check_in;
                $EndDate = $data->check_out;
                $new_room_number_id = $request->roombookingdetailid[$data->id];
                 $condition_check = $booking->check_single_rooms_using_date($new_room_number_id,$startDate,$EndDate);
                 if($condition_check==1) {
                    $Already_booked_Rooms[] = Rooms_manage::findOrFail($new_room_number_id)->room_no;                    
                 }else{

                      $room_type_id = Rooms_manage::findOrFail($new_room_number_id)->room_id;        
                      $price = Rooms::get_rooms_price($room_type_id,$startDate,$EndDate,null);
                      //room list
                        foreach($rooms_lists['room_type'] as $k=>$value) {                        
                        //dd($rooms_lists);
                            foreach($rooms_lists['room_numbers'][$k] as $key=>$val) {                           
                            if($key==$new_room_number_id) {
                                $exists_room_number_data = Helper::get_room_number_data($data->room_number_id);                                
                                $room_nuber_details = $exists_room_number_data->pluck('room_no','id')->toArray();                                
                                $room_type_name = $exists_room_number_data->first()->room_type->room_type;                                
                                $appends_position = array_search($value,$rooms_lists['room_type']);                                
                                $rooms_lists['room_numbers'][$appends_position]=array($data->room_number_id => $room_nuber_details[$data->room_number_id]) + $rooms_lists['room_numbers'][$appends_position];
                                //dd($rooms_lists['room_numbers'][$k][$key]);
                                $rooms_lists['room_numbers'][$k][$key]  = $rooms_lists['room_numbers'][$k][$key].' (RM'.$price.')';
                                $data->new_room_number_id = $key;
                                $data->update_room_price = $price;
                                $data->room_number_list = $rooms_lists;
                            }
                          }
                        }
                      //$data->room_number_list = $rooms_lists;
                      //room list                     
                      $new_amount+=$price;
                      $old_amount+=$data->room_price;
                      if($old_amount > $new_amount) {                           
                           $price_details['upgrade_amount'] = ($new_amount-$old_amount);
                           $price_details['service_charge'] = Helper::getTaxAmount($new_amount-$old_amount);               
                           $price_details['total_amount_to_pay'] = ($price_details['upgrade_amount']+$price_details['service_charge']);
                           $price_details['condition'] = 'sub';               
                     } else {                          
                           $price_details['upgrade_amount'] = ($new_amount-$old_amount);
                           $price_details['service_charge'] = Helper::getTaxAmount($new_amount-$old_amount);               
                           $price_details['total_amount_to_pay'] = ($price_details['upgrade_amount']+$price_details['service_charge']);
                           $price_details['condition'] = 'add';
                     }
                     $get_selected_booking_data[]=$data;
                 }
                //update price
            }                        
        }
        if(count($Already_booked_Rooms)!=0) {
            return redirect()->back()->with('error',' '.implode(',',$Already_booked_Rooms).' room are already booked. please choose different room');
        }
         //dd($booking_details);
         $booking_addon_details = $booking->booking_addon_details;         
         $total = number_format($booking_details->sum('subtotal_amount')+$booking->service_tax_amount,2);
         return view('admin.room_booking.booking_change_room',compact('booking','booking_details','booking_addon_details','total','price_details','get_selected_booking_data'));
    }

    public function booking_refund_view($bookingid) {
        $booking = Room_booking::findOrFail($bookingid);
        return view('admin.room_booking.refund',compact('booking'));
    }
    public function booking_refund_save(Request $request) {
        if($request->booking_id !='' && $request->discount_type !='' && $request->refund_amount !='' && $request->refund_payment_type !=''){
           
           if($request->discount_type==1) { //refund amount function
              $booking = Room_booking::findOrFail($request->booking_id);
              $booking->payment_status = 3;
              $booking->booking_status = 3;
              $booking->save();
              $booking_details = $booking->get_booking_details()->pluck('id');
              Room_booking_details::whereIn('id',$booking_details)->update(['checkin_status'=>1,'checkout_status'=>1]);
              $booking_payment = new Room_booking_payment();
              $booking_payment->booking_id = $booking->id;
              $booking_payment->total_amount = $request->refund_amount;
              $booking_payment->payment_status = $booking->payment_status;
              $booking_payment->payment_type = $request->refund_payment_type;
              $booking_payment->booking_details_id = implode(',',[]);
              $booking_payment->booking_addon_id = implode(',',[]);
              $booking_payment->invoice_type = 'refund';
              $booking_payment->refund_payment_check = 1;
              $booking_payment->save();
              //Room_booking_payment
              //room booking receipt
              $booking_receipt = new Room_booking_receipt();
              $booking_receipt->booking_id= $booking->id;
              $booking_receipt->receipt_no='';
              $booking_receipt->payment_table_id= $booking_payment->id;
              $booking_receipt->receipt_amount= $request->refund_amount;
              $booking_receipt->payment_status= $booking->payment_status;
              $booking_receipt->payment_type= $request->refund_payment_type;
              $booking_receipt->save();              
             //save invoice
               $invoice = new Invoice();
               $invoice->booking_id = $booking->id;
               $invoice->receipt_no = $booking_receipt->receipt_no;
               $invoice->admin_user_id = Auth::guard('admin')->user()->id;
               $invoice->invoice_customer_id = $booking->customer_id;
               $invoice->invoice_number = $invoice->invoiceNumber();
               $invoice->invoice_date = Carbon::now();
               $invoice->invoice_status = 'paid';
               $invoice->save();
             //save invoice
               return redirect('admin/bookinghistorylisting')->with('success','Successfully refunded.');
           }//refund amount function
            //without refund amount
              $booking = Room_booking::findOrFail($request->booking_id);
              //$booking->payment_status = 3;
              $booking->booking_status = 4;
              $booking->save();
              $booking_details = $booking->get_booking_details()->pluck('id');
              Room_booking_details::whereIn('id',$booking_details)->update(['checkin_status'=>1,'checkout_status'=>1]);
              $receipt_number = Room_booking_receipt::where('booking_id',$booking->id)->orderBy('id', 'desc')->first();
               $invoice = new Invoice();
               $invoice->booking_id = $booking->id;
               $invoice->receipt_no = $receipt_number->receipt_no;
               $invoice->admin_user_id = Auth::guard('admin')->user()->id;
               $invoice->invoice_customer_id = $booking->customer_id;
               $invoice->invoice_number = $invoice->invoiceNumber();
               $invoice->invoice_date = Carbon::now();
               $invoice->invoice_status = 'paid';
               $invoice->save();
            //without refund amount
           return redirect('admin/bookinghistorylisting')->with('success','Successfully refunded.');
        }        
        return redirect()->back()->with('error','Please check inputs');
    }

    public function send_email_to_customer($bookingid) {
        $booking = Room_booking::findOrFail($bookingid);
        $email_content_name = 'mail_sendto_customer_for_booking_pending_payment';
        $email_message = Email_Content::where('name','mail_sendto_customer_for_booking_pending_payment')->first();
        $sitesetting = Settings::first();
        $subject = str_replace(['site_name','booking_id'], [$sitesetting->site_name,$booking->booking_number],$email_message->subject);
        $body = str_replace(['customer_name','booking_id'], [$booking->guest->name,$booking->booking_number],$email_message->body);
        $to = $booking->guest->email;        
        return view('admin.room_booking.send_email_to_customer',compact('booking','subject','body','to'));
    }

    public function mail_send_to_customer_pending_payment(Request $request){        
        $booking = Room_booking::findOrFail($request->booking_id);        
        if (Mail::failures()) {
           return redirect()->back()->with('error','Sorry! Please try again latter');
         } else {
           return redirect()->back()->with('success','Great! Successfully send in your mail');
         }        
    }

    public function send_sms_to_customer($bookingid) {        
        $booking = Room_booking::findOrFail($bookingid);        
        $sitesetting = Settings::first();
        $message = "Dear ".$booking->guest->name.", Still You haven't paid Booking No: ".$booking->booking_number.". ".$sitesetting->site_name." Tel: ".$sitesetting->phone."";
        $contact_number = preg_replace('/(\W*)/', '', $booking->guest->contact_number);        
        return view('admin.room_booking.send_sms_to_customer',compact('booking','message','contact_number'));
    }

    public function sms_send_to_customer_pending_payment(Request $request){
        $booking = Room_booking::findOrFail($request->booking_id); 
        return redirect()->back()->with('success','Great! Successfully sent');
    }
    

}