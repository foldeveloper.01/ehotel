<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Sub_Gallery;
use App\Models\News;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;
use Input;

class News_Controller extends Controller
{
    public function listdata(Request $request){
        if($request->get('search')){
            $search = $request->get('search');
            $list = News::where('title','like', '%' .$search. '%')->where('deleted_at',null)->orderBy('id','DESC')->paginate(10);
            $list->appends(array('search'=> Input::get('search'),));
        }else{
            $search = '';
            $list = News::where('deleted_at',null)->orderBy('id','DESC')->paginate(10);
        }        
        return view('admin.news.list',compact('list','search'));
    }    
    public function createdata(){
        return view('admin.news.create');
    }
    public function savedata(Request $request){
        //dd($request->all());
        $validated = Validator::make($request->all(),[
            'date' => 'required',
            'title' => 'required|max:200',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $News = new News();
        if($request->hasFile('image'))
         {
            $file_name = Helper::setUploadfileName($request,'news').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/news',$file_name);
            $News->image  = explode ("/", $path)[3];
         }
        $News->news_date = date('Y-m-d', strtotime($request->date));
        $News->title = $request->title;
        $News->content = $request->content;
        $News->status = $request->status;
        $News->save();
        return redirect('admin/news')->with('success','Added sucessfully.');
    }    
    public function updatedata($id){
        $data = News::findOrFail($id);
        return view('admin.news.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){
            $validated = Validator::make($request->all(),[
            'date' => 'required',
            'title' => 'required|max:200',
            'image' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }      
        
        $News = News::findOrFail($id);
        if($request->hasFile('image'))
         {  
            //delete old files            
            if(Storage::disk('public')->exists('images/news/'.$News->image)){
               Storage::disk('public')->delete('images/news/'.$News->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'news').'.'.$request->file('image')->extension();
            $path = $request->file('image')->storeAs('public/images/news',$file_name);
            $News->image  = explode ("/", $path)[3];
         }
        $News->news_date = date('Y-m-d', strtotime($request->date));
        $News->title = $request->title;
        $News->content = $request->content;
        $News->status = $request->status;
        $News->save();
        return redirect('admin/news')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){        
         $data = News::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
         return redirect('admin/news')->with('success','Deleted sucessfully.');
    }

}