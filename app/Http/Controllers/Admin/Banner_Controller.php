<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Bank_Transfer;
use App\Models\Banner;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

class Banner_Controller extends Controller
{
    //index
    public function listdata(){        
        $list = Banner::where('deleted_at',null)->orderBy('id', 'desc')->get();
        return view('admin.banner.list',compact('list'));
    }
    public function createdata(){        
        return view('admin.banner.create');
    }
    public function savedata(Request $request){
       //dd($request->all());
        $validated = Validator::make($request->all(),[            
            'title' => 'max:200|unique:ehotel_banner,title,NULL,id,deleted_at,NULL',
            'banner' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $banner = new Banner();
        if($request->hasFile('banner'))
        {
            //$path = $request->file('banner')->store('public/images/banner');
            $file_name = Helper::setUploadfileName($request,'banner').'.'.$request->file('banner')->extension();
            $path = $request->file('banner')->storeAs('public/images/banner',$file_name);
            $banner->image  = explode ("/", $path)[3];
        }
        $banner->title = $request->title;
        $banner->title1 = $request->title1;
        $banner->url = $request->url;
        $banner->descriptions = $request->description;
        $banner->position = $request->position;
        $banner->status = $request->status;
        $banner->save();
        return redirect('admin/banner')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
       //dd($request->all());
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Banner::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }elseif($data[0]=='position'){
            if($request->selectedData==null) { $value=0;}else { $value=$request->selectedData; }          
            Banner::whereId($data[1])->update(['position'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){       
         $data = Banner::findOrFail($id);
        return view('admin.banner.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){        
            $validated = Validator::make($request->all(),[             
            'title' => 'max:200|unique:ehotel_banner,title,'.$id.',id,deleted_at,NULL',
            'banner' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $banner = Banner::findOrFail($id);
        if($request->hasFile('banner'))
        {   //delete old files            
            if(Storage::disk('public')->exists('images/banner/'.$banner->image)){
               Storage::disk('public')->delete('images/banner/'.$banner->image);
             }
            //delete old files
            $file_name = Helper::setUploadfileName($request,'banner').'.'.$request->file('banner')->extension();
            $path = $request->file('banner')->storeAs('public/images/banner',$file_name);
            $banner->image  = explode ("/", $path)[3];
        }
        $banner->title = $request->title;
        $banner->title1 = $request->title1;
        $banner->url = $request->url;
        $banner->descriptions = $request->description;
        $banner->position = $request->position;
        $banner->status = $request->status;
        $banner->save();
        return redirect('admin/banner')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', UserActivity::class);
        //policy
         $data = Banner::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/banner')->with('success','Deleted sucessfully.');
    }

}