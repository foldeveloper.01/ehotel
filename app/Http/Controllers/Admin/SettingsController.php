<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\{Settings,Admintypes,Admin,TaxSettings,SmtpSettings};
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use Helper;

class SettingsController extends Controller
{
    public function settings(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Settings::class);
        //policy
        $settingData = Settings::first();
        return view('admin.setting.settings',compact('settingData'));
    }
    
    //create settings
    public function savesettings(Request $request)
    {    //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Settings::class);
        //policy
         if($request->action_tab=='site') {
            $validated = Validator::make($request->all(),[
            'sitename' => 'required|max:200',
            'email' => 'required|max:100',
            'number' => 'required',
            'address' => 'required',
            //'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //'favicon' => 'nullable|mimes:jpeg,png,jpg,gif,svg,x-icon,icon,ico|max:2048'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        }
        //dd($request->all());
        //create data
        $data = Settings::count();
        $whatsapp = $request->whatsapp;
        $facebook = $request->facebook;
        $instagram = $request->instagram;
        $twitter = $request->twitter;
        $youtube = $request->youtube;
        if(isset($request->website_maintenance)){ $website_maintenance = '1';}else{ $website_maintenance = '0';}
        if($data==0){
           $user = new Settings();            
         /*if($request->hasFile('logo'))
         {
            $path = $request->file('logo')->store('public/images/settings');
            $user->logo  = explode ("/", $path)[3];
         }
         if($request->hasFile('favicon'))
         {
            $path = $request->file('favicon')->store('public/images/settings');
            $user->favicon  = explode ("/", $path)[3];
         }*/        
        $user->site_name = $request->sitename;
        $user->site_link  = ($request->sitelink) ? $request->sitelink : '';
        $user->email    = $request->email;
        $user->phone     = $request->number;
        $user->fax  = ($request->fax) ? $request->fax :'';
        $user->address  = $request->address;
        $user->meta_descriptions  = ($request->meta_descriptions) ? $request->meta_descriptions : '';
        $user->meta_keywords  = ($request->meta_keywords) ? $request->meta_keywords :'';
        $user->meta_author  = ($request->meta_author) ? $request->meta_author :'';
        $user->whatsapp  = $whatsapp;
        $user->facebook  = $facebook;
        $user->instagram  = $instagram;
        $user->twitter  = $twitter;
        $user->youtube  = $youtube;
        $user->website_maintenance  = $website_maintenance;
        $user->business_registration  = $request->business_registration;
        $user->save();
        }else{
        $user = Settings::first();
         /*if($request->hasFile('logo'))
         {
            $path = $request->file('logo')->store('public/images/settings');
            $user->logo  = explode ("/", $path)[3];
         }
         if($request->hasFile('favicon'))
         {
            $path = $request->file('favicon')->store('public/images/settings');
            $user->favicon  = explode ("/", $path)[3];
         }*/
        $user->site_name = $request->sitename;
        $user->site_link  = $request->sitelink;
        $user->email    =  ($request->email) ? $request->email:'';
        $user->phone     = ($request->number) ? $request->number:'';
        $user->fax  =  ($request->fax) ? $request->fax:'';
        $user->address  = ($request->address) ? $request->address:'';
        $user->meta_descriptions  = ($request->meta_descriptions)?:'';
        $user->meta_keywords  =  ($request->meta_keywords)?:'';
        $user->meta_author  =  ($request->meta_author)?:'';
        $user->whatsapp  = $whatsapp;
        $user->facebook  = $facebook;
        $user->instagram  = $instagram;
        $user->twitter  = $twitter;
        $user->youtube  = $youtube;
        $user->website_maintenance  = $website_maintenance;
        $user->business_registration  = $request->business_registration;
        $user->save();
        }
        return redirect('admin/settings')->with('success','updated sucessfully.');
    }
    

     public function smtpsettings(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', SmtpSettings::class);
        //policy
        $settingData = SmtpSettings::first();
        return view('admin.setting.smtp_settings',compact('settingData'));
    }

     //create settings
    public function savesmtpsettings(Request $request)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', SmtpSettings::class);
        //policy
        $validated = Validator::make($request->all(),[
        'server_name' => 'required|max:200',
        'port' => 'required|max:200',
        'user_name' => 'required|max:200',
        'password' => 'required|max:200',
        'from_name' => 'required|max:200',
        'reply_email' => 'required|max:200'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error', implode(",", $validated->errors()->all()));
        }
        //create data
        $smtp = SmtpSettings::count();
        if($smtp==0){
        $smtp = new SmtpSettings();
        $smtp->server_name = $request->server_name;
        $smtp->port  = ($request->port) ? $request->port : '';
        $smtp->user_name    = $request->user_name;
        $smtp->password     = $request->password;
        $smtp->from_name  = $request->from_name;
        $smtp->reply_email  = $request->reply_email;        
        $smtp->save();
        }else{
        $smtp = SmtpSettings::first();         
        $smtp->server_name = $request->server_name;
        $smtp->port  = ($request->port) ? $request->port : '';
        $smtp->user_name    = $request->user_name;
        $smtp->password     = $request->password;
        $smtp->from_name  = $request->from_name;
        $smtp->reply_email  = $request->reply_email;
        $smtp->save();
        }
        return redirect('admin/smtpsettings')->with('success','Updated sucessfully.');
    }
    //tax settings
    public function taxsettings(){
        $settingData = TaxSettings::first();
        //dd(Helper::getTaxSetting());
        return view('admin.setting.tax_settings',compact('settingData'));
    }

     //create settings
    public function savetaxsettings(Request $request)
    {  
        $validated = Validator::make($request->all(),[
        'booking_type' => 'required|max:3',
        'percentage' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
        'amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error', implode(",", $validated->errors()->all()));
        }
        //create data
        $tax = TaxSettings::count();
        if($tax==0){
          $tax = new TaxSettings();
          $tax->tax_type = $request->booking_type;
          $tax->tax_percentage  = $request->percentage;
          $tax->tax_amount    = $request->amount;
          $tax->save();
        }else{
          $tax = TaxSettings::first();         
          $tax->tax_type = $request->booking_type;
          $tax->tax_percentage  = $request->percentage;
          $tax->tax_amount    = $request->amount;
          $tax->save();
        }
        return redirect('admin/taxsettings')->with('success','Updated sucessfully.');
    }

}