<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Room_season_type;
use App\Models\Room_season_manage;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
class Rooms_season_Controller extends Controller {
    public function listdata(Request $request) {
        if ($request->get("season_year_update")) {
            $current_year = $request->get("season_year_update");
        } else {
            $current_year = date("Y");
        }
        if ($request->isMethod("post")) {
            if ($request->input("season_type")) {
                $start_date = Carbon::createFromFormat("d/m/Y", $request->input("event-start-date"))->format("Y-m-d");
                $end_date = Carbon::createFromFormat("d/m/Y", $request->input("event-end-date"))->format("Y-m-d");
                $event_name = $request->input("event-name");
                //start update data
                if ($request->input("event-index")) {
                    /*$book = Room_season_manage::findOrFail($request->input("event-index"));
                    $book->title = $request->input("event-name");
                    $book->start_date = $start_date;
                    $book->end_date = $end_date;
                    $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                    $book->room_season_type_id = $request->input("season_type");
                    $book->save();*/
                    $this->events_update_function($request);
                    $message = ucwords($request->input("season_type")) . " updated sucessfully";
                } else {
                    // two dates between condition check
                    $dateCheck = Room_season_manage::whereDate("start_date", "<=", $start_date)->whereDate("end_date", ">=", $end_date)->first();
                    if (is_object($dateCheck)) {
                        //dd('111');
                        $newDate = [];
                        $dateCheck->start_date;
                        $dateCheck->end_date;
                        $both_check_condition = 0;
                        $betwen_check_condition = 0;
                        $j = 0;
                        //update if old start date match for new start and end date
                        if ($dateCheck->start_date == $start_date && $dateCheck->start_date == $end_date) {
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        } elseif ($dateCheck->end_date == $start_date && $dateCheck->end_date == $end_date) {
                            //update if old end date match for new start and end date
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($dateCheck->end_date . "-1 day"));
                        } elseif ($dateCheck->start_date == $start_date && $dateCheck->end_date == $end_date) {
                            //update if old end date match for new start and end date
                            $both_check_condition = 1;
                            $dateCheck->title = $request->input("event-name");
                            $dateCheck->room_season_type_id = $request->input("season_type");
                            $dateCheck->save();
                        } elseif ($dateCheck->start_date == $start_date) {
                            // if choose old start date and new start date same,
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        } elseif ($dateCheck->end_date == $end_date) {
                            //old & new start date different but old & new end date same
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                        } else {
                            $betwen_check_condition = 1;
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $j++;
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        if ($both_check_condition == 0) {
                            if ($both_check_condition == 0) {
                                $dateCheck->start_date = $newDate[0]["start_date"];
                                $dateCheck->end_date = $newDate[0]["end_date"];
                                $dateCheck->save();
                            }
                            if ($betwen_check_condition == 1) {
                                $newrow = new Room_season_manage();
                                $newrow->title = $dateCheck->title;
                                $newrow->start_date = $newDate[1]["start_date"];
                                $newrow->end_date = $newDate[1]["end_date"];
                                $newrow->manage_year = $dateCheck->manage_year;
                                $newrow->room_season_type_id = $dateCheck->room_season_type_id;
                                $newrow->save();
                            }
                            //start new data create
                            $book = new Room_season_manage();
                            $book->title = $request->input("event-name");
                            $book->start_date = $start_date;
                            $book->end_date = $end_date;
                            $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                            $book->room_season_type_id = $request->input("season_type");
                            $book->save();
                            //end new data create
                            
                        }
                        //end two date betwen condition
                        
                    } else {
                        //dd($request->all());
                        //start&end date betwen date get
                        $dateCheck = Room_season_manage::whereDate("start_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->get();
                        $get_old_id = [];
                        if (count($dateCheck)) {
                            //dd('222');
                            foreach ($dateCheck as $key => $val) {
                                $get_old_id[] = $val->id;
                            }
                            Room_season_manage::whereIn("id", $get_old_id)->delete();
                        }
                        //start&end date betwen date get
                        //check end date betwen condition
                        $dateCheck = Room_season_manage::whereDate("start_date", ">=", $start_date)->whereDate("start_date", "<=", $end_date)->orderBy("id", "desc")->first();
                        if (is_object($dateCheck)) {
                            //dd('333');
                            $dateCheck->start_date = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $dateCheck->save();
                        }
                        //check end date betwen condition
                        //check start date betwen condition
                        $dateCheck = Room_season_manage::whereDate("end_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->orderBy("id", "desc")->first();
                        if (is_object($dateCheck)) {
                            //dd('444');
                            $dateCheck->end_date = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $dateCheck->save();
                        }
                        //check start date betwen condition
                        $book = new Room_season_manage();
                        $book->title = $request->input("event-name");
                        $book->start_date = $start_date;
                        $book->end_date = $end_date;
                        $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                        $book->room_season_type_id = $request->input("season_type");
                        $book->save();
                    }
                    $message = "Added sucessfully";
                }
                $current_year = Carbon::parse($start_date)->format("Y");
                return redirect()->back()->with("success", $message);
            }
        } else {
            $get_current_year_managedata = Room_season_manage::where("manage_year", "like", "%" . $current_year . "%")->get();
            $final_data = [];
            if (!$get_current_year_managedata->isEmpty()) {
                foreach ($get_current_year_managedata as $key => $val) {
                    $final_data[$key]["id"] = $val->id;
                    $final_data[$key]["name"] = $val->title;
                    $final_data[$key]["room_season_type_id"] = $val->room_season_type_id;
                    $final_data[$key]["color"] = @$val->getrooms_season_type($val->room_season_type_id);
                    $final_data[$key]["startDate"] = Carbon::parse($val->start_date)->format("Y, m, d");
                    $final_data[$key]["endDate"] = Carbon::parse($val->end_date)->format("Y, m, d");
                }
            }
            $room_season_type = Room_season_type::where("status", 0)->where("deleted_at", null)->orderBy("position", "asc")->get();
        }
        return view("admin.rooms_season.list", compact("current_year", "room_season_type", "final_data"));
    }
    public function removeEvent(Request $request) {
        $res_data["data"] = 0;
        if ($request->id) {
            $get_data = Room_season_manage::find($request->id);
            if (is_object($get_data)) {
                $res_data["data"] = 1;
                $get_data->delete();
            }
        }
        return response()->json($res_data);
        exit();
    }
    public function events_update_function($request){
                    // two dates between condition check
                $start_date = Carbon::createFromFormat("d/m/Y", $request->input("event-start-date"))->format('Y-m-d');
                $end_date = Carbon::createFromFormat("d/m/Y", $request->input("event-end-date"))->format('Y-m-d');
                $event_name = $request->input("event-name");
                Room_season_manage::findOrFail($request->input("event-index"))->delete();
                    $dateCheck = Room_season_manage::whereDate("start_date", "<=", $start_date)->whereDate("end_date", ">=", $end_date)->first();
                    if (is_object($dateCheck)) {
                        //dd('111');
                        $newDate = [];
                        $dateCheck->start_date;
                        $dateCheck->end_date;
                        $both_check_condition = 0;
                        $betwen_check_condition = 0;
                        $j = 0;
                        //update if old start date match for new start and end date
                        if ($dateCheck->start_date == $start_date && $dateCheck->start_date == $end_date) {
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        } elseif ($dateCheck->end_date == $start_date && $dateCheck->end_date == $end_date) {
                            //update if old end date match for new start and end date
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($dateCheck->end_date . "-1 day"));
                        } elseif ($dateCheck->start_date == $start_date && $dateCheck->end_date == $end_date) {
                            //update if old end date match for new start and end date
                            $both_check_condition = 1;
                            $dateCheck->title = $request->input("event-name");
                            $dateCheck->room_season_type_id = $request->input("season_type");
                            $dateCheck->save();
                        } elseif ($dateCheck->start_date == $start_date) {
                            // if choose old start date and new start date same,
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        } elseif ($dateCheck->end_date == $end_date) {
                            //old & new start date different but old & new end date same
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                        } else {
                            $betwen_check_condition = 1;
                            $newDate[$j]["start_date"] = $dateCheck->start_date;
                            $newDate[$j]["end_date"] = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $j++;
                            $newDate[$j]["start_date"] = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $newDate[$j]["end_date"] = $dateCheck->end_date;
                        }
                        if ($both_check_condition == 0) {
                            if ($both_check_condition == 0) {
                                $dateCheck->start_date = $newDate[0]["start_date"];
                                $dateCheck->end_date = $newDate[0]["end_date"];
                                $dateCheck->save();
                            }
                            if ($betwen_check_condition == 1) {
                                $newrow = new Room_season_manage();
                                $newrow->title = $dateCheck->title;
                                $newrow->start_date = $newDate[1]["start_date"];
                                $newrow->end_date = $newDate[1]["end_date"];
                                $newrow->manage_year = $dateCheck->manage_year;
                                $newrow->room_season_type_id = $dateCheck->room_season_type_id;
                                $newrow->save();
                            }
                            //start new data create
                            $book = new Room_season_manage();
                            $book->title = $request->input("event-name");
                            $book->start_date = $start_date;
                            $book->end_date = $end_date;
                            $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                            $book->room_season_type_id = $request->input("season_type");
                            $book->save();
                            //end new data create
                            
                        }
                        //end two date betwen condition
                        
                    } else {
                        //dd($request->all());
                        //start&end date betwen date get
                        $dateCheck = Room_season_manage::whereDate("start_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->get();
                        $get_old_id = [];
                        if (count($dateCheck)) {
                            //dd('222');
                            foreach ($dateCheck as $key => $val) {
                                $get_old_id[] = $val->id;
                            }
                            Room_season_manage::whereIn("id", $get_old_id)->delete();
                        }
                        //start&end date betwen date get
                        //check end date betwen condition
                        $dateCheck = Room_season_manage::whereDate("start_date", ">=", $start_date)->whereDate("start_date", "<=", $end_date)->orderBy("id", "desc")->first();
                        if (is_object($dateCheck)) {
                            //dd('333');
                            $dateCheck->start_date = date("Y-m-d", strtotime($end_date . "+1 day"));
                            $dateCheck->save();
                        }
                        //check end date betwen condition
                        //check start date betwen condition
                        $dateCheck = Room_season_manage::whereDate("end_date", ">=", $start_date)->whereDate("end_date", "<=", $end_date)->orderBy("id", "desc")->first();
                        if (is_object($dateCheck)) {
                            //dd('444');
                            $dateCheck->end_date = date("Y-m-d", strtotime($start_date . "-1 day"));
                            $dateCheck->save();
                        }
                        //check start date betwen condition
                        $book = new Room_season_manage();
                        $book->title = $request->input("event-name");
                        $book->start_date = $start_date;
                        $book->end_date = $end_date;
                        $book->manage_year = Carbon::parse($start_date)->format("Y") . "," . Carbon::parse($end_date)->format("Y");
                        $book->room_season_type_id = $request->input("season_type");
                        $book->save();
                    }
                    return true;
    }
}
