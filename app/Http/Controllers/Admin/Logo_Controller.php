<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Logo;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use App\Helpers\Helper;

class Logo_Controller extends Controller
{
    public function logo(Request $request){        
        $data = Logo::first();
        return view('admin.logo.create',compact('data'));
    }    
    //create settings
    public function savelogo(Request $request)
    {         
        $validated = Validator::make($request->all(),[        
          'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
          'favicon' => 'nullable|mimes:jpeg,png,jpg,gif,svg,x-icon,icon,ico|max:2048'
         ]);
        if ($validated->fails()) {
            //dd($validated->errors());
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error', implode(",", $validated->errors()->all()));
        }
        //create data
        $data = Logo::count();
        if($data==0){
           $logo = new Logo();            
             if($request->hasFile('logo'))
             {
                $file_name = Helper::setUploadfileName($request,'logo').'.'.$request->file('logo')->extension();
                $path = $request->file('logo')->storeAs('public/images/logo',$file_name);
                $logo->logo  = explode ("/", $path)[3];
             }
             if($request->hasFile('favicon'))
             {
                $file_name = Helper::setUploadfileName($request,'favicon').'.'.$request->file('favicon')->extension();
                $path = $request->file('favicon')->storeAs('public/images/logo',$file_name);
                $logo->favicon  = explode ("/", $path)[3];
             }
            $logo->save();
        }else{
             $logo = Logo::first();
             if($request->hasFile('logo'))
             {
                $file_name = Helper::setUploadfileName($request,'logo').'.'.$request->file('logo')->extension();
                $path = $request->file('logo')->storeAs('public/images/logo',$file_name);
                $logo->logo  = explode ("/", $path)[3];
             }
             if($request->hasFile('favicon'))
             {
                $file_name = Helper::setUploadfileName($request,'favicon').'.'.$request->file('favicon')->extension();
                $path = $request->file('favicon')->storeAs('public/images/logo',$request->file('favicon')->extension());
                $logo->favicon  = explode ("/", $path)[3];
             }
            $logo->save();
        }
        return redirect('admin/logo')->with('success','updated sucessfully.');
    }

}