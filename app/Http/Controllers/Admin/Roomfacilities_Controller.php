<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Room_facilities;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class Roomfacilities_Controller extends Controller
{
    public function listdata(){
        $list = Room_facilities::where('deleted_at',null)->orderBy('id','DESC')->get();
        return view('admin.room_facilities.list',compact('list'));
    }    
    public function createdata(){        
        return view('admin.room_facilities.create');
    }
    public function savedata(Request $request){       
        $validated = Validator::make($request->all(),[            
            'name' => 'max:200|unique:ehotel_room_facilities,name,NULL,id,deleted_at,NULL',
            'status' => 'required',
            'font_icon_name' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
        $facilities = new Room_facilities();
        $facilities->name = $request->name;
        $facilities->font_icon_name = $request->font_icon_name;
        $facilities->status = $request->status;
        $facilities->save();
        return redirect('admin/roomfacilities')->with('success','Added sucessfully.');
    }
    public function ajax_status_update(Request $request){
        $data = explode('_',$request->id);
        $res_data['data']='0';
        if($data[0]=='status'){
           if($request->selectedData=='Yes') { $value=0;}else {$value=1;}
            Room_facilities::whereId($data[1])->update(['status'=>$value]);
            $res_data['data']='1';
        }
        return response()->json($res_data); exit;
    }
    public function updatedata($id){        
         $data = Room_facilities::findOrFail($id);
        return view('admin.room_facilities.update', compact('data'));
    }
    public function dataupdate(Request $request, $id){       
            $validated = Validator::make($request->all(),[
            'name' => 'max:200|unique:ehotel_room_facilities,name,'.$id.',id,deleted_at,NULL',
            'status' => 'required',
            'font_icon_name' => 'required'
             ]);
            if ($validated->fails()) {
                return redirect()
                            ->back()
                            ->withErrors($validated)
                            ->withInput()
                            ->with('error', implode(",", $validated->errors()->all()));
            }
       
        $facilities = Room_facilities::findOrFail($id);
        $facilities->name = $request->name;
        $facilities->font_icon_name = $request->font_icon_name;
        $facilities->status = $request->status;
        $facilities->save();
        return redirect('admin/roomfacilities')->with('success','Updated sucessfully.');
    }
    public function deletedata(Request $request, $id){
         $data = Room_facilities::findOrFail($id);
         $data->status=1;
         $data->deleted_at= Carbon::now();
         $data->save();
        return redirect('admin/roomfacilities')->with('success','Deleted sucessfully.');
    }

}