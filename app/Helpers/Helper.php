<?php

namespace App\Helpers;

use App\Models\{Sociallink, SmtpSettings, Room_Setting, TaxSettings, Rooms, Rooms_manage, Room_booking_details, Room_season_bad_reserved, Room_booking, Room_booking_addon,Room_booking_payment,Room_season_manage,Room_season_type, Coupon_Room_Price};
use Carbon\Carbon;

class Helper {

    public static function getSMTPdata(){      
     $results=1;
     $getemaiData = SmtpSettings::count();
     if($getemaiData==0){
        return $results=0;  
    }
    $data = SmtpSettings::first();
    if($data->server_name=='' || $data->port==''|| $data->user_name=='' || $data->password=='' || $data->from_name=='' || $data->reply_email==''){
        return $results=0;
    }
         //Set the data in an array variable from settings table
    $mailConfig = [
        'transport' => 'smtp',
        'host' => $data->server_name,
        'port' => $data->port,
        'encryption' => 'tls',
        'username' => $data->user_name,
        'password' => $data->password,
        'timeout' => null
    ];
    $mailConfig1 = [
        'address' => $data->reply_email,
        'name' => $data->from_name
    ];

        //To set configuration values at runtime, pass an array to the config helper
    config(['mail.mailers.smtp' => $mailConfig]);
    config(['mail.from' => $mailConfig1]);
    return $results;
}
public static function setUploadfileName($request, $management){
    if(env('APP_URL')=="http://127.0.0.1:8000/"){
        $hostname = '127.0.0.1';
    }else{
        $hostname = explode('.', request()->getHost())[0];
    }
    return $hostname.'_ehotel_'.$management.'_'.date('dmy').rand(100,999);
}

public static function getTaxAmount($amount){
    //dd($amount);
    $returnData = 0.00;
    $tax = TaxSettings::first();
    if($tax){
        if($tax->tax_type==0) {
            $tax_percentage = ($amount * $tax->tax_percentage / 100);
            //dd($tax_percentage);
            $returnData = $tax_percentage;
        }elseif($tax->tax_type==1){
            $returnData = $tax->tax_amount;
        }
    }
    return $returnData;
}
public static function getTotalPlusTaxAmount($total,$tax){    
    $returnData = 0.00;
    if($total!=='' && $tax!==''){
        $returnData = ($total+$tax);
    }
    return $returnData;
}

public static function getTaxSetting(){
    $returnData = ['type'=>'percentage','category'=>'(%)','value'=>'0.00'];
    $tax = TaxSettings::first();
    if($tax){
        if($tax->tax_type==0){
            $returnData['value']=$tax->tax_percentage;           
        }elseif($tax->tax_type==1){
            $returnData['type']='amount';
            $returnData['category']='(RM)';
            $returnData['value']=$tax->tax_amount;
        }
    }
    return $returnData;
}

public static function build_calendar($month,$year,$dateArray){

     // Create array containing abbreviations of days of week.
     $daysOfWeek = array('S','M','T','W','T','F','S');

     // What is the first day of the month in question?
     $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

     // How many days does this month contain?
     $numberDays = date('t',$firstDayOfMonth);

     // Retrieve some information about the first day of the
     // month in question.
     $dateComponents = getdate($firstDayOfMonth);

     // What is the name of the month in question?
     $monthName = $dateComponents['month'];

     // What is the index value (0-6) of the first day of the
     // month in question.
     $dayOfWeek = $dateComponents['wday'];

     // Create the table tag opener and day headers

     $calendar = "<table class='calendar'>";
     $calendar .= "<caption>$monthName $year</caption>";
     $calendar .= "<tr>";

     // Create the calendar headers

     foreach($daysOfWeek as $day) {
          $calendar .= "<th class='header'>$day</th>";
     } 

     // Create the rest of the calendar

     // Initiate the day counter, starting with the 1st.

     $currentDay = 1;

     $calendar .= "</tr><tr>";

     // The variable $dayOfWeek is used to
     // ensure that the calendar
     // display consists of exactly 7 columns.

     if ($dayOfWeek > 0) { 
          $calendar .= "<td colspan='$dayOfWeek'>&nbsp;</td>"; 
     }
     
     $month = str_pad($month, 2, "0", STR_PAD_LEFT);
  
     while ($currentDay <= $numberDays) {

          // Seventh column (Saturday) reached. Start a new row.

          if ($dayOfWeek == 7) {

               $dayOfWeek = 0;
               $calendar .= "</tr><tr>";

          }
          
          $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
          
          $date = "$year-$month-$currentDayRel";

          $calendar .= "<td class='day' rel='$date'>$currentDay</td>";

          // Increment counters
 
          $currentDay++;
          $dayOfWeek++;

     }
     // Complete the row of the last week in month, if necessary
     if ($dayOfWeek != 7) {
          $remainingDays = 7 - $dayOfWeek;
          $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>";
     }     
     $calendar .= "</tr>";
     $calendar .= "</table>";
     return $calendar;
}
public static function RoomSetting($request,$type,$total_room) {
   $roomsetting = Room_Setting::first();
   $assigned_room_count = Rooms::where('deleted_at',null)->count();
   if($type=='create'){
       $total_room = ($roomsetting->total_room) ? : 0 ;
       $remaining_room = $roomsetting->remaining_room;
       if($remaining_room >= $request->total_room){
        $remaining_room = $roomsetting->remaining_room-$request->total_room;
        $alotted_room = $roomsetting->alotted_room+$request->total_room;
            //update room_setting
        $roomsetting->alotted_room = $alotted_room;
        $roomsetting->remaining_room = $remaining_room;
        $roomsetting->save();
            //update room_setting
    }else{
        $remaining_room = $roomsetting->remaining_room;
        if($remaining_room==0){
            return ['data'=>3,'message'=>'sorry you don\'t have enough alotted room to make this.please contact our administrator'];
        }
        return ['data'=>2,'message'=>'sorry you don\'t have enough alotted room to make this. you have only available '.$remaining_room.' rooms.'];
    }
    return ['data'=>1,'message'=>''];
}if($type=='update'){
   if($request->total_room!=$total_room){
    $inputvaluecheck = $total_room-$request->total_room;
    if(is_numeric($inputvaluecheck) && $inputvaluecheck >= 0) {                
       $alotted_room = $roomsetting->alotted_room-$inputvaluecheck;
       $remaining_room = $roomsetting->total_room-$alotted_room;
       $roomsetting->alotted_room = $alotted_room;
       $roomsetting->remaining_room = $remaining_room;
       $roomsetting->save();
       return ['data'=>1,'message'=>''];
   }else{
    $total_room = $request->total_room-$total_room;
    if($roomsetting->remaining_room >= $total_room){                
        $remaining_room = $roomsetting->total_room-($roomsetting->alotted_room+$total_room);
        $alotted_room = $roomsetting->alotted_room+$total_room;
                //update room_setting
        $roomsetting->alotted_room = $alotted_room;
        $roomsetting->remaining_room = $remaining_room;
        $roomsetting->save();
                //update room_setting
    }else{
        $remaining_room = $roomsetting->remaining_room;
        if($remaining_room==0){
            return ['data'=>3,'message'=>'sorry you don\'t have enough alotted room to make this.please contact our administrator'];
        }
        return ['data'=>2,'message'=>'sorry you don\'t have enough alotted room to make this. you have only available '.$remaining_room.' rooms.'];
    }
    return ['data'=>1,'message'=>''];
}
} 
return ['data'=>1,'message'=>''];  
}
}

public static function availableRoomsNumber($room_type_id,$start_date, $end_date){        
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');
        $getRoomsNumber = Rooms_manage::where('room_id',$room_type_id)->where('deleted_at',null)->pluck('room_no','id')->toArray(); 
        //dd($room_type_id);       
        $IgnoreRoomsNumber=[];
        $get_room_details = Room_booking_details::where('room_type_id',$room_type_id)
                                                ->where('checkout_status',0)
                                                ->where('deleted_at',null)
                                                /*->whereDate('check_in','<=',$start_date)
                                                ->whereDate('check_out','>=',$start_date)*/
                                                ->where([['check_in','<=',$start_date],['check_out','>=',$end_date]])
                                                ->whereBetween('check_in',array($start_date,$end_date))
                                                ->whereBetween('check_out',array($start_date,$end_date))
                                                ->pluck('id','room_number_id')->toArray();

        $feature_Booking_RoomsId = array_diff_key($getRoomsNumber,$get_room_details);        
        $check_bad_reserve_id = Room_season_bad_reserved::where('deleted_at',null)
                                                /*->whereDate('start_date','<=',$start_date)
                                                ->whereDate('end_date','>=',$end_date)*/
                                                ->where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                                                ->whereBetween('start_date',array($start_date,$end_date))
                                                ->whereBetween('end_date',array($start_date,$end_date))
                                                ->whereIn('room_number_id',array_keys($feature_Booking_RoomsId))
                                                ->pluck('room_number_id')
                                                ->toArray();
        //dd($check_bad_reserve_id);
        $getmatchId = array_keys($feature_Booking_RoomsId);
        $available_id = array_diff($getmatchId,$check_bad_reserve_id);
        if(count($available_id)!=0) {
           $feature_Booking_RoomsId_condition_added=[]; 
           foreach($available_id as $key=>$value) {
                   $feature_Booking_RoomsId_condition_added[$value]=$feature_Booking_RoomsId[$value];
           }           
           return $feature_Booking_RoomsId_condition_added;
        }        
        return $feature_Booking_RoomsId;
    }
public static function get_all_rooms($booking) {
       $finalData=[];
       $rooms_type = Rooms::where('deleted_at',null)->where('show_online',0)->pluck('room_type','id');
       foreach($rooms_type as $key=>$data){
        //dd($data);
         $RoomsNo=Helper::availableRoomsNumber($key,$booking->check_in,$booking->check_out);
            if(count($RoomsNo)){
             $finalData['room_type'][]=$data;
             $finalData['room_numbers'][]=$RoomsNo;
            }
       }
      return $finalData;
    }

public static function get_room_number_data($room_number_id) {      
      return Rooms_manage::where('id',$room_number_id);
}

public static function get_total_amount($bookingid) {    
         $booking = Room_booking::findOrFail($bookingid);
         $room_booking_details = Room_booking_details::where('booking_id',$bookingid)->where('deleted_at',null)->get();
         $room_booking_addon = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','exist')->where('deleted_at',null)->sum('total_amount');
         $new_invoice_amount = Room_booking_addon::where('booking_id',$bookingid)->where('invoice_type','new')->where('deleted_at',null)->sum('total_amount');
         $subtotal = ($room_booking_details->sum('subtotal_amount')+$booking->service_tax_amount);
         $discount = $booking->discount_amount;
         $total_amount = ($subtotal-$discount)+$room_booking_addon;
         $payment = Room_booking_payment::where('booking_id',$bookingid)->whereIn('invoice_type',['exist','booking_date_change','booking_room_change','booking_date_extend'])->where('refund_payment_check',0)->sum('total_amount');         
         $balance_amount = $payment-$total_amount;        
         if($balance_amount < 0 ){
             return $balance_amount;
         }else{
             return $total_amount;
         }
         
    }

public static function getRoomTypeDiscountPrice($request,$coupon_id) { 
    $check_in_date= Carbon::parse($request->checkin_date)->format('Y-m-d');
    $RoomTypeId = explode(',',$request->room_type_id);
    foreach($RoomTypeId as $key=>$value) {
        //dd(explode('_',$value)[0]);
       $RoomTypeId[$key]=explode('_',$value)[0];
    }
    //dd(array_count_values($RoomTypeId));
    $getQuantity = array_count_values($RoomTypeId);
    $Booking_Room_type_id = array_unique($RoomTypeId);    
    //dd($Booking_Room_type_id);
    $get_coupon_price = Coupon_Room_Price::where('coupon_id',$coupon_id)->first();    
    $Coupon_Room_type_id = $get_coupon_price->room_id;    
    $get_match_room_type_id =array_intersect($Coupon_Room_type_id,$Booking_Room_type_id);  
    //dd($get_match_room_type_id); 
    $rooms = Rooms::whereIn('id',$get_match_room_type_id)->get(); 
    $tax_percentage_amount=0; 
    foreach($rooms as $k=>$val) {
        $RoomQuantity = $getQuantity[$val->id]; 
        $start_date = Carbon::parse($request->checkin_date)->format('Y-m-d');
        $end_date = Carbon::parse($request->checkout_date)->format('Y-m-d');
        $room_season_manage_data = Room_season_manage::where('deleted_at',null)
                                                ->whereDate('start_date','>=',$start_date)
                                                ->whereDate('end_date','<=',$end_date)
                                                ->pluck('room_season_type_id')->toArray();        
        if(count($room_season_manage_data)) {
            $get_session_type = Room_season_type::whereIn('id',$room_season_manage_data)->get();            
            foreach($get_session_type as $key=>$value){
                if($value->id==1) {
                    //Weekend                    
                    $price = ($val->roomprice->normal_weekend*$RoomQuantity);
                    $percentage = $get_coupon_price->weekend_price;
                    $tax_percentage_amount+= ($price * $percentage / 100);
                }elseif($value->id==2) {
                    ///Promotion                    
                    $price = ($val->roomprice->normal_promotion*$RoomQuantity);
                    $percentage = $get_coupon_price->promotion_price;
                    $tax_percentage_amount+= ($price * $percentage / 100);
                }elseif($value->id==3) {
                    //School                    
                    $price = ($val->roomprice->normal_school_holiday*$RoomQuantity);
                    $percentage = $get_coupon_price->school_holiday_price;
                    $tax_percentage_amount+= ($price * $percentage / 100);
                }elseif($value->id==4) {
                    //public                    
                    $price = ($val->roomprice->normal_public_holiday*$RoomQuantity);
                    $percentage = $get_coupon_price->public_holiday_price;
                    $tax_percentage_amount+= ($price * $percentage / 100);                   
                }
            }
        } else {
           $price = ($val->roomprice->normal_price*$RoomQuantity);
           $percentage = $get_coupon_price->normal_price;
           $tax_percentage_amount+= ($price * $percentage / 100);
        }
    }    
    return $tax_percentage_amount;   
  }

}